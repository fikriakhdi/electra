<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Artomoro Jaya Email class.
 *
 * @class am_email
 * @author Iqbal
 */
class Mailer
{
	var $CI;
    var $active;

	/**
	 * Constructor - Sets up the object properties.
	 */
	function __construct()
    {
        $this->CI       =& get_instance();
        $this->active	= TRUE;

        require_once SWIFT_MAILSERVER;
	}

    /**
	 * Send email function.
	 *
     * @param string    $to         (Required)  To email destination
     * @param string    $subject    (Required)  Subject of email
     * @param string    $message    (Required)  Message of email
     * @param string    $from       (Optional)  From email
     * @param string    $from_name  (Optional)  From name email
	 * @return Mixed
	 */
	function send($to, $subject, $message, $from = '', $from_name = ''){
        if (!$this->active) return false;

		$transport = false;
		if ($mailserver_host = '') {
			$transport = Swift_SmtpTransport::newInstance($mailserver_host, 25)
	    		->setUsername('')
	     		->setPassword('');
		}

		try{
            //Create the Transport
            if(!$transport) $transport  = Swift_MailTransport::newInstance();
			else $transport  = Swift_MailTransport::newInstance($transport);
            //Create the message
            $mail       = Swift_Message::newInstance();
            //Give the message a subject
            $mail->setSubject($subject)
                 ->setFrom(array($from => $from_name))
                 ->setTo($to)
                 ->setBody($message->plain)
                 ->addPart($message->html, 'text/html');

	        //Create the Mailer using your created Transport
	        $mailer     = Swift_Mailer::newInstance($transport);
	        //Send the message
	        $result     = $mailer->send($mail);

			return $result;
		}catch (Exception $e){
			// should be database log in here
			return $e->getMessage(); // 'failed to gather MAILDATA';
		}

		return false;
	}

    function send_email($data){
        $email              = trim($data['email']);

        $plain_down     = get_settings('email_template');
        $html_down      = get_settings('email_template');

        $plain_down         = str_replace("%content%",             $data['content'], $plain_down);
        $plain_spon         = str_replace("%company_name%",     		COMPANY_NAME, $plain_down);
        $plain_spon         = str_replace("%year%",     			date('Y'), $plain_down);
        
        $html_down         = str_replace("%content%",             $data['content'], $html_down);
        $html_down         = str_replace("%company_name%",     		COMPANY_NAME, $html_down);
        $html_down         = str_replace("%year%",     			date('Y'), $html_down);

        $message            = new stdClass();
        $message->plain     = $plain_down;
        $message->html      = $html_down;

        $send               = $this->send(get_settings('company_email'), 'Message '.COMPANY_NAME, $message, $data['email'], $data['name']);

            return $send;
    }
    
    function send_email_change_password($email,$password){

        $plain_down     = get_settings('send_email_change_password');
        $html_down      = get_settings('send_email_change_password_html');
        $admin_contact  = get_settings('admin_contact');
        $plain_down         = str_replace("%email%",             $email, $plain_down);
        $plain_down         = str_replace("%password%",             $password, $plain_down);
        $plain_down         = str_replace("%website_url%",          base_url(), $plain_down);
        $plain_spon         = str_replace("%company_name%",     		COMPANY_NAME, $plain_down);
        $plain_spon         = str_replace("%year%",     			date('Y'), $plain_down);
        $plain_spon         = str_replace("%admin_contact%",     			$admin_contact, $plain_down);
        
        
        $html_down         = str_replace("%email%",             $email, $html_down);
        $html_down         = str_replace("%password%",             $password, $html_down);
        $html_down         = str_replace("%website_url%",          base_url(), $html_down);
        $html_down         = str_replace("%company_name%",     		COMPANY_NAME, $html_down);
        $html_down         = str_replace("%year%",     			date('Y'), $html_down);
        $html_down         = str_replace("%admin_contact%",     			$admin_contact, $html_down);
        
        $message            = new stdClass();
        $message->plain     = $plain_down;
        $message->html      = $html_down;

        $send               = $this->send($email, 'Informasi Ubah Password', $message, 'noreply@'.COMPANY_SITE, 'noreply@'.COMPANY_SITE);

            return $send;
    }
    
    function send_email_reset_password($email,$password){

        $plain_down     = get_settings('send_email_forgot');
        $html_down      = get_settings('send_email_forgot_html');
        $admin_contact  = get_settings('admin_contact');
        $plain_down         = str_replace("%email%",             $email, $plain_down);
        $plain_down         = str_replace("%password%",             $password, $plain_down);
        $plain_down         = str_replace("%website_url%",          base_url(), $plain_down);
        $plain_spon         = str_replace("%company_name%",     		COMPANY_NAME, $plain_down);
        $plain_spon         = str_replace("%year%",     			date('Y'), $plain_down);
        $plain_spon         = str_replace("%admin_contact%",     			$admin_contact, $plain_down);
        
        
        $html_down         = str_replace("%email%",             $email, $html_down);
        $html_down         = str_replace("%password%",             $password, $html_down);
        $html_down         = str_replace("%website_url%",          base_url(), $html_down);
        $html_down         = str_replace("%company_name%",     		COMPANY_NAME, $html_down);
        $html_down         = str_replace("%year%",     			date('Y'), $html_down);
        $html_down         = str_replace("%admin_contact%",     			$admin_contact, $html_down);
        
        $message            = new stdClass();
        $message->plain     = $plain_down;
        $message->html      = $html_down;

        $send               = $this->send($email, 'Informasi Reset Password', $message, 'noreply@'.COMPANY_SITE, 'noreply@'.COMPANY_SITE);

            return $send;
    }
    function send_email_donasi($data){

        $plain_down     = get_settings('send_email_donasi');
        $html_down      = get_settings('send_email_donasi_html');
        $plain_down         = str_replace("%nama_capaign%",             $data['nama_campaign'], $plain_down);
        $plain_down         = str_replace("%nominal_donasi%",             $data['nominal_donasi'], $plain_down);
        $plain_down         = str_replace("%tipe_pembayaran%",             $data['tipe_pembayaran'], $plain_down);
        $plain_down         = str_replace("%nama_bank%",             $data['nama_bank'], $plain_down);
        $plain_down         = str_replace("%cabang_bank%",             $data['cabang_bank'], $plain_down);
        $plain_down         = str_replace("%no_rek%",             $data['no_rek'], $plain_down);
        $plain_down         = str_replace("%an_bank%",             $data['an_bank'], $plain_down);
        $plain_spon         = str_replace("%company_name%",     		COMPANY_NAME, $plain_down);
        $plain_spon         = str_replace("%year%",     			date('Y'), $plain_down);
        
        $html_down         = str_replace("%nama_capaign%",             $data['nama_campaign'], $html_down);
        $html_down         = str_replace("%nominal_donasi%",             $data['nominal_donasi'], $html_down);
        $html_down         = str_replace("%tipe_pembayaran%",             $data['tipe_pembayaran'], $html_down);
        $html_down         = str_replace("%nama_bank%",             $data['nama_bank'], $html_down);
        $html_down         = str_replace("%cabang_bank%",             $data['cabang_bank'], $html_down);
        $html_down         = str_replace("%no_rek%",             $data['no_rek'], $html_down);
        $html_down         = str_replace("%an_bank%",             $data['an_bank'], $html_down);
        $html_down         = str_replace("%company_name%",     		COMPANY_NAME, $html_down);
        $html_down         = str_replace("%year%",     			date('Y'), $html_down);
        
        $message            = new stdClass();
        $message->plain     = $plain_down;
        $message->html      = $html_down;

        $send               = $this->send($data['email'], 'Informasi Transfer Donasi', $message, 'noreply@'.COMPANY_SITE, 'noreply@'.COMPANY_SITE);

            return $send;
    }
}

