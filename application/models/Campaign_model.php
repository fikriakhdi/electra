<?php
class campaign_model extends CI_Model{

  var $campaign                 = 'campaign';
  var $charity                  = 'charity';
  var $provinces                = 'faktur';
  var $regencies                = 'retur';
  var $settings                 = 'settings';
  var $user                     = 'user';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_campaign($data){
        $this->db->insert($this->campaign,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_campaign($where="", $limit=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->campaign);
        if($limit!="")
        $this->db->limit($limit['limit'],$limit['start']);
        $query=$this->db->get();
        return $query;
    }
    function update_campaign($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->campaign,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_campaign($id){
        $this->db->where('id',$id);
        $this->db->delete($this->campaign);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
    function read_campaign_limit($limit){
        $sql = "SELECT * FROM ".$this->campaign." WHERE status='open' LIMIT ".$limit;
        $qry = $this->db->query($sql);
        return $qry;
    }
    function sum_terkumpul($id_capaign){
        $sql = "SELECT SUM(nominal) terjadi FROM ".$this->charity." WHERE campaign_id='".$id_capaign."' AND status='paid' ORDER BY rand()";
        $qry = $this->db->query($sql);
        return $qry->row()->terjadi;
    }
}
?>
