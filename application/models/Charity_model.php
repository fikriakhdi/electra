<?php
class charity_model extends CI_Model{

  var $campaign                 = 'campaign';
  var $charity                  = 'charity';
  var $provinces                = 'faktur';
  var $regencies                = 'retur';
  var $settings                 = 'settings';
  var $member                   = 'member';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_charity($data){
        $this->db->insert($this->charity,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_charity($where=""){
        $this->db->select("A.*, B.name campaign_name, B.id campaign_id, C.name member_name, C.email, C.phone");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->charity." AS A");
        $this->db->join($this->campaign.' AS B', 'B.id = A.campaign_id', 'INNER');
        $this->db->join($this->member.' AS C', 'C.id = A.member_id', 'INNER');
        $query=$this->db->get();
        return $query;
    }
    function update_charity($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->charity,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_charity($id){
        $this->db->where('id',$id);
        $this->db->delete($this->charity);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>
