<?php
class member_model extends CI_Model{

  var $about_us                 = 'about_us';
  var $client                   = 'client';
  var $home_page                = 'home_page';
  var $navbar                   = 'navbar';
  var $products                 = 'products';
  var $key_feature              = 'key_feature';
  var $product_page             = 'product_page';
  var $settings                 = 'settings';
  var $slider                   = 'slider';
  var $member                   = 'member';
  var $testimonial              = 'testimonial';
  var $update_article           = 'update_article';
  var $update_social_share      = 'update_social_share';
  var $user                     = 'user';
  var $why_choose_us            = 'why_choose_us';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_member($data){
        $this->db->insert($this->member,$data);
        $flag=$this->db->insert_id();
        return $flag;
    }
    function read_member($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->member);
        $query=$this->db->get();
        return $query;
    }
    function update_member($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->member,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_member($id){
        $this->db->where('id',$id);
        $this->db->delete($this->member);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
    
    function login($email, $password, $type){
      $sql = "SELECT * FROM ".$this->member." WHERE email='".$email."' AND password='".$password."' AND type='".$type."'";
      $qry = $this->db->query($sql);
      return $qry;
    }
    
}
?>
