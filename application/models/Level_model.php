<?php
class level_model extends CI_Model{

  var $about_us                 = 'about_us';
  var $client                   = 'client';
  var $home_page                = 'home_page';
  var $navbar                   = 'navbar';
  var $products                 = 'products';
  var $key_feature              = 'key_feature';
  var $product_page             = 'product_page';
  var $settings                 = 'settings';
  var $slider                   = 'slider';
  var $member                   = 'member';
  var $level                    = 'level';
  var $testimonial              = 'testimonial';
  var $update_article           = 'update_article';
  var $update_social_share      = 'update_social_share';
  var $user                     = 'user';
  var $why_choose_us            = 'why_choose_us';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_level($data){
        $this->db->insert($this->level,$data);
        $flag=$this->db->insert_id();
        return $flag;
    }
    function read_level($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->level);
        $query=$this->db->get();
        return $query;
    }
    function update_level($data){
        $this->db->where('id_level',$data['id_level']);
        $this->db->update($this->level,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_level($id_level){
        $this->db->where('id_level',$id_level);
        $this->db->delete($this->level);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
    function read_memberandlevel($where=""){
        $sql = "SELECT A.*, B.nama_level level_name FROM member AS A
        JOIN level AS B ON B.id_level  = A.id_level
        
        ";
        if($where!="") $sql.=" WHERE ".$where;
        return $this->db->query($sql);
    }
    
}
?>
