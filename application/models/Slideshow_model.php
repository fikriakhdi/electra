<?php
class slideshow_model extends CI_Model{

  var $campaign                 = 'campaign';
  var $charity                  = 'charity';
  var $provinces                = 'faktur';
  var $regencies                = 'retur';
  var $settings                 = 'settings';
  var $user                     = 'user';
  var $slideshow                = 'slideshow';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_slideshow($data){
        $this->db->insert($this->slideshow,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_slideshow($where="", $limit=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->slideshow);
        if($limit!="")
        $this->db->limit($limit['limit'],$limit['start']);
        $query=$this->db->get();
        return $query;
    }
    function update_slideshow($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->slideshow,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_slideshow($id){
        $this->db->where('id',$id);
        $this->db->delete($this->slideshow);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}