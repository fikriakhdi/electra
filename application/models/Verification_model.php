<?php
class verification_model extends CI_Model{

  var $customer                 = 'customer';
  var $fakturin                 = 'fakturin';
  var $faktur                   = 'faktur';
  var $retur                    = 'retur';
  var $produksi                 = 'produksi';
  var $barang                   = 'barang';
  var $nota_penjualan           = 'nota_penjualan';
  var $member                   = 'member';
  var $verification             = 'verification';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_verification($data){
        $this->db->insert($this->verification,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_verification($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->verification);
        $query=$this->db->get();
        return $query;
    }
    function update_verification($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->verification,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_verification($id){
        $this->db->where('id',$id);
        $this->db->delete($this->verification);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>
