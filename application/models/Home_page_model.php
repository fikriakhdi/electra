<?php
class home_page_model extends CI_Model{

  var $about_us                 = 'about_us';
  var $client                   = 'client';
  var $home_page                = 'home_page';
  var $navbar                   = 'navbar';
  var $our_products             = 'our_products';
  var $product_key_feature      = 'product_key_feature';
  var $product_page             = 'product_page';
  var $settings                 = 'settings';
  var $slider                   = 'slider';
  var $testimonial              = 'testimonial';
  var $update_article           = 'update_article';
  var $update_social_share      = 'update_social_share';
  var $user                     = 'user';
  var $why_choose_us            = 'why_choose_us';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_home_page($data){
        $this->db->insert($this->home_page,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_home_page($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->home_page);
        $query=$this->db->get();
        return $query;
    }
    function update_home_page($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->home_page,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_home_page($id){
        $this->db->where('id',$id);
        $this->db->delete($this->home_page);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>