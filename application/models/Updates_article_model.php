<?php
class updates_article_model extends CI_Model{

  var $about_us                 = 'about_us';
  var $client                   = 'client';
  var $home_page                = 'home_page';
  var $navbar                   = 'navbar';
  var $our_product              = 'our_products';
  var $product_key_feature      = 'product_key_feature';
  var $product_page             = 'product_page';
  var $settings                 = 'settings';
  var $slider                   = 'slider';
  var $testimonial              = 'testimonial';
  var $updates_article          = 'updates_article';
  var $update_social_share      = 'update_social_share';
  var $user                     = 'user';
  var $why_choose_us            = 'why_choose_us';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_updates_article($data){
        $this->db->insert($this->updates_article,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_updates_article($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->updates_article);
        $query=$this->db->get();
        return $query;
    }
    function update_updates_article($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->updates_article,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_updates_article($id){
        $this->db->where('id',$id);
        $this->db->delete($this->updates_article);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>