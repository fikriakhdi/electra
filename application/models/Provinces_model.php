<?php
class provinces_model extends CI_Model{

  var $campaign                 = 'campaign';
  var $charity                  = 'charity';
  var $provinces                = 'faktur';
  var $regencies                = 'retur';
  var $settings                 = 'produksi';
  var $user                     = 'user';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_provinces($data){
        $this->db->insert($this->provinces,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_provinces($where){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->provinces);
        $query=$this->db->get();
        return $query;
    }
    function update_provinces($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->provinces,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_provinces($id){
        $this->db->where('id',$id);
        $this->db->delete($this->provinces);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
    function read_provincesails($where=""){
      $sql = " SELECT A.*, B.name, B.price FROM ".$this->provinces." AS A
      JOIN ".$this->book." AS B ON B.id=A.book_id
      ";
      if($where!="") $sql.=" WHERE ".$where;
      return $this->db->query($sql);
    }
}
?>
