<?php
class about_us_model extends CI_Model{

  var $about_us                 = 'about_us';
  var $client                   = 'client';
  var $home_page                = 'home_page';
  var $navbar                   = 'navbar';
  var $products                 = 'products';
  var $key_feature              = 'key_feature';
  var $product_page             = 'product_page';
  var $settings                 = 'settings';
  var $slider                   = 'slider';
  var $member                   = 'member';
  var $testimonial              = 'testimonial';
  var $update_article           = 'update_article';
  var $update_social_share      = 'update_social_share';
  var $user                     = 'user';
  var $why_choose_us            = 'why_choose_us';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_about_us($data){
        $this->db->insert($this->about_us,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_about_us($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->about_us);
        $query=$this->db->get();
        return $query;
    }
    function update_about_us($data){
        $this->db->where('lang',$data['lang']);
        $this->db->update($this->about_us,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_about_us($id){
        $this->db->where('id',$id);
        $this->db->delete($this->about_us);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>