<?php
class whatsapp_model extends CI_Model{

  var $whatsapp                  = 'whatsapp';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_whatsapp($data){
        $this->db->insert($this->whatsapp,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_whatsapp($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->whatsapp);
        $query=$this->db->get();
        return $query;
    }
    function update_whatsapp($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->whatsapp,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_whatsapp($id){
        $this->db->where('id',$id);
        $this->db->delete($this->whatsapp);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
}
?>
