<?php
class key_feature_model extends CI_Model{

  var $about_us                 = 'about_us';
  var $client                   = 'client';
  var $home_page                = 'home_page';
  var $navbar                   = 'navbar';
  var $product                  = 'product';
  var $key_feature              = 'key_feature';
  var $product_page             = 'product_page';
  var $settings                 = 'settings';
  var $slider                   = 'slider';
  var $testimonial              = 'testimonial';
  var $update_article           = 'update_article';
  var $update_social_share      = 'update_social_share';
  var $user                     = 'user';
  var $why_choose_us            = 'why_choose_us';
  public function __construct(){
            parent::__construct();
             $this->load->database();
         }
    function create_key_feature($data){
        $this->db->insert($this->key_feature,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function read_key_feature($where=""){
        $this->db->select("*");
        if($where!="")
        $this->db->where($where);
        $this->db->from($this->key_feature);
        $query=$this->db->get();
        return $query;
    }
    function update_key_feature($data){
        $this->db->where('id',$data['id']);
        $this->db->update($this->key_feature,$data);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function delete_key_feature($id){
        $this->db->where('id',$id);
        $this->db->delete($this->key_feature);
        $flag=$this->db->affected_rows();
        return $flag;
    }
    function custom_sql($sql){
      return $this->db->query($sql);
    }
    
    function get_key_feature_full($where=''){
        $sql="SELECT A.* FROM key_feature as A 
        JOIN product as B ON A.id_product = B.id ";
        if($where!="") $sql.=" WHERE ".$where;
        return $this->db->query($sql);
    }
}
?>