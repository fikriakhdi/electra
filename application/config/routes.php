<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller']                = 'frontend';
$route['404_override']                      = '';
$route['translate_uri_dashes']              = FALSE;
/*
LANGUAGE SWITCHER
*/
$route['en'] = 'LanguageSwitcher/switchLang/english';
$route['id'] = 'LanguageSwitcher/switchLang/indonesia';
/*
ROUTE BACKEND
*/
$route['administrator']                                         = 'backend';
$route['administrator/member_list']                             = 'backend/member_list';
$route['administrator/campaign_list']                           = 'backend/campaign_list';
$route['administrator/donasi_list']                             = 'backend/donasi_list';
$route['administrator/create_campaign']                         = 'backend/create_campaign';
$route['administrator/settings']                                = 'backend/settings';
$route['administrator/member_edit/(:num)']                      = 'backend/member_edit/$1';
$route['administrator/campaign_edit/(:num)']                    = 'backend/campaign_edit/$1';
$route['administrator/slideshow_edit/(:num)']                   = 'backend/slideshow_edit/$1';
$route['administrator/create_slideshow']                        = 'backend/create_slideshow';
$route['administrator/login']                                   = 'backend/login';
$route['administrator/logout']                                  = 'backend/logout';
$route['administrator/slideshow']                               = 'backend/slideshow';
$route['administrator/home/page_settings']                      = 'backend/home_page_settings';
$route['administrator/home/client_list']                        = 'backend/home_client_list';
$route['administrator/home/our_products']                       = 'backend/home_our_products';
$route['administrator/home/sliders']                            = 'backend/home_sliders';
$route['administrator/home/testimonial']                        = 'backend/home_testimonial';
$route['administrator/home/why_choose_us']                      = 'backend/home_why_choose_us';
$route['administrator/about_us/page_settings']                  = 'backend/about_us_page_settings';
$route['administrator/products/key_feature']                    = 'backend/products_key_feature';
$route['administrator/products/page_settings']                  = 'backend/products_page_settings';
$route['administrator/settings/contacts']                       = 'backend/settings_contacts';
$route['administrator/settings/social_media_list']              = 'backend/settings_social_media_list';
$route['administrator/news_update/news_manager']                = 'backend/news_update_news_manager';
$route['administrator/news_update/news_manager/edit/(:num)']    = 'backend/updates_article_edit/$1';
$route['administrator/pages/pages_manager']                     = 'backend/pages_manager';
$route['administrator/pages/pages_manager/edit/(:num)']         = 'backend/updates_pages_edit/$1';
$route['administrator/themes']                                  = "backend/themes";
$route['administrator/member/member_manager']                   = 'backend/member_manager';
$route['administrator/member/level_manager']                    = 'backend/level_manager';
$route['administrator/settings/whatsapp_list']                  = 'backend/whatsapp_list';
$route['administrator/subscriber_list']                         = 'backend/subscriber_list';

/*
ROUTE FRONTEND ElECTRA
*/
$route['products/(:num)']              = 'frontend/products/$1';
$route['news']                         = 'frontend/news';
$route['ditel_update']                 = 'frontend/ditel_update';
$route['about_us']                     = 'frontend/about_us';
$route['aboutusv2']                     = 'frontend/aboutusv2';
$route['detail_news/(:any)']           = 'frontend/detail_news/$1';
$route['hello_electra']                = 'frontend/hello_electra';
$route['pages/(:any)']                 = 'frontend/pages_read/$1';
$route['kirim_email']                  = 'frontend/send_message';




