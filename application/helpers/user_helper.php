<?php
if ( !function_exists('auth_redirect') ){
  function auth_redirect($redirect=""){
    $CI =& get_instance();
    $user_login = $CI->session->userdata('user_login');
    if($redirect!="") $CI->session->set_userdata('redirect', $redirect);
    if(!empty($user_login)) return true;
    else redirect('login');
  }
}

if ( !function_exists('auth_redirect_admin') ){
  function auth_redirect_admin(){
    $CI =& get_instance();
    $admin_login = $CI->session->userdata('admin_login');
    if(!empty($admin_login)) return true;
    redirect(base_url('administrator/login'));
  }
}

if ( !function_exists('get_current_member') ){
  function get_current_member(){
    $CI =& get_instance();
    $user_login = $CI->session->userdata('user_login');
    if(!empty($user_login)){
      $where = array(
        'email'=> $CI->session->userdata('user_login')
      );
      $data_user = $CI->member_model->read_member($where);
      if($data_user->num_rows()!=0){
        return $data_user->row();
      } else return false;
    } else return false;
  }
}

if ( !function_exists('get_settings_list') ){
  function get_settings_list($settings_type){
    $CI =& get_instance();
      $where = array(
        'type'=> $settings_type
      );
      $data = $CI->settings_model->read_settings($where);
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}

if ( !function_exists('get_current_member_admin') ){
  function get_current_member_admin(){
    $CI =& get_instance();
    $admin_login = $CI->session->userdata('admin_login');
    if(!empty($admin_login)){
      $where = array(
        'email'=> $CI->session->userdata('admin_login')
      );
      $data_user = $CI->member_model->read_member($where);
      if($data_user->num_rows()!=0){
        return $data_user->row();
      } else return false;
    } else return false;
  }
}

if ( !function_exists('get_themes_list') ){
  function get_themes_list(){
    $CI =& get_instance();
      $data = $CI->themes_model->read_themes();
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}

if ( !function_exists('get_subscriber_list') ){
  function get_subscriber_list(){
    $CI =& get_instance();
      $data = $CI->subscribe_model->read_subscribe();
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}


if ( !function_exists('is_admin') ){
  function is_admin(){
    $CI =& get_instance();
    $user_login = $CI->session->userdata('user_login');
    if(!empty($user_login)){
      $where = array(
        'username'=> $user_login
      );
      $data_user = $CI->member_model->read_member($where);
      if($data_user->num_rows()!=0){
        if($data_user->row()->type=='super_admin')
        return 1;
        else return 0;
      } else return false;
    } else return false;
  }
}

if ( !function_exists('get_all_member_list') ){
  function get_all_member_list(){
    $CI =& get_instance();
    $where = array('type!='=>'admin');
      $data_user = $CI->member_model->read_member($where);
      if($data_user->num_rows()!=0){
        return $data_user;
      } else return false;
  }
}

if ( !function_exists('get_all_faktur') ){
  function get_all_faktur(){
    $CI =& get_instance();
      $data_faktur = $CI->faktur_model->read_faktur();
      if($data_faktur->num_rows()!=0){
        return $data_faktur;
      } else return false;
  }
}

if ( !function_exists('get_all_returan') ){
  function get_all_returan($site_code){
    $CI =& get_instance();
    $where ="B.site_code='".$site_code."'";
    $data_customer = $CI->retur_model->read_retur($where);
    if($data_customer->num_rows()!=0){
        return $data_customer;
      } else return false;
  }
}

if ( !function_exists('get_all_produksi') ){
  function get_all_produksi(){
    $CI =& get_instance();
      $data_produksi = $CI->produksi_model->read_produksi();
      if($data_produksi->num_rows()!=0){
        return $data_produksi;
      } else return false;
  }
}

if ( !function_exists('get_produksi_by_month') ){
  function get_produksi_by_month($date, $site_code){
    $date_exploded = explode('-',$date);
    $year = $date_exploded[0];
    $month = $date_exploded[1];
    $CI =& get_instance();
    $where = "B.site_code='".$site_code."' AND YEAR(A.datecreated)='".$year."' AND MONTH(A.datecreated)='".$month."'";
      $data_produksi = $CI->produksi_model->read_produksi($where);
      if($data_produksi->num_rows()!=0){
        return $data_produksi;
      } else return false;
  }
}

if ( !function_exists('get_produksi_by_date') ){
  function get_produksi_by_date($date, $site_code){
    $CI =& get_instance();
    $where = "B.site_code='".$site_code."' AND DATE(A.tanggal_produksi)='".$date."'";
      $data_produksi = $CI->produksi_model->read_produksi($where);
      if($data_produksi->num_rows()!=0){
        return $data_produksi;
      } else return false;
  }
}

if ( !function_exists('get_fakturin_list_by_month') ){
  function get_fakturin_list_by_month($date){
    $date_exploded = explode('-',$date);
    $year = $date_exploded[0];
    $month = $date_exploded[1];
    $CI =& get_instance();
    $where = "YEAR(A.tanggal_faktur)='".$year."' AND MONTH(A.tanggal_faktur)='".$month."'";
      $data_fakturin = $CI->fakturin_model->read_fakturin($where);
      if($data_fakturin->num_rows()!=0){
        return $data_fakturin;
      } else return false;
  }
}

if ( !function_exists('get_fakturin_by_date') ){
  function get_fakturin_by_date($date){
    $CI =& get_instance();
    $where = "DATE(A.tanggal_faktur)='".$date."'";
      $data_fakturin = $CI->fakturin_model->read_fakturin($where);
      if($data_fakturin->num_rows()!=0){
        return $data_fakturin;
      } else return false;
  }
}

if ( !function_exists('get_fakturin_list') ){
  function get_fakturin_list($date){
    $CI =& get_instance();
      $data_fakturin = $CI->fakturin_model->read_fakturin();
      if($data_fakturin->num_rows()!=0){
        return $data_fakturin;
      } else return false;
  }
}

if ( !function_exists('get_customer_list') ){
  function get_customer_list(){
    $CI =& get_instance();
      $data_customer = $CI->customer_model->read_customer();
      if($data_customer->num_rows()!=0){
        return $data_customer;
      } else return false;
  }
}

if ( !function_exists('get_settings') ){
  function get_settings($name){
    $CI =& get_instance();
    $where = array('name'=>$name);
      $data = $CI->settings_model->read_settings($where);
      if($data->num_rows()!=0){
        return $data->row()->value;
      } else return false;
  }
}


if ( !function_exists('money') ){
  function money($number){
    return "Rp.".number_format($number,0,",",".");
  }
}

if ( !function_exists('generate_unique') ){
  function generate_unique(){
    return rand(1,9).rand(1,9).rand(1,9);
  }
}

if( !function_exists('hari')){
  function hari($hari){
    $harinya = "";
    switch ($hari) {
        case 'Sun':
        $harinya = "Minggu";
        break;
        case 'Mon':
        $harinya = "Senin";
        break;
        case 'Tue':
        $harinya = "Selasa";
        break;
        case 'Wed':
        $harinya = "Rabu";
        break;
        case 'Thu':
        $harinya = "Kamis";
        break;
        case 'Fri':
        $harinya = "Jumat";
        break;
        case 'Sat':
        $harinya = "Sabtu";
        break;
    }
    return $harinya;
  }
}

if( !function_exists('bulan')){
  function bulan($bulan){
    $bulannya = "";
    switch ($bulan) {
        case 'Jan':
        $bulannya = "Januari";
        break;
        case 'Feb':
        $bulannya = "Februari";
        break;
        case 'Mar':
        $bulannya = "Maret";
        break;
        case 'Apr':
        $bulannya = "April";
        break;
        case 'May':
        $bulannya = "Mei";
        break;
        case 'Jun':
        $bulannya = "Juni";
        break;
        case 'Jul':
        $bulannya = "Juli";
        break;
        case 'Aug':
        $bulannya = "Agustus";
        break;
        case 'Sep':
        $bulannya = "September";
        break;
        case 'Oct':
        $bulannya = "Oktober";
        break;
        case 'Nov':
        $bulannya = "November";
        break;
        case 'Dec':
        $bulannya = "Desember";
        break;
    }
    return $bulannya;
  }
}
if ( !function_exists('get_firstname') ){
  function get_firstname($name){
      $name_explode  = explode(' ', $name);
    return $name_explode[0];
  }
}
if ( !function_exists('count_member_total') ){
  function count_member_total(){
    $CI =& get_instance();
      $where = array(
          'type'=>'member',
          'status'=>'active'
      );
      $data = $CI->member_model->read_member($where);
      return $data->num_rows();
  }
}
if ( !function_exists('count_donasi_total') ){
  function count_donasi_total(){
    $CI =& get_instance();
      return $CI->member_model->count_donasi_total();
  }
}
if ( !function_exists('count_campaign_total') ){
  function count_campaign_total(){
    $CI =& get_instance();
      return $CI->campaign_model->read_campaign()->num_rows();
  }
}
if ( !function_exists('view_campaign_limit') ){
  function view_campaign_limit($limit){
    $CI =& get_instance();
      return $CI->campaign_model->read_campaign_limit($limit);
  }
}
if ( !function_exists('count_day_left') ){
  function count_day_left($date, $goal_day){
      $date_now         = new DateTime(date('Y-m-d'));
      $date             = date('Y-m-d', strtotime($date . ' +'.$goal_day.' day'));
      $date_reg_raw     = date('Y-m-d', strtotime($date));
      $date_compare     = new DateTime($date_reg_raw);
      $diff2            = $date_now->diff($date_compare)->format("%a");
      return $diff2;
  }
}
if ( !function_exists('sum_terkumpul') ){
  function sum_terkumpul($id_campaign){
      $CI =& get_instance();
      return $CI->campaign_model->sum_terkumpul($id_campaign);
  }
}
if ( !function_exists('persen_terkumpul') ){
  function persen_terkumpul($terkumpul, $target){
        if($terkumpul!=0){
        $jumlahtarget = $target;
        $jumlahterkumpul = $terkumpul;
        $persen = ($jumlahterkumpul/$jumlahtarget)*100;
        return $persen;
        } else return 0;
  }
}
if ( !function_exists('generate_unique') )
{
    function generate_unique(){
        $CI =& get_instance();
        $rand           = rand(1,9).rand(1,9).rand(1,9);
        $sql            = 'SELECT unique_id FROM charity WHERE unique_id LIKE "'.$rand.'"';
        $qry            = $CI->db->query($sql);
        if($qry->num_rows()!=0) return $rand;
        else return generate_unique();
    }
}
if ( !function_exists('get_list_donasi') ){
  function get_list_donasi($member_id){
    $CI =& get_instance();
    $where = array('A.member_id'=>$member_id);
      $data = $CI->charity_model->read_charity($where);
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}
if ( !function_exists('get_list_testimony') ){
  function get_list_testimony($lang){
    $CI =& get_instance();
      $where = array('lang'=>$lang);
      $data = $CI->testimony_model->read_testimony($where);
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}
if ( !function_exists('get_list_donatur') ){
  function get_list_donatur($campaign_id){
    $CI =& get_instance();
      $where = array('A.campaign_id'=>$campaign_id, 'A.status'=>'paid');
      $data = $CI->charity_model->read_charity($where);
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}
if ( !function_exists('get_list_donasi_all') ){
  function get_list_donasi_all(){
    $CI =& get_instance();
      $data = $CI->charity_model->read_charity();
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}
if ( !function_exists('get_list_news') ){
  function get_list_news($lang){
    $CI =& get_instance();
      $where = array('lang'=>$lang);
      $data = $CI->updates_article_model->read_updates_article($where);
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}

if ( !function_exists('get_list_pages') ){
  function get_list_pages(){
    $CI =& get_instance();
      $data = $CI->pages_model->read_pages();
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}

if ( !function_exists('get_list_testimonial') ){
  function get_list_testimonial($lang){
    $CI =& get_instance();
      $where = array('lang'=>$lang);
      $data = $CI->testimonial_model->read_testimonial($where);
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}

if ( !function_exists('get_list_key_feature') ){
  function get_list_key_feature($id_product=''){
    $CI =& get_instance();
      $where = ($id_product!=''?array('id_product'=>$id_product):'');
      $data = $CI->key_feature_model->read_key_feature($where);
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}

if ( !function_exists('get_list_key_feature_bylang') ){
  function get_list_key_feature_bylang($lang){
    $CI =& get_instance();
      $where = 'A.lang = "'.$lang.'"';
      $data = $CI->key_feature_model->get_key_feature_full($where);
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}

if ( !function_exists('get_list_slider') ){
  function get_list_slider(){
    $CI =& get_instance();
      $data = $CI->slider_model->read_slider();
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}

if ( !function_exists('get_list_client') ){
  function get_list_client(){
    $CI =& get_instance();
      $data = $CI->client_model->read_client();
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}

if ( !function_exists('get_list_product') ){
  function get_list_product($lang){
    $CI =& get_instance();
      $where = array('lang'=>$lang);
      $data = $CI->product_model->read_product($where);
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}

if ( !function_exists('get_list_why_choose_us') ){
  function get_list_why_choose_us($lang){
    $CI =& get_instance();
      $where = array('lang'=>$lang);
      $data = $CI->why_choose_us_model->read_why_choose_us($where);
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}

if ( !function_exists('get_list_member') ){
  function get_list_member(){
    $CI =& get_instance();
      $data = $CI->member_model->read_member();
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}

if ( !function_exists('read_memberandlevel') ){
  function read_memberandlevel(){
    $CI =& get_instance();
      $data = $CI->level_model->read_memberandlevel();
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}

if ( !function_exists('get_list_level') ){
  function get_list_level(){
    $CI =& get_instance();
      $data = $CI->level_model->read_level();
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}

if ( !function_exists('count_donatur_total') ){
  function count_donatur_total(){
    $CI =& get_instance();
      $where = array('A.status'=>'paid');
      $data = $CI->charity_model->read_charity($where);
      return $data->num_rows();
  }
}
if ( !function_exists('get_list_campaign') ){
  function get_list_campaign(){
    $CI =& get_instance();
      $data = $CI->campaign_model->read_campaign();
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}
if ( !function_exists('get_list_about_us') ){
  function get_list_about_us(){
    $CI =& get_instance();
      $data = $CI->about_us_model->read_about_us();
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}
if ( !function_exists('get_setting_editable') ){
  function get_setting_editable(){
    $CI =& get_instance();
      $where = array('editable'=>1);
      $data = $CI->settings_model->read_settings($where);
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}
if ( !function_exists('get_list_slideshow') ){
  function get_list_slideshow(){
    $CI =& get_instance();
      $data = $CI->slideshow_model->read_slideshow();
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}
if ( !function_exists('seo') ){
  function seo($seo){
    if($seo=="") return false;
    $seo = strtolower(url_title($seo));
      return $seo;
  }
}

if ( !function_exists('lang_convert') ){
  function lang_convert($lang){
    $lang = ($lang=='english'?'en':'id');
      return $lang;
  }
}

if ( !function_exists('crud_show') ){
  function crud_show($crud){
      return str_replace(',', ' ', $crud);
  }
}

if ( !function_exists('clean_content') ){
  function clean_content($content){
      return strip_tags($content, '<a><b><br><div><em><i><li><table><td><tr><span><sub><sup><strong><u><ul>');
  }
}

if ( !function_exists('view_short') ){
  function view_short($title){
      return substr($title,0,50);
  }
}
if ( !function_exists('get_list_whatsapp') ){
  function get_list_whatsapp(){
    $CI =& get_instance();
      $data = $CI->whatsapp_model->read_whatsapp();
      if($data->num_rows()!=0){
        return $data;
      } else return false;
  }
}
if ( !function_exists('cek_level') ){
  function cek_level($id_level){
    $CI =& get_instance();
      $where = array(
          'id_level'=>$id_level
      );
      $data = $CI->level_model->read_level($where);
      if($data->num_rows()!=0){
        return $data->row();
      } else return false;
  }
}
if ( !function_exists('phone_number') ){
  function phone_number($number){
      $first = substr($number, 0,1);
      if($first=="0") return "62".substr($number, 1);
      if($first=="+") return substr($number, 1);
      else return $number;
  }
}
if(!function_exists('get_priority_list')){
    function get_priority_list(){
        $CI =& get_instance();
     $priority = get_list_product($CI->session->userdata('lang_admin'));
     $priority_list = array();
     $priority_1 = false;
     $priority_2 = false;
     $priority_3 = false;
     $priority_4 = false;
     $priority_5 = false;
     $priority_6 = false;
     if($priority!=false){
          foreach($priority ->result() as $prio){
              if($prio->priority==1) $priority_1 = true;
              if($prio->priority==2) $priority_2 = true;
              if($prio->priority==3) $priority_3 = true;
              if($prio->priority==4) $priority_4 = true;
              if($prio->priority==5) $priority_5 = true;
              if($prio->priority==6) $priority_6 = true;
          }

     }
         if($priority_1!=true) $priority_list[] = 1;
         if($priority_2!=true) $priority_list[] = 2;
         if($priority_3!=true) $priority_list[] = 3;
         if($priority_4!=true) $priority_list[] = 4;
         if($priority_5!=true) $priority_list[] = 5;
         if($priority_6!=true) $priority_list[] = 6;
     return $priority_list;
    }
}
?>
