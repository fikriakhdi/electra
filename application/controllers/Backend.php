<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend extends CI_Controller
{
    
    function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
    }
    public function index()
    {
        auth_redirect_admin();
        $data['title']   = 'Dashboard';
        $data['content'] = VIEW_BACK . 'dashboard';
        $data['member']  = get_current_member_admin();$data['otoritas']= cek_level($data['member']->id_level);
        $this->load->view(VIEW_BACK . 'template', $data);
    }
    
    function change_lang($lang){
        $this->session->set_userdata('lang_admin', $lang);
        redirect(base_url('administrator'));
    }
    
    public function home_page_settings()
    {
        auth_redirect_admin();
        $data['title']   = 'Page Settings';
        $data['content'] = VIEW_BACK . 'home/page_settings';
        $data['member']  = get_current_member_admin();$data['otoritas']= cek_level($data['member']->id_level);
        $this->load->view(VIEW_BACK . 'template', $data);
    }
    
    public function home_our_products()
    {
        auth_redirect_admin();
        $data['title']   = 'Our Product';
        $data['content'] = VIEW_BACK . 'home/our_products';
        $data['member']  = get_current_member_admin();$data['otoritas']= cek_level($data['member']->id_level);
        $this->load->view(VIEW_BACK . 'template', $data);
    }    
    
    public function home_sliders()
    {
        auth_redirect_admin();
        $data['title']   = 'Sliders';
        $data['content'] = VIEW_BACK . 'home/sliders';
        $data['member']  = get_current_member_admin();$data['otoritas']= cek_level($data['member']->id_level);
        $this->load->view(VIEW_BACK . 'template', $data);
    }
    
    public function home_testimonial()
    {
        auth_redirect_admin();
        $data['title']   = 'Testimonial';
        $data['content'] = VIEW_BACK . 'home/testimonial';
        $data['member']  = get_current_member_admin();$data['otoritas']= cek_level($data['member']->id_level);
        $this->load->view(VIEW_BACK . 'template', $data);
    }

    public function home_why_choose_us()
    {
        auth_redirect_admin();
        $data['title']   = 'Why Choose Us';
        $data['content'] = VIEW_BACK . 'home/why_choose_us';
        $data['member']  = get_current_member_admin();$data['otoritas']= cek_level($data['member']->id_level);
        $this->load->view(VIEW_BACK . 'template', $data);
    }

    public function home_client_list()
    {
        auth_redirect_admin();
        $data['title']   = 'Client List';
        $data['content'] = VIEW_BACK . 'home/client_list';
        $data['member']  = get_current_member_admin();$data['otoritas']= cek_level($data['member']->id_level);
        $this->load->view(VIEW_BACK . 'template', $data);
    }
    
    public function about_us_page_settings()
    {
        auth_redirect_admin();
        $where           = array('lang'=>$this->session->userdata('lang_admin'));
        $about           = $this->about_us_model->read_about_us($where)->row();
        $data['title']   = 'Page Settings';
        $data['content'] = VIEW_BACK . 'about-us/page_settings';
        $data['member']  = get_current_member_admin();$data['otoritas']= cek_level($data['member']->id_level);
        $data['about']   = $about;
        $this->load->view(VIEW_BACK . 'template', $data);
    }
    
    public function products_page_settings()
    {
        auth_redirect_admin();
        $data['title']   = 'Page Settings';
        $data['content'] = VIEW_BACK . 'products/page_settings';
        $data['member']  = get_current_member_admin();$data['otoritas']= cek_level($data['member']->id_level);
        $this->load->view(VIEW_BACK . 'template', $data);
    }
    
    public function products_key_feature()
    {
        auth_redirect_admin();
        $data['title']   = 'Key Feature';
        $data['content'] = VIEW_BACK . 'products/key_feature';
        $data['member']  = get_current_member_admin();$data['otoritas']= cek_level($data['member']->id_level);
        $this->load->view(VIEW_BACK . 'template', $data);
    }
    
        public function news_update_news_manager()
    {
        auth_redirect_admin();
        $data['title']   = 'News Manager';
        $data['content'] = VIEW_BACK . 'news_&_update/news_manager';
        $data['member']  = get_current_member_admin();$data['otoritas']= cek_level($data['member']->id_level);
        $this->load->view(VIEW_BACK . 'template', $data);
    }
    
        public function settings_contacts()
    {
        auth_redirect_admin();
        $data['title']   = 'Contacts';
        $data['content'] = VIEW_BACK . 'settings/contacts';
        $data['member']  = get_current_member_admin();$data['otoritas']= cek_level($data['member']->id_level);
        $this->load->view(VIEW_BACK . 'template', $data);
    }
    
        public function settings_social_media_list()
    {
        auth_redirect_admin();
        $data['title']   = 'Social Media List';
        $data['content'] = VIEW_BACK . 'settings/social_media_list';
        $data['member']  = get_current_member_admin();$data['otoritas']= cek_level($data['member']->id_level);
        $this->load->view(VIEW_BACK . 'template', $data);
    }
    
    public function edit_social_media($name)
    {
        auth_redirect_admin();
        $where           = array('name'=>$name);
        $setting         = $this->settings_model->read_settings($where);
        if($setting->num_rows()!=0){
        $data['title']   = 'Key Feature';
        $data['content'] = VIEW_BACK . 'settings/social_media_edit';
        $data['member']  = get_current_member_admin();$data['otoritas']= cek_level($data['member']->id_level);
        $data['setting'] = $setting->row();
        $this->load->view(VIEW_BACK . 'template', $data);
        } else redirect(base_url());
    }
    
    function whatsapp_list(){
        auth_redirect_admin();
        $data['title']   = 'Whatsapp';
        $data['content'] = VIEW_BACK . 'settings/whatsapp';
        $data['member']  = get_current_member_admin();$data['otoritas']= cek_level($data['member']->id_level);
        $this->load->view(VIEW_BACK . 'template', $data);
    }
    
    function create_whatsapp(){
        $number = $this->input->post('number');
        $data_update = array(
            'number'=>phone_number($number),
            'datecreated'=>date('Y-m-d H:i:s'),
            'datemodified'=>date('Y-m-d H:i:s')
        );
        $this->whatsapp_model->create_whatsapp($data_update);
        //set flashdata
        $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Buat Whatsapp Berhasil!</div>');
        redirect(base_url('administrator/settings/whatsapp_list'));
    }
    
    function delete_whatsapp($id)
    {
       $flag = $this->whatsapp_model->delete_whatsapp($id);
        if ($flag) {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Berhasil!</div>');
            redirect(base_url('administrator/settings/whatsapp_list'));
        } else {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Gagal!</div>');
            redirect(base_url('administrator/settings/whatsapp_list'));
        }
    }
    
    function edit_whatsapp($id){
        $where = array('id'=>$id);
        $whatsapp = $this->whatsapp_model->read_whatsapp($where);
        if($whatsapp->num_rows()!=0){
        $data['title']   = 'Edit Whatsapp';
        $data['content'] = VIEW_BACK . 'settings/whatsapp_edit';
        $data['member']  = get_current_member_admin();$data['otoritas']= cek_level($data['member']->id_level);
        $data['whatsapp'] = $whatsapp->row();
        $this->load->view(VIEW_BACK . 'template', $data);
        } else redirect(base_url());
    }
    
    function whatsapp_edit_process(){
        $id     = $this->input->post('id');
        $number = $this->input->post('number');
        $data_update = array(
            'id'=>$id,
            'number'=>phone_number($number),
            'datemodified'=>date('Y-m-d H:i:s')
        );
        $this->whatsapp_model->update_whatsapp($data_update);
        //set flashdata
        $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Ubah Whatsapp Berhasil!</div>');
        redirect(base_url('administrator/settings/whatsapp_list'));
    }
    
    function themes(){
        auth_redirect_admin();
        $data['title']   = 'Themes';
        $data['content'] = VIEW_BACK . 'themes';
        $data['member']  = get_current_member_admin();$data['otoritas']= cek_level($data['member']->id_level);
        $this->load->view(VIEW_BACK . 'template', $data);
    }
    
    function create_theme(){
        $member         = get_current_member_admin();$data['otoritas']= cek_level($data['member']->id_level);
        $ok_ext         = array(
            'zip'
        );
        $destination    = 'assets/themes/'; // where our files will be stored
        if (!empty($_FILES['file']['name'])) {
            $file           = $_FILES['file'];
            $filename       = explode(".", $file["name"]);
            $file_name      = $file['name']; // file original name
            $file_extension = $filename[count($filename) - 1];
            $file_weight    = $file['size'];
            $file_type      = $file['type'];
            // If there is no error
            if ($file['error'] == 0) {
                // check if the extension is accepted
                if (in_array(strtolower($file_extension), $ok_ext)) {
                    // check if the size is not beyond expected size
                    // rename the file
                    $fileNewName = str_replace(" ", "_", strtolower(uniqid())) . '.' . $file_extension;
                    // and move it to the destination folder
                    if (move_uploaded_file($file['tmp_name'], $destination . $fileNewName)) {
                        $filepath = $destination . $fileNewName;
                        $path = pathinfo(realpath($filepath), PATHINFO_DIRNAME);
                        //extract zip 
                        $zip = new ZipArchive;
                        $res = $zip->open($file);
                        if ($res === TRUE) {
                          // extract it to the path we determined above
                          $zip->extractTo($path);
                          $zip->close();
                          //buka dokument txt untuk dapat settings theme
                          //pindahkan folder assetsnya ke folder assets asli
                          //simpan di database 
                            $data      = array(
                                'id'=>1,
                                'who_are_we_pic' => $save_path,
                                'who_are_we_content' => $who_are_we_content

                            );
                            $flag      = $this->about_us_model->update_about_us($data);
                            if ($flag) {
                                //set flashdata
                                $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Tambah Tema Berhasil!</div>');
                                redirect(base_url("administrator/themes"));
                            } else {
                                //set flashdata
                                $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Gagal! Terjadi kesalahan pada database.</div>');
                                redirect(base_url("administrator/themes"));
                            }
                        } else {
                          //set flashdata
                            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Terjadi kesalahan pada zip!</div>');
                            redirect(base_url("administrator/themes"));
                        }
                        
                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Gagal! Terjadi kesalahan saat mengunggah.</div>');
                        redirect(base_url('administrator/themes'));
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Gagal! Mohon hanya memilih file tipe .zip.</div>');
                    redirect(base_url('administrator/themes'));
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Mohon Pilih file terlebih dahulu.</div>');
                redirect(base_url('administrator/themes'));
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Mohon Pilih file terlebih dahulu.</div>');
                redirect(base_url('administrator/themes'));
        }
    }
    
    function disable_settings($settings_name){
        $data_update = array(
            'name'=>$settings_name,
            'status'=>0,
            'datemodified'=>date('Y-m-d H:i:s')
        );
        $this->settings_model->update_settings($data_update);
        //set flashdata
        $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Disable Settings Berhasil!</div>');
        redirect(base_url('administrator/settings/social_media_list'));
    }
    
     function enable_settings($settings_name){
        $data_update = array(
            'name'=>$settings_name,
            'status'=>1,
            'datemodified'=>date('Y-m-d H:i:s')
        );
        $this->settings_model->update_settings($data_update);
        //set flashdata
        $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Enable Settings Berhasil!</div>');
        redirect(base_url('administrator/settings/social_media_list'));
    }
    
    function social_media_edit_process(){
        $settings_name = $this->input->post('name');
        $value = $this->input->post('value');
        $data_update = array(
            'name'=>$settings_name,
            'value'=>$value,
            'datemodified'=>date('Y-m-d H:i:s')
        );
        $this->settings_model->update_settings($data_update);
        //set flashdata
        $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Ubah Settings Berhasil!</div>');
        redirect(base_url('administrator/settings/social_media_list'));
    }
    
    function aboutus_whoarewe()
    {
        $who_are_we_pic        = "";
        $who_are_we_content    = $this->input->post('who_are_we_content');
        $member                = get_current_member_admin();
        $ok_ext                = array(
            'jpg',
            'png',
            'jpeg',
            'bmp'
        ); // allow only these types of files
        $destination    = 'assets/img/about_us/'; // where our files will be stored
        $save_dest      = 'img/about_us/';
        if (!empty($_FILES['file']['name'])) {
            $file           = $_FILES['file'];
            $filename       = explode(".", $file["name"]);
            $file_name      = $file['name']; // file original name
            $file_extension = $filename[count($filename) - 1];
            $file_weight    = $file['size'];
            $file_type      = $file['type'];
            // If there is no error
            if ($file['error'] == 0) {
                // check if the extension is accepted
                if (in_array(strtolower($file_extension), $ok_ext)) {
                    // check if the size is not beyond expected size
                    // rename the file
                    $fileNewName = str_replace(" ", "_", strtolower(uniqid())) . '.' . $file_extension;
                    // and move it to the destination folder
                    if (move_uploaded_file($file['tmp_name'], $destination . $fileNewName)) {
                        $foto_path = $destination . $fileNewName;
                        $save_path = $destination. $fileNewName;
                        $data      = array(
                            'lang'=>$this->session->userdata('lang_admin'),
                            'who_are_we_pic' => $save_path,
                            'who_are_we_content' => $who_are_we_content
                            
                        );
                        $flag      = $this->about_us_model->update_about_us($data);
                        if ($flag) {
                            //set flashdata
                            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Berhasil!</div>');
                            redirect(base_url("administrator/about_us/page_settings"));
                        } else {
                            //set flashdata
                            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Gagal! Terjadi kesalahan pada database.</div>');
                            redirect(base_url("administrator/about_us/page_settings"));
                        }
                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Gagal! Terjadi kesalahan saat mengunggah.</div>');
                        redirect(base_url('administrator/about_us/page_settings'));
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Gagal! Mohon hanya memilih file tipe gambar.</div>');
                    redirect(base_url('administrator/about_us/page_settings'));
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Mohon Pilih file terlebih dahulu.</div>');
                redirect(base_url('administrator/about_us/page_settings'));
            }
        } else {
             $data      = array(
                            'lang'=>$this->session->userdata('lang_admin'),
                            'who_are_we_content' => $who_are_we_content
                            
                        );
                        $flag      = $this->about_us_model->update_about_us($data);
                        if ($flag) {
                            //set flashdata
                            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Berhasil!</div>');
                            redirect(base_url("administrator/about_us/page_settings"));
                        } else {
                            //set flashdata
                            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Gagal! Terjadi kesalahan pada database.</div>');
                            redirect(base_url("administrator/about_us/page_settings"));
                        }
        }
    }
    
    function aboutus_whatwedo()
    {
        $what_we_do_pic        = "";
        $what_we_do_content    = $this->input->post('what_we_do_content');
        $member         = get_current_member_admin();
        $ok_ext         = array(
            'jpg',
            'png',
            'jpeg',
            'bmp'
        ); // allow only these types of files
        $destination    = 'assets/img/about_us/'; // where our files will be stored
        $save_dest      = 'img/about_us/';
        if (!empty($_FILES['file1']['name'])) {
            $file           = $_FILES['file1'];
            $filename       = explode(".", $file["name"]);
            $file_name      = $file['name']; // file original name
            $file_extension = $filename[count($filename) - 1];
            $file_weight    = $file['size'];
            $file_type      = $file['type'];
            // If there is no error
            if ($file['error'] == 0) {
                // check if the extension is accepted
                if (in_array(strtolower($file_extension), $ok_ext)) {
                    // check if the size is not beyond expected size
                    // rename the file
                    $fileNewName = str_replace(" ", "_", strtolower(uniqid())) . '.' . $file_extension;
                    // and move it to the destination folder
                    if (move_uploaded_file($file['tmp_name'], $destination . $fileNewName)) {
                        $foto_path = $destination . $fileNewName;
                        $save_path = $save_dest . $fileNewName;
                        $data      = array(
                            'lang'=>$this->session->userdata('lang_admin'),
                            'what_we_do_pic' => $foto_path,
                            'what_we_do_content' => $what_we_do_content
                            
                        );
                        $flag      = $this->about_us_model->update_about_us($data);
                        if ($flag) {
                            //set flashdata
                            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Berhasil!</div>');
                            redirect(base_url("administrator/about_us/page_settings"));
                        } else {
                            //set flashdata
                            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Gagal! Terjadi kesalahan pada database.</div>');
                            redirect(base_url("administrator/about_us/page_settings"));
                        }
                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Gagal! Terjadi kesalahan saat mengunggah.</div>');
                        redirect(base_url('administrator/about_us/page_settings'));
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Gagal! Mohon hanya memilih file tipe gambar.</div>');
                    redirect(base_url('administrator/about_us/page_settings'));
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Mohon Pilih file terlebih dahulu.</div>');
                redirect(base_url('administrator/about_us/page_settings'));
            }
        } else {
             $data      = array(
                            'lang'=>$this->session->userdata('lang_admin'),
                            'what_we_do_content' => $what_we_do_content
                            
                        );
                        $flag      = $this->about_us_model->update_about_us($data);
                        if ($flag) {
                            //set flashdata
                            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Berhasil!</div>');
                            redirect(base_url("administrator/about_us/page_settings"));
                        } else {
                            //set flashdata
                            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Gagal! Terjadi kesalahan pada database.</div>');
                            redirect(base_url("administrator/about_us/page_settings"));
                        }
        }
    }

 function aboutus_whodoweworkwith()
    {
        $who_do_we_work_with_pic        = "";
        $who_do_we_work_with_content    = $this->input->post('who_do_we_work_with_content');
        $member         = get_current_member_admin();
        $ok_ext         = array(
            'jpg',
            'png',
            'jpeg',
            'bmp'
        ); // allow only these types of files
        $destination    = 'assets/img/about_us/'; // where our files will be stored
        $save_dest      = 'img/about_us/';
        if (!empty($_FILES['file2']['name'])) {
            $file           = $_FILES['file2'];
            $filename       = explode(".", $file["name"]);
            $file_name      = $file['name']; // file original name
            $file_extension = $filename[count($filename) - 1];
            $file_weight    = $file['size'];
            $file_type      = $file['type'];
            // If there is no error
            if ($file['error'] == 0) {
                // check if the extension is accepted
                if (in_array(strtolower($file_extension), $ok_ext)) {
                    // check if the size is not beyond expected size
                    // rename the file
                    $fileNewName = str_replace(" ", "_", strtolower(uniqid())) . '.' . $file_extension;
                    // and move it to the destination folder
                    if (move_uploaded_file($file['tmp_name'], $destination . $fileNewName)) {
                        $foto_path = $destination . $fileNewName;
                        $save_path = $save_dest . $fileNewName;
                        $data      = array(
                            'lang'=>$this->session->userdata('lang_admin'),
                            'who_do_we_work_with_pic' => $foto_path,
                            'who_do_we_work_with_content' => $who_do_we_work_with_content
                            
                        );
                        $flag      = $this->about_us_model->update_about_us($data);
                        if ($flag) {
                            //set flashdata
                            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Berhasil!</div>');
                            redirect(base_url("administrator/about_us/page_settings"));
                        } else {
                            //set flashdata
                            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Gagal! Terjadi kesalahan pada database.</div>');
                            redirect(base_url("administrator/about_us/page_settings"));
                        }
                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Gagal! Terjadi kesalahan saat mengunggah.</div>');
                        redirect(base_url('administrator/about_us/page_settings'));
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Gagal! Mohon hanya memilih file tipe gambar.</div>');
                    redirect(base_url('administrator/about_us/page_settings'));
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Mohon Pilih file terlebih dahulu.</div>');
                redirect(base_url('administrator/about_us/page_settings'));
            }
        } else {
             $data      = array(
                            'lang'=>$this->session->userdata('lang_admin'),
                            'who_do_we_work_with_content' => $who_do_we_work_with_content
                            
                        );
                        $flag      = $this->about_us_model->update_about_us($data);
                        if ($flag) {
                            //set flashdata
                            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Berhasil!</div>');
                            redirect(base_url("administrator/about_us/page_settings"));
                        } else {
                            //set flashdata
                            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Gagal! Terjadi kesalahan pada database.</div>');
                            redirect(base_url("administrator/about_us/page_settings"));
                        }
        }
    }

    function aboutus_whatwecandoforyou()
    {
        $what_we_can_do_for_you_pic        = "";
        $what_we_can_do_for_you_content    = $this->input->post('what_we_can_do_for_you_content');
        $member         = get_current_member_admin();
        $ok_ext         = array(
            'jpg',
            'png',
            'jpeg',
            'bmp'
        ); // allow only these types of files
        $destination    = 'assets/img/about_us/'; // where our files will be stored
        $save_dest      = 'img/about_us/';
        if (!empty($_FILES['file3']['name'])) {
            $file           = $_FILES['file3'];
            $filename       = explode(".", $file["name"]);
            $file_name      = $file['name']; // file original name
            $file_extension = $filename[count($filename) - 1];
            $file_weight    = $file['size'];
            $file_type      = $file['type'];
            // If there is no error
            if ($file['error'] == 0) {
                // check if the extension is accepted
                if (in_array(strtolower($file_extension), $ok_ext)) {
                    // check if the size is not beyond expected size
                    // rename the file
                    $fileNewName = str_replace(" ", "_", strtolower(uniqid())) . '.' . $file_extension;
                    // and move it to the destination folder
                    if (move_uploaded_file($file['tmp_name'], $destination . $fileNewName)) {
                        $foto_path = $destination . $fileNewName;
                        $save_path = $save_dest . $fileNewName;
                        $data      = array(
                            'lang'=>$this->session->userdata('lang_admin'),
                            'what_we_can_do_for_you_pic' => $foto_path,
                            'what_we_can_do_for_you_content' => $what_we_can_do_for_you_content
                            
                        );
                        $flag      = $this->about_us_model->update_about_us($data);
                        if ($flag) {
                            //set flashdata
                            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Berhasil!</div>');
                            redirect(base_url("administrator/about_us/page_settings"));
                        } else {
                            //set flashdata
                            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Gagal! Terjadi kesalahan pada database.</div>');
                            redirect(base_url("administrator/about_us/page_settings"));
                        }
                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Gagal! Terjadi kesalahan saat mengunggah.</div>');
                        redirect(base_url('administrator/about_us/page_settings'));
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>update Gagal! Mohon hanya memilih file tipe gambar.</div>');
                    redirect(base_url('administrator/about_us/page_settings'));
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Mohon Pilih file terlebih dahulu.</div>');
                redirect(base_url('administrator/about_us/page_settings'));
            }
        } else {
             $data      = array(
                            'lang'=>$this->session->userdata('lang_admin'),
                            'what_we_can_do_for_you_content' => $what_we_can_do_for_you_content
                            
                        );
                        $flag      = $this->about_us_model->update_about_us($data);
                        if ($flag) {
                            //set flashdata
                            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Berhasil!</div>');
                            redirect(base_url("administrator/about_us/page_settings"));
                        } else {
                            //set flashdata
                            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Gagal! Terjadi kesalahan pada database.</div>');
                            redirect(base_url("administrator/about_us/page_settings"));
                        }
        }
    }
    
        function updates_article_edit($id = 0)
    {
        auth_redirect_admin();
        $where        = array(
            'id' => $id
        );
        $updates_article_qry = $this->updates_article_model->read_updates_article($where);
        if ($updates_article_qry->num_rows() != 0) {
            $data['title']         = 'Update News';
            $data['content']       = VIEW_BACK . 'news_&_update/news_edit';
            $data['member']  = get_current_member_admin();$data['otoritas']= cek_level($data['member']->id_level);
            $data['updates_article'] = $updates_article_qry->row();
            $this->load->view(VIEW_BACK . 'template', $data);
        } else
            $this->load->view('404');
    }
    
    
         function testimonial_create()
    {
        $name           = $this->input->post('name');
        $company        = $this->input->post('company');
        $comment        = $this->input->post('comment');
        $member         = get_current_member_admin();
        $datetime       = date("Y-m-d H:i:s");
        $picture        = "";
        $ok_ext         = array(
            'jpg',
            'png',
            'jpeg',
            'bmp'
        ); // allow only these types of files
        $destination    = 'assets/img/testimonial'; // where our files will be stored
        $save_dest      = 'img/testimonial/';
        if (!empty($_FILES['file'])) {
            $file           = $_FILES['file'];
            $filename       = explode(".", $file["name"]);
            $file_name      = $file['name']; // file original name
            $file_extension = $filename[count($filename) - 1];
            $file_weight    = $file['size'];
            $file_type      = $file['type'];
            if ($_FILES['file']['tmp_name'] != '') {
                // If there is no error
                if ($file['error'] == 0) {
                    // check if the extension is accepted
                    if (in_array(strtolower($file_extension), $ok_ext)) {
                        // check if the size is not beyond expected size
                        // rename the file
                        $fileNewName = str_replace(" ", "_", strtolower(uniqid())) . '.' . $file_extension;
                        // and move it to the destination folder
                        if (move_uploaded_file($file['tmp_name'], $destination . $fileNewName)) {
                            $foto_path = $destination . $fileNewName;
                            $save_path = $save_dest . $fileNewName;
                            $data      = array(
                                'picture' => $foto_path,
                                'comment' => $comment,
                                'name' => $name,
                                'company' => $company,
                                'lang' => $this->session->userdata('lang_admin'),
                                'datecreated' => $datetime,
                                'datemodified' => $datetime
                            );
                            $flag      = $this->testimonial_model->create_testimonial($data);
                            if ($flag) {
                                //set flashdata
                                $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Testimoni Sukses!</div>');
                                redirect(base_url("administrator/home/testimonial"));
                            } else {
                                //set flashdata
                                $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Testimoni Gagal! Terjadi kesalahan pada database.</div>');
                                redirect(base_url("administrator/home/testimonial"));
                            }
                        } else {
                            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Testimoni Gagal! Terjadi kesalahan saat mengunggah.</div>');
                            redirect(base_url('administrator/home/testimonial'));
                        }
                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Campaign Gagal! Mohon hanya memilih file tipe gambar.</div>');
                        redirect(base_url('administrator/home/testimonial'));
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Campaign Gagal! Mohon Pilih file terlebih dahulu.</div>');
                    redirect(base_url('administrator/home/testimonial'));
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Campaign Gagal! Mohon Pilih file terlebih dahulu.</div>');
                redirect(base_url('administrator/home/testimonial'));
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Pengunggahan Bukti Transfer Gagal! Mohon Pilih file terlebih dahulu.</div>');
            redirect(base_url('administrator/home/testimonial'));
        }
    }
    
    function delete_testimonial($id)
    {
       $flag = $this->testimonial_model->delete_testimonial($id);
        if ($flag) {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Berhasil!</div>');
            redirect(base_url('administrator/home/testimonial'));
        } else {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Gagal!</div>');
            redirect(base_url('administrator/home/testimonial'));
        }
    }
    
 function testimonial_edit_process()
    {
        $id             = $this->input->post('id');
        $comment        = $this->input->post('comment');
        $name           = $this->input->post('name');
        $company        = $this->input->post('company');
       
        $member         = get_current_member_admin();
        $datetime       = date("Y-m-d H:i:s");
        $picture        = "";
        $ok_ext         = array(
            'jpg',
            'png',
            'jpeg',
            'bmp'
        ); // allow only these types of files
        $destination    = 'assets/img/testimonial/'; // where our files will be stored
        $save_dest      = 'img/testimonial/';
        if (!empty($_FILES['file']['name'])) {
            $file           = $_FILES['file'];
            $filename       = explode(".", $file["name"]);
            $file_name      = $file['name']; // file original name
            $file_extension = $filename[count($filename) - 1];
            $file_weight    = $file['size'];
            $file_type      = $file['type'];
            // If there is no error
            if ($file['error'] == 0) {
                // check if the extension is accepted
                if (in_array(strtolower($file_extension), $ok_ext)) {
                    // check if the size is not beyond expected size
                    // rename the file
                    $fileNewName = str_replace(" ", "_", strtolower(uniqid())) . '.' . $file_extension;
                    // and move it to the destination folder
                    if (move_uploaded_file($file['tmp_name'], $destination . $fileNewName)) {
//                        $foto_path = $destination . $fileNewName;
                            $foto_path = $destination . $fileNewName;
                            $save_path = $save_dest . $fileNewName;
                            $data      = array(
                                'id' => $id,
                                'picture' => $foto_path,
                                'comment' => $comment,
                                'name' => $name,
                                'company' => $company,
                                'datemodified' => $datetime
                            
                        );
                        $flag      = $this->testimonial_model->update_testimonial($data);
                        if ($flag) {
                            //set flashdata
                            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Berhasil!</div>');
                            redirect(base_url('administrator/home/testimonial'));
                        } else {
                            //set flashdata
                            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Gagal! Terjadi kesalahan pada database.</div>');
                            redirect(base_url('administrator/home/testimonial'));
                        }
                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Gagal! Terjadi kesalahan saat mengunggah.</div>');
                        redirect(base_url('administrator/home/testimonial'));
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Gagal! Mohon hanya memilih file tipe gambar.</div>');
                    redirect(base_url('administrator/home/testimonial'));
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Mohon Pilih file terlebih dahulu.</div>');
                redirect(base_url('administrator/home/testimonial'));
            }
        } else {
             $data      = array(
                                'id' => $id,
                                'comment' => $comment,
                                'name' => $name,
                                'company' => $company,
                                'datemodified' => $datetime
                        );
                        $flag      = $this->testimonial_model->update_testimonial($data);
                        if ($flag) {
                            //set flashdata
                            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Berhasil!</div>');
                            redirect(base_url('administrator/home/testimonial'));
                        } else {
                            //set flashdata
                            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Gagal! Terjadi kesalahan pada database.</div>');
                            redirect(base_url('administrator/home/testimonial'));
                        }
        }
    }
    
        function testimonial_edit($id = 0)
    {
        auth_redirect_admin();
        $where        = array(
            'id' => $id
        );
        $testimonial_qry = $this->testimonial_model->read_testimonial($where);
        if ($testimonial_qry->num_rows() != 0) {
            $data['title']         = 'Update Testimonial';
            $data['content']       = VIEW_BACK . 'home/testimonial_edit';
            $data['member']  = get_current_member_admin();$data['otoritas']= cek_level($data['member']->id_level);
            $data['testimonial'] = $testimonial_qry->row();
            $this->load->view(VIEW_BACK . 'template', $data);
        } else
            $this->load->view('404');
    }
    
    function settings_process()
    {
        $where    = array(
            'editable' => 1
        );
        $settings = $this->settings_model->read_settings($where);
        $data     = array();
        if ($settings->num_rows() != 0) {
            foreach ($settings->result() as $option) {
                $data['name']  = $option->name;
                $data['value'] = $this->input->post($option->name);
                $flag          = $this->settings_model->update_settings($data);
            }
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Campaign Sukses!</div>');
            redirect(base_url("administrator/settings"));
        }
    }
    
    
    function create_product()
    {
        $name           = $this->input->post('name');
        $description    = $this->input->post('description');
        $benefits_product_owner  = $this->input->post('benefits_product_owner');
        $benefits_travel_agent  = $this->input->post('benefits_travel_agent');
        $video_url      = $this->input->post('video_url');
        $demo_url       = $this->input->post('demo_url');
        $priority       = $this->input->post('priority');
        $member         = get_current_member_admin();
        $datetime       = date("Y-m-d H:i:s");
        $icon           = upload_file("icon", "images", 'assets/img/icon/img/Product/');
        $file           = upload_file("file", "images", 'assets/img/icon/img/Product/');
        $desktop_image   = upload_file("desktop_image", "images", 'assets/img/icon/img/Product/');
        $mobile_image   = upload_file("mobile_image", "images", 'assets/img/icon/img/Product/');
        //pengecekan error
        if ($file== 'empty') {
             $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Product Gagal! Mohon Pilih file terlebih dahulu.</div>');
            redirect(base_url('administrator/products/page_settings'));
        }
        if ($file== 'error') {
             $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Product Gagal! File Corrupt.</div>');
            redirect(base_url('administrator/products/page_settings'));
        }
        if ($file== 'error_extension') {
              $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Product Gagal! Mohon hanya memilih file tipe gambar.</div>');
            redirect(base_url('administrator/products/page_settings'));
        }
        if ($file== 'error_upload') {
             $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Product Gagal! Terjadi kesalahan saat mengunggah.</div>');
            redirect(base_url('administrator/products/page_settings'));
        }
                if ($icon== 'empty') {
             $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Product Gagal! Mohon Pilih file terlebih dahulu.</div>');
            redirect(base_url('administrator/products/page_settings'));
        }
        if ($icon== 'error') {
             $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Product Gagal! File Corrupt.</div>');
            redirect(base_url('administrator/products/page_settings'));
        }
        if ($icon== 'error_extension') {
              $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Product Gagal! Mohon hanya memilih file tipe gambar.</div>');
            redirect(base_url('administrator/products/page_settings'));
        }
        if ($icon== 'error_upload') {
             $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Product Gagal! Terjadi kesalahan saat mengunggah.</div>');
            redirect(base_url('administrator/products/page_settings'));
        }
        
        $data      = array(
                        'name' => $name,
                        'description' => clean_content($description),
                        'icon' => $icon,
                        'benefits_product_owner' => clean_content($benefits_product_owner),
                        'benefits_travel_agent' => clean_content($benefits_travel_agent),
                        'priority' =>(empty($priority)?NULL:$priority),
                        'video_url'      => $video_url,
                        'demo_url'      => $demo_url,
                        'header_image' => $file,
                        'desktop_image' => $desktop_image,
                        'mobile_image' => $mobile_image,
                        'lang' => $this->session->userdata('lang_admin'),
                        'datecreated' => $datetime,
                        'datemodified' => $datetime
                    );
                    $flag      = $this->product_model->create_product($data);
                    if ($flag) {
                        //set flashdata
                        $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Product Sukses!</div>');
                        redirect(base_url("administrator/products/page_settings"));
                    } else {
                        //set flashdata
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Product Gagal! Terjadi kesalahan pada database.</div>');
                        redirect(base_url("administrator/products/page_settings"));
                    }
            } //else {
//                $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Product Gagal! Mohon Pilih file terlebih dahulu.</div>');
//                redirect(base_url('administrator/products/page_settings'));
//            }
//        } else {
//            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Pengunggahan Bukti Transfer Gagal! Mohon Pilih file terlebih dahulu.</div>');
//            redirect(base_url('administrator/products/page_settings'));
//        }
    
        function product_edit_process()
        {   
        $id             = $this->input->post('id');
        $name           = $this->input->post('name');
        $description    = $this->input->post('description');
        $benefits_product_owner  = $this->input->post('benefits_product_owner');
        $benefits_travel_agent  = $this->input->post('benefits_travel_agent');
        $video_url       = $this->input->post('video_url');
        $demo_url       = $this->input->post('demo_url');
        $priority       = $this->input->post('priority');
        $member         = get_current_member_admin();
        $datetime       = date("Y-m-d H:i:s");
        $icon           = upload_file("icon", "images", 'assets/img/icon/img/Product/');
        $file           = upload_file("file", "images", 'assets/img/icon/img/Product/');
        $desktop_image  = upload_file("desktop_image", "images", 'assets/img/icon/img/Product/');
        $mobile_image   = upload_file("mobile_image", "images", 'assets/img/icon/img/Product/');
        $data           = array(
                        'id' => $id, 
                        'name' => $name,
                        'description' => clean_content($description),
                        'benefits_product_owner' => clean_content($benefits_product_owner),
                        'benefits_travel_agent' => clean_content($benefits_travel_agent),
                        'video_url'      => $video_url,
                        'priority' =>(empty($priority)?null:$priority),
                        'demo_url'      => $demo_url,
                        'datemodified' => $datetime
                    );
            if($icon!="empty") $data['icon']=$icon;
            if($file!="empty") $data['header_image']=$file;
            if($desktop_image!="empty") $data['desktop_image']=$desktop_image;
            if($mobile_image!="empty") $data['mobile_image']=$mobile_image;
                    $flag      = $this->product_model->update_product($data);
                    if ($flag) {
                        //set flashdata
                        $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Berhasil!</div>');
                        redirect(base_url("administrator/products/page_settings"));
                    } else {
                        //set flashdata
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Gagal! Terjadi kesalahan pada database.</div>');
                        redirect(base_url("administrator/products/page_settings"));
                    }
            
            } 
    
    function product_edit($id = 0)
    {
        auth_redirect_admin();
        $where        = array(
            'id' => $id
        );
        $product_qry = $this->product_model->read_product($where);
        if ($product_qry->num_rows() != 0) {
            $data['title']         = 'Update Our Products';
            $data['content']       = VIEW_BACK . 'products/product_edit';
            $data['member']  = get_current_member_admin();$data['otoritas']= cek_level($data['member']->id_level);
            $data['product'] = $product_qry->row();
            $this->load->view(VIEW_BACK . 'template', $data);
        } else
            $this->load->view('404');
    }
    
    function delete_product($id)
    {
       $flag = $this->product_model->delete_product($id);
        if ($flag) {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Berhasil!</div>');
            redirect(base_url('administrator/products/page_settings'));
        } else {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Gagal!</div>');
            redirect(base_url('administrator/products/page_settings'));
        }
    }

     function create_why_choose_us()
    {
        $title          = $this->input->post('title');
        $description    = $this->input->post('content');
        $member         = get_current_member_admin();
        $datetime       = date("Y-m-d H:i:s");
        $logo           = upload_file("logo", "images", 'assets/img/icon/img/why-choose-us/');
        $image          = upload_file("image", "images", 'assets/img/icon/img/why-choose-us/');
        //pengecekan error
        if ($logo== 'empty') {
             $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Mohon Pilih file terlebih dahulu.</div>');
            redirect(base_url('administrator/home/why_choose_us'));
        }
        if ($logo== 'error') {
             $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! File Corrupt.</div>');
            redirect(base_url('administrator/home/why_choose_us'));
        }
        if ($image== 'error_extension') {
              $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Mohon hanya memilih file tipe gambar.</div>');
            redirect(base_url('administrator/home/why_choose_us'));
        }
        if ($image== 'error_upload') {
             $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Terjadi kesalahan saat mengunggah.</div>');
            redirect(base_url('administrator/home/why_choose_us'));
        }
        if ($image== 'empty') {
             $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Mohon Pilih file terlebih dahulu.</div>');
            redirect(base_url('administrator/home/why_choose_us'));
        }
        if ($image== 'error') {
             $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! File Corrupt.</div>');
            redirect(base_url('administrator/home/why_choose_us'));
        }
        if ($image== 'error_extension') {
              $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Mohon hanya memilih file tipe gambar.</div>');
            redirect(base_url('administrator/home/why_choose_us'));
        }
        if ($image== 'error_upload') {
             $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Terjadi kesalahan saat mengunggah.</div>');
            redirect(base_url('administrator/home/why_choose_us'));
        }
        
        $data      = array(
                        'title' => $title,
                        'description' => $description,
                        'logo' => $logo,
                        'image' => $image,
                        'lang' => $this->session->userdata('lang_admin'),
                        'datecreated' => $datetime,
                        'datemodified' => $datetime
                    );
                    $flag      = $this->why_choose_us_model->create_why_choose_us($data);
                    if ($flag) {
                        //set flashdata
                        $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Sukses!</div>');
                        redirect(base_url("administrator/home/why_choose_us"));
                    } else {
                        //set flashdata
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Terjadi kesalahan pada database.</div>');
                        redirect(base_url("administrator/home/why_choose_us"));
                    }
            }
    
        function why_choose_us_edit($id = 0)
    {
        auth_redirect_admin();
        $where        = array(
            'id' => $id
        );
        $why_choose_us_qry = $this->why_choose_us_model->read_why_choose_us($where);
        if ($why_choose_us_qry->num_rows() != 0) {
            $data['title']         = 'Update Why Choose US';
            $data['content']       = VIEW_BACK . 'home/why_choose_us_edit';
            $data['member']        = get_current_member_admin();$data['otoritas']= cek_level($data['member']->id_level);
            $data['why_choose_us'] = $why_choose_us_qry->row();
            $this->load->view(VIEW_BACK . 'template', $data);
        } else
            $this->load->view('404');
    }
    
    function delete_why_choose_us($id)
    {
       $flag = $this->why_choose_us_model->delete_why_choose_us($id);
        if ($flag) {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Berhasil!</div>');
            redirect(base_url('administrator/home/why_choose_us'));
        } else {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Gagal!</div>');
            redirect(base_url('administrator/home/why_choose_us'));
        }
    }
    
    function why_choose_us_edit_process()
    {
        $id             = $this->input->post('id');
        $title          = $this->input->post('title');
        $description    = $this->input->post('description');
        $member         = get_current_member_admin();
        $datetime       = date("Y-m-d H:i:s");
        $logo           = upload_file("logo", "images", 'assets/img/icon/img/why-choose-us/');        
        $image          = upload_file("image", "images", 'assets/img/icon/img/why-choose-us/');
        //pengecekan error
        
        $data      = array(
                        'id'    => $id,
                        'title' => $title,
                        'description' => $description,
                        'datemodified' => $datetime
                    );
        if($logo!="empty") $data['logo']=$logo;
        if($image!="empty") $data['image']=$image;
                    $flag      = $this->why_choose_us_model->update_why_choose_us($data);
                    if ($flag) {
                        //set flashdata
                        $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Berhasil!</div>');
                        redirect(base_url("administrator/home/why_choose_us"));
                    } else {
                        //set flashdata
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Gagal! Terjadi kesalahan pada database.</div>');
                        redirect(base_url("administrator/home/why_choose_us"));
                    }
            }
    
         function create_key_feature()
    {
        $id_product     = $this->input->post('id_product');
        $lang           = $this->input->post('lang');
        $title          = $this->input->post('title');
        $member         = get_current_member_admin();
        $datetime       = date("Y-m-d H:i:s");
        $icon           = upload_file("icon", "images", 'assets/img/icon/img/Key Feature/');
        //pengecekan error
        if ($icon== 'empty') {
             $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Mohon Pilih file terlebih dahulu.</div>');
            redirect(base_url('administrator/products/key_feature'));
        }
        if ($icon== 'error') {
             $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! File Corrupt.</div>');
            redirect(base_url('administrator/products/key_feature'));
        }
        if ($icon== 'error_extension') {
              $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Mohon hanya memilih file tipe gambar.</div>');
            redirect(base_url('administrator/products/key_feature'));
        }
        if ($icon== 'error_upload') {
             $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Product Gagal! Terjadi kesalahan saat mengunggah.</div>');
            redirect(base_url('administrator/hproducts/key_feature'));
        }
        
        $data      = array(
                        'id_product' => $id_product,
                        'icon' => $icon,
                        'lang' => $this->session->userdata('lang_admin'),
                        'title' => $title,
                        'datecreated' => $datetime,
                        'datemodified' => $datetime
                    );
                    $flag      = $this->key_feature_model->create_key_feature($data);
                    if ($flag) {
                        //set flashdata
                        $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Sukses!</div>');
                        redirect(base_url("administrator/products/key_feature"));
                    } else {
                        //set flashdata
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Terjadi kesalahan pada database.</div>');
                        redirect(base_url("administrator/products/key_feature"));
                    }
            }
    
       function key_feature_edit_process()
    {   
        $id             = $this->input->post('id');
        $id_product     = $this->input->post('id_product');
        $lang           = $this->input->post('lang');
        $title          = $this->input->post('title');
        $member         = get_current_member_admin();
        $datetime       = date("Y-m-d H:i:s");
        $icon           = upload_file("icon", "images", 'assets/img/icon/img/Key Feature/');
        //pengecekan error
        $data      = array(
                        'id' => $id,
                        'id_product' => $id_product,
                        'lang' => $this->session->userdata('lang_admin'),
                        'title' => $title,
                        'datemodified' => $datetime
                    );
           if($icon!="empty") $data['icon']=$icon;
                    $flag      = $this->key_feature_model->update_key_feature($data);
                    if ($flag) {
                        //set flashdata
                        $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Berhasil!</div>');
                        redirect(base_url("administrator/products/key_feature"));
                    } else {
                        //set flashdata
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>update Gagal! Terjadi kesalahan pada database.</div>');
                        redirect(base_url("administrator/products/key_feature"));
                    }
            }
    
            function key_feature_edit($id = 0)
    {
        auth_redirect_admin();
        $where        = array(
            'id' => $id
        );
        $key_feature_qry = $this->key_feature_model->read_key_feature($where);
        if ($key_feature_qry->num_rows() != 0) {
            $data['title']         = 'Update Why Choose US';
            $data['content']       = VIEW_BACK . 'products/key_feature_edit';
            $data['member']  = get_current_member_admin();$data['otoritas']= cek_level($data['member']->id_level);
            $data['key_feature'] = $key_feature_qry->row();
            $this->load->view(VIEW_BACK . 'template', $data);
        } else
            $this->load->view('404');
    }
    
    function delete_key_feature($id)
    {
       $flag = $this->key_feature_model->delete_key_feature($id);
        if ($flag) {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Berhasil!</div>');
            redirect(base_url('administrator/products/key_feature'));
        } else {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Gagal!</div>');
            redirect(base_url('administrator/products/key_feature'));
        }
    }
    
    function create_slider()
    {
        $member         = get_current_member_admin();
        $datetime       = date("Y-m-d H:i:s");
        $picture        = "";
        $ok_ext         = array(
            'jpg',
            'png',
            'jpeg',
            'bmp'
        ); // allow only these types of files
        $destination    = 'assets/img/banner/'; // where our files will be stored
        $save_dest      = 'images/campaign/';
        if (!empty($_FILES['file'])) {
            $file           = $_FILES['file'];
            $filename       = explode(".", $file["name"]);
            $file_name      = $file['name']; // file original name
            $file_extension = $filename[count($filename) - 1];
            $file_weight    = $file['size'];
            $file_type      = $file['type'];
            if ($_FILES['file']['tmp_name'] != '') {
                // If there is no error
                if ($file['error'] == 0) {
                    // check if the extension is accepted
                    if (in_array(strtolower($file_extension), $ok_ext)) {
                        // check if the size is not beyond expected size
                        // rename the file
                        $fileNewName = str_replace(" ", "_", strtolower(uniqid())) . '.' . $file_extension;
                        // and move it to the destination folder
                        if (move_uploaded_file($file['tmp_name'], $destination . $fileNewName)) {
                            $foto_path = $destination . $fileNewName;
                            $save_path = $save_dest . $fileNewName;
                            $data      = array(
                                'picture' => $foto_path,
                                'datecreated' => $datetime,
                                'datemodified' => $datetime
                            );
                            $flag      = $this->slider_model->create_slider($data);
                            if ($flag) {
                                //set flashdata
                                $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Slider Sukses!</div>');
                                redirect(base_url("administrator/home/sliders"));
                            } else {
                                //set flashdata
                                $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Slider Gagal! Terjadi kesalahan pada database.</div>');
                                redirect(base_url("administrator/home/sliders"));
                            }
                        } else {
                            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Slider Gagal! Terjadi kesalahan saat mengunggah.</div>');
                            redirect(base_url('administrator/home/sliders'));
                        }
                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Slider Gagal! Mohon hanya memilih file tipe gambar.</div>');
                        redirect(base_url('administrator/home/sliders'));
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Slider Gagal! Mohon Pilih file terlebih dahulu.</div>');
                    redirect(base_url('administrator/home/sliders'));
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Slider Gagal! Mohon Pilih file terlebih dahulu.</div>');
                redirect(base_url('administrator/home/sliders'));
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Pengunggahan Slider Gagal! Mohon Pilih file terlebih dahulu.</div>');
            redirect(base_url('administrator/home/sliders'));
        }
    }
    
    function slider_edit_process()
    {
        $id             = $this->input->post('id');       
        $member         = get_current_member_admin();
        $datetime       = date("Y-m-d H:i:s");
        $picture        = "";
        $ok_ext         = array(
            'jpg',
            'png',
            'jpeg',
            'bmp'
        ); // allow only these types of files
        $destination    = 'assets/img/banner/'; // where our files will be stored
        $save_dest      = 'img/testimonial/';
        if (!empty($_FILES['file']['name'])) {
            $file           = $_FILES['file'];
            $filename       = explode(".", $file["name"]);
            $file_name      = $file['name']; // file original name
            $file_extension = $filename[count($filename) - 1];
            $file_weight    = $file['size'];
            $file_type      = $file['type'];
            // If there is no error
            if ($file['error'] == 0) {
                // check if the extension is accepted
                if (in_array(strtolower($file_extension), $ok_ext)) {
                    // check if the size is not beyond expected size
                    // rename the file
                    $fileNewName = str_replace(" ", "_", strtolower(uniqid())) . '.' . $file_extension;
                    // and move it to the destination folder
                    if (move_uploaded_file($file['tmp_name'], $destination . $fileNewName)) {
//                        $foto_path = $destination . $fileNewName;
                            $foto_path = $destination . $fileNewName;
                            $save_path = $save_dest . $fileNewName;
                            $data      = array(
                                'id' => $id,
                                'picture' => $foto_path,
                                'datemodified' => $datetime
                            
                        );
                        $flag      = $this->slider_model->update_slider($data);
                        if ($flag) {
                            //set flashdata
                            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Berhasil!</div>');
                            redirect(base_url('administrator/home/sliders'));
                        } else {
                            //set flashdata
                            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Gagal! Terjadi kesalahan pada database.</div>');
                            redirect(base_url('administrator/home/sliders'));
                        }
                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Gagal! Terjadi kesalahan saat mengunggah.</div>');
                        redirect(base_url('administrator/home/sliders'));
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Gagal! Mohon hanya memilih file tipe gambar.</div>');
                    redirect(base_url('administrator/home/sliders'));
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Mohon Pilih file terlebih dahulu.</div>');
                redirect(base_url('administrator/home/sliders'));
            }
        } else {
             $data      = array(
                                'id' => $id,
                                'datemodified' => $datetime
                        );
                        $flag      = $this->slider_model->update_slider($data);
                        if ($flag) {
                            //set flashdata
                            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Berhasil!</div>');
                            redirect(base_url('administrator/home/sliders'));
                        } else {
                            //set flashdata
                            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Gagal! Terjadi kesalahan pada database.</div>');
                            redirect(base_url('administrator/home/sliders'));
                        }
        }
    }
    
    function slider_edit($id = 0)
    {
        auth_redirect_admin();
        $where        = array(
            'id' => $id
        );
        $slider_qry = $this->slider_model->read_slider($where);
        if ($slider_qry->num_rows() != 0) {
            $data['title']         = 'Update Testimonial';
            $data['content']       = VIEW_BACK . 'home/sliders_edit';
            $data['member']  = get_current_member_admin();$data['otoritas']= cek_level($data['member']->id_level);
            $data['slider'] = $slider_qry->row();
            $this->load->view(VIEW_BACK . 'template', $data);
        } else
            $this->load->view('404');
    }
    
    function delete_slider($id)
    {
       $flag = $this->slider_model->delete_slider($id);
        if ($flag) {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Berhasil!</div>');
            redirect(base_url('administrator/home/sliders'));
        } else {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Gagal!</div>');
            redirect(base_url('administrator/home/sliders'));
        }
    }
    
    function create_client()
    {
        $client_name    = $this->input->post('client_name');
        $member         = get_current_member_admin();
        $datetime       = date("Y-m-d H:i:s");
        $file           = "";
        $ok_ext         = array(
            'jpg',
            'png',
            'jpeg',
            'bmp'
        ); // allow only these types of files
        $destination    = 'assets/img/client/'; // where our files will be stored
        $save_dest      = 'images/campaign/';
        if (!empty($_FILES['file'])) {
            $file           = $_FILES['file'];
            $filename       = explode(".", $file["name"]);
            $file_name      = $file['name']; // file original name
            $file_extension = $filename[count($filename) - 1];
            $file_weight    = $file['size'];
            $file_type      = $file['type'];
            if ($_FILES['file']['tmp_name'] != '') {
                // If there is no error
                if ($file['error'] == 0) {
                    // check if the extension is accepted
                    if (in_array(strtolower($file_extension), $ok_ext)) {
                        // check if the size is not beyond expected size
                        // rename the file
                        $fileNewName = str_replace(" ", "_", strtolower(uniqid())) . '.' . $file_extension;
                        // and move it to the destination folder
                        if (move_uploaded_file($file['tmp_name'], $destination . $fileNewName)) {
                            $foto_path = $destination . $fileNewName;
                            $save_path = $save_dest . $fileNewName;
                            $data      = array(
                                'client_name' => $client_name,
                                'client_logo' => $foto_path,
                                'datecreated' => $datetime,
                                'datemodified' => $datetime
                            );
                            $flag      = $this->client_model->create_client($data);
                            if ($flag) {
                                //set flashdata
                                $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Client Sukses!</div>');
                                redirect(base_url("administrator/home/client_list"));
                            } else {
                                //set flashdata
                                $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Client Gagal! Terjadi kesalahan pada database.</div>');
                                redirect(base_url("administrator/home/client_list"));
                            }
                        } else {
                            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Client Gagal! Terjadi kesalahan saat mengunggah.</div>');
                            redirect(base_url('administrator/home/client_list'));
                        }
                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Client Gagal! Mohon hanya memilih file tipe gambar.</div>');
                        redirect(base_url('administrator/home/client_list'));
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Client Gagal! Mohon Pilih file terlebih dahulu.</div>');
                    redirect(base_url('administrator/home/client_list'));
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Client Gagal! Mohon Pilih file terlebih dahulu.</div>');
                redirect(base_url('administrator/home/client_list'));
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Pengunggahan Bukti Transfer Gagal! Mohon Pilih file terlebih dahulu.</div>');
            redirect(base_url('administrator/home/client_list'));
        }
    }
    
        function client_edit_process()
    {
        $id             = $this->input->post('id'); 
        $client_name    = $this->input->post('client_name');
        $member         = get_current_member_admin();
        $datetime       = date("Y-m-d H:i:s");
        $file           = "";
        $ok_ext         = array(
            'jpg',
            'png',
            'jpeg',
            'bmp'
        ); // allow only these types of files
        $destination    = 'assets/img/banner/'; // where our files will be stored
        $save_dest      = 'img/testimonial/';
        if (!empty($_FILES['file']['name'])) {
            $file           = $_FILES['file'];
            $filename       = explode(".", $file["name"]);
            $file_name      = $file['name']; // file original name
            $file_extension = $filename[count($filename) - 1];
            $file_weight    = $file['size'];
            $file_type      = $file['type'];
            // If there is no error
            if ($file['error'] == 0) {
                // check if the extension is accepted
                if (in_array(strtolower($file_extension), $ok_ext)) {
                    // check if the size is not beyond expected size
                    // rename the file
                    $fileNewName = str_replace(" ", "_", strtolower(uniqid())) . '.' . $file_extension;
                    // and move it to the destination folder
                    if (move_uploaded_file($file['tmp_name'], $destination . $fileNewName)) {
//                        $foto_path = $destination . $fileNewName;
                            $foto_path = $destination . $fileNewName;
                            $save_path = $save_dest . $fileNewName;
                            $data      = array(
                                'id' => $id,
                                'client_name' => $client_name,
                                'client_logo' => $foto_path,
                                'datemodified' => $datetime
                        );
                        $flag      = $this->client_model->update_client($data);
                        if ($flag) {
                            //set flashdata
                            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Berhasil!</div>');
                            redirect(base_url('administrator/home/client_list'));
                        } else {
                            //set flashdata
                            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Gagal! Terjadi kesalahan pada database.</div>');
                            redirect(base_url('administrator/home/client_list'));
                        }
                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Gagal! Terjadi kesalahan saat mengunggah.</div>');
                        redirect(base_url('administrator/home/client_list'));
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Gagal! Mohon hanya memilih file tipe gambar.</div>');
                    redirect(base_url('administrator/home/client_list'));
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Mohon Pilih file terlebih dahulu.</div>');
                redirect(base_url('administrator/home/client_list'));
            }
        } else {
             $data      = array(
                                'id' => $id,
                                'client_name' => $client_name,
                                'datemodified' => $datetime
                        );
                        $flag      = $this->client_model->update_client($data);
                        if ($flag) {
                            //set flashdata
                            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Berhasil!</div>');
                            redirect(base_url('administrator/home/client_list'));
                        } else {
                            //set flashdata
                            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Gagal! Terjadi kesalahan pada database.</div>');
                            redirect(base_url('administrator/home/client_list'));
                        }
        }
    }
    
    function client_edit($id = 0)
    {
        auth_redirect_admin();
        $where        = array(
            'id' => $id
        );
        $client_qry = $this->client_model->read_client($where);
        if ($client_qry->num_rows() != 0) {
            $data['title']         = 'Update Client';
            $data['content']       = VIEW_BACK . 'home/client_list_edit';
            $data['member']        = get_current_member_admin();
            $data['client'] = $client_qry->row();
            $this->load->view(VIEW_BACK . 'template', $data);
        } else
            $this->load->view('404');
    }
    
        function delete_client($id)
    {
       $flag = $this->client_model->delete_client($id);
        if ($flag) {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Berhasil!</div>');
            redirect(base_url('administrator/home/client_list'));
        } else {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Gagal!</div>');
            redirect(base_url('administrator/home/client_list'));
        }
    }
    
    
    public function member_list()
    {
        auth_redirect_admin();
        $data['title']   = 'Member List';
        $data['content'] = VIEW_BACK . 'member_list';
        $data['member']  = get_current_member_admin();$data['otoritas']= cek_level($data['member']->id_level);
        $this->load->view(VIEW_BACK . 'template', $data);
    }
    
    public function campaign_list()
    {
        auth_redirect_admin();
        $data['title']   = 'Campaign List';
        $data['content'] = VIEW_BACK . 'campaign_list';
        $data['member']  = get_current_member_admin();$data['otoritas']= cek_level($data['member']->id_level);
        $this->load->view(VIEW_BACK . 'template', $data);
    }
    
    public function create_campaign()
    {
        auth_redirect_admin();
        $data['title']   = 'Buat Campaign Baru';
        $data['content'] = VIEW_BACK . 'create_campaign';
        $data['member']  = get_current_member_admin();$data['otoritas']= cek_level($data['member']->id_level);
        $this->load->view(VIEW_BACK . 'template', $data);
    }
    
    public function donasi_list()
    {
        auth_redirect_admin();
        $data['title']   = 'List Para Pendonasi';
        $data['content'] = VIEW_BACK . 'donasi_list';
        $data['member']  = get_current_member_admin();$data['otoritas']= cek_level($data['member']->id_level);
        $this->load->view(VIEW_BACK . 'template', $data);
    }
    
    public function slideshow()
    {
        auth_redirect_admin();
        $data['title']   = 'Slideshow';
        $data['content'] = VIEW_BACK . 'slideshow';
        $data['member']  = get_current_member_admin();$data['otoritas']= cek_level($data['member']->id_level);
        $this->load->view(VIEW_BACK . 'template', $data);
    }
    
    public function settings()
    {
        auth_redirect_admin();
        $data['title']   = 'Settings';
        $data['content'] = VIEW_BACK . 'settings';
        $data['member']  = get_current_member_admin();$data['otoritas']= cek_level($data['member']->id_level);
        $this->load->view(VIEW_BACK . 'template', $data);
    }
    
    function login()
    {
        if (empty($this->session->userdata('admin_login')))
            $this->load->view(VIEW_BACK . 'login');
        else
            redirect(base_url('administrator/'));
    }
    
    function login_process()
    {
        $email    = $this->input->post('email');
        $password = hash('md5', $this->input->post('password'));
        $where    = array(
            'email'=>$email,
            'password'=>$password
        );
        $flag     = $this->member_model->read_member($where)->num_rows();
        if ($flag) {
            $this->session->set_userdata('admin_login', $email);            
            $this->session->set_userdata('lang_admin', 'en');
            $data = array(
                'info' => 'success',
                'message' => '<div class="alert alert-success"><strong>Sukses!</strong> Sesaat lagi anda akan masuk..</div>'
            );
            die(json_encode($data));
            redirect(base_url(''));
        } else {
            $data = array(
                'info' => 'failed',
                'message' => '<div class="alert alert-danger"><strong>Error!</strong> Login Gagal! Silahkan Cek Email/Password Anda!</div>'
            );
            die(json_encode($data));
        }
    }
    
    function logout()
    {
        $this->session->set_userdata('admin_login', "");
        redirect(base_url('administrator/login'));
    }
    
    function create_campaign_process()
    {
        $name           = $this->input->post('name');
        $goal_days      = $this->input->post('goal_days');
        $goal_nominal   = $this->input->post('goal_nominal');
        $description    = $this->input->post('description');
        $details        = $this->input->post('details');
        $status         = 'open';
        $member         = get_current_member_admin();
        $datetime       = date("Y-m-d H:i:s");
        $image_featured = "";
        $ok_ext         = array(
            'jpg',
            'png',
            'jpeg',
            'bmp'
        ); // allow only these types of files
        $destination    = 'assets/images/campaign/'; // where our files will be stored
        $save_dest      = 'images/campaign/';
        if (!empty($_FILES['file'])) {
            $file           = $_FILES['file'];
            $filename       = explode(".", $file["name"]);
            $file_name      = $file['name']; // file original name
            $file_extension = $filename[count($filename) - 1];
            $file_weight    = $file['size'];
            $file_type      = $file['type'];
            if ($_FILES['file']['tmp_name'] != '') {
                // If there is no error
                if ($file['error'] == 0) {
                    // check if the extension is accepted
                    if (in_array(strtolower($file_extension), $ok_ext)) {
                        // check if the size is not beyond expected size
                        // rename the file
                        $fileNewName = str_replace(" ", "_", strtolower(uniqid())) . '.' . $file_extension;
                        // and move it to the destination folder
                        if (move_uploaded_file($file['tmp_name'], $destination . $fileNewName)) {
                            $foto_path = $destination . $fileNewName;
                            $save_path = $save_dest . $fileNewName;
                            $data      = array(
                                'member_id' => $member->id,
                                'name' => $name,
                                'description' => $description,
                                'details' => $details,
                                'goal_nominal' => $goal_nominal,
                                'goal_days' => $goal_days,
                                'image_feature' => $save_path,
                                'status' => $status,
                                'datecreated' => $datetime,
                                'datemodified' => $datetime
                            );
                            $flag      = $this->campaign_model->create_campaign($data);
                            if ($flag) {
                                //set flashdata
                                $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Campaign Sukses!</div>');
                                redirect(base_url("administrator/create_campaign"));
                            } else {
                                //set flashdata
                                $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Campaign Gagal! Terjadi kesalahan pada database.</div>');
                                redirect(base_url("administrator/create_campaign"));
                            }
                        } else {
                            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Campaign Gagal! Terjadi kesalahan saat mengunggah.</div>');
                            redirect(base_url('administrator/create_campaign'));
                        }
                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Campaign Gagal! Mohon hanya memilih file tipe gambar.</div>');
                        redirect(base_url('administrator/create_campaign'));
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Campaign Gagal! Mohon Pilih file terlebih dahulu.</div>');
                    redirect(base_url('administrator/create_campaign'));
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Campaign Gagal! Mohon Pilih file terlebih dahulu.</div>');
                redirect(base_url('administrator/create_campaign'));
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Pengunggahan Bukti Transfer Gagal! Mohon Pilih file terlebih dahulu.</div>');
            redirect(base_url('administrator/create_campaign'));
        }
    }
    
    function settings_proces()
    {
        $where    = array(
            'editable' => 1
        );
        $settings = $this->settings_model->read_settings($where);
        $data     = array();
        if ($settings->num_rows() != 0) {
            foreach ($settings->result() as $option) {
                $data['name']  = $option->name;
                $data['value'] = $this->input->post($option->name);
                $flag          = $this->settings_model->update_settings($data);
            }
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Campaign Sukses!</div>');
            redirect(base_url("administrator/settings"));
        }
    }
    
    function create_slideshow()
    {
        auth_redirect_admin();
        $data['title']          = 'Buat Slideshow';
        $data['content']        = VIEW_BACK . 'slideshow_create';
        $data['member']         = get_current_member_admin();
        $this->load->view(VIEW_BACK . 'template', $data);
    }
    function slideshow_edit($slideshow_id = 0)
    {
        auth_redirect_admin();
        $where      = array(
            'id' => $slideshow_id
        );
        $member_qry = $this->slideshow_model->read_slideshow($where);
        if ($member_qry->num_rows() != 0) {
            $data['title']          = 'Edit Slideshow';
            $data['content']        = VIEW_BACK . 'slideshow_edit';
            $data['member']         = get_current_member_admin();
            $data['slideshow_edit'] = $member_qry->row();
            $this->load->view(VIEW_BACK . 'template', $data);
        } else
            $this->load->view('404');
    }
    
    
    function slideshow_edit_process()
    {
        $id             = $this->input->post('id');
        $title_one      = $this->input->post('title_one');
        $title_two      = $this->input->post('title_two');
        $description    = $this->input->post('description');
        $member         = get_current_member_admin();
        $datetime       = date("Y-m-d H:i:s");
        $image_featured = "";
        $ok_ext         = array(
            'jpg',
            'png',
            'jpeg',
            'bmp'
        ); // allow only these types of files
        $destination    = 'assets/images/slideshow'; // where our files will be stored
        $save_dest      = 'images/slideshow';
        if (!empty($_FILES['file']['name'])) {
            $file           = $_FILES['file'];
            $filename       = explode(".", $file["name"]);
            $file_name      = $file['name']; // file original name
            $file_extension = $filename[count($filename) - 1];
            $file_weight    = $file['size'];
            $file_type      = $file['type'];
            // If there is no error
            if ($file['error'] == 0) {
                // check if the extension is accepted
                if (in_array(strtolower($file_extension), $ok_ext)) {
                    // check if the size is not beyond expected size
                    // rename the file
                    $fileNewName = str_replace(" ", "_", strtolower(uniqid())) . '.' . $file_extension;
                    // and move it to the destination folder
                    if (move_uploaded_file($file['tmp_name'], $destination . $fileNewName)) {
                        $foto_path = $destination . $fileNewName;
                        $save_path = $save_dest . $fileNewName;
                        $data      = array(
                            'id' => $id,
                            'title_one' => $title_one,
                            'title_two' => $title_two,
                            'description' => $description,
                            'background_picture' => $save_path,
                            'datemodified' => $datetime
                        );
                        $flag      = $this->slideshow_model->update_slideshow($data);
                        if ($flag) {
                            //set flashdata
                            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Edit Slideshow Sukses!</div>');
                            redirect(base_url("administrator/slideshow"));
                        } else {
                            //set flashdata
                            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Edit Slideshow Gagal! Terjadi kesalahan pada database.</div>');
                            redirect(base_url("administrator/slideshow"));
                        }
                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Edit Slideshow Gagal! Terjadi kesalahan saat mengunggah.</div>');
                        redirect(base_url('administrator/slideshow'));
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Edit Slideshow Gagal! Mohon hanya memilih file tipe gambar.</divslideshow');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Mohon pilih file terlebih dahulu.</div>');
                redirect(base_url('administrator/slideshow'));
            }
        } else {
            $data = array(
                'id' => $id,
                'title_one' => $title_one,
                'title_two' => $title_two,
                'description' => $description,
                'datemodified' => $datetime
            );
            $flag = $this->slideshow_model->update_slideshow($data);
            if ($flag) {
                //set flashdata
                $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Edit Slideshow Sukses!</div>');
                redirect(base_url("administrator/slideshow"));
            } else {
                //set flashdata
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Edit Slideshow Gagal! Terjadi kesalahan pada database.</div>');
                redirect(base_url("administrator/slideshow"));
            }
        }
    }
    
    function slideshow_create_process()
    {
        $id             = $this->input->post('id');
        $title_one      = $this->input->post('title_one');
        $title_two      = $this->input->post('title_two');
        $description    = $this->input->post('description');
        $member         = get_current_member_admin();
        $datetime       = date("Y-m-d H:i:s");
        $image_featured = "";
        $ok_ext         = array(
            'jpg',
            'png',
            'jpeg',
            'bmp'
        ); // allow only these types of files
        $destination    = 'assets/images/slideshow'; // where our files will be stored
        $save_dest      = 'images/slideshow';
        if (!empty($_FILES['file']['name'])) {
            $file           = $_FILES['file'];
            $filename       = explode(".", $file["name"]);
            $file_name      = $file['name']; // file original name
            $file_extension = $filename[count($filename) - 1];
            $file_weight    = $file['size'];
            $file_type      = $file['type'];
            // If there is no error
            if ($file['error'] == 0) {
                // check if the extension is accepted
                if (in_array(strtolower($file_extension), $ok_ext)) {
                    // check if the size is not beyond expected size
                    // rename the file
                    $fileNewName = str_replace(" ", "_", strtolower(uniqid())) . '.' . $file_extension;
                    // and move it to the destination folder
                    if (move_uploaded_file($file['tmp_name'], $destination . $fileNewName)) {
                        $foto_path = $destination . $fileNewName;
                        $save_path = $save_dest . $fileNewName;
                        $data      = array(
                            'title_one' => $title_one,
                            'title_two' => $title_two,
                            'description' => $description,
                            'background_picture' => $save_path,
                            'datemodified' => $datetime
                        );
                        $flag      = $this->slideshow_model->create_slideshow($data);
                        if ($flag) {
                            //set flashdata
                            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Edit Slideshow Sukses!</div>');
                            redirect(base_url("administrator/slideshow"));
                        } else {
                            //set flashdata
                            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Edit Slideshow Gagal! Terjadi kesalahan pada database.</div>');
                            redirect(base_url("administrator/slideshow"));
                        }
                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Edit Slideshow Gagal! Terjadi kesalahan saat mengunggah.</div>');
                        redirect(base_url('administrator/slideshow'));
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Edit Slideshow Gagal! Mohon hanya memilih file tipe gambar.</divslideshow');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Mohon pilih file terlebih dahulu.</div>');
                redirect(base_url('administrator/slideshow'));
            }
        } else {
                //set flashdata
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Buat Slideshow Gagal! Terjadi kesalahan pada database.</div>');
                redirect(base_url("administrator/slideshow"));
        }
    }
    
    
    function delete_slideshow($slideshow_id)
    {
        $flag = $this->slideshow_model->delete_slideshow($slideshow_id);
        if ($flag) {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Member Berhasil!</div>');
            redirect(base_url('administrator/slideshow'));
        } else {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Member Gagal!</div>');
            redirect(base_url('administrator/slideshow'));
        }
    }
    
    
        function delete_updates_article($id)
    {
       $flag = $this->updates_article_model->delete_updates_article($id);
        if ($flag) {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Berhasil!</div>');
            redirect(base_url('administrator/news_update/news_manager'));
        } else {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Gagal!</div>');
            redirect(base_url('administrator/news_update/news_manager'));
        }
    }
    
    
    function campaign_edit_process()
    {
        $who_are_we_pic        = "";
        $who_are_we_content    = $this->input->post('who_are_we_content');
        $member         = get_current_member_admin();
        $ok_ext         = array(
            'jpg',
            'png',
            'jpeg',
            'bmp'
        ); // allow only these types of files
        $destination    = 'assets/img/about_us/'; // where our files will be stored
        $save_dest      = '/img/about_us/';
        if (!empty($_FILES['file']['name'])) {
            $file           = $_FILES['file'];
            $filename       = explode(".", $file["name"]);
            $file_name      = $file['name']; // file original name
            $file_extension = $filename[count($filename) - 1];
            $file_weight    = $file['size'];
            $file_type      = $file['type'];
            // If there is no error
            if ($file['error'] == 0) {
                // check if the extension is accepted
                if (in_array(strtolower($file_extension), $ok_ext)) {
                    // check if the size is not beyond expected size
                    // rename the file
                    $fileNewName = str_replace(" ", "_", strtolower(uniqid())) . '.' . $file_extension;
                    // and move it to the destination folder
                    if (move_uploaded_file($file['tmp_name'], $destination . $fileNewName)) {
//                        $foto_path = $destination . $fileNewName;
                        $save_path = $destination . $fileNewName;
                        $data      = array(
                            'id'=>1,
                            'who_are_we_pic' => $save_path,
                            'who_are_we_content' => $who_are_we_content
                            
                        );
                        $flag      = $this->about_us_model->update_about_us($data);
                        if ($flag) {
                            //set flashdata
                            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Berhasil!</div>');
                            redirect(base_url("administrator/about_us/page_settings"));
                        } else {
                            //set flashdata
                            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Gagal! Terjadi kesalahan pada database.</div>');
                            redirect(base_url("administrator/about_us/page_settings"));
                        }
                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Gagal! Terjadi kesalahan saat mengunggah.</div>');
                        redirect(base_url('administrator/about_us/page_settings'));
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Gagal! Mohon hanya memilih file tipe gambar.</div>');
                    redirect(base_url('administrator/about_us/page_settings'));
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Mohon Pilih file terlebih dahulu.</div>');
                redirect(base_url('administrator/about_us/page_settings'));
            }
        } else {
             $data      = array(
                            'id'=>1,
                            'who_are_we_content' => $who_are_we_content
                            
                        );
                        $flag      = $this->about_us_model->update_about_us($data);
                        if ($flag) {
                            //set flashdata
                            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Berhasil!</div>');
                            redirect(base_url("administrator/about_us/page_settings"));
                        } else {
                            //set flashdata
                            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Gagal! Terjadi kesalahan pada database.</div>');
                            redirect(base_url("administrator/about_us/page_settings"));
                        }
        }
    }
    
         function news_manager_create()
    {
        $title          = $this->input->post('title');
        $content        = $this->input->post('content');
        $member         = get_current_member_admin();
        $datetime       = date("Y-m-d H:i:s");
        $image_feature  = "";
        $ok_ext         = array(
            'jpg',
            'png',
            'jpeg',
            'bmp'
        ); // allow only these types of files
        $destination    = 'assets/img/icon/img/Updates/'; // where our files will be stored
        $save_dest      = 'img/news_updates/';
        if (!empty($_FILES['file'])) {
            $file           = $_FILES['file'];
            $filename       = explode(".", $file["name"]);
            $file_name      = $file['name']; // file original name
            $file_extension = $filename[count($filename) - 1];
            $file_weight    = $file['size'];
            $file_type      = $file['type'];
            if ($_FILES['file']['tmp_name'] != '') {
                // If there is no error
                if ($file['error'] == 0) {
                    // check if the extension is accepted
                    if (in_array(strtolower($file_extension), $ok_ext)) {
                        // check if the size is not beyond expected size
                        // rename the file
                        $fileNewName = str_replace(" ", "_", strtolower(uniqid())) . '.' . $file_extension;
                        // and move it to the destination folder
                        if (move_uploaded_file($file['tmp_name'], $destination . $fileNewName)) {
                            $foto_path = $destination . $fileNewName;
                            $save_path = $save_dest . $fileNewName;
                            $data      = array(
                                
                                'title' => $title,
                                'seo' => seo($title),
                                'image_feature' => $foto_path,
                                'content' => $content,
                                'lang' => $this->session->userdata('lang_admin'),
                                'datecreated' => $datetime,
                                'datemodified' => $datetime
                            );
                            $flag      = $this->updates_article_model->create_updates_article($data);
                            if ($flag) {
                                //set flashdata
                                $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan News Berhasil!</div>');
                                redirect(base_url("administrator/news_update/news_manager"));
                            } else {
                                //set flashdata
                                $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan News Gagal! Terjadi kesalahan pada database.</div>');
                                redirect(base_url("administrator/news_update/news_manager"));
                            }
                        } else {
                            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan News Gagal! Terjadi kesalahan saat mengunggah.</div>');
                            redirect(base_url('administrator/news_update/news_manager'));
                        }
                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan News Gagal! Mohon hanya memilih file tipe gambar.</div>');
                        redirect(base_url('administrator/news_update/news_manager'));
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan News Gagal! Mohon Pilih file terlebih dahulu.</div>');
                    redirect(base_url('administrator/news_update/news_manager'));
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan News Gagal! Mohon Pilih file terlebih dahulu.</div>');
                redirect(base_url('administrator/news_update/news_manager'));
            }
        } 
    }
    
       function news_edit_process()
    {
        $id             = $this->input->post('id');
        $title          = $this->input->post('title');
        $content        = $this->input->post('content');
        $member         = get_current_member_admin();
        $datetime       = date("Y-m-d H:i:s");
        $image_feature  = "";
        $ok_ext         = array(
            'jpg',
            'png',
            'jpeg',
            'bmp'
        ); // allow only these types of files
        $destination    = 'assets/img/icon/img/Updates/'; // where our files will be stored
        $save_dest      = 'img/icon/img/Updates/';
        if (!empty($_FILES['file']['name'])) {
            $file           = $_FILES['file'];
            $filename       = explode(".", $file["name"]);
            $file_name      = $file['name']; // file original name
            $file_extension = $filename[count($filename) - 1];
            $file_weight    = $file['size'];
            $file_type      = $file['type'];
            // If there is no error
            if ($file['error'] == 0) {
                // check if the extension is accepted
                if (in_array(strtolower($file_extension), $ok_ext)) {
                    // check if the size is not beyond expected size
                    // rename the file
                    $fileNewName = str_replace(" ", "_", strtolower(uniqid())) . '.' . $file_extension;
                    // and move it to the destination folder
                    if (move_uploaded_file($file['tmp_name'], $destination . $fileNewName)) {
//                        $foto_path = $destination . $fileNewName;
                        $save_path = $destination . $fileNewName;
                        $data      = array(
                                'id' => $id,
                                'title' => $title,
                                'seo' => seo($title),
                                'image_feature' => $save_path,
                                'content' => $content,
                                'datemodified' => $datetime
                            
                        );
                        $flag      = $this->updates_article_model->update_updates_article($data);
                        if ($flag) {
                            //set flashdata
                            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Berhasil!</div>');
                            redirect(base_url('administrator/news_update/news_manager'));
                        } else {
                            //set flashdata
                            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Gagal! Terjadi kesalahan pada database.</div>');
                            redirect(base_url('administrator/news_update/news_manager'));
                        }
                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Gagal! Terjadi kesalahan saat mengunggah.</div>');
                        redirect(base_url('administrator/news_update/news_manager'));
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Gagal! Mohon hanya memilih file tipe gambar.</div>');
                    redirect(base_url('administrator/news_update/news_manager'));
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Mohon Pilih file terlebih dahulu.</div>');
                redirect(base_url('administrator/news_update/news_manager'));
            }
        } else {
             $data      = array(
                                'id' => $id,
                                'title' => $title,
                                'seo' => seo($title),
                                'content' => $content
                        );
                        $flag      = $this->updates_article_model->update_updates_article($data);
                        if ($flag) {
                            //set flashdata
                            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Berhasil!</div>');
                            redirect(base_url('administrator/news_update/news_manager'));
                        } else {
                            //set flashdata
                            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Gagal! Terjadi kesalahan pada database.</div>');
                            redirect(base_url('administrator/news_update/news_manager'));
                        }
        }
    }
    
    function set_themes($themes_id){
        $settings_name = 'themes';
        if($themes_id!=""){
            if($themes_id!="default"){
                $where = array(
                    'id'=>$themes_id
                );
                $themes_qry = $this->themes_model->read_themes($where);
                if($themes_qry->num_rows()!=0){
                    $data_update = array(
                    'name'=>$settings_name,
                    'value'=>$themes_qry->row()->themes_name
                );
                $this->settings_model->update_settings($data_update);
                //set flashdata
                $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Set Tema Berhasil!</div>');
                redirect(base_url('administrator/themes'));
                } else {
                     //set flashdata
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Set Tema Gagal!</div>');
                redirect(base_url('administrator/themes'));
                }
            } else {
                $data_update = array(
                    'name'=>$settings_name,
                    'value'=>'default'
                );
                $this->settings_model->update_settings($data_update);
                //set flashdata
                $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Set Tema Berhasil!</div>');
                redirect(base_url('administrator/themes'));
            }
        }
    }
    
    function contact_edit_process(){
        //address
        $data_update = array(
            'name'=>'company_address',
            'value'=>$this->input->post('company_address')
        );
        $this->settings_model->update_settings($data_update);
        //phone
        $data_update = array(
            'name'=>'company_phone',
            'value'=>$this->input->post('company_phone')
        );
        $this->settings_model->update_settings($data_update);
        //email
        $data_update = array(
            'name'=>'company_email',
            'value'=>$this->input->post('company_email')
        );
        $this->settings_model->update_settings($data_update);
        //latitude
        $data_update = array(
            'name'=>'latitude',
            'value'=>$this->input->post('latitude')
        );
        $this->settings_model->update_settings($data_update);
        //longitude
        $data_update = array(
            'name'=>'longitude',
            'value'=>$this->input->post('longitude')
        );
        $this->settings_model->update_settings($data_update);
        //set flashdata
        $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Perubahan berhasil disimpan!</div>');
        redirect(base_url('administrator/settings/contacts'));
    }
    
    function pages_manager()
    {
        auth_redirect_admin();
        $data['title']       = 'Pages Manager';
        $data['content']     = VIEW_BACK . 'pages/pages_manager';
        $data['member']  = get_current_member_admin();$data['otoritas']= cek_level($data['member']->id_level);
        $this->load->view(VIEW_BACK . 'template', $data);
    }
    
    function page_manager_create(){
        auth_redirect_admin();
        $title          = $this->input->post('title');
        $content        = $this->input->post('content');
        $seo            = $this->input->post('seo');
        $datetime       = date("Y-m-d H:i:s");
        $file           = upload_file("file", "images", 'assets/img/pages/');
        //pengecekan error
        if ($file== 'empty') {
             $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Page Gagal! Mohon Pilih file terlebih dahulu.</div>');
            redirect(base_url('administrator/pages/pages_manager'));
        }
        if ($file== 'error') {
             $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Page Gagal! File Corrupt.</div>');
            redirect(base_url('administrator/pages/pages_manager'));
        }
        if ($file== 'error_extension') {
              $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Page Gagal! Mohon hanya memilih file tipe gambar.</div>');
            redirect(base_url('administrator/pages/pages_manager'));
        }
        if ($file== 'error_upload') {
             $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Page Gagal! Terjadi kesalahan saat mengunggah.</div>');
            redirect(base_url('administrator/pages/pages_manager'));
        }
        
        $data      = array(
                        'title' => $title,
                        'content' => $content,
                        'seo' => seo($seo),
                        'lang' => $this->session->userdata('lang_admin'),
                        'picture' => $file,
                        'datecreated' => $datetime,
                        'datemodified' => $datetime
                    );
                    $flag      = $this->pages_model->create_pages($data);
                    if ($flag) {
                        //set flashdata
                        $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Sukses!</div>');
                        redirect(base_url("administrator/pages/pages_manager"));
                    } else {
                        //set flashdata
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Terjadi kesalahan pada database.</div>');
                        redirect(base_url("administrator/pages/pages_manager"));
                    }
    }
    function delete_pages($id)
    {
       $flag = $this->pages_model->delete_pages($id);
        if ($flag) {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Page Berhasil!</div>');
            redirect(base_url('administrator/pages/pages_manager'));
        } else {
            //set flashdata
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Page Gagal!</div>');
            redirect(base_url('administrator/pages/pages_manager'));
        }
    }
    function updates_pages_edit($id = 0)
    {
        auth_redirect_admin();
        $where        = array(
            'id' => $id
        );
        $pages_qry = $this->pages_model->read_pages($where);
        if ($pages_qry->num_rows() != 0) {
            $data['title']         = 'Update Pages';
            $data['content']       = VIEW_BACK . 'pages/pages_edit';
            $data['member']  = get_current_member_admin();$data['otoritas']= cek_level($data['member']->id_level);
            $data['pages']         = $pages_qry->row();
            $this->load->view(VIEW_BACK . 'template', $data);
        } else
            $this->load->view('404');
    }
    function pages_edit_process(){
        auth_redirect_admin();
        $id             = $this->input->post('id');
        $title          = $this->input->post('title');
        $content        = $this->input->post('content');
        $seo            = $this->input->post('seo');
        $datetime       = date("Y-m-d H:i:s");
        $file           = upload_file("file", "images", 'assets/img/pages/');
        $data      = array(
                        'id'=>$id,
                        'title' => $title,
                        'content' => $content,
                        'seo' => seo($seo),
                        'datemodified' => $datetime
                    );
        if($file!="empty") $data['picture']=$file;
                    $flag      = $this->pages_model->update_pages($data);
                    if ($flag) {
                        //set flashdata
                        $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Data Sukses!</div>');
                        redirect(base_url("administrator/pages/pages_manager"));
                    } else {
                        //set flashdata
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Update Data Gagal! Terjadi kesalahan pada database.</div>');
                        redirect(base_url("administrator/pages/pages_manager"));
                    }
    }
    
        function member_manager()
    {
        auth_redirect_admin();
        $data['title']       = 'Member Manager';
        $data['content']     = VIEW_BACK . 'member/member_manager';
        $data['member']  = get_current_member_admin();$data['otoritas']= cek_level($data['member']->id_level);
        $this->load->view(VIEW_BACK . 'template', $data);
    }
    
         function create_member_manager()
    {
        $name          = $this->input->post('name');
        $email          = $this->input->post('email');
        $password       = md5($this->input->post('password'));
        $no_telepon     = $this->input->post('no_telepon');
        $id_level       = $this->input->post('id_level');
        $member         = get_current_member_admin();
        $datetime       = date("Y-m-d H:i:s");
                
        $data      = array(
                        'name' => $name,
                        'email' => $email,
                        'password' => $password,
                        'no_telepon' => $no_telepon,
                        'id_level' => $id_level,
                        'datecreated' => $datetime,
                        'datemodified' => $datetime
                    );
                    $flag      = $this->member_model->create_member($data);
                    if ($flag) {
                        //set flashdata
                        $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Sukses!</div>');
                        redirect(base_url("administrator/member/member_manager"));
                    } else {
                        //set flashdata
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Terjadi kesalahan pada database.</div>');
                        redirect(base_url("administrator/member/member_manager"));
                    }
            }
    
            function delete_member($id)
        {
           $flag = $this->member_model->delete_member($id);
            if ($flag) {
                //set flashdata
                $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Berhasil!</div>');
                redirect(base_url('administrator/member/member_manager'));
            } else {
                //set flashdata
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Gagal!</div>');
                redirect(base_url('administrator/member/member_manager'));
            }
        }
    

    function member_edit($id = 0)
    {
        auth_redirect_admin();
        $where      = array(
            'id' => $id
        );
        $member_edit_qry = $this->member_model->read_member($where);
        if ($member_edit_qry->num_rows() != 0) {
            $data['title']       = 'Edit Member';
            $data['content']     = VIEW_BACK . 'member/member_manager_edit';
            $data['member']  = get_current_member_admin();$data['otoritas']= cek_level($data['member']->id_level);
            $data['member_edit'] = $member_edit_qry->row();
            $this->load->view(VIEW_BACK . 'template', $data);
        } else
            $this->load->view('404');
    }
    
    
    
    function member_edit_process()
      {
        $id             = $this->input->post('id');
        $name           = $this->input->post('name');
        $email          = $this->input->post('email');
        $password       = md5($this->input->post('password'));
        $no_telepon     = $this->input->post('no_telepon');
        $id_level       = $this->input->post('id_level');
        $member         = get_current_member_admin();
        $datetime       = date("Y-m-d H:i:s");
                
        $data      = array(
                        'id' => $id,
                        'name' => $name,
                        'email' => $email,
                        'password' => $password,
                        'no_telepon' => $no_telepon,
                        'id_level' => $id_level,
                        'datemodified' => $datetime
                    );
                    $flag      = $this->member_model->update_member($data);
                    if ($flag) {
                        //set flashdata
                        $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Sukses!</div>');
                        redirect(base_url("administrator/member/member_manager"));
                    } else {
                        //set flashdata
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Penambahan Data Gagal! Terjadi kesalahan pada database.</div>');
                        redirect(base_url("administrator/member/member_manager"));
                    }
            }
    
    function level_manager()
        {
            auth_redirect_admin();
            $data['title']       = 'Level Manager';
            $data['content']     = VIEW_BACK . 'member/level_manager';
            $data['member']      = get_current_member_admin();
            $data['otoritas']= cek_level($data['member']->id_level);
            $this->load->view(VIEW_BACK . 'template', $data);
        }
    
         function create_level_manager()
    {
        $nama_level             = $this->input->post('nama_level');
        $otoritas_home          ="";
         $otoritas_home         ="";
         $otoritas_products     ="";
         $otoritas_news_updates ="";
         $otoritas_pages        ="";
         $otoritas_settings     ="";
         $otoritas_themes       = "";
         $otoritas_about_us     = "";
         $otoritas_member       = "";
        $otoritas_home.= (!empty($this->input->post('home_c'))?$this->input->post('home_c'):'');
        $otoritas_home.= (!empty($this->input->post('home_r'))?",".$this->input->post('home_r'):'');
        $otoritas_home.= (!empty($this->input->post('home_u'))?",".$this->input->post('home_u'):'');
        $otoritas_home.= (!empty($this->input->post('home_d'))?",".$this->input->post('home_d'):'');
        $otoritas_products.= (!empty($this->input->post('products_c'))?$this->input->post('products_c'):'');
        $otoritas_products.= (!empty($this->input->post('products_r'))?",".$this->input->post('products_r'):'');
        $otoritas_products.= (!empty($this->input->post('products_u'))?",".$this->input->post('products_u'):'');
        $otoritas_products.= (!empty($this->input->post('products_d'))?",".$this->input->post('products_d'):'');
        $otoritas_news_updates.= (!empty($this->input->post('news_updates_c'))?$this->input->post('news_updates_c'):'');
        $otoritas_news_updates.= (!empty($this->input->post('news_updates_r'))?",".$this->input->post('news_updates_r'):'');
        $otoritas_news_updates.= (!empty($this->input->post('news_updates_u'))?",".$this->input->post('news_updates_u'):'');
        $otoritas_news_updates.= (!empty($this->input->post('news_updates_d'))?",".$this->input->post('news_updates_d'):'');
        $otoritas_pages.= (!empty($this->input->post('pages_c'))?$this->input->post('pages_c'):'');
        $otoritas_pages.= (!empty($this->input->post('pages_r'))?",".$this->input->post('pages_r'):'');
        $otoritas_pages.= (!empty($this->input->post('pages_u'))?",".$this->input->post('pages_u'):'');
        $otoritas_pages.= (!empty($this->input->post('pages_d'))?",".$this->input->post('pages_d'):'');
        $otoritas_settings.= (!empty($this->input->post('settings_c'))?$this->input->post('settings_c'):'');
        $otoritas_settings.= (!empty($this->input->post('settings_r'))?",".$this->input->post('settings_r'):'');
        $otoritas_settings.= (!empty($this->input->post('settings_u'))?",".$this->input->post('settings_u'):'');
        $otoritas_settings.= (!empty($this->input->post('settings_d'))?",".$this->input->post('settings_d'):'');
        $otoritas_themes.= (!empty($this->input->post('themes_c'))?$this->input->post('themes_c'):'');
        $otoritas_themes.= (!empty($this->input->post('themes_r'))?",".$this->input->post('themes_r'):'');
        $otoritas_themes.= (!empty($this->input->post('themes_u'))?",".$this->input->post('themes_u'):'');
        $otoritas_themes.= (!empty($this->input->post('themes_d'))?",".$this->input->post('themes_d'):'');
        $otoritas_about_us.= (!empty($this->input->post('about_us_c'))?$this->input->post('about_us_c'):'');
        $otoritas_about_us.= (!empty($this->input->post('about_us_r'))?",".$this->input->post('about_us_r'):'');
        $otoritas_about_us.= (!empty($this->input->post('about_us_u'))?",".$this->input->post('about_us_u'):'');
        $otoritas_about_us.= (!empty($this->input->post('about_us_d'))?",".$this->input->post('about_us_d'):'');
        $otoritas_member  .= (!empty($this->input->post('member_c'))?$this->input->post('member_c'):'');
        $otoritas_member  .= (!empty($this->input->post('member_r'))?",".$this->input->post('member_r'):'');
        $otoritas_member  .= (!empty($this->input->post('member_u'))?",".$this->input->post('member_u'):'');
        $otoritas_member  .= (!empty($this->input->post('member_d'))?",".$this->input->post('member_d'):'');
        $otoritas_subscribe  .= (!empty($this->input->post('subscribe_c'))?$this->input->post('subscribe_c'):'');
        $otoritas_subscribe  .= (!empty($this->input->post('subscribe_r'))?",".$this->input->post('subscribe_r'):'');
        $otoritas_subscribe  .= (!empty($this->input->post('subscribe_u'))?",".$this->input->post('subscribe_u'):'');
        $otoritas_subscribe  .= (!empty($this->input->post('subscribe_d'))?",".$this->input->post('subscribe_d'):'');
        $member                 = get_current_member_admin();
        $datetime               = date("Y-m-d H:i:s");
                
        $data           = array(
                        'nama_level'            => $nama_level,
                        'otoritas_home'         => $otoritas_home,
                        'otoritas_products'     => $otoritas_products,
                        'otoritas_news_updates' => $otoritas_news_updates,
                        'otoritas_pages'        => $otoritas_pages,
                        'otoritas_settings'     => $otoritas_settings,
                        'otoritas_themes'       => $otoritas_themes,
                        'otoritas_about_us'     => $otoritas_about_us,
                        'otoritas_member'       => $otoritas_member,
                        'otoritas_subscribe'    => $otoritas_subscribe,
                        'datecreated'           => $datetime,
                        'datemodified'          => $datetime
                        
                    );
                    $flag      = $this->level_model->create_level($data);
                    if ($flag) {
                        //set flashdata
                        $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Berhasil!</div>');
                        redirect(base_url('administrator/member/level_manager'));
                    } else {
                        //set flashdata
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Gagal!</div>');
                        redirect(base_url('administrator/member/level_manager'));
                    }
            }
    
    function level_edit_process()
    {
        $id_level               = $this->input->post('id_level');
        $nama_level             = $this->input->post('nama_level');
        $otoritas_home          ="";
        $otoritas_home          ="";
        $otoritas_products      ="";
        $otoritas_news_updates  ="";
        $otoritas_pages         ="";
        $otoritas_settings      ="";
        $otoritas_themes        = "";
        $otoritas_about_us      = "";
        $otoritas_member        = "";
        $otoritas_subscribe     = "";
        $otoritas_home.= (!empty($this->input->post('home_c'))?$this->input->post('home_c'):'');
        $otoritas_home.= (!empty($this->input->post('home_r'))?",".$this->input->post('home_r'):'');
        $otoritas_home.= (!empty($this->input->post('home_u'))?",".$this->input->post('home_u'):'');
        $otoritas_home.= (!empty($this->input->post('home_d'))?",".$this->input->post('home_d'):'');
        $otoritas_products.= (!empty($this->input->post('products_c'))?$this->input->post('products_c'):'');
        $otoritas_products.= (!empty($this->input->post('products_r'))?",".$this->input->post('products_r'):'');
        $otoritas_products.= (!empty($this->input->post('products_u'))?",".$this->input->post('products_u'):'');
        $otoritas_products.= (!empty($this->input->post('products_d'))?",".$this->input->post('products_d'):'');
        $otoritas_news_updates.= (!empty($this->input->post('news_updates_c'))?$this->input->post('news_updates_c'):'');
        $otoritas_news_updates.= (!empty($this->input->post('news_updates_r'))?",".$this->input->post('news_updates_r'):'');
        $otoritas_news_updates.= (!empty($this->input->post('news_updates_u'))?",".$this->input->post('news_updates_u'):'');
        $otoritas_news_updates.= (!empty($this->input->post('news_updates_d'))?",".$this->input->post('news_updates_d'):'');
        $otoritas_pages.= (!empty($this->input->post('pages_c'))?$this->input->post('pages_c'):'');
        $otoritas_pages.= (!empty($this->input->post('pages_r'))?",".$this->input->post('pages_r'):'');
        $otoritas_pages.= (!empty($this->input->post('pages_u'))?",".$this->input->post('pages_u'):'');
        $otoritas_pages.= (!empty($this->input->post('pages_d'))?",".$this->input->post('pages_d'):'');
        $otoritas_settings.= (!empty($this->input->post('settings_c'))?$this->input->post('settings_c'):'');
        $otoritas_settings.= (!empty($this->input->post('settings_r'))?",".$this->input->post('settings_r'):'');
        $otoritas_settings.= (!empty($this->input->post('settings_u'))?",".$this->input->post('settings_u'):'');
        $otoritas_settings.= (!empty($this->input->post('settings_d'))?",".$this->input->post('settings_d'):'');
        $otoritas_themes.= (!empty($this->input->post('themes_c'))?$this->input->post('themes_c'):'');
        $otoritas_themes.= (!empty($this->input->post('themes_r'))?",".$this->input->post('themes_r'):'');
        $otoritas_themes.= (!empty($this->input->post('themes_u'))?",".$this->input->post('themes_u'):'');
        $otoritas_themes.= (!empty($this->input->post('themes_d'))?",".$this->input->post('themes_d'):'');
        $otoritas_about_us.= (!empty($this->input->post('about_us_c'))?$this->input->post('about_us_c'):'');
        $otoritas_about_us.= (!empty($this->input->post('about_us_r'))?",".$this->input->post('about_us_r'):'');
        $otoritas_about_us.= (!empty($this->input->post('about_us_u'))?",".$this->input->post('about_us_u'):'');
        $otoritas_about_us.= (!empty($this->input->post('about_us_d'))?",".$this->input->post('about_us_d'):'');
        $otoritas_member  .= (!empty($this->input->post('member_c'))?$this->input->post('member_c'):'');
        $otoritas_member  .= (!empty($this->input->post('member_r'))?",".$this->input->post('member_r'):'');
        $otoritas_member  .= (!empty($this->input->post('member_u'))?",".$this->input->post('member_u'):'');
        $otoritas_member  .= (!empty($this->input->post('member_d'))?",".$this->input->post('member_d'):'');
        $otoritas_subscribe  .= (!empty($this->input->post('subscribe_c'))?$this->input->post('subscribe_c'):'');
        $otoritas_subscribe  .= (!empty($this->input->post('subscribe_r'))?",".$this->input->post('subscribe_r'):'');
        $otoritas_subscribe  .= (!empty($this->input->post('subscribe_u'))?",".$this->input->post('subscribe_u'):'');
        $otoritas_subscribe  .= (!empty($this->input->post('subscribe_d'))?",".$this->input->post('subscribe_d'):'');
        $member                 = get_current_member_admin();
        $datetime               = date("Y-m-d H:i:s");
                
        $data           = array(
                        'id_level'              => $id_level, 
                        'nama_level'            => $nama_level,
                        'otoritas_home'         => $otoritas_home,
                        'otoritas_products'     => $otoritas_products,
                        'otoritas_news_updates' => $otoritas_news_updates,
                        'otoritas_pages'        => $otoritas_pages,
                        'otoritas_settings'     => $otoritas_settings,
                        'otoritas_themes'       => $otoritas_themes,
                        'otoritas_about_us'     => $otoritas_about_us,
                        'otoritas_member'       => $otoritas_member,
                        'otoritas_subscribe'    => $otoritas_subscribe,
                        'datemodified'          => $datetime
                        
                    );
                    $flag      = $this->level_model->update_level($data);
                    if ($flag) {
                        //set flashdata
                        $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Edit Data Berhasil!</div>');
                        redirect(base_url("administrator/member/level_manager"));
                    } else {
                        //set flashdata
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Edit Data Berhasil!</div>');
                        redirect(base_url("administrator/member/level_manager"));
                    }
    }
    
        function delete_level($id_level)
        {
           $flag = $this->level_model->delete_level($id_level);
            if ($flag) {
                //set flashdata
                $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Berhasil!</div>');
                redirect(base_url('administrator/member/level_manager'));
            } else {
                //set flashdata
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Gagal!</div>');
                redirect(base_url('administrator/member/level_manager'));
            }
        }
    
    function level_edit($id_level = 0)
    {
        auth_redirect_admin();
        $where      = array(
            'id_level' => $id_level
        );
        $level_edit_qry = $this->level_model->read_level($where);
        if ($level_edit_qry->num_rows() != 0) {
            $data['title']       = 'Edit Member';
            $data['content']     = VIEW_BACK . 'member/level_manager_edit';
            $data['member']        = get_current_member_admin();$data['otoritas']= cek_level($data['member']->id_level);
            $data['level']       = $level_edit_qry->row();
            $this->load->view(VIEW_BACK . 'template', $data);
        } else
            $this->load->view('404');
    }
    function subscriber_list()
    {
        auth_redirect_admin();
        $data['title']       = 'Subscriber List';
        $data['content']     = VIEW_BACK . 'subscriber_list';
        $data['member']  = get_current_member_admin();$data['otoritas']= cek_level($data['member']->id_level);
        $this->load->view(VIEW_BACK . 'template', $data);
    }
    
    function subscriber_edit($id = 0)
    {
        auth_redirect_admin();
        $where      = array(
            'id' => $id
        );
        $subscribe_data = $this->subscribe_model->read_subscribe($where);
        if ($subscribe_data->num_rows() != 0) {
            $data['title']       = 'Edit Subscribe';
            $data['content']     = VIEW_BACK . 'subscribe_edit';
           $data['member']  = get_current_member_admin();$data['otoritas']= cek_level($data['member']->id_level);
            $data['subscribe']   = $subscribe_data->row();
            $this->load->view(VIEW_BACK . 'template', $data);
        } else
            $this->load->view('404');
    }
    
    function create_subscribe(){
        $email          = $this->input->post('email');
        $datetime       = date("Y-m-d H:i:s");
        $where      = array(
            'email' => $email
        );
        $subscribe_data = $this->subscribe_model->read_subscribe($where);
        if($subscribe_data->num_rows()==0){
        $data      = array(
                        'email' => $email,
                        'datecreated' => $datetime,
                        'datemodified' => $datetime
                    );
                    $flag      = $this->subscribe_model->create_subscribe($data);
                    if ($flag) {
                        //set json
                        $data_json = array(
                            'info'=>'success',
                            'message'=>'<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Subscribe berhasil.</div>'
                        );
                        die(json_encode($data_json));
                    } else {
                        //set json
                        $data_json = array(
                            'info'=>'error',
                            'message'=>'<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Subscribe gagal.</div>'
                        );
                        die(json_encode($data_json));
                    }
        } else {
             //set json
                        $data_json = array(
                            'info'=>'error',
                            'message'=>'<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Subscribe gagal, Email sudah terdaftar.</div>'
                        );
                        die(json_encode($data_json));
        }
    }
    
    function subscribe_edit_process(){
        $id             = $this->input->post('id');
        $email          = $this->input->post('email');
        $datetime       = date("Y-m-d H:i:s");
        $data      = array(
                        'id' => $id,
                        'email' => $email,
                        'datecreated' => $datetime,
                        'datemodified' => $datetime
                    );
                    $flag      = $this->subscribe_model->update_subscribe($data);
                    //set flashdata
                    $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Edit Subscribe berhasil.</div>');
                    redirect(base_url('administrator/subscriber_list'));
    }
    
    function delete_subscriber($id)
        {
           $flag = $this->subscribe_model->delete_subscribe($id);
            if ($flag) {
                //set flashdata
                $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Berhasil!</div>');
                redirect(base_url('administrator/subscriber_list'));
            } else {
                //set flashdata
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Hapus Data Gagal!</div>');
                redirect(base_url('administrator/subscriber_list'));
            }
        }
    
    function upload_file_summernote()
        {
         $file = upload_file("file", "images", 'assets/img/pages/');
        if($file!="empty"){
            echo base_url($file);
        } else
            {
              echo  $message = 'Ooops!  Your upload triggered the following error:  '.$file;
            }
        
        }
    
}

?>