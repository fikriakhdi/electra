<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frontend extends CI_Controller {

  function __construct(){
    parent::__construct();
    date_default_timezone_set("Asia/Jakarta");
    $lang = $this->session->userdata('site_lang');
      if(empty($lang)) $this->session->set_userdata('site_lang', 'english');
      $siteLang = $lang;

       if ($siteLang) {

           $this->lang->load('information',$siteLang);

       } else {

           $this->lang->load('information','english');

       }
  }

	public function index()
	{
        $where                          = array('lang'=>lang_convert($this->session->userdata('site_lang')));
        $about                          = $this->about_us_model->read_about_us($where)->row();
        $themes                         = get_settings('themes')."/";
		$data['title']					= COMPANY_NAME.' | '.'Home';
		$data['content']				= $themes.'home';
		$data['member'] 				= get_current_member();
        $data['about']                  = $about;
        $data['themes']                 = get_settings('themes');
		$this->load->view($themes.'template', $data);
	}

   public function about_us()
 	{
        $where                          = array('lang'=>lang_convert($this->session->userdata('site_lang')));
        $about                          = $this->about_us_model->read_about_us($where)->row();
        $themes                         = get_settings('themes')."/";
 		$data['title']					= COMPANY_NAME.' | '.'About Us';
 		$data['content']				= $themes.'about_us';
        $data['about']                  = $about;
        $data['themes']                 = get_settings('themes');
 		$this->load->view($themes.'template', $data);
 	}

     public function aboutusv2()
 	{
        $where                          = array('lang'=>lang_convert($this->session->userdata('site_lang')));
        $about                          = $this->about_us_model->read_about_us($where)->row();
        $themes                         = get_settings('themes')."/";
 		$data['title']					= COMPANY_NAME.' | '.'About Us';
 		$data['content']				= $themes.'aboutusv2';
        $data['about']                  = $about;
 		$this->load->view($themes.'template', $data);
 	}

    public function products($id_products)
 	{
        $themes                         = get_settings('themes')."/";
        $language                       = array('lang'=>lang_convert($this->session->userdata('site_lang')));
        $where                          = array('id'=>$id_products);
        $prod                           = $this->product_model->read_product($where,$language);
        if($prod->num_rows()!=0){
            $products                       = $prod->row();
            $data['title']					= COMPANY_NAME.' | '.$products->name;
            $data['content']				= $themes.'products';
            $data['products']               = $products;
            $data['themes']                 = get_settings('themes');
            $this->load->view($themes.'template', $data);
        } else redirect('404');

 	}
    public function news($page=0)
 	{
        $where                          = array('lang'=>lang_convert($this->session->userdata('site_lang')));
        $themes                         = get_settings('themes')."/";
        $config['total_rows'] = $this->updates_article_model->read_updates_article($where)->num_rows(); //total row
        $config['per_page'] = 9;  //show record per halaman
        $config["uri_segment"] = 3;  // uri parameter
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
        // Membuat Style pagination untuk BootStrap v4
        $config['base_url'] =   BASE_URL.'news';
        $config["full_tag_open"] = '<ul class="pagination">';
        $config["full_tag_close"] = '</ul>';
        $config["first_link"] = "&laquo;";
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config["first_tag_close"] = "</li>";
        $config["last_link"] = "&raquo;";
        $config["last_tag_open"] = "<li>";
        $config["last_tag_close"] = "</li>";
        $config['next_link'] = '&gt;';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '<li>';
        $config['prev_link'] = '&lt;';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '<li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $page = ($page!='')? $page : 0;
        $config["cur_page"] = $page;
        $this->pagination->initialize($config);
        $data['page'] = (!empty($page) ? $page:0);
        $limit['limit'] =  $config["per_page"];
        $limit['start'] = $data['page'];
        //panggil function get_mahasiswa_list yang ada pada mmodel mahasiswa_model.
        $data['news']                   = $this->updates_article_model->read_updates_article($where,$limit);
        $data['pagination']             = $this->pagination->create_links();
 		$data['title']					= COMPANY_NAME.' | '.'News & Updates';
 		$data['content']				= $themes.'news';
        $data['themes']                 = get_settings('themes');
 		$this->load->view($themes.'template', $data);
 	}
    public function detail_news($seo)
 	{
        $themes                         = get_settings('themes')."/";
        $where                          = array('seo'=>$seo);
        $news                           = $this->updates_article_model->read_updates_article($where);
        if($news->num_rows()!=0){
            $detail_news                    = $news->row();
            $data['title']					= COMPANY_NAME.' | '.$detail_news->title;
            $data['content']				= $themes.'detail_news';
            $data['detail_news']            = $detail_news;
            $data['themes']                 = get_settings('themes');
            $this->load->view($themes.'template', $data);
        } else redirect('404');
 	}

    public function hello_electra()
 	{
        $themes                         = get_settings('themes')."/";
 		$data['title']					= COMPANY_NAME.' | '.'Hello Electra';
 		$data['content']				= $themes.'hello_electra';
        $data['themes']                 = get_settings('themes');
 		$this->load->view($themes.'template', $data);
 	}

    function pages_read($seo){
        if($seo!=""){
            $where = array('seo'=>$seo);
            $page_qry = $this->pages_model->read_pages($where);
            if($page_qry->num_rows()!=0){
                $page = $page_qry->row();
                $themes                         = get_settings('themes')."/";
                $data['title']					= COMPANY_NAME.' | '.$page->title;
                $data['content']				= $themes.'pages';
                $data['page']                   = $page;
                $data['themes']                 = get_settings('themes');
                $this->load->view($themes.'template', $data);
            } else $this->load->view('404');
        } else $this->load->view('404');
    }

    function send_message(){
        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $message = $this->input->post('message');
        $data_email = array(
            'email'=>$email,
            'name'=>$name,
            'content'=>$message
        );
        $this->mailer->send_email($data_email);
         //set flashdata
        $this->session->set_flashdata('message', '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Pesan berhasil dikirim!</div>');
        redirect(base_url('hello_electra'));
    }

 }
