<section class="content list-content">
  <div class="col-md-12 pos-con">
    <div class="head-title">
      <h2><span class="fa fa-wheelchair"style="padding-right:10px"></span> Buat Campaign Baru</h2>
      <hr>
    </div>
    <div class="col-md-12 datatble-content">
        <?php if(!empty($this->session->userdata('message'))) echo $this->session->userdata('message');?>
      <form method="post" action="<?php echo base_url('backend/create_campaign_process');?>" enctype="multipart/form-data">
        <div class="form-group">
          <label for="judulcampaign">Judul Campaign<span style="color:#f00">*</span></label>
          <input type="text" class="form-control" id="name" name="name" aria-describedby="emailHelp" placeholder="Judul Campaign Maksimal 40 karakter" maxlength="40" required>
        </div>
        <div class="form-group">
          <label for="batas_waktu_campaign">Batas Hari Campaign Aktif Menerima Donasi<span style="color:#f00">*</span></label>
          <input type="number" class="form-control" id="goal_days"  name="goal_days" placeholder="Masukan Batasan Hari" required>
        </div>
        <div class="form-group">
          <label for="jumlah_dana_campaign">Jumlah Dana Yang Dibutuhkan<span style="color:#f00">*</span></label>
          <input type="text" class="form-control" id="goal_nominal"  name="goal_nominal" placeholder="Jumlah Donasi" required>
        </div>
          <div class="form-group">
          <label for="judulcampaign">Deskripsi Singkat<span style="color:#f00">*</span></label>
          <input type="text" class="form-control" id="description" name="description" aria-describedby="emailHelp" placeholder="Deskripsi Singkat Maksimal 100 karakter" maxlength="100" required>
        </div>
        <div class="form-group">
          <label for="jumlah_dana_campaign">Masukan Gambar Campaign<span style="color:#f00">*</span></label>
          <input name="file" type="file" accept='image/*' required>
        </div>
      <div class="form-group">
          <label for="jumlah_dana_campaign">Detail Campaign<span style="color:#f00">*</span></label>
          <textarea class="summernote" id="detail"  name="details" required></textarea>
        </div>
        <div class="footer-form">
          <div class="right">
            <button type="submit" class="btn btn-success">Simpan</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</section>