<section class="content list-content">
<div class="col-md-12">
    <?php if(!empty($this->session->userdata('message'))) echo $this->session->userdata('message');?>
  <form method="post" action="<?php echo base_url('backend/settings_process');?>">
    <div class="col-md-12">
      <br>
      <?php 
        $settings = get_setting_editable();
            if($settings!=false){
      foreach($settings->result() as $option){
        $name = strtoupper(str_replace("_", " ", $option->name));
        echo '
        <div class="form-group">
        <label>'.$name.'</label>
        <input type="text" name="'.$option->name.'" id="'.$option->name.'" class="form-control" value="'.$option->value.'">
      </div>
        ';
      }}?>
      <div class="form-group">
        <button type="submit" class="btn btn-primary btn-block">Simpan</button>
      </div>
    </div>
</form>
</div>
</section>