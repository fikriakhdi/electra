<?php 
$otoritas_det = explode(",", $otoritas->otoritas_about_us);
$c = (strpos($otoritas->otoritas_about_us, "C")===false?0:1);
$r = (strpos($otoritas->otoritas_about_us, "R")===false?0:1);
$u = (strpos($otoritas->otoritas_about_us, "U")===false?0:1);
$d = (strpos($otoritas->otoritas_about_us, "D")===false?0:1);
?>
<section class="content list-content">
    <div class="col-md-12 pos-con">
        <div class="head-title">
            <h2><span class="fa fa-pencil" style="padding-right:10px"></span> About Us Page Settings</h2>
            <hr>
        </div>

<!--home-content-top starts from here-->
<section class="home-content-top">
    <!--our-quality-shadow-->
    <?php if(!empty($this->session->userdata('message'))) echo $this->session->userdata('message');?>
    <div class="clearfix">
    <div class="tabbable-panel margin-tops4  datatble-content">
      <div class="tabbable-line">
        <ul class="nav nav-tabs tabtop  tabsetting" class="align=center">
          <li class="active"> <a href="#tab_default_1" data-toggle="tab"> Who Are We </a> </li>
          <li> <a href="#tab_default_2" data-toggle="tab"> What We Do</a> </li>
          <li> <a href="#tab_default_3" data-toggle="tab"> Who Do We Work With </a> </li>
          <li> <a href="#tab_default_4" data-toggle="tab"> Who We Can Do For You </a> </li>
        </ul>
        <div class="tab-content margin-tops">
        <!--Tab1-->    
          <div class="tab-pane active fade in" id="tab_default_1">
                
                <?php if($u==1) { ?>
                <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/aboutus_whoarewe');?>" enctype="multipart/form-data">
                    <?php }  ?>
                    <?php if($u==0){ ?>
                    <form class="login100-form validate-form" method="post" action="" enctype="multipart/form-data">
                        <?php } ?>
                    <div class="col-md-4">
                        <br><br>
                      <div class="" class="heading4"> <img src="<?php echo base_url($about->who_are_we_pic);?>" class="img-responsive"> </div>
                                        <br>
                        <?php if($u==1) { ?>
                        <div class="input-group image-preview">
                        <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                        <span class="input-group-btn">
                            <!-- image-preview-clear button -->
                            <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                <span class="glyphicon glyphicon-remove"></span> Clear
                            </button>
                            <!-- image-preview-input -->
                            <div class="btn btn-default image-preview-input">
                                <span class="glyphicon glyphicon-folder-open"></span>
                                <span class="image-preview-input-title">Browse</span>
                                <input type="file" accept="image/png, image/jpeg, image/gif" name="file"/> <!-- rename it -->
                            </div>
                        </span>
                    </div><!-- /input-group image-preview [TO HERE]--> 
                    <?php } ?>
                    </div>
                    <div class="col-md-7">
                        <div class="form-group">
                            <h4 class="heading4">Who Are We?</h4>
                            <textarea class="form-control" rows="10" name="who_are_we_content"><?php echo $about->who_are_we_content;?></textarea>
                        </div>
                        <?php if($u==1) { ?>
                        <div align="right">
                        <button type="submit" class="btn btn-primary" align="right">Update</button>
                        </div>
                        <?php } ?>
                    </div>
                </form>
              </form>
            </div>

            <!-- END Tab1-->
            
          <div class="tab-pane fade" id="tab_default_2">
                <?php if($u==1) { ?>
                <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/aboutus_whatwedo');?>" enctype="multipart/form-data">
                    <?php } ?>
                    <?php if($u==0) { ?>
                    <form class="login100-form validate-form" method="post" action="" enctype="multipart/form-data">
                        <?php } ?>
            <div class="col-md-4">
                <br><br>
             <div class="row"> <img src="<?php echo base_url($about->what_we_do_pic);?>" class="img-responsive"> </div>
                <br>
                <?php if($u==1) { ?>
                <div class="input-group image-preview">
                <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                <span class="input-group-btn">
                    <!-- image-preview-clear button -->
                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                        <span class="glyphicon glyphicon-remove"></span> Clear
                    </button>
                    <!-- image-preview-input -->
                    <div class="btn btn-default image-preview-input">
                        <span class="glyphicon glyphicon-folder-open"></span>
                        <span class="image-preview-input-title">Browse</span>
                        <input type="file" accept="image/png, image/jpeg, image/gif" name="file1"/> <!-- rename it -->
                    </div>
                </span>
            </div><!-- /input-group image-preview [TO HERE]--> 
                <?php } ?>
            </div>
            <div class="col-md-7">
                <div class="form-group">
                    <h4 class="heading4">What We Do?</h4>
                    <textarea class="form-control" rows="9" name="what_we_do_content"><?php echo $about->what_we_do_content;?></textarea>
                </div>
                <?php if($u==1) { ?>
                <div align="right">
                <button type="submit" class="btn btn-primary" aliogn="right">Update</button>
                </div>
                <?php } ?>
            </div>
              </form>
              </form>
          </div>
            
          <div class="tab-pane fade" id="tab_default_3">
              <?php if($u==1) { ?>
                <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/aboutus_whodoweworkwith');?>" enctype="multipart/form-data">
                        <?php } ?>
                    <?php if($u==0) { ?>
                <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/aboutus_whodoweworkwith');?>" enctype="multipart/form-data">
                    <?php } ?>
                    
            <div class="col-md-4">
                <br><br>
             <div class="row"> <img src="<?php echo base_url($about->who_do_we_work_with_pic);?>" class="img-responsive"> </div>
                <br>
                <?php if($u==1) { ?>
                <div class="input-group image-preview">
                <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                <span class="input-group-btn">
                    <!-- image-preview-clear button -->
                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                        <span class="glyphicon glyphicon-remove"></span> Clear
                    </button>
                    <!-- image-preview-input -->
                    <div class="btn btn-default image-preview-input">
                        <span class="glyphicon glyphicon-folder-open"></span>
                        <span class="image-preview-input-title">Browse</span>
                        <input type="file" accept="image/png, image/jpeg, image/gif" name="file2"/> <!-- rename it -->
                    </div>
                </span>
            </div><!-- /input-group image-preview [TO HERE]--> 
                <?php }?>
            </div>
            <div class="col-md-7">
                <div class="form-group">
                    <h4 class="heading4">Who Do We Work With?</h4>
                    <textarea class="form-control" rows="10" name="who_do_we_work_with_content"><?php echo $about->who_do_we_work_with_content;?></textarea>
                </div>
                <?php if($u==1) { ?>
                <div align="right">
                <button type="submit" class="btn btn-primary" aliogn="right">Update</button>
                </div>
                <?php } ?>
            </div>
            </form>
            </form>
          </div>
            
            
         <div class="tab-pane fade" id="tab_default_4" >
             <?php if($u==1) { ?>
                <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/aboutus_whatwecandoforyou');?>" enctype="multipart/form-data">
                    <?php } ?>
                    <?php if($u==0) { ?>
                <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/aboutus_whatwecandoforyou');?>" enctype="multipart/form-data">
                    <?php } ?>
            <div class="col-md-4">
                <br><br>
             <div class="row"> <img src="<?php echo base_url($about->what_we_can_do_for_you_pic);?>" class="img-responsive"> </div>
                <br>
                <?php if($u==1) { ?>
            <div class="input-group image-preview">
                <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                <span class="input-group-btn">
                    <!-- image-preview-clear button -->
                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                        <span class="glyphicon glyphicon-remove"></span> Clear
                    </button>
                    <!-- image-preview-input -->
                    <div class="btn btn-default image-preview-input">
                        <span class="glyphicon glyphicon-folder-open"></span>
                        <span class="image-preview-input-title">Browse</span>
                        <input type="file" accept="image/png, image/jpeg, image/gif" name="file3"/> <!-- rename it -->
                    </div>
                </span>
            </div><!-- /input-group image-preview [TO HERE]--> 
                <?php } ?>
            </div>
            <div class="col-md-7">
                <div class="form-group">
                    <h4 class="heading4">What We Can Do For You?</h4>
                    <textarea class="form-control" rows="10" name="what_we_can_do_for_you_content"><?php echo $about->what_we_can_do_for_you_content;?></textarea>
                </div>
                <?php if($u==1) { ?>
                <div align="right">
                <button type="submit" class="btn btn-primary" aliogn="right">Update</button>
                </div>
                <?php } ?>
            </div>
             </form>
             </form>
          </div>
          </div>
        </div>
      </div>
    </div>

</section>
<!--home-content-top ends here--> 

    </div>
</section>
<script>
    $(document).ready(function(){
       $("#status").val('<?php echo $member_edit->status;?>').change(); 
    });
</script>