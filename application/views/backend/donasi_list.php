<section class="content list-content">
  <div class="col-md-12 pos-con">
    <div class="head-title">
      <h2><span class="fa fa-money"style="padding-right:10px"></span> List Donasi</h2>
      <hr>
    </div>
      <div>
        <?php if(!empty($this->session->flashdata('message'))) echo $this->session->flashdata('message');?>
            </div>
    <div class="col-md-12 datatble-content">
      <div class="content-datatable table-responsive">
        <table class="table table-striped table-bordered" style="width:100%">
          <thead>
            <tr class="title-datable">
              <th>NO</th>
              <th>Nama Member</th>
              <th>Email</th>
              <th>No Telp</th>
              <th>Jumlah Donasi</th>
              <th>Kode Unik</th>
              <th>Nama Campaign</th>
              <th>Waktu</th>
              <th>Status</th>
              <th>Bukti Transfer</th>
              <th>Konfirmasi</th>
            </tr>
          </thead>
            <tbody>
              <?php 
              $donasi_list = get_list_donasi_all();
              if($donasi_list!=false){
                  $num=0;
                  foreach($donasi_list->result() as $donasi){
                      $num++;
                      $status = ($donasi->status=='paid'?'<span class="label label-success">Dibayar</span>':'<span class="label label-warning">Belum Dibayar</span>');
                      $download_bukti_transfer = "<a href='".ASSETS.$donasi->bukti_transfer."' class='btn btn-success btn-block'>Download</a>";
                      $konfirmasi = ($donasi->status=='paid'?'Dikonfirmasi':'<p data-placement="top" data-toggle="tooltip" title="Konfirmasi"><a href="'.base_url('backend/konfirmasi_donasi/'.$donasi->id).'" class="btn btn-primary btn-xs" ><span class="glyphicon glyphicon-check"></span></a></p>');
              ?>
            <tr>
              <td><?php echo $num++;?></td>
              <td><?php echo $donasi->member_name;?></td>
              <td><?php echo $donasi->email;?></td>
              <td><?php echo $donasi->phone;?></td>
              <td><?php echo money($donasi->nominal);?></td>
              <td><?php echo $donasi->unique_id;?></td>
              <td><?php echo $donasi->campaign_name;?></td>
              <td><?php echo $donasi->datecreated;?></td>
              <td><?php echo $status;?></td>
              <td><?php echo ($donasi->bukti_transfer==''?'Belum Ada':$download_bukti_transfer);?></td>
              <td><?php echo $konfirmasi;?></td>
            </tr>
              <?php }} else echo '<tr><td>Belum ada data</td></tr>';?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</section>
<style>
  table.dataTable thead th {
  vertical-align:middle!important;
  font-weight:600!important;
}
  .table-striped>tbody>tr:nth-of-type(odd) {
    background:#d2d2d2;
  }
</style>