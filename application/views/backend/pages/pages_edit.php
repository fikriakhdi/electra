<section class="content list-content">
    <div class="col-md-12 pos-con">
        <div class="head-title">
            <h2><span class="fa fa-pencil" style="padding-right:10px"></span> Edit Pages</h2>
            <hr>
        </div>
        <a href="<?php echo base_url('administrator/news/pages_manager');?>" class="btn btn-primary"><span class="fa fa-arrow-left"></span> Kembali</a>
        <div class="col-md-12 datatble-content">
            <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/pages_edit_process');?>" enctype="multipart/form-data">
                <input name="id" type="hidden" value="<?php echo $pages->id;?>">
                            <div class="form-group">
                              <label for="judulcampaign">Title<span style="color:#f00">*</span></label>
                              <input type="text" class="form-control" id="title" name="title" aria-describedby="emailHelp" placeholder="Type TItle" maxlength="150"  value="<?php echo $pages->title;?>" required>
                            </div>
                            <div class="form-group">
                              <label for="judulcampaign">SEO TItle<span style="color:#f00">*</span></label>
                              <input type="text" class="form-control" id="seo" name="seo" aria-describedby="emailHelp" placeholder="Type SEO title" maxlength="150"  value="<?php echo $pages->title;?>" required>
                                <label>Permalink : <?php echo base_url('pages/');?></label>
                            </div>
                            <div class="form-group">
                              <label for="batas_waktu_campaign">Content Body<span style="color:#f00">*</span></label>
                                <textarea class="summernote" name="content" row="100"><?php echo $pages->content;?></textarea>
                            </div>
                            <div class="form-group">
                                <br>
                                 <img src="<?php echo base_url($pages->picture);?>" class="img-responsive"> </div>
                                    <br>
                              <label for="jumlah_dana_campaign">Image Feature<span style="color:#f00">*</span></label>
                                <div class="input-group image-preview">
                                <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                                <span class="input-group-btn">
                                    <!-- image-preview-clear button -->
                                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                        <span class="glyphicon glyphicon-remove"></span> Clear
                                    </button>
                                    <!-- image-preview-input -->
                                    <div class="btn btn-default image-preview-input">
                                        <span class="glyphicon glyphicon-folder-open"></span>
                                        <span class="image-preview-input-title">Browse</span>
                                        <input type="file" accept="image/png, image/jpeg, image/gif" name="file"/> <!-- rename it -->
                                    </div>
                                </span>
                            </div>
                <div class="footer-form">
                    <br>
                    <button type="submit" class="btn btn-success">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</section>
<script>
    $(document).ready(function(){
       $("#status").val('<?php echo $member_edit->status;?>').change(); 
    });
</script>