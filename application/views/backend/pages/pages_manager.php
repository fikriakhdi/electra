<?php 
$otoritas_det = explode(",", $otoritas->otoritas_pages);
$c = (strpos($otoritas->otoritas_pages, "C")===false?0:1);
$r = (strpos($otoritas->otoritas_pages, "R")===false?0:1);
$u = (strpos($otoritas->otoritas_pages, "U")===false?0:1);
$d = (strpos($otoritas->otoritas_pages, "D")===false?0:1);
?>
<section class="content list-content">
    <div class="col-md-12 pos-con">
        <div class="head-title">
            <h2><span class="fa fa-file" style="padding-right:10px"></span>Pages Manager</h2>
            <hr>
        </div>
            <!--home-content-top starts from here-->
            <section class="home-content-top">
                <!--our-quality-shadow-->
                <?php if(!empty($this->session->userdata('message'))) echo $this->session->userdata('message');?>
                <div class="clearfix">
                <div class="tabbable-panel margin-tops4  datatble-content">
                  <div class="tabbable-line">
                    <ul class="nav nav-tabs tabtop  tabsetting" class="align=center">
                    <?php if($r==1) { ?> <li> <a id="menu_1" href="#tab_default_1" data-toggle="tab">Page List</a> </li><?php } ?>
                    <?php if($c==1) { ?> <li> <a id="menu_2" href="#tab_default_2" data-toggle="tab"> Add Page</a> </li><?php } ?>
                    </ul>
                    <div class="tab-content margin-tops">
                    <!--Tab1-->
                        <?php if($r==1) { ?>
                      <div class="tab-pane active fade in" id="tab_default_1">

                            <form class="login100-form validate-form" method="post" enctype="multipart/form-data">
                                <div class="col-md-12 datatble-content">
                                  <div class="content-datatable table-responsive">
                                    <table id="example" class="table table-striped table-bordered datatable" style="width:100%">
                                      <thead>
                                        <tr class="title-datable">
                                          <th>No</th>
                                          <th>Judul</th>
                                          <th>Link</th>
                                          <th>Tanggal Dibuat</th>
                                          <th>Tanggal Diubah</th>
                                            <?php if($u==1) { ?>
                                          <th>Edit</th>
                                            <?php } ?>
                                            <?php if($d==1) { ?>
                                          <th>Hapus</th>
                                            <?php } ?>
                                        </tr>
                                      </thead>
                                      <tbody>
                                          <?php 
                                          $pages_list = get_list_pages($this->session->userdata('lang_admin'));
                                          if($pages_list!=false){
                                              $num=0;
                                              foreach($pages_list ->result() as $pages){
                                                  $num++;
                                          ?>
                                        <tr>
                                          <td><?php echo $num;?></td>
                                            <td><?php echo $pages->title;?></td>
                                            <td><a href="<?php echo base_url('pages/'.$pages->seo);?>"><?php echo base_url('pages/'.$pages->seo);?></a></td>
                                          <td><?php echo $pages->datecreated;?> </td>
                                          <td><?php echo $pages->datemodified;?></td>
                                            <?php if($u==1) { ?>
                                          <td><p data-placement="top" data-toggle="tooltip" title="Edit"><a href="<?php echo base_url('administrator/pages/pages_manager/edit/'.$pages->id);?>" class="btn btn-primary btn-xs" data-title="Edit"  ><span class="glyphicon glyphicon-pencil"></span></a></p></td>
                                            <?php } ?>
                                            <?php if($d==1) { ?>
                                          <td><p data-placement="top" data-toggle="tooltip" title="Hapus"><button type="button" class="btn btn-xs btn-danger delete_btn" data-toggle="modal" data-target="#exampleModal" id_url="<?php echo base_url('backend/delete_pages/'.$pages->id);?>"><span class="glyphicon glyphicon-trash"></span></button></p></td>
                                            <?php } ?>
                                        </tr>
                                          <?php }}?>
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                          </form>
                        </div>
                        <?php } ?>
<!-- Button trigger modal -->



                        
                        <!-- END Tab1-->
                        <?php if($c==1) { ?>
                      <div class="tab-pane fade" id="tab_default_2">
                    <div class="col-md-12 datatble-content">
                        <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/page_manager_create');?>" enctype="multipart/form-data">
                            <input name="id" type="hidden" value="">
                            <div class="form-group">
                              <label for="judulcampaign">Title<span style="color:#f00">*</span></label>
                              <input type="text" class="form-control" id="title" name="title" aria-describedby="emailHelp" placeholder="Type TItle" maxlength="150"  value="" required>
                            </div>
                            <div class="form-group">
                              <label for="judulcampaign">SEO TItle<span style="color:#f00">*</span></label>
                              <input type="text" class="form-control" id="seo" name="seo" aria-describedby="emailHelp" placeholder="Type SEO title" maxlength="150"  value="" required>
                                <label>Permalink : <?php echo base_url('pages/');?></label>
                            </div>
                            <div class="form-group">
                              <label for="content-body">Content Body<span style="color:#f00">*</span></label>
                                <textarea class="summernote" name="content" row="100"></textarea>
                            </div>
                            <div class="form-group">
                              <label for="jumlah_dana_campaign">Image Feature<span style="color:#f00">*</span></label>
                                <div class="input-group image-preview">
                                <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                                <span class="input-group-btn">
                                    <!-- image-preview-clear button -->
                                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                        <span class="glyphicon glyphicon-remove"></span> Clear
                                    </button>
                                    <!-- image-preview-input -->
                                    <div class="btn btn-default image-preview-input">
                                        <span class="glyphicon glyphicon-folder-open"></span>
                                        <span class="image-preview-input-title">Browse</span>
                                        <input type="file" accept="image/png, image/jpeg, image/gif" name="file"/> <!-- rename it -->
                                    </div>
                                </span>
                            </div>
                            </div>

                            <div class="footer-form">
                                <button type="submit" class="btn btn-success">Simpan</button>
                            </div>
                        </form>
                    </div>
                      </div>
                        <?php } ?>
                      </div>
                    </div>
                  </div>
                </div>

            </section>
            <div id="exampleModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Delete Data</h4>
                    </div>
                    <div class="modal-body">
                      Aoakah anda yakin untuk menghapus data ini
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                      <a  class="btn btn-danger" id="delete_footer" href="">Ya</a>
                    </div>
                  </div>
                </div>
              </div>
            <!--home-content-top ends here--> 
    </div>
</section>


                      <?php
                      if($r==1 && $c==1) { ?>
                      <script>
                          $(document).ready(function(){
                      $("#menu_1").click();
                              });
                      </script>
                      <?php } else if($r==1) { ?>
                      <script>
                          $(document).ready(function(){
                      $("#menu_1").click();
                          });
                      </script>
                      <?php }  else if($c==1) { ?>
                      <script>
                          $(document).ready(function(){
                      $("#menu_2").click();
                              });
                      </script>
                      <?php } ?>