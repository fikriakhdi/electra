<section class="content list-content">
    <div class="col-md-12 pos-con">
        <div class="head-title">
            <h2><span class="fa fa-pencil" style="padding-right:10px"></span> Edit Products</h2>
            <hr>
        </div>
        <a href="<?php echo base_url('administrator/products/page_settings');?>" class="btn btn-primary"><span class="fa fa-arrow-left"></span> Kembali</a>
        <div class="col-md-12 datatble-content">
                            <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/product_edit_process');?>" enctype="multipart/form-data">
                             <input name="id" type="hidden" value="<?php echo $product->id;?>">
                            <div class="form-group">
                              <label for="judulcampaign">Name of Product</label>
                              <input type="text" class="form-control" id="title" name="name" aria-describedby="emailHelp" placeholder="" maxlength="100"  value="<?php echo $product->name;?>">
                            </div>
                            <div class="form-group">
                                <label>Icon </label>
                            <div class="picture-wrapper">
                                    <img src="<?php echo (empty($product->icon)?'assets/img/no-image.jpg':base_url($product->icon));?>" class="change_picture profile-picture picture-src" id="icon_image_pic" data-file="icon_image">
                                    <input class="file_input_logo hide" id="icon_image" type="file" accept="image/png, image/jpeg, image/gif" name="icon" accept="image/*" onchange="imagepreview(this, 'icon_image_pic')">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Header Image </label>
                            <div class="picture-wrapper">
                                    <img src="<?php echo (empty($product->header_image)?'assets/img/no-image.jpg':base_url($product->header_image));?>" class="change_picture profile-picture picture-src" id="header_image_pic" data-file="header_image">
                                    <input class="file_input_logo hide" id="header_image" type="file" accept="image/png, image/jpeg, image/gif" name="file" accept="image/*" onchange="imagepreview(this, 'header_image_pic')">
                                </div>
                            </div>
                            <div class="form-group">
                                 <label>Desktop View</label>
                            <div class="picture-wrapper">
                                    <img src="<?php echo (empty($product->desktop_image)?'assets/img/no-image.jpg':base_url($product->desktop_image));?>" class="change_picture profile-picture picture-src" id="desktop_image_pic" data-file="desktop_image">
                                    <input class="file_input_logo hide" id="desktop_image" type="file" accept="image/png, image/jpeg, image/gif" name="desktop_image" accept="image/*" onchange="imagepreview(this, 'desktop_image_pic')">
                                </div>
                            </div>
                            <div class="form-group">
                                 <label>Mobile View</label>
                            <div class="picture-wrapper">
                                    <img src="<?php echo (empty($product->mobile_image)?'assets/img/no-image.jpg':base_url($product->mobile_image));?>" class="change_picture profile-picture picture-src" id="mobile_image_pic" data-file="mobile_image">
                                    <input class="file_input_logo hide" id="mobile_image" type="file" accept="image/png, image/jpeg, image/gif" name="mobile_image" accept="image/*" onchange="imagepreview(this, 'mobile_image_pic')">
                                </div>
                            </div>
                            <div class="form-group">
                              <label for="batas_waktu_campaign">Description</label>
                                <textarea class="form-control" name="description" rows="10"><?php echo $product->description;?></textarea>
                            </div>
                                <div class="form-group">
                              <label for="batas_waktu_campaign">Nilai Priority</label>
                                <select class="form-control" name="priority">
                                    <option value="">Pilih Nilai Priority</option>
                                    <?php if(!empty($product->priority)) {?>
                                    <option value="<?php echo $product->priority;?>"><?php echo $product->priority;?></option>
                                    <?php }?>
                                    <?php 
                                         $priority_list = get_priority_list($this->session->userdata('lang_admin'));
                                         if($priority_list!=false){
                                             foreach($priority_list as $priority){
                                          ?>
                                    <option value="<?php echo $priority;?>"><?php echo $priority;?></option>
                                    <?php }}  else echo ' <option value="">Tidak ada pilihan priority</option>'; ?>
                                </select>
                                    <span class="label label-primary">Nilai terbesar akan menjadi urutan pertama</span>
                            </div>
                            <div class="form-group">
                              <label for="batas_waktu_campaign">Benefit for Product Owner</label>
                                <input type="text" class="form-control tokenfield" name="benefits_product_owner" value="<?php echo $product->benefits_product_owner;?>">
                                <span class="label label-primary">Gunakan "Enter" Sebagai pemisah</span>
                            </div>
                            <div class="form-group">
                              <label for="batas_waktu_campaign">Benefit for Travel Agent</label>
                                <input type="text" class="form-control tokenfield" name="benefits_travel_agent" value="<?php echo $product->benefits_travel_agent;?>">
                                <span class="label label-primary">Gunakan "Enter" Sebagai pemisah</span>

                            </div>
                            <div class="form-group">
                              <label for="judulcampaign">Video Url</label>
                              <input type="text" class="form-control" id="title" name="video_url" aria-describedby="emailHelp" placeholder="" maxlength="200"  value="<?php echo $product->video_url;?>" >
                            </div>
                            <div class="form-group">
                              <label for="judulcampaign">DEMO Url</label>
                              <input type="text" class="form-control" id="demo_url" name="demo_url" aria-describedby="emailHelp" placeholder="" maxlength="200"  value="<?php echo $product->demo_url;?>">
                            </div>

                            <div class="footer-form">
                                <button type="submit" class="btn btn-success">Simpan</button>
                            </div>
                        </form>
        
        </div>
    </div>
</section>
<script>
    $(document).ready(function(){
        $("#priority").val('<?php echo $product->priority;?>').change(); 
    });
</script>