<section class="content list-content">
    <div class="col-md-12 pos-con">
        <div class="head-title">
            <h2><span class="fa fa-pencil" style="padding-right:10px"></span> Edit PKey Feature</h2>
            <hr>
        </div>
        <a href="<?php echo base_url('administrator/products/key_feature');?>" class="btn btn-primary"><span class="fa fa-arrow-left"></span> Kembali</a>
        <div class="col-md-12 datatble-content">
                            <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/key_feature_edit_process');?>" enctype="multipart/form-data">
                             <input name="id" type="hidden" value="<?php echo $key_feature->id;?>">
                            <div class="form-group">
                              <label for="judulcampaign">Product</label>
                              <select class="form-control" id="id_product" name="id_product" aria-describedby="emailHelp" placeholder="">
                                  <option value="">Pilih..</option>
                                  <?php
                                  $product_list = get_list_product($this->session->userdata('lang_admin'));
                                  if($product_list!=false){ 
                                   foreach($product_list->result() as $product){
                                  ?>
                                      <option value="<?php echo $product->id;?>"><?php echo $product->name;?></option>
                                  <?php }}
                                  ?>
                                </select>
                               
                            </div>
                                 
                            <div class="form-group">
                              <label for="judulcampaign">Title<span style="color:#f00">*</span></label>
                              <input type="text" class="form-control" id="title" name="title" aria-describedby="emailHelp" placeholder="" maxlength="100"  value="<?php echo $key_feature->title;?>">
                            </div>
                            <div class="form-group">
                                <label>Icon<span style="color:#f00">*</span> </label>
                            <div class="picture-wrapper">
                                    <img src="<?php echo (empty($key_feature->icon)?'assets/img/no-image.jpg':base_url($key_feature->icon));?>" class="change_picture profile-picture picture-src" id="change_picture" data-file="profilepic">
                                    <input class="file_input_logo hide" id="profilepic" type="file" accept="image/png, image/jpeg, image/gif" name="icon" accept="image/*" onchange="imagepreview(this, 'change_picture')">
                                </div>
                            </div>
                            <div class="footer-form"><br>
                              <div>
                                <button type="submit" class="btn btn-success">Simpan</button>
                              </div>
                            </div>
                        </form>
        
        </div>
    </div>
</section>
<script>
    $(document).ready(function(){
       $("#status").val('<?php echo $member_edit->status;?>').change(); 
    });
</script>