<?php 
$otoritas_det = explode(",", $otoritas->otoritas_products);
$c = (strpos($otoritas->otoritas_products, "C")===false?0:1);
$r = (strpos($otoritas->otoritas_products, "R")===false?0:1);
$u = (strpos($otoritas->otoritas_products, "U")===false?0:1);
$d = (strpos($otoritas->otoritas_products, "D")===false?0:1);
?>
<section class="content list-content">
    <div class="col-md-12 pos-con">
        <div class="head-title">
            <h2><span class="fa fa-pencil" style="padding-right:10px"></span> Products Page Settings</h2>
            <hr>
        </div>
            <!--home-content-top starts from here-->
            <section class="home-content-top">
                <!--our-quality-shadow-->
                <?php if(!empty($this->session->userdata('message'))) echo $this->session->userdata('message');?>
                <div class="clearfix">
                <div class="tabbable-panel margin-tops4  datatble-content">
                  <div class="tabbable-line">
                    <ul class="nav nav-tabs tabtop  tabsetting" class="align=center">
                     <?php if($r==1) { ?> <li> <a id="menu_1" href="#tab_default_1" data-toggle="tab"> Products List</a> </li><?php } ?>
                      <?php if($c==1) { ?> <li> <a id="menu_2" href="#tab_default_2" data-toggle="tab"> Add Products</a> </li><?php } ?>
                    </ul>
                    <div class="tab-content margin-tops">
                    <!--Tab1-->  
                        <?php if($r==1) { ?>
                      <div class="tab-pane active fade in" id="tab_default_1">

                            <form class="login100-form validate-form" method="post" enctype="multipart/form-data">
                                <div class="col-md-12 datatble-content">
                                  <div class="content-datatable table-responsive">
                                    <table id="example" class="table table-striped table-bordered datatable" style="width:100%">
                                      <thead>
                                        <tr class="title-datable">
                                          <th>No</th>
                                          <th>Name</th>
                                          <th>Nilai Priority</th>
                                          <th>Video URL</th>
                                          <th>Demo URL</th>
                                          <th>Tanggal Dibuat</th>
                                          <th>Tanggal Diubah</th>
                                            <?php if($u==1){ ?>
                                          <th>Edit</th>
                                            <?php } ?>
                                            <?php if($d==1){ ?>
                                          <th>Hapus</th>
                                            <?php } ?>
                                        </tr>
                                      </thead>
                                      <tbody>
                                          <?php 
                                          $product = get_list_product($this->session->userdata('lang_admin'));
                                          if($product!=false){
                                              $num=0;
                                              foreach($product ->result() as $product){
                                                  $num++;
                                          ?>
                                        <tr>
                                          <td><?php echo $num;?></td>
                                          <td><?php echo $product->name;?></td>
                                           <td><?php echo ($product->priority!=null?$product->priority:'Tidak memiliki nilai priority');?></td>
                                          <td><a href="<?php echo $product->video_url;?>"><?php echo $product->video_url;?></a></td>
                                          <td><a href="<?php echo $product->demo_url;?>"><?php echo $product->demo_url;?></a></td>
                                          <td><?php echo $product->datecreated;?></td>
                                          <td><?php echo $product->datemodified;?></td>
                                            <?php if($u==1){ ?>
                                          <td><p data-placement="top" data-toggle="tooltip" title="Edit"><a href="<?php echo base_url('backend/product_edit/'.$product->id);?>" class="btn btn-primary btn-xs" data-title="Edit"  ><span class="glyphicon glyphicon-pencil"></span></a></p></td>
                                            <?php } ?>
                                           <?php if($d==1){ ?>
                                          <td><p data-placement="top" data-toggle="tooltip" title="Hapus"><button type="button" class="btn btn-xs btn-danger delete_btn" data-toggle="modal" data-target="#exampleModal" id_url="<?php echo base_url('backend/delete_product/'.$product->id);?>"><span class="glyphicon glyphicon-trash"></span></button></p></td>
                                            <?php } ?>
                                        </tr>
                                          <?php }} ?>
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                          </form>
                        </div>
                        <?php } ?>

<!-- Button trigger modal -->



                        
                        <!-- END Tab1-->
                        <?php if($c==1) { ?>
                      <div class="tab-pane  fade" id="tab_default_2">
                    <div class="col-md-12 datatble-content">
                        <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/create_product');?>" enctype="multipart/form-data">
                            <input name="id" type="hidden" value="">
                            <div class="form-group">
                              <label for="judulcampaign">Name of Product<span style="color:#f00">*</span></label>
                              <input type="text" class="form-control" id="title" name="name" aria-describedby="emailHelp" placeholder="" maxlength="100"  value="" required>
                            </div>
                            <div class="form-group">
                                <label>Icon<span style="color:#f00">*</span> </label>
                            <div class="picture-wrapper">
                                    <img src="<?php echo base_url('assets/img/no-image.jpg');?>" class="change_picture profile-picture picture-src" id="icon_image_pic" data-file="icon_image">
                                    <input class="file_input_logo hide" id="icon_image" type="file" accept="image/png, image/jpeg, image/gif" name="icon" accept="image/*" onchange="imagepreview(this, 'icon_image_pic')">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Header Image<span style="color:#f00">*</span> </label>
                            <div class="picture-wrapper">
                                    <img src="<?php echo base_url('assets/img/no-image.jpg');?>" class="change_picture profile-picture picture-src" id="header_image_pic" data-file="header_image">
                                    <input class="file_input_logo hide" id="header_image" type="file" accept="image/png, image/jpeg, image/gif" name="file" accept="image/*" onchange="imagepreview(this, 'header_image_pic')">
                                </div>
                            </div>
                            <div class="form-group">
                                 <label>Desktop View<span style="color:#f00">*</span> </label>
                            <div class="picture-wrapper">
                                    <img src="<?php echo base_url('assets/img/no-image.jpg');?>" class="change_picture profile-picture picture-src" id="desktop_image_pic" data-file="desktop_image">
                                    <input class="file_input_logo hide" id="desktop_image" type="file" accept="image/png, image/jpeg, image/gif" name="desktop_image" accept="image/*" onchange="imagepreview(this, 'desktop_image_pic')">
                                </div>
                            </div>
                            <div class="form-group">
                                 <label>Mobile View<span style="color:#f00">*</span> </label>
                            <div class="picture-wrapper">
                                    <img src="<?php echo base_url('assets/img/no-image.jpg');?>" class="change_picture profile-picture picture-src" id="mobile_image_pic" data-file="mobile_image">
                                    <input class="file_input_logo hide" id="mobile_image" type="file" accept="image/png, image/jpeg, image/gif" name="mobile_image" accept="image/*" onchange="imagepreview(this, 'mobile_image_pic')">
                                </div>
                            </div>
                            <div class="form-group">
                              <label for="batas_waktu_campaign">Description<span style="color:#f00">*</span></label>
                                <textarea class="form-control" name="description" rows="10"></textarea>
                            </div>
                            <div class="form-group">
                              <label for="batas_waktu_campaign">Nilai Priority</label>
                                <select class="form-control" name="priority">
                                    <option value="">Pilih Nilai Priority</option>
                                    <?php 
                                         $priority_list = get_priority_list($this->session->userdata('lang_admin'));
                                         if($priority_list!=false){
                                             foreach($priority_list as $priority){
                                          ?>
                                    <option value="<?php echo $priority;?>"><?php echo $priority;?></option>
                                    <?php }}  else echo ' <option value="">Tidak ada pilihan priority</option>'; ?>
                                </select>
                                <span class="label label-primary">Nilai terbesar akan menjadi urutan pertama</span>
                            </div>
                            <div class="form-group">
                              <label for="batas_waktu_campaign">Benefit for Product Owner<span style="color:#f00">*</span></label><br>
                                <input type="text" class="form-control tokenfield" name="benefits_product_owner" />
                                <span class="label label-primary">Gunakan "Enter" Sebagai pemisah</span>

                            </div>
                            <div class="form-group">
                              <label for="batas_waktu_campaign">Benefit for Travel Agent<span style="color:#f00">*</span></label><br>
                                <input type="text" class="form-control tokenfield" name="benefits_travel_agent">
                                <span class="label label-primary">Gunakan "Enter" Sebagai pemisah</span>
                            </div>
                            <div class="form-group">
                              <label for="judulcampaign">Video Url<span style="color:#f00">*</span></label>
                              <input type="text" class="form-control" id="video_url" name="video_url" aria-describedby="emailHelp" placeholder="" maxlength="200"  value="" required>
                            </div>
                            <div class="form-group">
                              <label for="judulcampaign">DEMO Url<span style="color:#f00">*</span></label>
                              <input type="text" class="form-control" id="demo_url" name="demo_url" aria-describedby="emailHelp" placeholder="" maxlength="200"  value="" required>
                            </div>
                            <div class="footer-form"><br>
                              <div>
                                <button type="submit" class="btn btn-success">Simpan</button>
                              </div>
                            </div>

                        </form>
                    </div>
                      </div>
                        <?php } ?>
                      </div>
                    </div>
                  </div>
                </div>

            </section>
            <div id="exampleModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Delete Data</h4>
                    </div>
                    <div class="modal-body">
                      Apakah anda yakin untuk menghapus data ini
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                      <a  class="btn btn-danger" id="delete_footer" href="">Ya</a>
                    </div>
                  </div>
                </div>
              </div>
            <!--home-content-top ends here--> 
    </div>
</section>
                      <?php
                      if($r==1 && $c==1) { ?>
                      <script>
                          $(document).ready(function(){
                      $("#menu_1").click();
                              });
                      </script>
                      <?php } else if($r==1) { ?>
                      <script>
                          $(document).ready(function(){
                      $("#menu_1").click();
                          });
                      </script>
                      <?php }  else if($c==1) { ?>
                      <script>
                          $(document).ready(function(){
                      $("#menu_2").click();
                              });
                      </script>
                      <?php } ?>