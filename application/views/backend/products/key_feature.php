<?php 
$otoritas_det = explode(",", $otoritas->otoritas_products);
$c = (strpos($otoritas->otoritas_products, "C")===false?0:1);
$r = (strpos($otoritas->otoritas_products, "R")===false?0:1);
$u = (strpos($otoritas->otoritas_products, "U")===false?0:1);
$d = (strpos($otoritas->otoritas_products, "D")===false?0:1);
?>
<section class="content list-content">
    <div class="col-md-12 pos-con">
        <div class="head-title">
            <h2><span class="fa fa-pencil" style="padding-right:10px"></span> Key Feature Page Settings</h2>
            <hr>
        </div>
            <!--home-content-top starts from here-->
            <section class="home-content-top">
                <!--our-quality-shadow-->
                <?php if(!empty($this->session->userdata('message'))) echo $this->session->userdata('message');?>
                <div class="clearfix">
                <div class="tabbable-panel margin-tops4  datatble-content">
                  <div class="tabbable-line">
                    <ul class="nav nav-tabs tabtop  tabsetting" class="align=center">
                        <?php if($r==1) { ?> <li> <a id="menu_1" href="#tab_default_1" data-toggle="tab"> Key Feature List</a> </li><?php } ?>
                      <?php if($c==1) { ?> <li> <a id="menu_2" href="#tab_default_2" data-toggle="tab"> Add Key Feature</a> </li><?php } ?>
                    </ul>
                    <div class="tab-content margin-tops">
                    <!--Tab1-->    
                        <?php if($r==1) { ?>
                      <div class="tab-pane active fade in" id="tab_default_1">

                            <form class="login100-form validate-form" method="post" enctype="multipart/form-data">
                                <div class="col-md-12 datatble-content">
                                  <div class="content-datatable table-responsive">
                                    <table id="example" class="table table-striped table-bordered datatable" style="width:100%">
                                      <thead>
                                        <tr class="title-datable">
                                          <th>No</th>
                                          <th>Title</th>
                                          <th>ID Product</th>
                                          <th>Bahasa</th>
                                          <th>Tanggal Dibuat</th>
                                          <th>Tanggal Diubah</th>
                                          <?php if($u==1){ ?>
                                          <th>Edit</th>
                                            <?php } ?>
                                            <?php if($d==1){ ?>
                                          <th>Hapus</th>
                                            <?php } ?>
                                        </tr>
                                      </thead>
                                      <tbody>
                                          <?php 
                                          $key_feature = get_list_key_feature_bylang($this->session->userdata('lang_admin'));
                                          if($key_feature!=false){
                                              $num=0;
                                              foreach($key_feature ->result() as $key_feature){
                                                  $num++;
                                          ?>
                                        <tr>
                                          <td><?php echo $num;?></td>
                                          <td><?php echo $key_feature->title;?></td>
                                          <td><?php echo $key_feature->id_product;?></td>
                                          <td><?php echo $key_feature->lang;?></td>
                                          <td><?php echo $key_feature->datecreated;?> </td>
                                          <td><?php echo $key_feature->datemodified;?></td>
                                            <?php if($u==1){ ?>
                                          <td><p data-placement="top" data-toggle="tooltip" title="Edit"><a href="<?php echo base_url('backend/key_feature_edit/'.$key_feature->id);?>" class="btn btn-primary btn-xs" data-title="Edit"  ><span class="glyphicon glyphicon-pencil"></span></a></p></td>
                                            <?php }?>
                                            <?php if($d==1){ ?>
                                          <td><p data-placement="top" data-toggle="tooltip" title="Hapus"><button type="button" class="btn btn-xs btn-danger delete_btn" data-toggle="modal" data-target="#exampleModal" id_url="<?php echo base_url('backend/delete_key_feature/'.$key_feature->id);?>"><span class="glyphicon glyphicon-trash"></span></button></p></td>
                                            <?php }?>
                                        </tr>
                                          <?php }} ?>
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                          </form> 
                        </div>
                        <?php }?>

<!-- Button trigger modal -->



                        
                        <!-- END Tab1-->
                        <?php if($c==1) { ?>
                      <div class="tab-pane fade" id="tab_default_2">
                    <div class="col-md-12 datatble-content">
                        <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/create_key_feature');?>" enctype="multipart/form-data">
                            <input name="id" type="hidden" value="">
                            <div class="form-group">
                              <label for="judulcampaign">Product<span style="color:#f00">*</span></label>
                              <select class="form-control" id="id_product" name="id_product" aria-describedby="emailHelp" placeholder="" required>
                                  <option value="">Pilih..</option>
                                  <?php
                                  $product_list = get_list_product($this->session->userdata('lang_admin'));
                                  if($product_list!=false){ 
                                   foreach($product_list->result() as $product){
                                  ?>
                                      <option value="<?php echo $product->id;?>"><?php echo $product->name;?></option>
                                  <?php }}
                                  ?>
                                </select>
                            </div>
                            <div class="form-group">
                              <label for="judulcampaign">Title<span style="color:#f00">*</span></label>
                              <input type="text" class="form-control" id="title" name="title" aria-describedby="emailHelp" placeholder="" maxlength="100"  value="" required>
                            </div>
                            <div class="form-group">
                                <label>Icon<span style="color:#f00">*</span> </label>
                            <div class="picture-wrapper">
                                    <img src="<?php echo base_url('assets/img/no-image.jpg');?>" class="change_picture profile-picture picture-src" id="change_picture" data-file="profilepic">
                                    <input class="file_input_logo hide" id="profilepic" type="file" accept="image/png, image/jpeg, image/gif" name="icon" accept="image/*" onchange="imagepreview(this, 'change_picture')">
                                </div>
                            </div>

                            <div class="footer-form"><br>
                              <div>
                                <button type="submit" class="btn btn-success">Simpan</button>
                              </div>
                            </div>
                        </form>
                    </div>
                      </div>
                        <?php }?>
                      </div>
                    </div>
                  </div>
                </div>

            </section>
            <div id="exampleModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Delete Data</h4>
                    </div>
                    <div class="modal-body">
                      Aoakah anda yakin untuk menghapus data ini
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                      <a  class="btn btn-danger" id="delete_footer" href="">Ya</a>
                    </div>
                  </div>
                </div>
              </div>
            <!--home-content-top ends here--> 
    </div>
</section>
 <?php
                      if($r==1 && $c==1) { ?>
                      <script>
                          $(document).ready(function(){
                      $("#menu_1").click();
                              });
                      </script>
                      <?php } else if($r==1) { ?>
                      <script>
                          $(document).ready(function(){
                      $("#menu_1").click();
                          });
                      </script>
                      <?php }  else if($c==1) { ?>
                      <script>
                          $(document).ready(function(){
                      $("#menu_2").click();
                              });
                      </script>
                      <?php } ?>