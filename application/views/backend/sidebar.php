    <li>
      <a href="<?php echo base_url('administrator');?>">
        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        <span class="pull-right-container">
        </span>
      </a>
    </li>
<?php if($otoritas->otoritas_home!=""){ ?>
    <li class="treeview">
        <a href="#"><i class="fa fa-home"></i> <span>Home</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo base_url('administrator/home/why_choose_us');?>">Why Choose Us</a></li>
          <li><a href="<?php echo base_url('administrator/home/testimonial');?>">Testimonial</a></li>
          <li><a href="<?php echo base_url('administrator/home/client_list');?>">Client List</a></li>
          <li><a href="<?php echo base_url('administrator/home/sliders');?>">Sliders</a></li>
        </ul>
    </li>
<?php } ?>
<?php if($otoritas->otoritas_products!=""){ ?>
    <li class="treeview">
        <a href="#"><i class="fa fa-product-hunt"></i> <span>Products</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo base_url('administrator/products/page_settings');?>">Page Settings</a></li>
          <li><a href="<?php echo base_url('administrator/products/key_feature');?>">Key Feature</a></li>
        </ul>
    </li>
<?php } ?>
<?php if($otoritas->otoritas_news_updates!=""){ ?>
    <li class="treeview">
        <a href="#"><i class="fa fa-newspaper-o"></i> <span>News & Updates</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo base_url('administrator/news_update/news_manager');?>">News Manager</a></li>
        </ul>
    </li>
<?php } ?>
<?php if($otoritas->otoritas_pages!=""){ ?>
    <li class="treeview">
        <a href="#"><i class="fa fa-file"></i> <span>Pages</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo base_url('administrator/pages/pages_manager');?>">Pages Manager</a></li>
        </ul>
    </li>
<?php } ?>
<?php if($otoritas->otoritas_settings!=""){ ?>
    <li class="treeview">
        <a href="#"><i class="fa fa-cogs"></i> <span>Settings</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo base_url('administrator/settings/contacts');?>">Contacts</a></li>
          <li><a href="<?php echo base_url('administrator/settings/social_media_list');?>">Social Meda List</a></li>
            <li><a href="<?php echo base_url('administrator/settings/whatsapp_list');?>">Whatsapp List</a></li>
        </ul>
    </li>
<?php }?>
<?php if($otoritas->otoritas_themes!=""){ ?>
    <li>
        <a href="<?php echo base_url('administrator/themes');?>"><i class="fa fa-themeisle"></i> <span>Themes</span>
            </a>
    </li>
<?php } ?>
<?php if($otoritas->otoritas_about_us!=""){ ?>
    <li class="treeview">
        <a href="#"><i class="fa fa-address-card"></i> <span>About Us</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo base_url('administrator/about_us/page_settings');?>">Page Settings</a></li>
        </ul>
    </li>
<?php }?> 
<?php if($otoritas->otoritas_member!=""){ ?>
    <li class="treeview">
        <a href="#"><i class="fa fa-users"></i> <span>Member</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo base_url('administrator/member/member_manager');?>">Member Manager</a></li>
          <li><a href="<?php echo base_url('administrator/member/level_manager');?>">Level Manager</a></li>
        </ul>
    </li>
<?php }?>
<?php if($otoritas->otoritas_subscribe!=""){ ?>
    <li>
        <a href="<?php echo base_url('administrator/subscriber_list');?>"><i class="fa fa-list"></i> <span>Subscriber List</span>
        </a>
    </li>
<?php }?>