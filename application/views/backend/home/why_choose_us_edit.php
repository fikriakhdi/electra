<section class="content list-content">
    <div class="col-md-12 pos-con">
        <div class="head-title">
            <h2><span class="fa fa-pencil" style="padding-right:10px"></span> Edit Why Choose Us</h2>
            <hr>
        </div>
        <a href="<?php echo base_url('administrator/home/why_choose_us');?>" class="btn btn-primary"><span class="fa fa-arrow-left"></span> Kembali</a>
        <div class="col-md-12 datatble-content">
            <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/why_choose_us_edit_process');?>" enctype="multipart/form-data">
                <input name="id" type="hidden" value="<?php echo $why_choose_us->id;?>">
                    <div class="form-group">
                        <label for="judulcampaign">Title</label>
                        <input type="text" class="form-control" id="title" name="title" aria-describedby="emailHelp" placeholder="" maxlength="100"  value="<?php echo $why_choose_us->title;?>">
                    </div>
                    <div class="form-group">
                        <label>Logo</label>
                            <div class="picture-wrapper">
                                <img src="<?php echo (empty($why_choose_us->logo)?'assets/img/no-image.jpg':base_url($why_choose_us->logo));?>" class="change_picture profile-picture picture-src" id="change_picture" data-file="profilepic">
                                <input class="file_input_logo hide" id="profilepic" type="file" accept="image/png, image/jpeg, image/gif" name="logo" accept="image/*" onchange="imagepreview(this, 'change_picture')">
                            </div>
                    </div>
                    <div class="form-group">
                        <label>Image</label>
                    <div class="picture-wrapper">
                            <img src="<?php echo (empty($why_choose_us->image)?'assets/img/no-image.jpg':base_url($why_choose_us->image));?>" class="change_picture profile-picture picture-src" id="header_image_pic" data-file="header_image">
                            <input class="file_input_logo hide" id="header_image" type="file" accept="image/png, image/jpeg, image/gif" name="image" accept="image/*" onchange="imagepreview(this, 'header_image_pic')">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="batas_waktu_campaign">Description</label>
                        <textarea class="form-control" name="description" rows="10"><?php echo $why_choose_us->description;?></textarea>
                    </div>
                    <div class="footer-form"><br>
                        <div>
                            <button type="submit" class="btn btn-success">Simpan</button>
                        </div>
                    </div>
            </form>
        
        </div>
    </div>
</section>