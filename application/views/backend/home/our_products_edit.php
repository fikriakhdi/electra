<section class="content list-content">
    <div class="col-md-12 pos-con">
        <div class="head-title">
            <h2><span class="fa fa-pencil" style="padding-right:10px"></span> Edit Products</h2>
            <hr>
        </div>
        <a href="<?php echo base_url('administrator/home/our_products');?>" class="btn btn-primary"><span class="fa fa-arrow-left"></span> Kembali</a>
        <div class="col-md-12 datatble-content">
                            <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/product_edit_process');?>" enctype="multipart/form-data">
                                 <input name="id_product" type="hidden" value="<?php echo $our_products->id;?>">
                            <div class="form-group">
                              <label for="judulcampaign">Name<span style="color:#f00">*</span></label>
                              <input type="text" class="form-control" id="title" name="name" aria-describedby="emailHelp" placeholder="" maxlength="100"  value="<?php echo $our_products->name;?>" required>
                            </div>
                            <div class="form-group">
                              <label for="batas_waktu_campaign">Description<span style="color:#f00">*</span></label>
                                <textarea id="summernote" name="description" rows="100"><?php echo $our_products->description;?></textarea>
                                  <script>
                                    $(document).ready(function() {
                                        $('#summernote').summernote();
                                    });
                                  </script>
                            </div>
                            <div class="form-group">
                              <label for="jumlah_dana_campaign">Logo<span style="color:#f00">*</span></label>
                                <img src="<?php echo base_url($our_products->logo);?>" class="img-responsive"><br>
                                <div class="input-group image-preview">
                                <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                                <span class="input-group-btn">
                                    <!-- image-preview-clear button -->
                                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                        <span class="glyphicon glyphicon-remove"></span> Clear
                                    </button>
                                    <!-- image-preview-input -->
                                    <div class="btn btn-default image-preview-input">
                                        <span class="glyphicon glyphicon-folder-open"></span>
                                        <span class="image-preview-input-title">Browse</span>
                                        <input type="file" accept="image/png, image/jpeg, image/gif" name="file"/> <!-- rename it -->
                                    </div>
                                </span>
                            </div>
                            </div>

                            <div class="footer-form">
                              <div class="right">
                                <button type="submit" class="btn btn-success">Simpan</button>
                              </div>
                            </div>
                        </form>
        
        </div>
    </div>
</section>
<script>
    $(document).ready(function(){
       $("#status").val('<?php echo $member_edit->status;?>').change(); 
    });
</script>