<?php 
$otoritas_det = explode(",", $otoritas->otoritas_home);
$c = (strpos($otoritas->otoritas_home, "C")===false?0:1);
$r = (strpos($otoritas->otoritas_home, "R")===false?0:1);
$u = (strpos($otoritas->otoritas_home, "U")===false?0:1);
$d = (strpos($otoritas->otoritas_home, "D")===false?0:1);
?>
<section class="content list-content">
    <div class="col-md-12 pos-con">
        <div class="head-title">
            <h2><span class="fa fa-pencil" style="padding-right:10px"></span> Why Choose Us Page Settings</h2>
            <hr>
        </div>
            <!--home-content-top starts from here-->
            <section class="home-content-top">
                <!--our-quality-shadow-->
                <?php if(!empty($this->session->userdata('message'))) echo $this->session->userdata('message');?>
                <div class="clearfix">
                <div class="tabbable-panel margin-tops4  datatble-content">
                  <div class="tabbable-line">
                    <ul class="nav nav-tabs tabtop  tabsetting" class="align=center">
                     <?php if($r==1) { ?> <li> <a id="menu_1" href="#tab_default_1" data-toggle="tab"> Why Choose Us List</a> </li><?php } ?>
                      <?php if($c==1) { ?> <li> <a id="menu_2" href="#tab_default_2" data-toggle="tab"> Add Why Choose Us</a> </li><?php } ?>
                    </ul>
                    <div class="tab-content margin-tops">
                    <!--Tab1-->
                        <?php if($r==1) { ?>
                      <div class="tab-pane active fade in" id="tab_default_1">

                            <form class="login100-form validate-form" method="post" enctype="multipart/form-data">
                                <div class="col-md-12 datatble-content">
                                  <div class="content-datatable table-responsive">
                                    <table id="example" class="table table-striped table-bordered datatable" style="width:100%">
                                      <thead>
                                        <tr class="title-datable">
                                          <th>No</th>
                                          <th>Title</th>
                                          <th>Description</th>
                                          <th>Tanggal Dibuat</th>
                                          <th>Tanggal Diubah</th>
                                            <?php if($u==1) { ?>
                                          <th>Edit</th>
                                            <?php } ?>
                                            <?php if($d==1) { ?>
                                          <th>Hapus</th>
                                            <?php } ?>
                                        </tr>
                                      </thead>
                                      <tbody>
                                          <?php 
                                          $why_choose_us = get_list_why_choose_us($this->session->userdata('lang_admin'));
                                          if($why_choose_us!=false){
                                              $num=0;
                                              foreach($why_choose_us ->result() as $why_choose_us){
                                                  $num++;
                                          ?>
                                        <tr>
                                          <td><?php echo $num;?></td>
                                          <td><?php echo $why_choose_us->title;?></td>
                                          <td><?php echo $why_choose_us->description;?></td>
                                          <td><?php echo $why_choose_us->datecreated;?> </td>
                                          <td><?php echo $why_choose_us->datemodified;?></td>
                                            <?php if($u==1) { ?>
                                          <td><p data-placement="top" data-toggle="tooltip" title="Edit"><a href="<?php echo base_url('backend/why_choose_us_edit/'.$why_choose_us->id);?>" class="btn btn-primary btn-xs" data-title="Edit"  ><span class="glyphicon glyphicon-pencil"></span></a></p></td>
                                            <?php } ?>
                                           <?php if($d==1){ ?>
                                          <td><p data-placement="top" data-toggle="tooltip" title="Hapus"><button type="button" class="btn btn-xs btn-danger delete_btn" data-toggle="modal" data-target="#exampleModal" id_url="<?php echo base_url('backend/delete_why_choose_us/'.$why_choose_us->id);?>"><span class="glyphicon glyphicon-trash"></span></button></p></td>
                                            <?php } ?>
                                        </tr>
                                          <?php }}?>
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                          </form>
                        </div>
                        <?php } ?>
<!-- Button trigger modal -->



                        
                        <!-- END Tab1-->
                        <?php if($c==1) { ?>
                      <div class="tab-pane fade" id="tab_default_2">
                    <div class="col-md-12 datatble-content">
                       <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/create_why_choose_us');?>" enctype="multipart/form-data">
                            <input name="id" type="hidden" value="">
                            <div class="form-group">
                              <label for="judulcampaign">Title<span style="color:#f00">*</span></label>
                              <input type="text" class="form-control" id="title" name="title" aria-describedby="emailHelp" placeholder="" maxlength="100"  value="" required>
                            </div>
                            <div class="form-group">
                                <label>Logo<span style="color:#f00">*</span> </label>
                            <div class="picture-wrapper">
                                    <img src="<?php echo base_url('assets/img/no-image.jpg');?>" class="change_picture profile-picture picture-src" id="change_picture" data-file="profilepic">
                                    <input class="file_input_logo hide" id="profilepic" type="file" accept="image/png, image/jpeg, image/gif" name="logo" accept="image/*" onchange="imagepreview(this, 'change_picture')">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Image<span style="color:#f00">*</span> </label>
                            <div class="picture-wrapper">
                                    <img src="<?php echo base_url('assets/img/no-image.jpg');?>" class="change_picture profile-picture picture-src" id="header_image_pic" data-file="header_image">
                                    <input class="file_input_logo hide" id="header_image" type="file" accept="image/png, image/jpeg, image/gif" name="image" accept="image/*" onchange="imagepreview(this, 'header_image_pic')">
                                </div>
                            </div>
                            <div class="form-group">
                              <label for="batas_waktu_campaign">Description<span style="color:#f00">*</span></label>
                                <textarea name="content" class="form-control" rows="10"></textarea>
                            </div>
                            <div class="footer-form"><br>
                              <div>
                                <button type="submit" class="btn btn-success">Simpan</button>
                              </div>
                            </div>
                        </form>
                    </div>
                      </div>
                        <?php } ?>
                      </div>
                    </div>
                  </div>
                </div>

            </section>
            <div id="exampleModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Delete Data</h4>
                    </div>
                    <div class="modal-body">
                      Aoakah anda yakin untuk menghapus data ini
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                      <a  class="btn btn-danger" id="delete_footer" href="">Ya</a>
                    </div>
                  </div>
                </div>
              </div>
            <!--home-content-top ends here--> 
    </div>
</section>

                      <?php
                      if($r==1 && $c==1) { ?>
                      <script>
                          $(document).ready(function(){
                      $("#menu_1").click();
                              });
                      </script>
                      <?php } else if($r==1) { ?>
                      <script>
                          $(document).ready(function(){
                      $("#menu_1").click();
                          });
                      </script>
                      <?php }  else if($c==1) { ?>
                      <script>
                          $(document).ready(function(){
                      $("#menu_2").click();
                              });
                      </script>
                      <?php } ?>