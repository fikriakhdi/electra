<section class="content list-content">
    <div class="col-md-12 pos-con">
        <div class="head-title">
            <h2><span class="fa fa-pencil" style="padding-right:10px"></span> Our Products Page Settings</h2>
            <hr>
        </div>
            <!--home-content-top starts from here-->
            <section class="home-content-top">
                <!--our-quality-shadow-->
                <?php if(!empty($this->session->userdata('message'))) echo $this->session->userdata('message');?>
                <div class="clearfix">
                <div class="tabbable-panel margin-tops4  datatble-content">
                  <div class="tabbable-line">
                    <ul class="nav nav-tabs tabtop  tabsetting" class="align=center">
                      <li class="active"> <a href="#tab_default_1" data-toggle="tab"> Products List</a> </li>
                      <li> <a href="#tab_default_2" data-toggle="tab"> Add Products</a> </li>
                    </ul>
                    <div class="tab-content margin-tops">
                    <!--Tab1-->    
                      <div class="tab-pane active fade in" id="tab_default_1">

                            <form class="login100-form validate-form" method="post" enctype="multipart/form-data">
                                <div class="col-md-12 datatble-content">
                                  <div class="content-datatable table-responsive">
                                    <table id="example" class="table table-striped table-bordered datatable" style="width:100%">
                                      <thead>
                                        <tr class="title-datable">
                                          <th>No</th>
                                          <th>Name</th>
                                          <th>Descriptions</th>
                                          <th>Tanggal Dibuat</th>
                                          <th>Tanggal Diubah</th>
                                          <th>Edit</th>
                                          <th>Hapus</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                          <?php 
                                          $our_products = get_list_our_products();
                                          if($our_products ->num_rows()!=0){
                                              $num=0;
                                              foreach($our_products ->result() as $our_products){
                                                  $num++;
                                          ?>
                                        <tr>
                                          <td><?php echo $num++;?></td>
                                          <td><?php echo $our_products->name;?></td>
                                          <td><?php echo $our_products->description;?></td>
                                          <td><?php echo $our_products->datecreated;?> </td>
                                          <td><?php echo $our_products->datemodified;?></td>
                                          <td><p data-placement="top" data-toggle="tooltip" title="Edit"><a href="<?php echo base_url('backend/our_products_edit/'.$our_products->id);?>" class="btn btn-primary btn-xs" data-title="Edit"  ><span class="glyphicon glyphicon-pencil"></span></a></p></td>
                                          <td><p data-placement="top" data-toggle="tooltip" title="Hapus"><button type="button" class="btn btn-xs btn-danger delete_btn" data-toggle="modal" data-target="#exampleModal" id_url="<?php echo base_url('backend/delete_our_products/'.$our_products->id);?>"><span class="glyphicon glyphicon-trash"></span></button></p></td>
                                        </tr>
                                          <?php }} else echo '<tr><td>Belum ada data</td></tr>';?>
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                          </form>
                        </div>

<!-- Button trigger modal -->



                        
                        <!-- END Tab1-->
                      <div class="tab-pane fade" id="tab_default_2">
                    <div class="col-md-12 datatble-content">
                        <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/create_our_products');?>" enctype="multipart/form-data">
                            <input name="id" type="hidden" value="">
                            <div class="form-group">
                              <label for="judulcampaign">Name<span style="color:#f00">*</span></label>
                              <input type="text" class="form-control" id="title" name="name" aria-describedby="emailHelp" placeholder="" maxlength="100"  value="" required>
                            </div>
                            <div class="form-group">
                              <label for="batas_waktu_campaign">Description<span style="color:#f00">*</span></label>
                                <textarea id="summernote" name="description" rows="100"></textarea>
                                  <script>
                                    $(document).ready(function() {
                                        $('#summernote').summernote();
                                    });
                                  </script>
                            </div>
                            <div class="form-group">
                              <label for="jumlah_dana_campaign">Logo<span style="color:#f00">*</span></label>
                                <div class="input-group image-preview">
                                <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                                <span class="input-group-btn">
                                    <!-- image-preview-clear button -->
                                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                        <span class="glyphicon glyphicon-remove"></span> Clear
                                    </button>
                                    <!-- image-preview-input -->
                                    <div class="btn btn-default image-preview-input">
                                        <span class="glyphicon glyphicon-folder-open"></span>
                                        <span class="image-preview-input-title">Browse</span>
                                        <input type="file" accept="image/png, image/jpeg, image/gif" name="file"/> <!-- rename it -->
                                    </div>
                                </span>
                            </div>
                            </div>

                            <div class="footer-form">
                              <div class="right">
                                <button type="submit" class="btn btn-success">Simpan</button>
                              </div>
                            </div>
                        </form>
                    </div>
                      </div>
                      </div>
                    </div>
                  </div>
                </div>

            </section>
            <div id="exampleModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Delete Data</h4>
                    </div>
                    <div class="modal-body">
                      Aoakah anda yakin untuk menghapus data ini
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                      <a  class="btn btn-danger" id="delete_footer" href="">Ya</a>
                    </div>
                  </div>
                </div>
              </div>
            <!--home-content-top ends here--> 
    </div>
</section>