<section class="content list-content">
    <div class="col-md-12 pos-con">
        <div class="head-title">
            <h2><span class="fa fa-pencil" style="padding-right:10px"></span> Edit client</h2>
            <hr>
        </div>
        <a href="<?php echo base_url('administrator/home/client_list');?>" class="btn btn-primary"><span class="fa fa-arrow-left"></span> Kembali</a>
        <div class="col-md-12 datatble-content">
                            <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/client_edit_process');?>" enctype="multipart/form-data">
                            <input name="id" type="hidden" value="<?php echo $client->id;?>">
                            <div class="form-group">
                              <label for="judulcampaign">Client Name</label>
                              <input type="text" class="form-control" id="title" name="client_name"  placeholder="" maxlength="100"  value="<?php echo $client->client_name;?>" required>
                            </div>
                            <div class="form-group">
                                <label>Logo</label>
                            <div class="picture-wrapper">
                                    <img src="<?php echo (empty($client->client_logo)?'assets/img/no-img.jpg':base_url($client->client_logo));?>" class="change_picture profile-picture picture-src" id="change_picture" data-file="profilepic">
                                    <input class="file_input_logo hide" id="profilepic" type="file" accept="image/png, image/jpeg, image/gif" name="file" accept="image/*" onchange="imagepreview(this, 'change_picture')">
                                </div>
                            </div>

                            <div class="footer-form"><br>
                              <div>
                                <button type="submit" class="btn btn-success">Simpan</button>
                              </div>
                            </div>
                        </form>
        
        </div>
    </div>
</section>
<script>
    $(document).ready(function(){
       $("#status").val('<?php echo $member_edit->status;?>').change(); 
    });
</script>