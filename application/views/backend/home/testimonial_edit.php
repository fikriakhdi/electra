<section class="content list-content">
    <div class="col-md-12 pos-con">
        <div class="head-title">
            <h2><span class="fa fa-pencil" style="padding-right:10px"></span> Edit Testimonial</h2>
            <hr>
        </div>
        <a href="<?php echo base_url('administrator/home/testimonial');?>" class="btn btn-primary"><span class="fa fa-arrow-left"></span> Kembali</a>
        <div class="col-md-12 datatble-content">
                            <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/testimonial_edit_process');?>" enctype="multipart/form-data">
                            <input name="id" type="hidden" value="<?php echo $testimonial->id;?>">
                            <div class="form-group">
                              <label for="judulcampaign">Name</label>
                              <input type="text" class="form-control" id="title" name="name" aria-describedby="emailHelp" placeholder="" maxlength="100"  value="<?php echo $testimonial->name;?>" required>
                            </div>
                            <div class="form-group">
                                <label>Profile Picture </label>
                            <div class="picture-wrapper">
                                    <img src="<?php echo (empty($testimonial->picture)?'assets/img/no-image.jpg':base_url($testimonial->picture));?>" class="change_picture profile-picture picture-src" id="change_picture" data-file="profilepic">
                                    <input class="file_input_logo hide" id="profilepic" type="file" accept="image/png, image/jpeg, image/gif" name="file" accept="image/*" onchange="imagepreview(this, 'change_picture')">
                                </div>
                            </div>
                            <div class="form-group">
                              <label for="judulcampaign">Company</label>
                              <input type="text" class="form-control" id="company" name="company" aria-describedby="emailHelp" placeholder="" maxlength="100"  value="<?php echo $testimonial->company;?>" required>
                            </div>
                            <div class="form-group">
                              <label for="batas_waktu_campaign">Comment</label>
                                <textarea class="summernote" name="comment" rows="100"><?php echo $testimonial->comment;?></textarea>

                            </div>
                            <div class="footer-form"><br>
                              <div>
                                <button type="submit" class="btn btn-success">Simpan</button>
                              </div>
                            </div>
                        </form>
        
        </div>
    </div>
</section>
<script>
    $(document).ready(function(){
       $("#status").val('<?php echo $member_edit->status;?>').change(); 
    });
</script>