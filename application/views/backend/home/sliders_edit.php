<section class="content list-content">
    <div class="col-md-12 pos-con">
        <div class="head-title">
            <h2><span class="fa fa-pencil" style="padding-right:10px"></span> Edit Slider</h2>
            <hr>
        </div>
        <a href="<?php echo base_url('administrator/home/sliders');?>" class="btn btn-primary"><span class="fa fa-arrow-left"></span> Kembali</a>
        <div class="col-md-12 datatble-content">
                            <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/slider_edit_process');?>" enctype="multipart/form-data">
                            <input name="id" type="hidden" value="<?php echo $slider->id;?>">
                            <div class="form-group">
                              <label for="jumlah_dana_campaign">Picture<span style="color:#f00">*</span></label>
                                 <img src="<?php echo base_url($slider->picture);?>" class="img-responsive"><br>
                                <div class="input-group image-preview">
                                <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                                <span class="input-group-btn">
                                    <!-- image-preview-clear button -->
                                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                        <span class="glyphicon glyphicon-remove"></span> Clear
                                    </button>
                                    <!-- image-preview-input -->
                                    <div class="btn btn-default image-preview-input">
                                        <span class="glyphicon glyphicon-folder-open"></span>
                                        <span class="image-preview-input-title">Browse</span>
                                        <input type="file" accept="image/png, image/jpeg, image/gif" name="file"/> <!-- rename it -->
                                    </div>
                                </span>
                            </div>
                            </div>
                            <div class="footer-form"><br>
                              <div>
                                <button type="submit" class="btn btn-success">Simpan</button>
                              </div>
                            </div>
                        </form>
        
        </div>
    </div>
</section>
<script>
    $(document).ready(function(){
       $("#status").val('<?php echo $member_edit->status;?>').change(); 
    });
</script>