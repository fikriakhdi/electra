<section class="content list-content">
  <div class="col-md-12 pos-con">
    <div class="head-title">
      <h2><span class="fa fa-list"style="padding-right:10px"></span> Subscriber List/h2>
      <hr>
    </div>
      <?php if(!empty($this->session->userdata('message'))) echo $this->session->userdata('message');?>
    <div class="col-md-12 datatble-content">
      <div class="content-datatable table-responsive">
        <table id="example" class="table table-striped table-bordered" style="width:100%">
          <thead>
            <tr class="title-datable">
              <th>NO</th>
              <th>Nama</th>
              <th>Tanggal Daftar</th>
              <th>Email</th>
              <th>No Hp</th>
              <th>Status</th>
              <th>Edit</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
              <?php 
              $member_list = get_all_member_list();
              if($member_list!=false){
                  $num=0;
                  foreach($member_list->result() as $member_data){
                      $num++;
                      $status = ($member_data->status=='active'?'<span class="label label-success">Aktif</span>':'<span class="label label-warning">Belum Aktif</span>');
              ?>
            <tr>
              <td><?php echo $num++;?></td>
              <td><?php echo $member_data->name;?></td>
              <td><?php echo $member_data->datecreated;?></td>
              <td><?php echo $member_data->email;?></td>
              <td><?php echo $member_data->phone;?></td>
              <td><?php echo $status;?></td>
                <td><p data-placement="top" data-toggle="tooltip" title="Edit"><a href="<?php echo base_url('administrator/member_edit/'.$member_data->id);?>" class="btn btn-primary btn-xs" data-title="Edit"  ><span class="glyphicon glyphicon-pencil"></span></a></p></td>
                <td><p data-placement="top" data-toggle="tooltip" title="Hapus"><button class="btn btn-danger btn-xs delete_button" data-toggle="modal" data-target="#delete_modal" id_url="<?php echo base_url('backend/delete_member/');?>" id_data="<?php echo $member_data->id;?>"><span class="glyphicon glyphicon-trash"></span></button></p></td>
            </tr>
              <?php }} ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</section>
<div id="delete_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Data</h4>
        </div>
        <div class="modal-body">
          Aoakah anda yakin untuk menghapus data ini
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
          <a  class="btn btn-danger" id="delete_footer" href="#">Ya</a>
        </div>
      </div>
    </div>
  </div>
<style>
  .table-striped>tbody>tr:nth-of-type(odd) {
    background:#d2d2d2;
  }
</style>