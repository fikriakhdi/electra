<section class="content list-content">
    <div class="col-md-12 pos-con">
        <div class="head-title">
            <h2><span class="fa fa-pencil" style="padding-right:10px"></span>Buat Slideshow</h2>
            <hr>
        </div>
        <a href="<?php echo base_url('administrator/slideshow');?>" class="btn btn-primary"><span class="fa fa-arrow-left"></span> Kembali</a>
        <div class="col-md-12 datatble-content">
            <?php if(!empty($this->session->userdata('message'))) echo $this->session->userdata('message');?>
            <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/slideshow_create_process');?>" enctype="multipart/form-data">
                <div class="form-group">
                  <label for="judulcampaign">Judul Utama<span style="color:#f00">*</span></label>
                  <input type="text" class="form-control" id="title_one" name="title_one" placeholder="Judul Maksimal 30 karakter" maxlength="30" required>
                </div>
                <div class="form-group">
                  <label for="judulcampaign">Judul Kedua<span style="color:#f00">*</span></label>
                  <input type="text" class="form-control" id="title_two" name="title_two" aria-describedby="emailHelp" placeholder="Judul Maksimal 40 karakter" maxlength="45" required>
                </div>
                 <div class="form-group">
                  <label for="judulcampaign">Deskripsi<span style="color:#f00">*</span></label>
                  <input type="text" class="form-control" id="description" name="description" aria-describedby="emailHelp" placeholder="Deskripsi Maksimal 185 karakter" maxlength="185" required>
                </div>
                <div class="form-group">
                  <label for="jumlah_dana_campaign">Masukan Gambar Latar<span style="color:#f00">*</span></label>
                  <input name="file" type="file" accept='image/*'>
                </div>
                <div class="footer-form">
                    <button type="submit" class="btn btn-success">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</section>
<script>
    $(document).ready(function(){
       $("#status").val('<?php echo $member_edit->status;?>').change(); 
    });
</script>