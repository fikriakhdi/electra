<section class="content list-content">
    <div class="col-md-12 pos-con">
        <div class="head-title">
            <h2><span class="fa fa-edit" style="padding-right:10px"></span> Edit Subscriber</h2>
            <hr>
        </div>
        <a href="<?php echo base_url('administrator/subscriber_list');?>" class="btn btn-primary"><span class="fa fa-arrow-left"></span> Kembali</a>
        <div class="col-md-12 datatble-content">
            <?php if(!empty($this->session->userdata('message'))) echo $this->session->userdata('message');?>
            <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/subscribe_edit_process');?>" enctype="multipart/form-data">
                <div class="form-group">
                  <label for="judulcampaign">Number<span style="color:#f00">*</span></label>
                     <input type="hidden"  id="id" name="id" aria-describedby="emailHelp"  value="<?php echo $subscribe->id;?>" readonly>
                  <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp"  value="<?php echo $subscribe->email;?>">
                </div>
                <div class="footer-form">
                    <button type="submit" class="btn btn-success">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</section>