<section class="content list-content">
    <div class="col-md-12 pos-con">
        <div class="head-title">
            <h2><span class="fa fa-cogs" style="padding-right:10px"></span> Edit Social Media</h2>
            <hr>
        </div>
        <a href="<?php echo base_url('administrator/settings/social_media_list');?>" class="btn btn-primary"><span class="fa fa-arrow-left"></span> Kembali</a>
        <div class="col-md-12 datatble-content">
            <?php if(!empty($this->session->userdata('message'))) echo $this->session->userdata('message');?>
            <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/social_media_edit_process');?>" enctype="multipart/form-data">
                <div class="form-group">
                  <label for="judulcampaign">Nama<span style="color:#f00">*</span></label>
                  <input type="text" class="form-control" id="name" name="name" aria-describedby="emailHelp"  value="<?php echo $setting->name;?>" readonly>
                </div>
                <div class="form-group">
                  <label for="judulcampaign">Nama<span style="color:#f00">*</span></label>
                  <input type="text" class="form-control" id="value" name="value" aria-describedby="emailHelp"  value="<?php echo $setting->value;?>" >
                </div>
                <div class="footer-form">
                    <button type="submit" class="btn btn-success">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</section>
<script>
    $(document).ready(function(){
       $("#status").val('<?php echo $member_edit->status;?>').change(); 
    });
</script>