<?php 
$otoritas_det = explode(",", $otoritas->otoritas_settings);
$c = (strpos($otoritas->otoritas_settings, "C")===false?0:1);
$r = (strpos($otoritas->otoritas_settings, "R")===false?0:1);
$u = (strpos($otoritas->otoritas_settings, "U")===false?0:1);
$d = (strpos($otoritas->otoritas_settings, "D")===false?0:1);
?>
<section class="content list-content">
    <div class="col-md-12 pos-con">
        <div class="head-title">
            <h2><span class="fa fa-cogs" style="padding-right:10px"></span> Social Media Settings</h2>
            <hr>
        </div>
        <!--home-content-top starts from here-->
        <section class="home-content-top">
            <!--our-quality-shadow-->
            <?php if(!empty($this->session->userdata('message'))) echo $this->session->userdata('message');?>
            <div class="clearfix">
                <div class="tabbable-panel margin-tops4  datatble-content">
                    <div class="tabbable-line">
                        <ul class="nav nav-tabs tabtop  tabsetting" class="align=center">
                            <li class="active"> <a href="#tab_default_1" data-toggle="tab"> Social Media List</a> </li>
                        </ul>
                        <div class="tab-content margin-tops">
                            <!--Tab1-->
                            <div class="tab-pane active fade in" id="tab_default_1">

                                <form class="login100-form validate-form" method="post" enctype="multipart/form-data">
                                    <div class="col-md-12 datatble-content">
                                        <div class="content-datatable table-responsive">
                                            <table id="example" class="table table-striped table-bordered datatable" style="width:100%">
                                                <thead>
                                                    <tr class="title-datable">
                                                        <th>No</th>
                                                        <th>Nama</th>
                                                        <th>URL</th>
                                                        <?php if($u==1) {?>
                                                        <th>Edit</th>
                                                        <?php } ?>
                                                        <?php if($d==1) { ?>
                                                        <th>Action</th>
                                                        <?php } ?>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php 
                                          $settings_list = get_settings_list('social_media');
                                          if($settings_list!=false){
                                              $num=0;
                                              foreach($settings_list->result() as $settings){
                                                  $num++;
                                                  $disable = "<a href='".base_url('backend/disable_settings/'.$settings->name)."' class='btn btn-warning'>Disable</a>";
                                                  $enable = "<a href='".base_url('backend/enable_settings/'.$settings->name)."' class='btn btn-success'>Enable</a>";
                                          ?>
                                                    <tr>
                                                        <td>
                                                            <?php echo $num;?>
                                                        </td>
                                                        <td>
                                                            <?php echo $settings->name;?>
                                                        </td>
                                                        <td>
                                                             <?php echo ($settings->value!=""?'<a href="'.$settings->value.'">Click Here</a>':'Belum ada');?>
                                                        </td>
                                                        <?php if($u==1) {?>
                                                        <td><a href="<?php echo base_url('backend/edit_social_media/'.$settings->name);?>" data-placement="top" data-toggle="tooltip" title="Edit"><button type="button" class="btn btn-xs btn-success"><span class="fa fa-edit"></span></button></a></td>
                                                        <?php } ?>
                                                        <?php if($d==1) { ?>
                                                        <td>
                                                            <?php echo ($settings->status==1?$disable:$enable);?>
                                                        </td>
                                                        <?php } ?>
                                                    </tr>
                                                    <?php }}?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <!-- Button trigger modal -->




                            <!-- END Tab1-->
                            <div class="tab-pane fade" id="tab_default_2">
                                <div class="col-md-12 datatble-content">
                                    <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/create_theme');?>" enctype="multipart/form-data">
                                        <input name="id" type="hidden" value="">
                                        <div class="form-group">
                                            <label for="jumlah_dana_campaign">Theme File<span style="color:#f00">*</span></label>
                                            <div class="input-group image-preview">
                                                <input type="text" class="form-control image-preview-filename" disabled="disabled" > <!-- don't give a name === doesn't send on POST/GET -->
                                                <span class="input-group-btn">
                                                    <!-- image-preview-clear button -->
                                                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                                        <span class="glyphicon glyphicon-remove"></span> Clear
                                                    </button>
                                                    <!-- image-preview-input -->
                                                    <div class="btn btn-default image-preview-input">
                                                        <span class="glyphicon glyphicon-folder-open"></span>
                                                        <span class="image-preview-input-title">Browse</span>
                                                        <input type="file" accept=".zip" name="file" required/> <!-- rename it -->
                                                    </div>
                                                </span>
                                            </div>
                                        </div>

                                        <div class="footer-form">
                                                <button type="submit" class="btn btn-success">Install</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
        <div id="deleteModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Delete Data</h4>
                    </div>
                    <div class="modal-body">
                        Aoakah anda yakin untuk menghapus data ini
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                        <a class="btn btn-danger" id="delete_footer" href="">Ya</a>
                    </div>
                </div>
            </div>
        </div>
        <div id="previewModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content" >
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Preview Themes</h4>
                    </div>
                    <div class="modal-body" id="previewBody">
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!--home-content-top ends here-->
    </div>
</section>

                      <?php
                      if($r==1 && $c==1) { ?>
                      <script>
                          $(document).ready(function(){
                      $("#menu_1").click();
                              });
                      </script>
                      <?php } else if($r==1) { ?>
                      <script>
                          $(document).ready(function(){
                      $("#menu_1").click();
                          });
                      </script>
                      <?php }  else if($c==1) { ?>
                      <script>
                          $(document).ready(function(){
                      $("#menu_2").click();
                              });
                      </script>
                      <?php } ?>