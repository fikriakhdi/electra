<?php 
$otoritas_det = explode(",", $otoritas->otoritas_settings);
$c = (strpos($otoritas->otoritas_settings, "C")===false?0:1);
$r = (strpos($otoritas->otoritas_settings, "R")===false?0:1);
$u = (strpos($otoritas->otoritas_settings, "U")===false?0:1);
$d = (strpos($otoritas->otoritas_settings, "D")===false?0:1);
?>
<style type="text/css">
  #map {
    margin-top: 10px;
    width: 100%;
    height: 250px;
    padding: 10px;
  }
</style>

<section class="content list-content">
    <div class="col-md-12 pos-con">
        <div class="head-title">
            <h2><span class="fa fa-cogs" style="padding-right:10px"></span> Contact Settings</h2>
            <hr>
        </div>
        
        <div class="col-md-12 datatble-content">
            <?php if(!empty($this->session->userdata('message'))) echo $this->session->userdata('message');?>
            <?php if($u==1) { ?>
            <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/contact_edit_process');?>" enctype="multipart/form-data">
                <?php } ?>
                <?php if($u==0) { ?>
                <form class="login100-form validate-form" method="post" action="" enctype="multipart/form-data">
                <?php } ?>
                <div class="form-group">
                  <label for="judulcampaign">Address<span style="color:#f00">*</span></label>
                  <input type="text" class="form-control" id="company_address" name="company_address" aria-describedby="emailHelp"  value="<?php echo get_settings('company_address');?>">
                </div>
                <div class="form-group">
                  <label for="judulcampaign">Phone<span style="color:#f00">*</span></label>
                  <input type="text" class="form-control" id="company_phone" name="company_phone" aria-describedby="emailHelp"  value="<?php echo get_settings('company_phone');?>" >
                </div>
                <div class="form-group">
                  <label for="judulcampaign">Email<span style="color:#f00">*</span></label>
                  <input type="email" class="form-control" id="company_email" name="company_email" aria-describedby="emailHelp"  value="<?php echo get_settings('company_email');?>" >
                </div>
                <div class="form-group">
                <label>Location :</label>
                <input type="text" name='venue_city' id='nama_lokasi' class="form-control" placeholder="Location" value="<?php echo get_settings('company_address');?>" required="required">
                <div id="map"></div>
                <span class="label label-primary">Move pin exactly where your company placed *</span>
                <input type="hidden" name='latitude' id='latitude' value="<?php echo get_settings('latitude');?>">
                <input type="hidden" name='longitude' id='longitude'  value="<?php echo get_settings('longitude');?>">
                </div>
                    <?php if($u==1) { ?>
                <div class="footer-form">
                    <br>
                    <button type="submit" class="btn btn-success">Simpan</button>
                </div>
                    <?php } ?>
            </form>
            </form>
        </div>
<!--
  
-->
    </div>
</section>

<script src="http://maps.google.com/maps/api/js?key=AIzaSyD98vdNV_tuiFcWI1C_9df63zTjznqTQtU&sensor=false&libraries=places" type="text/javascript"></script>
<script type="text/javascript">
  var latLng = new google.maps.LatLng(<?php echo get_settings('latitude');?>, <?php echo get_settings('longitude');?>);
  var title_place = "<?php echo COMPANY_NAME;?>";
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 15,
    center: latLng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });
  /* buat marker yang bisa di drag lalu
   panggil fungsi updateMarkerPosition(latLng)
  dan letakan posisi terakhir di id=latitude dan id=longitude
  */
  // var marker = new google.maps.Marker({
  //     position : latLng,
  //     title : 'lokasi',
  //     map : map,
  //     draggable : true
  //   });
  //* Fungsi untuk mendapatkan nilai latitude longitude
  function updateMarkerPosition(latLng) {
    document.getElementById('latitude').value = [latLng.lat()]
    document.getElementById('longitude').value = [latLng.lng()]
  }


  var input = document.getElementById('nama_lokasi');
  var searchBox = new google.maps.places.SearchBox(input);
  // map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
  map.addListener('bounds_changed', function() {
    searchBox.setBounds(map.getBounds());
  });

  var markers;
        markers = new google.maps.Marker({
        map: map,
//         icon: icon,
        title: "<?php echo COMPANY_NAME?>",
        position: latLng,
        draggable: true
      });
  markers.addListener('drag', function() {
        // ketika marker di drag, otomatis nilai latitude dan longitude
        //menyesuaikan dengan posisi marker
//     alert(this.title);
        updateMarkerPosition(this.getPosition());
      });
  // Listen for the event fired when the user selects a prediction and retrieve
  // more details for that place.
  searchBox.addListener('places_changed', function() {
    if(markers!=null){
    markers.setMap(null);
    }
    var places = searchBox.getPlaces();

    if (places.length == 0) {
      return;
    }

//       markers.setMap(null);
    markers="";
    // For each place, get the icon, name and location.
    var bounds = new google.maps.LatLngBounds();
    places.forEach(function(place) {
      if (!place.geometry) {
        console.log("Returned place contains no geometry");
        return;
      }
      var icon = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };

      // Create a marker for each place.
      
//       markers.setMap(null);
      markers=new google.maps.Marker({
        map: map,
        // icon: icon,
        title: place.name,
        position: place.geometry.location,
        draggable: true
      });
      latLng = place.geometry.location;
      updateMarkerPosition(latLng);
      markers.addListener('drag', function() {
        // ketika marker di drag, otomatis nilai latitude dan longitude
        //menyesuaikan dengan posisi marker
        updateMarkerPosition(markers.getPosition());
      });
      if (place.geometry.viewport) {
        // Only geocodes have viewport.
        bounds.union(place.geometry.viewport);
      } else {
        bounds.extend(place.geometry.location);
      }
    });
    map.fitBounds(bounds);
  });
</script>