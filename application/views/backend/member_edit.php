<section class="content list-content">
    <div class="col-md-12 pos-con">
        <div class="head-title">
            <h2><span class="fa fa-pencil" style="padding-right:10px"></span> Edit Member</h2>
            <hr>
        </div>
        <a href="<?php echo base_url('administrator/member_list');?>" class="btn btn-primary"><span class="fa fa-arrow-left"></span> Kembali</a>
        <div class="col-md-12 datatble-content">
            <?php if(!empty($this->session->userdata('message'))) echo $this->session->userdata('message');?>
            <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/member_edit_process');?>">
                <input name="id" type="hidden" value="<?php echo $member_edit->id;?>">
                <div class="form-group">
                    <span>Nama Lengkap</span>
                    <input class="form-control" type="text" name="name" placeholder="Name..." value="<?php echo $member_edit->name;?>">
                    <span class="focus-input100"></span>
                </div>

                <div class="form-group">
                    <span>Email</span>
                    <input class="form-control" type="text" name="email" placeholder="Email addess..." value="<?php echo $member_edit->email;?>">
                    <span class="focus-input100"></span>
                </div>

                <div class="form-group">
                    <span>No. Handphone</span>
                    <input class="form-control" type="text" name="phone" placeholder="No. Handphone..." pattern="^[0-9]*$" value="<?php echo $member_edit->phone;?>" >
                    <span class="focus-input100"></span>
                </div>
                
                <div class="form-group">
                    <span>Status</span>
                    <select class="form-control"  name="status" id="status">
                        <option value="">Pilih Status</option>
                        <option value="active">Aktif</option>
                        <option value="nonactive">Non-Aktif</option>
                    </select>
                    <span class="focus-input100"></span>
                </div>
                
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</section>
<script>
    $(document).ready(function(){
       $("#status").val('<?php echo $member_edit->status;?>').change(); 
    });
</script>