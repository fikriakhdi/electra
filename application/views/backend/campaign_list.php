<section class="content list-content">
  <div class="col-md-12 pos-con">
    <div class="head-title">
      <h2><span class="fa fa-wheelchair"style="padding-right:10px"></span> Campaign List</h2>
      <hr>
    </div>
      <?php if(!empty($this->session->userdata('message'))) echo $this->session->userdata('message');?>
    <div class="col-md-12 datatble-content">
      <div class="content-datatable table-responsive">
        <table id="example" class="table table-striped table-bordered" style="width:100%">
          <thead>
            <tr class="title-datable">
              <th>NO</th>
              <th>Nama Campaign</th>
              <th>Target Hari</th>
              <th>Dana Yang Dibutuhkan</th>
              <th>Jumlah Donasi Sekarang</th>
              <th>Tanggal Dibuat</th>
              <th>Status</th>
              <th>Edit</th>
              <th>Hapus</th>
            </tr>
          </thead>
          <tbody>
              <?php 
              $campaign_list = get_list_campaign();
              if($campaign_list->num_rows()!=0){
                  $num=0;
                  foreach($campaign_list->result() as $campaign){
                      $num++;
                      $status = ($campaign->status=='open'?'<span class="label label-success">Aktif</span>':'<span class="label label-danger">Selesai</span>');
              ?>
            <tr>
              <td><?php echo $num++;?></td>
                <td><a href="<?php echo base_url('campaign/'.$campaign->id);?>"><?php echo $campaign->name;?></a></td>
              <td><?php echo $campaign->goal_days;?> Hari</td>
              <td><?php echo money($campaign->goal_nominal);?></td>
              <td><?php echo money(sum_terkumpul($campaign->id));?></td>
              <td><?php echo $campaign->datecreated;?></td>
              <td><?php echo $status;?></td>
              <td><p data-placement="top" data-toggle="tooltip" title="Edit"><a href="<?php echo base_url('administrator/campaign_edit/'.$campaign->id);?>" class="btn btn-primary btn-xs" data-title="Edit"  ><span class="glyphicon glyphicon-pencil"></span></a></p></td>
                <td><p data-placement="top" data-toggle="tooltip" title="Hapus"><button class="btn btn-danger btn-xs delete_button" data-toggle="modal" data-target="#delete_modal" id_url="<?php echo base_url('backend/delete_campaign/');?>" id_data="<?php echo $campaign->id;?>"><span class="glyphicon glyphicon-trash"></span></button></p></td>
            </tr>
              <?php }} else echo '<tr><td>Belum ada data</td></tr>';?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</section>
<div id="delete_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Data</h4>
        </div>
        <div class="modal-body">
          Aoakah anda yakin untuk menghapus data ini
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
          <a  class="btn btn-danger" id="delete_footer" href="#">Ya</a>
        </div>
      </div>
    </div>
  </div>
<style>
  table.dataTable thead th {
  vertical-align:middle!important;
  font-weight:600!important;
}
  .table-striped>tbody>tr:nth-of-type(odd) {
    background:#d2d2d2;
  }
</style>