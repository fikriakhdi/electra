<section class="content list-content">
  <div class="col-md-12 pos-con">
    <div class="head-title">
      <h2><span class="fa fa-sliders"style="padding-right:10px"></span> Slideshow</h2>
      <hr>
    </div>
      <div>
        <?php if(!empty($this->session->flashdata('message'))) echo $this->session->flashdata('message');?>
            </div>
      <div class="pull-right">
          <a href="<?php echo base_url('administrator/create_slideshow');?>" class="btn btn-primary"><span class="fa fa-plus"></span> Buat Slideshow</a>
      </div>
    <div class="col-md-12 datatble-content">
      <div class="content-datatable table-responsive">
        <table class="table table-striped table-bordered" style="width:100%">
          <thead>
            <tr class="title-datable">
              <th>NO</th>
              <th>Judul Utama</th>
              <th>Judul kedua</th>
              <th>Deskripsi</th>
              <th>Gambar</th>
              <th>Edit</th>
              <th>Delete</th>
            </tr>
          </thead>
            <tbody>
              <?php 
              $slideshow_list = get_list_slideshow();
              if($slideshow_list!=false){
                  $num=0;
                  foreach($slideshow_list->result() as $slideshow){
                      $num++;
              ?>
            <tr>
              <td><?php echo $num++;?></td>
              <td><?php echo $slideshow->title_one;?></td>
              <td><?php echo $slideshow->title_two;?></td>
              <td><?php echo $slideshow->description;?></td>
              <td><a href="<?php echo ASSETS.$slideshow->background_picture;?>" class="btn btn-primary">Download</a></td>
              <td><p data-placement="top" data-toggle="tooltip" title="Edit"><a href="<?php echo base_url('administrator/slideshow_edit/'.$slideshow->id);?>" class="btn btn-primary btn-xs" data-title="Edit"  ><span class="glyphicon glyphicon-pencil"></span></a></p></td>
                <td><p data-placement="top" data-toggle="tooltip" title="Hapus"><button class="btn btn-danger btn-xs delete_button" data-toggle="modal" data-target="#delete_modal" id_url="<?php echo base_url('backend/delete_slideshow/');?>" id_data="<?php echo $slideshow->id;?>"><span class="glyphicon glyphicon-trash"></span></button></p></td>
            </tr>
              <?php }} else echo '<tr><td>Belum ada data</td></tr>';?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</section>
<div id="delete_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Data</h4>
        </div>
        <div class="modal-body">
          Aoakah anda yakin untuk menghapus data ini
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
          <a  class="btn btn-danger" id="delete_footer" href="#">Ya</a>
        </div>
      </div>
    </div>
  </div>
<style>
  table.dataTable thead th {
  vertical-align:middle!important;
  font-weight:600!important;
}
  .table-striped>tbody>tr:nth-of-type(odd) {
    background:#d2d2d2;
  }
</style>