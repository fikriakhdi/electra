<?php 
$otoritas_det = explode(",", $otoritas->otoritas_member);
$c = (strpos($otoritas->otoritas_member, "C")===false?0:1);
$r = (strpos($otoritas->otoritas_member, "R")===false?0:1);
$u = (strpos($otoritas->otoritas_member, "U")===false?0:1);
$d = (strpos($otoritas->otoritas_member, "D")===false?0:1);
?>
<section class="content list-content">
    <div class="col-md-12 pos-con">
        <div class="head-title">
            <h2><span class="fa fa-users" style="padding-right:10px"></span>Level Manager</h2>
            <hr>
        </div>
        <!--home-content-top starts from here-->
        <section class="home-content-top">
            <!--our-quality-shadow-->
            <?php if(!empty($this->session->userdata('message'))) echo $this->session->userdata('message');?>
            <div class="clearfix">
                <div class="tabbable-panel margin-tops4  datatble-content">
                    <div class="tabbable-line">
                        <ul class="nav nav-tabs tabtop  tabsetting" class="align=center">
                    <?php if($r==1) { ?> <li> <a id="menu_1" href="#tab_default_1" data-toggle="tab">Level List</a> </li><?php } ?>
                    <?php if($c==1) { ?> <li> <a id="menu_2" href="#tab_default_2" data-toggle="tab"> Add Level</a> </li><?php } ?>
                        </ul>
                        <div class="tab-content margin-tops">
                            <!--Tab1-->
                            <?php if($r==1) { ?>
                            <div class="tab-pane active fade in" id="tab_default_1">

                                <form class="login100-form validate-form" method="post" enctype="multipart/form-data">
                                    <div class="col-md-12 datatble-content">
                                        <div class="content-datatable table-responsive">
                                            <table id="example" class="table table-striped table-bordered datatable" style="width:100%">
                                                <thead>
                                                    <tr class="title-datable">
                                                        <th>No</th>
                                                        <th>Nama Level</th>
                                                        <th>Home</th>
                                                        <th>Products</th>
                                                        <th>News& Updates</th>
                                                        <th>Pages</th>
                                                        <th>Settings</th>
                                                        <th>Themes</th>
                                                        <th>About Us</th>
                                                        <th>Member</th>
                                                        <th>Subscribe</th>
                                                        <?php if($u==1) {?>
                                                        <th>Edit</th>
                                                        <?php } ?>
                                                        <?php if($d==1) { ?>
                                                        <th>Hapus</th>
                                                        <?php } ?>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php 
                                          $member_list = get_list_level();
                                          if($member_list!=false){
                                              $num=0;
                                              foreach($member_list ->result() as $member){
                                                  $num++;
                                          ?>
                                                    <tr>
                                                        <td>
                                                            <?php echo $num;?>
                                                        </td>
                                                        <td>
                                                            <?php echo $member->nama_level;?>
                                                        </td>
                                                        <td>
                                                            <?php echo crud_show($member->otoritas_home);?>
                                                        </td>
                                                        <td>
                                                            <?php echo crud_show($member->otoritas_products);?>
                                                        </td>
                                                        <td>
                                                            <?php echo crud_show($member->otoritas_news_updates);?>
                                                        </td>
                                                        <td>
                                                            <?php echo crud_show($member->otoritas_pages);?>
                                                        </td>
                                                        <td>
                                                            <?php echo crud_show($member->otoritas_settings);?>
                                                        </td>
                                                        <td>
                                                            <?php echo crud_show($member->otoritas_themes);?>
                                                        </td>
                                                        <td>
                                                            <?php echo crud_show($member->otoritas_about_us);?>
                                                        </td>
                                                        <td>
                                                            <?php echo crud_show($member->otoritas_member);?>
                                                        </td>
                                                        <td>
                                                            <?php echo crud_show($member->otoritas_subscribe);?>
                                                        </td>
                                                        <?php if($u==1) { ?>
                                                        <td>
                                                            <p data-placement="top" data-toggle="tooltip" title="Edit"><a href="<?php echo base_url('backend/level_edit/'.$member->id_level);?>" class="btn btn-primary btn-xs" data-title="Edit"><span class="glyphicon glyphicon-pencil"></span></a></p>
                                                        </td>
                                                        <?php } ?>
                                                        <?php if($d==1) { ?>
                                                        <td>
                                                            <p data-placement="top" data-toggle="tooltip" title="Hapus"><button type="button" class="btn btn-xs btn-danger delete_btn" data-toggle="modal" data-target="#exampleModal" id_url="<?php echo base_url('backend/delete_level/'.$member->id_level);?>"><span class="glyphicon glyphicon-trash"></span></button></p>
                                                        </td>
                                                        <?php } ?>
                                                    </tr>
                                                    <?php }}?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <?php } ?>

                            <!-- Button trigger modal -->




                            <!-- END Tab1-->
                            <?php if($c==1) { ?>
                            <div class="tab-pane fade" id="tab_default_2">
                                <div class="col-md-12 datatble-content">
                                    <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/create_level_manager');?>" enctype="multipart/form-data">
                                        <input name="id" type="hidden" value="">
                                        <div class="form-group">
                                            <label>Nama Level <span style="color:#f00">*</span></label>
                                            <input type="text" class="form-control" id="title" name="nama_level" aria-describedby="emailHelp" placeholder="" maxlength="150" value="" required>
                                        </div>
                                        <div class="form-group  col-md-4">
                                            <label>Home <span style="color:#f00">*</span></label>
                                            <div class="container-fluid ">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" name="home_c" value="C">
                                                    <label class="form-check-label">Create</label>
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox2" name="home_r" value="R">
                                                    <label class="form-check-label">Read</label>
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox3" name="home_u" value="U">
                                                    <label class="form-check-label">Update</label>
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox3" name="home_d" value="D">
                                                    <label class="form-check-label">Delete</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group  col-md-4">
                                            <label>Products <span style="color:#f00">*</span></label>
                                            <div class="container-fluid">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" name="products_c" value="C">
                                                    <label class="form-check-label">Create</label>
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox2" name="products_r" value="R">
                                                    <label class="form-check-label">Read</label>
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox3" name="products_u" value="U">
                                                    <label class="form-check-label">Update</label>
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox3" name="products_d" value="D">
                                                    <label class="form-check-label">Delete</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label>News & Updates <span style="color:#f00">*</span></label>
                                            <div class="container-fluid">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" name="news_updates_c" value="C">
                                                    <label class="form-check-label">Create</label>
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox2" name="news_updates_r" value="R">
                                                    <label class="form-check-label">Read</label>
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox3" name="news_updates_u" value="U">
                                                    <label class="form-check-label">Update</label>
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox3" name="news_updates_d" value="D">
                                                    <label class="form-check-label">Delete</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label>Pages <span style="color:#f00">*</span></label>
                                            <div class="container-fluid">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" name="pages_c" value="C">
                                                    <label class="form-check-label">Create</label>
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox2" name="pages_r" value="R">
                                                    <label class="form-check-label">Read</label>
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox3" name="pages_u" value="U">
                                                    <label class="form-check-label">Update</label>
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox3" name="pages_d" value="D">
                                                    <label class="form-check-label">Delete</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label>Settings <span style="color:#f00">*</span></label>
                                            <div class="container-fluid">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" name="settings_c" value="C">
                                                    <label class="form-check-label">Create</label>
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox2" name="settings_r" value="R">
                                                    <label class="form-check-label">Read</label>
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox3" name="settings_u" value="U">
                                                    <label class="form-check-label">Update</label>
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox3" name="settings_d" value="D">
                                                    <label class="form-check-label">Delete</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label>Themes <span style="color:#f00">*</span></label>
                                            <div class="container-fluid">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" name="themes_c" value="C">
                                                    <label class="form-check-label">Create</label>
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox2" name="themes_r" value="R">
                                                    <label class="form-check-label">Read</label>
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox3" name="themes_u" value="U">
                                                    <label class="form-check-label">Update</label>
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox3" name="themes_d" value="D">
                                                    <label class="form-check-label">Delete</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label>About Us <span style="color:#f00">*</span></label>
                                            <div class="container-fluid">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" name="about_us_c" value="C">
                                                    <label class="form-check-label">Create</label>
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox2" name="about_us_r" value="R">
                                                    <label class="form-check-label">Read</label>
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox3" name="about_us_u" value="U">
                                                    <label class="form-check-label">Update</label>
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox3" name="about_us_d" value="D">
                                                    <label class="form-check-label">Delete</label>
                                                </div>
                                            </div>
                                        </div>   
                                        <div class="form-group col-md-4">
                                            <label>Member<span style="color:#f00">*</span></label>
                                            <div class="container-fluid">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" name="member_c" value="C">
                                                    <label class="form-check-label">Create</label>
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox2" name="member_r" value="R">
                                                    <label class="form-check-label">Read</label>
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox3" name="member_u" value="U">
                                                    <label class="form-check-label">Update</label>
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox3" name="member_d" value="D">
                                                    <label class="form-check-label">Delete</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label>Subscribe<span style="color:#f00">*</span></label>
                                            <div class="container-fluid">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" name="subscribe_c" value="C">
                                                    <label class="form-check-label">Create</label>
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox2" name="subscribe_r" value="R">
                                                    <label class="form-check-label">Read</label>
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox3" name="subscribe_u" value="U">
                                                    <label class="form-check-label">Update</label>
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox3" name="subscribe_d" value="D">
                                                    <label class="form-check-label">Delete</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <div class="container-fluid">
                                                <label class="form-check-label"> </label>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" name="" onclick="toggle(this);" value="C">
                                                    <label class="form-check-label">Check All</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="footer-form col-md-12">
                                            <br><br><button type="submit" class="btn btn-success">Simpan</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>

        </section>
        <div id="exampleModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Delete Data</h4>
                    </div>
                    <div class="modal-body">
                        Aoakah anda yakin untuk menghapus data ini
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                        <a class="btn btn-danger" id="delete_footer" href="">Ya</a>
                    </div>
                </div>
            </div>
        </div>
        <!--home-content-top ends here-->
    </div>
</section>

                      <?php
                      if($r==1 && $c==1) { ?>
                      <script>
                          $(document).ready(function(){
                      $("#menu_1").click();
                              });
                      </script>
                      <?php } else if($r==1) { ?>
                      <script>
                          $(document).ready(function(){
                      $("#menu_1").click();
                          });
                      </script>
                      <?php }  else if($c==1) { ?>
                      <script>
                          $(document).ready(function(){
                      $("#menu_2").click();
                              });
                      </script>
                      <?php } ?>