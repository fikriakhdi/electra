<?php 
$otoritas_det = explode(",", $otoritas->otoritas_member);
$c = (strpos($otoritas->otoritas_member, "C")===false?0:1);
$r = (strpos($otoritas->otoritas_member, "R")===false?0:1);
$u = (strpos($otoritas->otoritas_member, "U")===false?0:1);
$d = (strpos($otoritas->otoritas_member, "D")===false?0:1);
?>
<section class="content list-content">
    <div class="col-md-12 pos-con">
        <div class="head-title">
            <h2><span class="fa fa-users" style="padding-right:10px"></span>Member Manager</h2>
            <hr>
        </div>
            <!--home-content-top starts from here-->
            <section class="home-content-top">
                <!--our-quality-shadow-->
                <?php if(!empty($this->session->userdata('message'))) echo $this->session->userdata('message');?>
                <div class="clearfix">
                <div class="tabbable-panel margin-tops4  datatble-content">
                  <div class="tabbable-line">
                    <ul class="nav nav-tabs tabtop  tabsetting" class="align=center">
                    <?php if($r==1) { ?> <li> <a id="menu_1" href="#tab_default_1" data-toggle="tab">Member List</a> </li><?php } ?>
                    <?php if($c==1) { ?> <li> <a id="menu_2" href="#tab_default_2" data-toggle="tab"> Add Member</a> </li><?php } ?>
                    </ul>
                    <div class="tab-content margin-tops">
                    <!--Tab1-->
                        <?php if($r==1) { ?>
                      <div class="tab-pane active fade in" id="tab_default_1">

                            <form class="login100-form validate-form" method="post" enctype="multipart/form-data">
                                <div class="col-md-12 datatble-content">
                                  <div class="content-datatable table-responsive">
                                    <table id="example" class="table table-striped table-bordered datatable" style="width:100%">
                                      <thead>
                                        <tr class="title-datable">
                                          <th>No</th>
                                          <th>Email</th>
                                          <th>No Telepon</th>
                                          <th>Level</th>
                                          <th>Tanggal Dibuat</th>
                                          <th>Tanggal Diubah</th>
                                            <?php if($u==1) { ?>
                                          <th>Edit</th>
                                            <?php } ?>
                                            <?php if($d==1) { ?>
                                          <th>Hapus</th>
                                            <?php } ?>
                                        </tr>
                                      </thead>
                                      <tbody>
                                          <?php 
                                          $member_list = read_memberandlevel();
                                          if($member_list!=false){
                                              $num=0;
                                              foreach($member_list ->result() as $member){
                                                  if($member->id!=1){
                                                  $num++;
                                          ?>
                                        <tr>
                                          <td><?php echo $num;?></td>
                                          <td><?php echo $member->email;?></td>
                                          <td><?php echo $member->no_telepon;?></td>
                                          <td><?php echo $member->level_name;?></td>
                                          <td><?php echo $member->datecreated;?> </td>
                                          <td><?php echo $member->datemodified;?></td>
                                            <?php if($u==1) { ?>
                                          <td><p data-placement="top" data-toggle="tooltip" title="Edit"><a href="<?php echo base_url('backend/member_edit/'.$member->id);?>" class="btn btn-primary btn-xs" data-title="Edit"  ><span class="glyphicon glyphicon-pencil"></span></a></p></td>
                                            <?php } ?>
                                            <?php if($d==1) { ?>
                                          <td><p data-placement="top" data-toggle="tooltip" title="Hapus"><button type="button" class="btn btn-xs btn-danger delete_btn" data-toggle="modal" data-target="#exampleModal" id_url="<?php echo base_url('backend/delete_member/'.$member->id);?>"><span class="glyphicon glyphicon-trash"></span></button></p></td>
                                            <?php } ?>
                                        </tr>
                                          <?php } }}?>
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                          </form>
                        </div>
                        <?php } ?>

<!-- Button trigger modal -->



                        
                        <!-- END Tab1-->
                        <?php if($c==1) { ?>
                      <div class="tab-pane fade" id="tab_default_2">
                    <div class="col-md-12 datatble-content">
                        <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/create_member_manager');?>" enctype="multipart/form-data">
                            <input name="id" type="hidden" value="">
                            <div class="form-group">
                              <label for="name">Name<span style="color:#f00">*</span></label>
                              <input type="text" class="form-control" id="title" name="name" aria-describedby="emailHelp" placeholder="Type name" maxlength="150"  value="" required>
                            </div>
                            <div class="form-group">
                              <label for="email">Email<span style="color:#f00">*</span></label>
                              <input type="text" class="form-control" id="title" name="email" aria-describedby="emailHelp" placeholder="Type email" maxlength="150"  value="" required>
                            </div>
                            <div class="form-group">
                              <label for="password">Password<span style="color:#f00">*</span></label>
                              <input type="password" class="form-control" id="title" name="password" aria-describedby="emailHelp" placeholder="Type Password" maxlength="150"  value="" required>
                            </div>
                            <div class="form-group">
                              <label for="no_telepon">No. Telepon<span style="color:#f00">*</span></label>
                              <input type="text" class="form-control" id="title" name="no_telepon" aria-describedby="emailHelp" placeholder="Type No Telepon" maxlength="150"  value="" required>
                            </div>
                            <div class="form-group">
                              <label for="id_level">Level<span style="color:#f00">*</span></label>
                              <select class="form-control" name="id_level" aria-describedby="emailHelp" placeholder="Type TItle" maxlength="150"  value="" required>
                                  <option value="">Pilih Level</option>
                                  <?php
                                  $level_list = get_list_level();
                                  if($level_list!=false){ 
                                      foreach($level_list->result() as $level){
                                  ?>
                                  <option value="<?php echo $level->id_level;?>"><?php echo $level->nama_level;?></option>
                                  <?php }} else echo '<option value="">Tidak ada pilihan level</option>';
                                  ?>
                                </select>
                            </div>
                            <div class="footer-form">
                                <button type="submit" class="btn btn-success">Simpan</button>
                            </div>
                        </form>
                    </div>
                      </div>
                        <?php } ?>
                      </div>
                    </div>
                  </div>
                </div>

            </section>
            <div id="exampleModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Delete Data</h4>
                    </div>
                    <div class="modal-body">
                      Aoakah anda yakin untuk menghapus data ini
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                      <a  class="btn btn-danger" id="delete_footer" href="">Ya</a>
                    </div>
                  </div>
                </div>
              </div>
            <!--home-content-top ends here--> 
    </div>
</section>
                      <?php
                      if($r==1 && $c==1) { ?>
                      <script>
                          $(document).ready(function(){
                      $("#menu_1").click();
                              });
                      </script>
                      <?php } else if($r==1) { ?>
                      <script>
                          $(document).ready(function(){
                      $("#menu_1").click();
                          });
                      </script>
                      <?php }  else if($c==1) { ?>
                      <script>
                          $(document).ready(function(){
                      $("#menu_2").click();
                              });
                      </script>
                      <?php } ?>