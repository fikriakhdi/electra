<section class="content list-content">
    <div class="col-md-12 pos-con">
        <div class="head-title">
            <h2><span class="fa fa-pencil" style="padding-right:10px"></span> Edit Level</h2>
            <hr>
        </div>
        <a href="<?php echo base_url('administrator/member/level_manager');?>" class="btn btn-primary"><span class="fa fa-arrow-left"></span> Kembali</a>
        <div class="col-md-12 datatble-content">
            <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/level_edit_process');?>" enctype="multipart/form-data">
                            <input name="id_level" type="hidden" value="<?php echo $level->id_level;?>">
<div class="form-group">
                               <label>Nama Level <span style="color:#f00">*</span></label>
                              <input type="text" class="form-control" id="title" name="nama_level" aria-describedby="emailHelp" placeholder="" maxlength="150"  value="<?php echo $level->nama_level;?>" required>
                            </div>
                            <div class="form-group  col-md-4" >
                                <label>Home <span style="color:#f00">*</span></label>
                                    <div class="container-fluid ">
                                        <?php
                                        $home = explode(",", $level->otoritas_home);
                                        ?>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="home_c" name="home_c" value="C">
                                        <label class="form-check-label">Create</label>
                                        <input class="form-check-input" type="checkbox" id="home_r" name="home_r" value="R">
                                        <label class="form-check-label">Read</label>
                                        <input class="form-check-input" type="checkbox" id="home_u" name="home_u" value="U">
                                        <label class="form-check-label">Update</label>
                                        <input class="form-check-input" type="checkbox" id="home_d" name="home_d" value="D">
                                        <label class="form-check-label">Delete</label>
                                    </div>
                                        <script>
                                            <?php
                                            $no = 0;
                                            foreach($home as $item){
                                                $no++;
                                                if($item=='C')
                                                echo '$("#home_c").prop("checked", true);';
                                                if($item=='R')
                                                echo '$("#home_r").prop("checked", true);';
                                                if($item=='U')
                                                echo '$("#home_u").prop("checked", true);';
                                                if($item=='D')
                                                echo '$("#home_d").prop("checked", true);';
                                            }
                                            ?>
                                        </script>
                                    </div>
                            </div>
                             <div class="form-group  col-md-4">
                                <label>Products <span style="color:#f00">*</span></label>
                                    <div class="container-fluid">
                                        <?php
                                        $products = explode(",", $level->otoritas_products);
                                        ?>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="products_c" name="products_c" value="C">
                                        <label class="form-check-label">Create</label>
                                        <input class="form-check-input" type="checkbox" id="products_r" name="products_r" value="R">
                                        <label class="form-check-label">Read</label>
                                        <input class="form-check-input" type="checkbox" id="products_u" name="products_u" value="U">
                                        <label class="form-check-label">Update</label>
                                        <input class="form-check-input" type="checkbox" id="products_d" name="products_d" value="D">
                                        <label class="form-check-label">Delete</label>
                                    </div>
                                        <script>
                                            <?php
                                            $no = 0;
                                            foreach($products as $item){
                                                $no++;
                                                if($item=='C')
                                                echo '$("#products_c").prop("checked", true);';
                                                if($item=='R')
                                                echo '$("#products_r").prop("checked", true);';
                                                if($item=='U')
                                                echo '$("#products_u").prop("checked", true);';
                                                if($item=='D')
                                                echo '$("#products_d").prop("checked", true);';
                                            }
                                            ?>
                                        </script>
                                    </div>
                            </div>
                            <div class="form-group col-md-4">
                                <label>News & Updates <span style="color:#f00">*</span></label>
                                    <div class="container-fluid">
                                        <?php
                                        $news = explode(",", $level->otoritas_news_updates);
                                        ?>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="news_updates_c" name="news_updates_c" value="C">
                                        <label class="form-check-label">Create</label>
                                        <input class="form-check-input" type="checkbox" id="news_updates_r" name="news_updates_r" value="R">
                                        <label class="form-check-label">Read</label>
                                        <input class="form-check-input" type="checkbox" id="news_updates_u" name="news_updates_u" value="U">
                                        <label class="form-check-label">Update</label>
                                        <input class="form-check-input" type="checkbox" id="news_updates_d" name="news_updates_d" value="D">
                                        <label class="form-check-label">Delete</label>
                                    </div>
                                        <script>
                                            <?php
                                            $no = 0;
                                            foreach($news as $item){
                                                $no++;
                                                if($item=='C')
                                                echo '$("#news_updates_c").prop("checked", true);';
                                                if($item=='R')
                                                echo '$("#news_updates_r").prop("checked", true);';
                                                if($item=='U')
                                                echo '$("#news_updates_u").prop("checked", true);';
                                                if($item=='D')
                                                echo '$("#news_updates_d").prop("checked", true);';
                                            }
                                            ?>
                                        </script>
                                    </div>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Pages <span style="color:#f00">*</span></label>
                                    <div class="container-fluid">
                                        <?php
                                        $pages = explode(",", $level->otoritas_pages);
                                        ?>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="pages_c" name="pages_c" value="C">
                                        <label class="form-check-label">Create</label>
                                        <input class="form-check-input" type="checkbox" id="pages_r" name="pages_r" value="R">
                                        <label class="form-check-label">Read</label>
                                        <input class="form-check-input" type="checkbox" id="pages_u" name="pages_u" value="U">
                                        <label class="form-check-label">Update</label>
                                        <input class="form-check-input" type="checkbox" id="pages_d" name="pages_d" value="D">
                                        <label class="form-check-label">Delete</label>
                                    </div>
                                        <script>
                                            <?php
                                            $no = 0;
                                            foreach($pages as $item){
                                                $no++;
                                                if($item=='C')
                                                echo '$("#pages_c").prop("checked", true);';
                                                if($item=='R')
                                                echo '$("#pages_r").prop("checked", true);';
                                                if($item=='U')
                                                echo '$("#pages_u").prop("checked", true);';
                                                if($item=='D')
                                                echo '$("#pages_d").prop("checked", true);';
                                            }
                                            ?>
                                        </script>
                                    </div>
                            </div>
                             <div class="form-group col-md-4">
                                <label>Settings <span style="color:#f00">*</span></label>
                                    <div class="container-fluid">
                                        <?php
                                        $settings = explode(",", $level->otoritas_settings);
                                        ?>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="settings_c" name="settings_c" value="C">
                                        <label class="form-check-label">Create</label>
                                        <input class="form-check-input" type="checkbox" id="settings_r" name="settings_r" value="R">
                                        <label class="form-check-label">Read</label>
                                        <input class="form-check-input" type="checkbox" id="settings_u" name="settings_u" value="U">
                                        <label class="form-check-label">Update</label>
                                        <input class="form-check-input" type="checkbox" id="settings_d" name="settings_d" value="D">
                                        <label class="form-check-label">Delete</label>
                                    </div>
                                        <script>
                                            <?php
                                            $no = 0;
                                            foreach($settings as $item){
                                                $no++;
                                                if($item=='C')
                                                echo '$("#settings_c").prop("checked", true);';
                                                if($item=='R')
                                                echo '$("#settings_r").prop("checked", true);';
                                                if($item=='U')
                                                echo '$("#settings_u").prop("checked", true);';
                                                if($item=='D')
                                                echo '$("#settings_d").prop("checked", true);';
                                            }
                                            ?>
                                        </script>
                                    </div>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Themes <span style="color:#f00">*</span></label>
                                    <div class="container-fluid">
                                        <?php
                                        $themes = explode(",", $level->otoritas_themes);
                                        ?>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="themes_c" name="themes_c" value="C">
                                        <label class="form-check-label">Create</label>
                                        <input class="form-check-input" type="checkbox" id="themes_r" name="themes_r" value="R">
                                        <label class="form-check-label">Read</label>
                                        <input class="form-check-input" type="checkbox" id="themes_u" name="themes_u" value="U">
                                        <label class="form-check-label">Update</label>
                                        <input class="form-check-input" type="checkbox" id="themes_d" name="themes_d" value="D">
                                        <label class="form-check-label">Delete</label>
                                    </div>
                                        <script>
                                            <?php
                                            $no = 0;
                                            foreach($themes as $item){
                                                $no++;
                                                if($item=='C')
                                                echo '$("#themes_c").prop("checked", true);';
                                                if($item=='R')
                                                echo '$("#themes_r").prop("checked", true);';
                                                if($item=='U')
                                                echo '$("#themes_u").prop("checked", true);';
                                                if($item=='D')
                                                echo '$("#themes_d").prop("checked", true);';
                                            }
                                            ?>
                                        </script>
                                    </div>
                            </div>
                            <div class="form-group col-md-4">
                                <label>About Us <span style="color:#f00">*</span></label>
                                    <div class="container-fluid">
                                        <?php
                                        $about_us = explode(",", $level->otoritas_about_us);
                                        ?>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="about_us_c" name="about_us_c" value="C">
                                        <label class="form-check-label">Create</label>
                                        <input class="form-check-input" type="checkbox" id="about_us_r" name="about_us_r" value="R">
                                        <label class="form-check-label">Read</label>
                                        <input class="form-check-input" type="checkbox" id="about_us_u" name="about_us_u" value="U">
                                        <label class="form-check-label">Update</label>
                                        <input class="form-check-input" type="checkbox" id="about_us_d" name="about_us_d" value="D">
                                        <label class="form-check-label">Delete</label>
                                    </div>
                                        <script>
                                            <?php
                                            $no = 0;
                                            foreach($about_us as $item){
                                                $no++;
                                                if($item=='C')
                                                echo '$("#about_us_c").prop("checked", true);';
                                                if($item=='R')
                                                echo '$("#about_us_r").prop("checked", true);';
                                                if($item=='U')
                                                echo '$("#about_us_u").prop("checked", true);';
                                                if($item=='D')
                                                echo '$("#about_us_d").prop("checked", true);';
                                            }
                                            ?>
                                        </script>
                                    </div>
                            </div> 
                            <div class="form-group col-md-4">
                                <label>Member <span style="color:#f00">*</span></label>
                                    <div class="container-fluid">
                                        <?php
                                        $member = explode(",", $level->otoritas_member);
                                        ?>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="member_c" name="member_c" value="C">
                                        <label class="form-check-label">Create</label>
                                        <input class="form-check-input" type="checkbox" id="member_r" name="member_r" value="R">
                                        <label class="form-check-label">Read</label>
                                        <input class="form-check-input" type="checkbox" id="member_u" name="member_u" value="U">
                                        <label class="form-check-label">Update</label>
                                        <input class="form-check-input" type="checkbox" id="member_d" name="member_d" value="D">
                                        <label class="form-check-label">Delete</label>
                                    </div>
                                        <script>
                                            <?php
                                            $no = 0;
                                            foreach($member as $item){
                                                $no++;
                                                if($item=='C')
                                                echo '$("#member_c").prop("checked", true);';
                                                if($item=='R')
                                                echo '$("#member_r").prop("checked", true);';
                                                if($item=='U')
                                                echo '$("#member_u").prop("checked", true);';
                                                if($item=='D')
                                                echo '$("#member_d").prop("checked", true);';
                                            }
                                            ?>
                                        </script>
                                    </div>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Subscribe<span style="color:#f00">*</span></label>
                                <div class="container-fluid">
                                    <?php
                                        $subscribe = explode(",", $level->otoritas_subscribe);
                                        ?>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="subscribe_c" name="subscribe_c" value="C">
                                        <label class="form-check-label">Create</label>
                                        <input class="form-check-input" type="checkbox" id="subscribe_r" name="subscribe_r" value="R">
                                        <label class="form-check-label">Read</label>
                                        <input class="form-check-input" type="checkbox" id="subscribe_u" name="subscribe_u" value="U">
                                        <label class="form-check-label">Update</label>
                                        <input class="form-check-input" type="checkbox" id="subscribe_d" name="subscribe_d" value="D">
                                        <label class="form-check-label">Delete</label>
                                    </div>
                                    <script>
                                            <?php
                                            $no = 0;
                                            foreach($subscribe as $item){
                                                $no++;
                                                if($item=='C')
                                                echo '$("#subscribe_c").prop("checked", true);';
                                                if($item=='R')
                                                echo '$("#subscribe_r").prop("checked", true);';
                                                if($item=='U')
                                                echo '$("#subscribe_u").prop("checked", true);';
                                                if($item=='D')
                                                echo '$("#subscribe_d").prop("checked", true);';
                                            }
                                            ?>
                                        </script>
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                    <div class="container-fluid">
                                        <label class="form-check-label"> </label>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" name="about_us_c" onclick="toggle(this);" value="C">
                                        <label class="form-check-label">Check All</label>
                                    </div>
                                    </div>
                            </div>
                            <div class="footer-form col-md-12">
                                <button type="submit" class="btn btn-success">Simpan</button>
                            </div>
            </form>
        
        </div>
    </div>
    

</section>