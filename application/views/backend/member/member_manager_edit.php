<section class="content list-content">
    <div class="col-md-12 pos-con">
        <div class="head-title">
            <h2><span class="fa fa-pencil" style="padding-right:10px"></span> Edit Member</h2>
            <hr>
        </div>
        <a href="<?php echo base_url('administrator/member/member_manager');?>" class="btn btn-primary"><span class="fa fa-arrow-left"></span> Kembali</a>
        <div class="col-md-12 datatble-content">
            <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/member_edit_process');?>" enctype="multipart/form-data">
                            <input name="id" type="hidden" value="<?php echo $member_edit->id;?>">
                            <div class="form-group">
                              <label for="email">Name</label>
                              <input type="text" class="form-control" id="title" name="name" aria-describedby="emailHelp" placeholder="" maxlength="150"  value="<?php echo $member_edit->name;?>" required>
                            </div> <div class="form-group">
                              <label for="email">Email</label>
                              <input type="text" class="form-control" id="title" name="email" aria-describedby="emailHelp" placeholder="" maxlength="150"  value="<?php echo $member_edit->email;?>" required>
                            </div>
                            <div class="form-group">
                              <label for="password">Password</label>
                              <input type="password" class="form-control" id="title" name="password" aria-describedby="emailHelp" placeholder="Masukkan Password baru jika ingin diubah" maxlength="150"  value="" >
                            </div>
                            <div class="form-group">
                              <label for="no_telepon">No. Telepon</label>
                              <input type="text" class="form-control" id="title" name="no_telepon" aria-describedby="emailHelp" placeholder="" maxlength="150"  value="<?php echo $member_edit->no_telepon;?>" required>
                            </div>
                            <div class="form-group">
                              <label for="id_level">ID Level</label>
                              <select class="form-control" name="id_level" id="id_level" aria-describedby="emailHelp" placeholder="Type TItle" maxlength="150"  value="" required>
                                  <option value="">Pilih Level</option>
                                  <?php
                                  $level_list = get_list_level();
                                  if($level_list!=false){ 
                                      foreach($level_list->result() as $level){
                                  ?>
                                  <option value="<?php echo $level->id_level;?>"><?php echo $level->nama_level;?></option>
                                  <?php }} else echo '<option value="">Tidak ada pilihan level</option>';
                                  ?>
                                </select>
                                <script>
                                $(document).ready(function(){
                                   $("#id_level").val("<?php echo $member_edit->id_level;?>").change(); 
                                });
                                </script>
                            </div>
                            <div class="footer-form">
                                <button type="submit" class="btn btn-success">Simpan</button>
                            </div>
            </form>
        
        </div>
    </div>
</section>