<section class="content list-content">
    <div class="col-md-12 pos-con">
        <div class="head-title">
            <h2><span class="fa fa-pencil" style="padding-right:10px"></span> Edit News</h2>
            <hr>
        </div>
        <a href="<?php echo base_url('administrator/news_update/news_manager');?>" class="btn btn-primary"><span class="fa fa-arrow-left"></span> Kembali</a>
        <div class="col-md-12 datatble-content">
            <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/news_edit_process');?>" enctype="multipart/form-data">
                <input name="id" type="hidden" value="<?php echo $updates_article->id;?>">
                <div class="form-group">
                  <label for="judulcampaign">Judul<span style="color:#f00">*</span></label>
                  <input type="text" class="form-control" id="title" name="title" aria-describedby="emailHelp" placeholder="Masukkan Judul" maxlength="100"  value="<?php echo $updates_article->title;?>" required>
                </div>
                <div class="form-group">
                  <label for="batas_waktu_campaign">Content Body<span style="color:#f00">*</span></label>
                    <textarea class="summernote" name="content" rows="100"><?php echo $updates_article->content;?></textarea>

                </div>
                            <div class="form-group">
                                <label>Thumbnail<span style="color:#f00">*</span> </label>
                            <div class="picture-wrapper">
                                    <img src="<?php echo (empty($updates_article->image_feature)?'assets/img/no-image.jpg':base_url($updates_article->image_feature));?>" class="change_picture profile-picture picture-src" id="change_picture" data-file="profilepic">
                                    <input class="file_input_logo hide" id="profilepic" type="file" accept="image/png, image/jpeg, image/gif" name="file" accept="image/*" onchange="imagepreview(this, 'change_picture')">
                                </div>
                            </div>
                <div class="footer-form"><br>
                  <div>
                    <button type="submit" class="btn btn-success">Simpan</button>
                  </div>
                </div>
            </form>
        </div>
    </div>
</section>
<script>
    $(document).ready(function(){
       $("#status").val('<?php echo $member_edit->status;?>').change(); 
    });
</script>