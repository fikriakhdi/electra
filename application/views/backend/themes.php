<?php 
$otoritas_det = explode(",", $otoritas->otoritas_themes);
$c = (strpos($otoritas->otoritas_themes, "C")===false?0:1);
$r = (strpos($otoritas->otoritas_themes, "R")===false?0:1);
$u = (strpos($otoritas->otoritas_themes, "U")===false?0:1);
$d = (strpos($otoritas->otoritas_themes, "D")===false?0:1);
?>
<section class="content list-content">
    <div class="col-md-12 pos-con">
        <div class="head-title">
            <h2><span class="fa fa-themeisle" style="padding-right:10px"></span> Themes</h2>
            <hr>
        </div>
        <!--home-content-top starts from here-->
        <section class="home-content-top">
            <!--our-quality-shadow-->
            <?php if(!empty($this->session->userdata('message'))) echo $this->session->userdata('message');?>
            <div class="clearfix">
                <div class="tabbable-panel margin-tops4  datatble-content">
                    <div class="tabbable-line">
                        <ul class="nav nav-tabs tabtop  tabsetting" class="align=center">
                    <?php if($r==1) { ?> <li> <a id="menu_1" href="#tab_default_1" data-toggle="tab">Themes List</a> </li><?php } ?>
                    <?php if($c==1) { ?> <li> <a id="menu_2" href="#tab_default_2" data-toggle="tab"> Add Themes</a> </li><?php } ?>
                        </ul>
                        <div class="tab-content margin-tops">
                            <!--Tab1-->
                            <?php if($r==1) { ?>
                            <div class="tab-pane active fade in" id="tab_default_1">

                                <form class="login100-form validate-form" method="post" enctype="multipart/form-data">
                                    <div class="col-md-12 datatble-content">
                                        <div class="content-datatable table-responsive">
                                            <table id="example" class="table table-striped table-bordered datatable" style="width:100%">
                                                <thead>
                                                    <tr class="title-datable">
                                                        <th>No</th>
                                                        <th>Nama</th>
                                                        <th>Preview</th>
                                                        <?php if($u==1) { ?>
                                                        <th>Set</th>
                                                        <?php } ?>
                                                        <?php if($d==1) { ?>
                                                        <th>Hapus</th>
                                                        <?php } ?>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $set = "<a href='".base_url('backend/set_themes/default')."' class='btn btn-success'>Set</a>";
                                                    $already_set = "<a href='#' class='btn btn-success' disabled>Currently set</a>";
                                                    ?>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>Electra Default</td>
                                                        <td><a url="<?php echo base_url('assets/default/');?>" data-toggle="modal" data-target="#previewModal" class="btn btn-primary">Preview</a> </td>
                                                        <?php if($u==1) { ?>
                                                        <td>
                                                            <?php echo (get_settings('themes')=='default'?$already_set:$set);?>
                                                        </td>
                                                        <?php } ?>
                                                        <?php if($d==1) {?>
                                                        <td></td>
                                                        <?php } ?>
                                                    </tr>
                                                    <?php 
                                          $themes = get_themes_list();
                                          if($themes!=false){
                                              $num=1;
                                              foreach($themes->result() as $theme){
                                                  $num++;
                                                  $set = "<a href='".base_url('backend/set_themes/'.$theme->id)."' class='btn btn-success'>Set</a>";
                                                  $already_set = "<a href='#' class='btn btn-success' disabled>Currently set</a>";
                                          ?>
                                                    <tr>
                                                        <td>
                                                            <?php echo $num++;?>
                                                        </td>
                                                        <td>
                                                            <?php echo $theme->themes_name;?>
                                                        </td>
                                                        <td><a url="<?php echo base_url('assets/'.$theme->themes_name);?>" data-toggle="modal" data-target="#previewModal" class="btn btn-primary">Preview</a> </td>
                                                        <?php if($u==1) { ?>
                                                        <td>
                                                            <?php echo (get_settings('themes')==$theme->themes_name?$already_set:$set);?>
                                                        </td>
                                                        <?php } ?>
                                                        <?php if($d==1) { ?>
                                                        <td>
                                                            <p data-placement="top" data-toggle="tooltip" title="Hapus"><button type="button" class="btn btn-xs btn-danger delete_btn" data-toggle="modal" data-target="#deleteModal" id_url="<?php echo base_url('backend/delete_theme/'.$theme->id);?>"><span class="glyphicon glyphicon-trash"></span></button></p>
                                                        </td>
                                                        <?php } ?>
                                                    </tr>
                                                    <?php }}?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <?php } ?>
                            <!-- Button trigger modal -->




                            <!-- END Tab1-->
                            <?php if($c==1) { ?>
                            <div class="tab-pane fade" id="tab_default_2">
                                <div class="col-md-12 datatble-content">
                                    <form class="login100-form validate-form" method="post" action="<?php echo base_url('backend/create_theme');?>" enctype="multipart/form-data">
                                        <input name="id" type="hidden" value="">
                                        <div class="form-group">
                                            <label for="jumlah_dana_campaign">Theme File<span style="color:#f00">*</span></label>
                                            <div class="input-group image-preview">
                                                <input type="text" class="form-control image-preview-filename" disabled="disabled" > <!-- don't give a name === doesn't send on POST/GET -->
                                                <span class="input-group-btn">
                                                    <!-- image-preview-clear button -->
                                                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                                        <span class="glyphicon glyphicon-remove"></span> Clear
                                                    </button>
                                                    <!-- image-preview-input -->
                                                    <div class="btn btn-default image-preview-input">
                                                        <span class="glyphicon glyphicon-folder-open"></span>
                                                        <span class="image-preview-input-title">Browse</span>
                                                        <input type="file" accept=".zip" name="file" required/> <!-- rename it -->
                                                    </div>
                                                </span>
                                            </div>
                                        </div>

                                        <div class="footer-form">
                                                <button type="submit" class="btn btn-success">Install</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>

        </section>
        <div id="deleteModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Delete Data</h4>
                    </div>
                    <div class="modal-body">
                        Aoakah anda yakin untuk menghapus data ini
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                        <a class="btn btn-danger" id="delete_footer" href="">Ya</a>
                    </div>
                </div>
            </div>
        </div>
        <div id="previewModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content" >
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Preview Themes</h4>
                    </div>
                    <div class="modal-body" id="previewBody">
                        <div id="myCarousel" class="carousel slide" data-ride="carousel">
                          <!-- Indicators -->
                          <ol class="carousel-indicators">
                            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel" data-slide-to="1"></li>
                            <li data-target="#myCarousel" data-slide-to="2"></li>
                          </ol>

                          <!-- Wrapper for slides -->
                          <div class="carousel-inner">
                            <div class="item active">
                              <img src="https://www.w3schools.com/bootstrap/la.jpg" alt="Los Angeles">
                            </div>

                            <div class="item">
                              <img src="https://www.w3schools.com/bootstrap/la.jpg" alt="Chicago">
                            </div>

                            <div class="item">
                              <img src="https://www.w3schools.com/bootstrap/la.jpg" alt="New York">
                            </div>
                          </div>

                          <!-- Left and right controls -->
                          <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                            <span class="sr-only">Previous</span>
                          </a>
                          <a class="right carousel-control" href="#myCarousel" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                            <span class="sr-only">Next</span>
                          </a>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!--home-content-top ends here-->
    </div>
</section>
                      <?php
                      if($r==1 && $c==1) { ?>
                      <script>
                          $(document).ready(function(){
                      $("#menu_1").click();
                              });
                      </script>
                      <?php } else if($r==1) { ?>
                      <script>
                          $(document).ready(function(){
                      $("#menu_1").click();
                          });
                      </script>
                      <?php }  else if($c==1) { ?>
                      <script>
                          $(document).ready(function(){
                      $("#menu_2").click();
                              });
                      </script>
                      <?php } ?>