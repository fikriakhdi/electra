<section class="content list-content">
  <div class="col-md-12 pos-con">
    <div class="head-title">
      <h2><span class="fa fa-list"style="padding-right:10px"></span> List Subscriber</h2>
      <hr>
    </div>
      <?php if(!empty($this->session->userdata('message'))) echo $this->session->userdata('message');?>
    <div class="col-md-12 datatble-content">
      <div class="content-datatable table-responsive">
        <table id="example" class="table table-striped table-bordered" style="width:100%">
          <thead>
            <tr class="title-datable">
              <th>NO</th>
              <th>Email</th>
              <th>Edit</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
              <?php 
              $subscriber_list = get_subscriber_list();
              if($subscriber_list!=false){
                  $num=0;
                  foreach($subscriber_list->result() as $subscriber){
                      $num++;
              ?>
            <tr>
              <td><?php echo $num++;?></td>
              <td><?php echo $subscriber->email;?></td>
                <td><p data-placement="top" data-toggle="tooltip" title="Edit"><a href="<?php echo base_url('backend/subscriber_edit/'.$subscriber->id);?>" class="btn btn-primary btn-xs" data-title="Edit"  ><span class="glyphicon glyphicon-pencil"></span></a></p></td>
                <td><p data-placement="top" data-toggle="tooltip" title="Hapus"><button class="btn btn-danger btn-xs delete_btn" data-toggle="modal" data-target="#delete_modal" id_url="<?php echo base_url('backend/delete_subscriber/'. $subscriber->id);?>"><span class="glyphicon glyphicon-trash"></span></button></p></td>
            </tr>
              <?php }}?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</section>
<div id="delete_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Data</h4>
        </div>
        <div class="modal-body">
          Aoakah anda yakin untuk menghapus data ini
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
          <a  class="btn btn-danger" id="delete_footer" href="#">Ya</a>
        </div>
      </div>
    </div>
  </div>
<style>
  .table-striped>tbody>tr:nth-of-type(odd) {
    background:#d2d2d2;
  }
</style>