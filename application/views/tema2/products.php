<!--
  Jakarta : 17/10/2018
  create By : Maningcorp
-->
<!--DOCTYPE HTML-->
<style>
  .body ul {
    padding: 0;
    font-size: 18px;
    list-style-image: url(<?php echo BASE_URL;?>/assets/img/icon/list-style.png);
  }
</style>
<div class="container-fluid">
  <!--open banner product content-->
  <section class=" row banner-pos-cont products-backgroud" id="banner-content" data-speed="4" style="background-image: url(<?php echo base_url($products->header_image);?>);">
    <div class="container-fluid content-firs">
    </div>
    <div class="col-md-12 banner-form-text-product header-caption">
      <h1><?php echo $products->name;?></h1>
    </div>
  </section>
  <!--close banner product slide content-->

  <!--open product owner content-->
  <section class="row" id="product-owner-content">
    <div class="section-1-pro content-1-pro-tema2">
        <div class="col-md-12 pro-cont-tema2" data-aos="fade-up" data-aos-duration="500">
          <div class="post-list">
            <div class="head">
                <h3><?php echo $this->lang->line('benefit_for'); echo '<b> '.$this->lang->line('product_owner').'</b>';?></h3>
            </div>
            <div class="body">
              <ul>
                <?php
                  $benefit_for_product_owner = explode(";", $products->benefits_product_owner);
                  foreach($benefit_for_product_owner as $item){
                  ?>
                  <li><?php echo $item;?></li>
                  <?php } ?>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="section-1-pro content-2-pro-tema2">
        <div class="col-md-12 post-con-image">
          <div class="post-img">
            <img src="<?php echo BASE_URL;?>/assets/img/icon/electraios-01.png">
          </div>
            <div class="screen-content1 screen-content-scroll" style="background-image:url(<?php echo BASE_URL.$products->desktop_image;?>)">
              </div>
        </div>
      </div>
    
    <!--close container-->
  </section>
  <!--close product owner content-->
  
  
   <!--open travel agent content-->
  <section class="row" id="product-agent-content">
      <div class="section-1-pro content-1-pro-tema2">
        <div class="col-md-12 pro-cont-tema2" data-aos="fade-right" data-aos-duration="500">
          <div class="post-list">
            <div class="head">
              <h3><?php echo $this->lang->line('benefit_for'); echo '<b> '.$this->lang->line('travel_agent').'</b>';?></h3>
            </div>
            <div class="body">
              <ul>
                <?php
                  $benefit_for_travel_agent = explode(";", $products->benefits_travel_agent);
                  foreach($benefit_for_travel_agent as $item){
                  ?>
                  <li><?php echo $item;?></li>
                  <?php } ?>
              </ul>
            </div>
          </div>
        </div>
      </div>
        
      <div class="section-1-pro content-2-pro-tema2">
        <div class="col-md-12 post-con-image">
          <div class="post-img">
            <img src="<?php echo BASE_URL;?>/assets/img/icon/electraios-02.png" class="laptop">
              <div class="screen-content2 screen-content-scroll" style="background-image:url(<?php echo BASE_URL.$products->desktop_image;?>)">
              </div>
            <div class="iphone-pos">
              <img src="<?php echo BASE_URL;?>/assets/img/icon/electraios-03.png" class="iphone">
                <div class="screen-content3 screen-content-scroll" style="background-image:url(<?php echo BASE_URL.$products->mobile_image;?>)">
                </div>
            </div>
          </div>
        </div>
      </div>

      
  </section>
  <!--close travel agen content-->

  <!--open Our Key Feature content-->
    <?php 
      $key_feature = get_list_key_feature($products->id);
      if($key_feature!=false){
      ?>
  <section class=" row why-pos-cont" id="our-key-feature-content">
    <div class="container">
      <div class="col-md-12">
        <div class="title-why">
          <h2 class="title-all-h2">
            <?php if($this->session->userdata('site_lang')=='english'){ ?>
            <?php echo $this->lang->line('our'); echo '<b>'.$this->lang->line('key_feature').'</b>';?>
            <?php } else { ?> 
                <?php echo $this->lang->line('channel'); echo $this->lang->line('our');?>
          <?php } ?>
          </h2>
        </div>
          <?php
            foreach($key_feature->result() as $key_feature){
              echo '
                <div class="col-sm-3">
                  <div class="pos-icon-key-tema2">
                    <div class="head"> 
                      <img src="'.base_url($key_feature->icon).'">
                    </div>
                    <div class="body">
                      <h4>'.$key_feature->title.'</h4>
                    </div>
                  </div>
                </div>
            ';
            } ?>
        <div class="col-sm-3">
          <div class="pos-icon-key-tema2">
            <div class="head">
            <img src="<?php echo BASE_URL;?>assets/img/icon/ICON-ELECTRA-10v2.png">
            </div>
            <div class="body">
              <h4>System Integration</h4>
            </div>
          </div>
        </div>

        <div class="col-sm-3">
          <div class="pos-icon-key-tema2">
            <div class="head">
                <img src="<?php echo BASE_URL;?>assets/img/icon/ICON-ELECTRA-11v2.png">
            </div>
            <div class="body">
              <h4>Channel Distribution</h4>
            </div>
          </div>
        </div>
        
        <div class="col-sm-3">
          <div class="pos-icon-key-tema2">
            <div class="head">
                <img src="<?php echo BASE_URL;?>assets/img/icon/ICON-ELECTRA-12v2.png">
            </div>
            <div class="body">
              <h4>Auto Refund</h4>
            </div>
          </div>
        </div>
      </div>
      
      <div class="col-md-12 post-3-icon">
          <br><br>
       <div class="col-md-3 pos-icon-pro" >
          <div class="pos-icon-key-tema2" style="margin-left:50%">
            <div class="head">
                <img src="<?php echo BASE_URL;?>assets/img/icon/ICON-ELECTRA-13.png">
            </div>
            <div class="body">
              <h4>Departure Control System</h4>
            </div>
          </div>
        </div>
        
        <div class="col-md-3 pos-icon-pro" >
          <div class="pos-icon-key-tema2" >
            <div class="head">
                <img src="<?php echo BASE_URL;?>assets/img/icon/ICON-ELECTRA-14.png">
            </div>
            <div class="body">
              <h4>B2B</h4>
            </div>
          </div>
        </div>
        
        <div class="col-md-3 pos-icon-pro">
          <div class="pos-icon-key-tema2" style="margin-right:50%">
            <div class="head">
                <img src="<?php echo BASE_URL;?>assets/img/icon/ICON-ELECTRA-15.png">
            </div>
            <div class="body">
              <h4>B2C</h4>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
     <?php 
                    }
          ?> 
  <!--close  Our Key Feature content-->

  <!--open our clients content-->
  <section class=" row clients-pos-cont" id="our-clients-content">
    <div class="container-fluid full-width">
      <div class="post-video">
        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/tWsNi9w0VWk?rel=0&amp;autoplay=1&loop=1&playlist=kC1jy2se9mM&mute=1&amp;controls=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
      </div>
    </div>
  </section>

  <!--open testimony slide content-->
  <section class="row testimony-pos-cont" id="banner-content">
      <div class="cont-form">
        <div class="title-why">
          <h2 class="title-all-h2">Interested With <b>Our Product?</b></h2>
        </div>
        <div class="pos-form-product">
          <form method="post" action="">
             <div class="form-group"> 
                <label class="label1">To: <?php echo get_settings('company_email');?></label>
              </div>
               <div class="form-group"> 
                  <input type="text" class="form-controls" id="name" name="name" required placeholder="Your Name">
                </div>
               <div class="form-group">
                   <input type="email" class="form-controls" id="email" name="email" required placeholder="Your Email">
                </div>
                <div class="form-group">
                    <textarea  class="form-controls" id="message" name="message" required placeholder="Your Message"></textarea>
                </div>
              <button class="btn btn-transparent" type="submit">Submit</button>
          </form>
        </div>
      </div>
  </section>
  <!--close testimony slide content-->
</div>
<!--close container-->