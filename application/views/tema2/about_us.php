<div class="position-aboutus-tema2">
    
<div class="position-content-tema2-about-us">
  <section class="page-section">
    <div class="image" data-type="background" data-speed="7" style="background-image: url(<?php echo base_url($about->who_are_we_pic);?>);filter:brightness(50%)"></div>
    <div class="stuff" data-type="content"><h2>Who Are We ?</h2><p><?php echo $about->who_are_we_content;?></p></div>
  </section>

  <section class="page-section">
    <div class="image" data-type="background" data-speed="6" style="background-image: url(<?php echo base_url($about->what_we_do_pic);?>);filter:brightness(50%)"></div>
    <div class="stuff" data-type="content">
      <h1>What We Do ?</h1>
      <p><?php echo $about->what_we_do_content;?></p>
    </div>
  </section>

  <section class="page-section">
    <div class="image" data-type="background" data-speed="5" style="background-image: url(<?php echo base_url($about->who_do_we_work_with_pic);?>);filter:brightness(50%)"></div>
    <div class="stuff" data-type="content">
      <h1>Who Do We Work With ?</h1>
       <p><?php echo base_url($about->who_do_we_work_with_content);?></p>
      <a href="#">Our Client</a><hr>
    </div>
  </section>

  <section class="page-section">
    <div class="image" data-type="background" data-speed="3" style="background-image: url(<?php echo base_url($about->what_we_can_do_for_you_pic);?>);filter:brightness(50%)"></div>
    <div class="stuff" data-type="content">
      <h1>What We Can Do For You ?</h1>
      <p><?php echo base_url($about->what_we_can_do_for_you_content);?></p>
      <a href="#">Our Product</a><hr>
    </div>
  </section>
</div>
  
  
  
<!-----------------close about content view for pc--------------------------->
  
  <!--open testimony slide content-->
  <section class="row testimony-pos-cont" id="banner-content">
      <div class="cont-form">
        <div class="title-why">
          <h2 class="title-all-h2">Interested With <b>Our Product?</b></h2>
        </div>
        <div class="pos-form-product">
          <form method="post" action="">
             <div class="form-group"> 
                <label class="label1">To: <?php echo get_settings('company_email');?></label>
              </div>
               <div class="form-group"> 
                  <input type="text" class="form-controls" id="name" name="name" required placeholder="Your Name">
                </div>
               <div class="form-group">
                   <input type="email" class="form-controls" id="email" name="email" required placeholder="Your Email">
                </div>
                <div class="form-group">
                    <textarea  class="form-controls" id="message" name="message" required placeholder="Your Message"></textarea>
                </div>
              <button class="btn btn-transparent" type="submit">Submit</button>
          </form>
        </div>
      </div>
  </section>
  <!--close testimony slide content-->
</div>
            