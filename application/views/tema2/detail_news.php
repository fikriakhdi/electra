<div class="container">
  <section class="content-news-tema2">
    <div class="head">
      <div class="img-pos">
        <div class="img-news" style="background-image: url(<?php echo base_url($detail_news->image_feature);?>);"></div>
      </div>
      <div class="share-and-title-pos">
        <div class="right">
          <h3><?php echo $detail_news->title;?></h3>
          <span><?php echo date('d M Y', strtotime($detail_news->datecreated));?></span>
        </div>
        <div class="left">
          <ul>
            <li><a href="https://www.facebook.com/sharer.php?u=<?php echo base_url('detail_news/'.$detail_news->seo);?>&t=<?php echo $detail_news->title;?>" target="_blank"><span class="fa fa-facebook face"></span></a></li>
            <li><a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo base_url('detail_news/'.$detail_news->seo);?>&title=<?php echo $detail_news->title;?>&source=<?php echo COMPANY_NAME;?>" target="_blank"><span class="fa fa-linkedin linkdin"></span></a></li>
            <li><a href="http://twitter.com/share?text=<?php echo $detail_news->title;?>&url=http://<?php echo base_url('detail_news/'.$detail_news->seo);?>&hashtags=#<?php echo COMPANY_NAME;?>"><span class="fa fa-twitter email" target="_blank"></span></a></li>
            <li><a class="copy-btn" data-clipboard-text="<?php echo base_url('detail_news/'.$detail_news->seo);?>"><span class="fa fa-link link"></span></a></li>
          </ul>
        </div>
      </div>
    </div>
    <hr class="line">
    <div class="body">
        <?php echo $detail_news->content;?>
    </div>
  </section>
</div>