<div class="container-fluid" id="hello_electra_tema2">
    <section class="page-section" id="content-hello-electra_tema2">
        <div class="col-md-6 contact_tema2">
          <div class="hello_electra_map" id="pos-con-map">
              <div id="map"></div>
          </div>
          <div class="infopos">
            <button id="btn1" class="buttonProps red" onclick="toggleColor()">
            <div class="head">
              <a>Address</a>
            </div>
            <div class="body">
              
              <ul>
                <li class="text-tit">Head Office</li>
                <li><span class="fa fa-map-marker"></span><p>Jalan Mampang Perapatan Raya No. 93 tegal Parang Mampang Perapatan, Jakarta Selatan DKI Jakarta Indonesia</p> </li>
              </ul>
            </div>
              </button>
          </div>
          <div class="infopos-mob">
            <button id="btn2" class="buttonProps-mob red1" onclick="toggleColor1()">
            <div class="head">
              <a>Address</a>
            </div>
            <div class="body">
              
              <ul>
                <li class="text-tit">Head Office</li>
                <li><span class="fa fa-map-marker"></span><p>Jalan Mampang Perapatan Raya No. 93 tegal Parang Mampang Perapatan, Jakarta Selatan DKI Jakarta Indonesia</p> </li>
              </ul>
            </div>
              </button>
          </div>
        </div>
        <div class="col-md-6 drop_message_tema2">
           
          <div class="pos-form-product-tema2">
            <h3>Send Your Message</h3>
          <form method="post" action="">
             <div class="form-group"> 
                <label class="label1">To: <?php echo get_settings('company_email');?></label>
              </div>
               <div class="form-group"> 
                  <input type="text" class="form-controls" id="name" name="name" required placeholder="Your Name">
                </div>
               <div class="form-group">
                   <input type="email" class="form-controls" id="email" name="email" required placeholder="Your Email">
                </div>
                <div class="form-group">
                    <textarea  class="form-controls" id="message" name="message" required placeholder="Your Message"></textarea>
                </div>
              <button class="btn btn-transparent" type="submit">Submit</button>
          </form>
        </div>
        </div>
    </section>
</div><!--close container fluid-->
  <!--javascript for map-->
  <script src="http://maps.google.com/maps/api/js?key=AIzaSyD98vdNV_tuiFcWI1C_9df63zTjznqTQtU&sensor=false&libraries=places" type="text/javascript"></script>
<script type="text/javascript">
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 15,
    center: new google.maps.LatLng(<?php echo get_settings('latitude');?>, <?php echo get_settings('longitude');?>),
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });
  var position={lat: <?php echo get_settings('latitude');?>, lng: <?php echo get_settings('longitude');?>};
   var marker=new google.maps.Marker({
        map: map,
        // icon: icon,
        title: "Sabre Indonesia",
        position: position,
        draggable: false
      });
</script>