<!--
  Jakarta : 17/10/2018
  create By : Maningcorp
-->
<!--DOCTYPE HTML-->
<div class="container-fluid" id="container_body" color="transparent">
  <!--open header content-->
  <section class="row header-pos-cont">
    <div class="col-md-12 header" id="header">
    </div>
  </section>
  <!--close header content-->

  <!--open banner slide content-->
  <section class="row banner-pos-cont page-section" id="banner-content" data-speed="4">
    <div class="container-fluid content-firs">
      <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <?php 
            $slider = get_list_slider($this->session->userdata('lang_admin'));
            if($slider!=false){
            for($i=0;$i<$slider->num_rows();$i++){
            ?>
          <li data-target="#myCarousel" data-slide-to="<?php echo $i;?>" class="<?php echo ($i==0?'active':'');?>"></li>
            <?php }}?>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner header-carousel">
            <?php 
            $slider = get_list_slider($this->session->userdata('lang_admin'));
            $i=0;
            if($slider!=false){
            foreach($slider->result() as $slide){
                $i++;
            ?>
            <div class="item <?php echo ($i==1?'active':'');?>">
            <div class="slider-banner" data-speed="4"></div>
            <img src="<?php echo base_url($slide->picture);?>" alt="slider 1" style="width:100%;filter:brightness(80%);">
          </div>
            <?php }}?>
        </div>
<!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>           
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
      </div>
    </div>

    <div class="banner-form-text d-sm-none">
      <div class="content-left">
        <div class="pos-text-banner header-caption">
          <h1>Behind Every Great Travel Company</h1>
          <p>We understand every travel industry need to keep up with the pace of change in the digital era in order to appealto customer demand.
          </p>
        </div>
      </div>
    </div>
      <div class="btn-scroll-to-explore">
          <a href="#about-us-content" data-text="scroll to explore" class="scrollTo effect-shine"></a>
      </div>
    
  </section>
  <!--close banner slide content-->

  <!--open about us content-->
  <section class="row about-pos-cont2 background-color page-section" id="about-us-content" style="background-image:url(<?php echo BASE_URL;?>/assets/img/banner/slider2-01.jpg);">
    <div class="container-fluid content-firs">
      <div class="content-text about-us " data-aos="fade-up">
        <h3 style="font-weight: lighter; font-family: helvetica;"><?php echo $about->who_are_we_content;?></h3>
        <a href="<?php echo base_url('about_us');?>" class="button-all btn-about"><?php echo $this->lang->line('main_header_about_us');?></a>
      </div>
    </div>
  </section>
  <!--close about us content-->

  <!--open our product content-->
  <section class="row pro-pos-cont-tema2 page-section" id="our-product-content">
    <div class="container pos pro-con">
        <div class="size-for-mobile" data-aos="fade-up" data-aos-duration="200">
          <div class="card-pro-title">
            <?php if($this->session->userdata('site_lang')=='english'){ ?>
            <h2 class="title-pro-unborder"><?php echo $this->lang->line('our');?></h2>
            <h2 class="title-pro-border"><?php echo $this->lang->line('product');?></h2>
            <?php } else { ?> 
                <h2 class="title-pro-border"><?php echo $this->lang->line('product');?></h2>
            <h2 class="title-pro-unborder"><?php echo $this->lang->line('our');?></h2>
                <?php } ?>
          </div>
        </div>
        <?php
          $product = get_list_product(lang_convert($this->session->userdata('site_lang')));
          if($product!=false){
            foreach($product->result() as $item){
              echo '
                <div class="size-for-mobile" data-aos="fade-up" data-aos-duration="500" ">
                 <a href="'.base_url('products/'.$item->id).'">
                  <div class="card-pro pos-card-pro-tema2" style="background-image:url(\''.base_url($item->header_image).'\');background-size:cover;background-repeat: no-repeat;">
                    <div class=" card-pro-tema2">
                      <div class="head-card head-card-tema2">
                        <img src="'.base_url($item->icon).'" alt="responsive">
                      </div>
                      <div class="body-card body-card-tema2">
                        <h3>'.$item->name.'</h3>
                        <p>'.$item->description.'</p>
                      </div>
                      <div class="footer-card">
                        <a class="more" href="'.base_url('products/'.$item->id).'">'.$this->lang->line('see_more').'</a>
                      </div>
                    </div>
                  </div>
                  </a>
                </div> 
           ';
            }
          }
          ?>
    </div>
    <!--close container-->
  </section>
  <!--close our product content-->

  <!--open why choos us content-->
  <section class="row why-pos-cont-tema2 page-section" id="choose-us-content">
    <div class="pos pro-con-tema2">
      <div class="pos-tema2-cont">
        <div class="title-why">
            <h2 class="title-all-h2-tema2"><?php echo $this->lang->line('why');?><b> <?php echo $this->lang->line('chooseus');?></b></h2>
        </div>
         <?php
          $product = get_list_why_choose_us(lang_convert($this->session->userdata('site_lang')));
          if($product!=false){
              $num = 0;
            foreach($product->result() as $item){
                $num++;
                if ($num%2!=0){
                    echo'
                        <div class="size-for-mobile-tema2" data-aos="fade-up" data-aos-duration="200">
                          <div class="background-why-choose-us-tema2" style="background-image:url(\''.base_url($item->image).'\');background-size:cover;background-repeat: no-repeat;height:100%"></div>
                        </div>

                        <div class="size-for-mobile-tema2" data-aos="fade-up" data-aos-duration="500">
                            <div class="Text-why-choose-us-tema2">
                              <h3>'.$item->title.'</h3>
                              <p>'.$item->description.'</p>
                            </div>
                        </div>
                    ';
                } else { echo'
                        <div class="size-for-mobile-tema2" data-aos="fade-up" data-aos-duration="500">
                            <div class="Text-why-choose-us-tema2">
                              <h3>'.$item->title.'</h3>
                              <p>'.$item->description.'</p>
                            </div>
                        </div>
                        
                        <div class="size-for-mobile-tema2" data-aos="fade-up" data-aos-duration="200">
                          <div class="background-why-choose-us-tema2" style="background-image:url(\''.base_url($item->image).'\');background-size:cover;background-repeat: no-repeat;height:100%"></div>
                        </div>
                        ';
                }
            
           ;
            }
          }
          ?>
          

      </div>
    </div>
  </section>
  
  <!-----------open content why chose us for mobile-------------->
  <section class="content-for-mob-choseus" id="content-for-mobile-choseus-tema2">
    <div class="pos-card-tema2-chose">
      <div  data-aos="fade-up" data-aos-duration="200" class="pos-img" style="background-image:url(<?php echo BASE_URL;?>/assets/img/icon/img/Why-Choose-Us/5be73c4995f64.png);">
      </div>
      <div  data-aos="fade-up" data-aos-duration="200" class="pos-discrp">
        <h3>Sales Monitoring</h3>
        <p>Make it easy for you to identify all your guests and bookers at different periods, in order to make you stay ahead of the competition.</p>
      </div>
    </div>
    
     <div class="pos-card-tema2-chose">
      <div  data-aos="fade-up" data-aos-duration="200" class="pos-img" style="background-image:url(<?php echo BASE_URL;?>/assets/img/icon/img/Why-Choose-Us/5be54072734fe.jpg);">
      </div>
      <div  data-aos="fade-up" data-aos-duration="200" class="pos-discrp">
        <h3>Any Type of Customer</h3>
        <p>Our booking engine provides a platform where any type of your customer gets a great experience of online ticket bookings, whether they are a travel agency (B2B), direct customer (B2C), or a corporate (B2E).</p>
      </div>
    </div>
    
     <div class="pos-card-tema2-chose">
      <div  data-aos="fade-up" data-aos-duration="200" class="pos-img" style="background-image:url(<?php echo BASE_URL;?>/assets/img/icon/img/Why-Choose-Us/5bec5e5aa537a.jpg);">
      </div>
      <div  data-aos="fade-up" data-aos-duration="200" class="pos-discrp">
        <h3>Simple Process</h3>
        <p>Once you have captured your customer’s attention, it is important to make the actual booking process quick, simple and user friendly. Offering multiple methods of payment is one of the solution.</p>
      </div>
    </div>
    
  </section>
  <!-----------close content why chose us for mobile-------------->
  <!--close why choos us content-->
  
 
  <!--open our clients content-->
  <section class="row clients-pos-cont-tema2 page-section" id="our-clients-content">
    <div class="container pos pro-con">
      <div class="content-our-clients">
        <div class="title-why">
            <?php if($this->session->userdata('site_lang')=='english') {?>
            <h2 class="title-all-h2-tema2"><?php echo $this->lang->line('our');?> <b><?php echo $this->lang->line('client');?></b> </h2>
            <?php } else { ?>
            <h2 class="title-all-h2-tema2"><b><?php echo $this->lang->line('client');?></b> <?php echo $this->lang->line('our');?></h2>
            <?php } ?>
        </div>
        <?php
          $client = get_list_client();
          if($client!=false){
            foreach($client->result() as $item){
              echo '
              
              <div class="our-client-size-tema2 clients">
              <img src="'.base_url($item->client_logo).'" style="width: 65%;max-height: 45%;">
              </div>
           ';
            }
          }
          ?>        
    </div>
    </div>
  </section>

  <!--open testimony slide content-->
  <section class="row testimony-pos-cont page-section" id="banner-content">
    <div class="container">
      <div class="title-why">
          <h2 class="title-all-h2-tema2-tema2-tema2-tema2-tema2-tema2-tema2"><?php echo $this->lang->line('whatour');?><b> <?php echo $this->lang->line('clientsay');?></b></h2>
      </div>
      <div class="pos-testimony-tema2">
         <?php
          $testimonial = get_list_testimonial(lang_convert($this->session->userdata('site_lang')));
          if($testimonial!=false){
            foreach($testimonial->result() as $item){
              echo '
                  <div class="card-testimony-tema2">
                  <div class="head">
                    <div class="span-icon-testimony">
                      <img src="'.base_url("assets/tema2/img/icon/Icon-testi-theme-1-08.png").'" alt="icon testimony">
                    </div>
                    <div class="head-user-picture">
                    <img src="'.base_url($item->picture).'" alt="image clients">
                    </div>
                  </div>
                  <div class="body">
                    <p>
                      '.$item->comment.'
                    </p>
                  </div>
                  <div class="footer">
                    <h4>'.$item->name.' - Facebook</h4>
                  </div>
                </div>
           ';
            }
          } 
          ?>   
<!--
        <div class="card-testimony-tema2">
          <div class="head">
            <div class="span-icon-testimony">
              <img src="<?php echo BASE_URL;?>assets/tema2/img/icon/Icon-testi-theme-1-08.png" alt="icon testimony">
            </div>
            <div class="head-user-picture">
            <img src="<?php echo BASE_URL;?>assets/img/testi-02.png" alt="image clients">
            </div>
          </div>
          <div class="body">
            <p>
              Solution for insurance company to manage their product and to market their product via online with the exiting distribution channel
              Solution for insurance company to manage their product and to market their product via online with the exiting distribution channel
              
            </p>
          </div>
          <div class="footer">
            <h4>Name Of Clients</h4>
          </div>
        </div>
        
        <div class="card-testimony-tema2">
          <div class="head">
            <div class="span-icon-testimony">
              <img src="<?php echo BASE_URL;?>assets/tema2/img/icon/Icon-testi-theme-1-08.png" alt="icon testimony">
            </div>
            <div class="head-user-picture">
              <img src="<?php echo BASE_URL;?>assets/img/testi-03.png" alt="image clients">
            </div>
          </div>
          <div class="body">
            <p>
              Solution for insurance company to manage their product and to market their product via online with the exiting distribution channel
              Solution for insurance company to manage their product and to market their product via online with the exiting distribution channel
              
            </p>
          </div>
          <div class="footer">
            <h4>Name Of Clients</h4>
          </div>
        </div>
        
        <div class="card-testimony-tema2">
          <div class="head">
            <div class="span-icon-testimony">
              <img src="<?php echo BASE_URL;?>assets/tema2/img/icon/Icon-testi-theme-1-08.png" alt="icon testimony">
            </div>
            <div class="head-user-picture">
              <img src="<?php echo BASE_URL;?>assets/img/testi-03.png" alt="image clients">
            </div>
          </div>
          <div class="body">
            <p>
              Solution for insurance company to manage their product and to market their product via online with the exiting distribution channel
              Solution for insurance company to manage their product and to market their product via online with the exiting distribution channel
              
            </p>
          </div>
          <div class="footer">
            <h4>Name Of Clients</h4>
          </div>
        </div>
-->
      </div>
    </div>
  </section>
  <!--close testimony slide content-->
</div>
<!--close container-->
