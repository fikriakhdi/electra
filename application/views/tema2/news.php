<div class="container-fluid under-navbar">
  <!--open banner slide content-->
  <section class=" row update-pos-cont" id="update-content">
      <br>
    <div class="container-fluid">

        <?php
              if($news->num_rows()!=0){
                foreach($news->result() as $item){
                  ?>
                  <div class="col-md-4">
                    <div class="card-upadte-tema2">
                      <div class="card-news-tema2" style="background-image:url(<?php echo base_url($item->image_feature);?>)">
                      </div>
                      <div class="text-body">
                        <h4><?php echo view_short($item->title);?></h4>
                        <span><?php echo date("Y-m-d", strtotime($item->datecreated));?></span>
                      </div>
                      <div class="text-footer">
                        <a href="<?php echo base_url('detail_news/'.$item->seo);?>"><?php echo $this->lang->line('continue_reading');?></a>
                      </div>
                    </div>
                  </div>
                <?php } ?>
              <?php } ?>
        
        <div class="col-md-12">
             <div class="col-md-12">
                <p style="text-align:center">
                    <?php echo $pagination; ?>
                      </p>
                    </div>
            <br>
        </div>
    </div>
               
  </section>
</div>