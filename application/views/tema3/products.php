<!--
  Jakarta : 17/10/2018
  create By : Maningcorp
-->
<!--DOCTYPE HTML-->
<?php
$product = get_list_product(lang_convert($this->session->userdata('site_lang')));
?>
    <section id="navbar"  >
            <!--open header nav for tablet, laptop and PC -->
        <div class="overlay"></div>

        <!-- Page Content -->
        <nav class=" navbar-default background-color nav-tlp" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand navbar-center" href="<?php echo base_url();?>"><img src="<?php echo ASSETS.'img/electra.png';?>" class="img-responsive"></a>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="navbar-collapse collapse">
           <ul class="nav navbar-nav nav-left">
                <li><a href="<?php echo base_url();?>"><?php echo $this->lang->line('main_header_home');?></a></li>
                <li><a href="<?php echo base_url('about_us');?>"><?php echo $this->lang->line('main_header_about_us');?></a></li>
                <li class="drop"><a href="#our-product-content" class="scrollTo"><?php echo $this->lang->line('our_products');?></a>
                  <div class="dropdownContain">
                    <div class="dropOut">
                        <?php
                          if($product!=false){
                              echo '<ul>';
                            foreach($product->result() as $item){
                              echo '
                                <li><a href="'.base_url('products/'.$item->id).'">'.$item->name.'</a></li>

                           ';
                            }
                              echo '</ul>';
                          }
                          ?>
                    </div>
                  </div>
                </li>
            </ul>
            <ul class="nav navbar-nav nav-right">
            <li><a href="<?php echo base_url('news');?>"><?php echo $this->lang->line('main_header_news_&_updates');?></a></li>
                <li><a href="<?php echo base_url('hello_electra');?>"><?php echo $this->lang->line('main_header_hello_electra');?></a></li>
                <?php
                $navbar = get_list_pages();
                if($navbar!=false){ ?>
                <li class="drop"><a href="#pages" class="scrollTo"><?php echo $this->lang->line('main_header_pages');?></a>
                    <div class="dropdownContain">
                    <div class="dropOut">
                         <?php
                          foreach($navbar->result() as $nav){
                              echo '
                              <ul>
                                <li><a href="'.base_url('pages/'.$nav->seo).'">'.$nav->title.'</a></li>
                              </ul>
                           ';
                            }
                          ?>
                    </div>
                  </div>
                </li>
                <?php 
                }
                ?>
            </ul>
           <ul class="nav navbar-nav navbar-right">
               
                <li><a style="    padding-top: 2px;border: solid 1px #fff;border-radius: 12px;" href="<?php echo base_url('en');?>">EN</a></li>
                <li style="padding:0"><a style="    padding-top: 2px;margin-left: 5px;;border: solid 1px #fff;border-radius: 12px;" href="<?php echo base_url('id');?>">ID</a></li>
            </ul>
        </div>
    </nav>
    <!--close header nav for tablet, laptop and PC -->
    </section>
<style>
  .body ul {
    padding: 0;
    font-size: 18px;
    list-style-image: url(<?php echo BASE_URL;?>/assets/img/icon/list-style.png);
  }
</style>
<div class="container-fluid" style="overflow:hidden">
    
  <!--open banner product content-->
  <section class=" row banner-pos-cont products-backgroud" id="banner-content" data-speed="4" style="background-image: url(<?php echo base_url($products->header_image);?>);">
    <div class="container-fluid content-firs">
    </div>
    <div class="col-md-12 banner-form-text-product header-caption">
      <h1><?php echo $products->name;?></h1>
    </div>
  </section>
  <!--close banner product slide content-->

  <!--open product content-->
  <section class=" row pos-cont-product " id="product-content">
    <div class="container-fluid content-firs-product">
      <div class="content-text product" data-aos="fade-up">
        <h3><?php echo $products->description;?>
        </h3>
      </div>
    </div>
  </section>
  <!--close product content-->

  <!--open product owner content-->
  <section class="row" id="product-owner-content">
            <div class="section-1-pro content-2-pro-tema2">
        <div class="col-md-12 post-con-image">
          <div class="post-img">
               <img src="<?php echo BASE_URL;?>/assets/img/icon/electraios-01.png" class="imac">
              
            <img src="<?php echo BASE_URL;?>/assets/img/icon/electraios-02.png" class="laptop">
            <div class="iphone-pos">
              <img src="<?php echo BASE_URL;?>/assets/img/icon/electraios-03.png" class="iphone">
            </div>
              <div class="screen-content1 screen-content-scroll" style="background-image:url(<?php echo BASE_URL.$products->desktop_image;?>)">
              </div>
              <div class="screen-content2 screen-content-scroll" style="background-image:url(<?php echo BASE_URL.$products->desktop_image;?>)">
              </div>
              <div class="screen-content3 screen-content-scroll1" style="background-image:url(<?php echo BASE_URL.$products->desktop_image;?>)">
              </div>
          </div>
        </div>
      </div>
    <div class="section-1-pro content-1-pro-tema2">
        <div class="col-md-6 pro-cont" data-aos="fade-up" data-aos-duration="500">
          <div class="post-list">
            <div class="head">
                <h3><?php echo $this->lang->line('benefit_for'); echo '<b> '.$this->lang->line('product_owner').'</b>';?></h3>
            </div>
            <div class="body">
              <ul>
                <?php
                  $benefit_for_product_owner = explode(";", $products->benefits_product_owner);
                  foreach($benefit_for_product_owner as $item){
                  ?>
                  <li><?php echo $item;?></li>
                  <?php } ?>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-md-6 pro-cont-tema2" data-aos="fade-up" data-aos-duration="700">
          <div class="post-list">
            <div class="head">
              <h3><?php echo $this->lang->line('benefit_for'); echo '<b> '.$this->lang->line('travel_agent').'</b>';?></h3>
            </div>
            <div class="body">
              <ul>
                <?php
                  $benefit_for_travel_agent = explode(";", $products->benefits_travel_agent);
                  foreach($benefit_for_travel_agent as $item){
                  ?>
                  <li><?php echo $item;?></li>
                  <?php } ?>
              </ul>
            </div>
          </div>
        </div>
      </div>
    
    <!--close container-->
  </section>
  <!--close product owner content-->

  <!--open Our Key Feature content-->
    <?php 
      $key_feature = get_list_key_feature($products->id);
      if($key_feature!=false){
      ?>
  <section class=" row why-pos-cont" id="our-key-feature-content">
    <div class="container">
      <div class="col-md-12">
        <div class="title-why">
          <h2 class="title-all-h2">
            <?php if($this->session->userdata('site_lang')=='english'){ ?>
            <?php echo $this->lang->line('our'); echo '<b>'.$this->lang->line('key_feature').'</b>';?>
            <?php } else { ?> 
                <?php echo $this->lang->line('channel'); echo $this->lang->line('our');?>
          <?php } ?>
          </h2>
        </div>
          <?php
            foreach($key_feature->result() as $key_feature){
              echo '
                <div class="col-sm-3">
                  <div class="pos-icon-key-tema2">
                    <div class="head"> 
                      <img src="'.base_url($key_feature->icon).'">
                    </div>
                    <div class="body">
                      <h4>'.$key_feature->title.'</h4>
                    </div>
                  </div>
                </div>
            ';
            } ?>
      </div>
    </div>
  </section>
     <?php 
                    }
          ?> 
  <!--close  Our Key Feature content-->

  <!--open our clients content-->
  <section class=" row clients-pos-cont" id="our-clients-content">
    <div class="container-fluid full-width">
      <div class="post-video">
        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/tWsNi9w0VWk?rel=0&amp;autoplay=1&loop=1&playlist=kC1jy2se9mM&mute=1&amp;controls=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
      </div>
    </div>
  </section>

  <!--open testimony slide content-->
   <section class="row testimony-pos-cont" id="banner-content">
    <div class="container">
      <div class="cont-form">
        <div class="title-why">
            <h2 class="title-all-h2"><?php echo $this->lang->line('interested_with'); echo '<b> '.$this->lang->line('our_product?').'</b>';?></h2>
        </div>
        <div class="pos-form-product">
          <form method="post" action="">
             <div class="form-group"> 
                <label class="label1">To: <?php echo get_settings('company_email');?></label>
              </div>
               <div class="form-group"> 
                  <input type="text" class="form-controls" id="name" name="name" required placeholder="Your Name">
                </div>
               <div class="form-group">
                   <input type="email" class="form-controls" id="email" name="email" required placeholder="Your Email">
                </div>
                <div class="form-group">
                    <textarea  class="form-controls" id="message" name="message" required placeholder="Your Message"></textarea>
                </div>
              <button class="btn btn-transparent" type="submit">Submit</button>
          </form>
        </div>
      </div>
    </div>
  </section>
  <!--close testimony slide content-->
</div>
<!--close container-->
