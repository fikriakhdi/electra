    <section id="navbar"  >
            <!--open header nav for tablet, laptop and PC -->
        <div class="overlay"></div>

        <!-- Page Content -->
        <nav class=" navbar-default background-color nav-tlp" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand navbar-center" href="<?php echo base_url();?>"><img src="<?php echo ASSETS.'img/electra.png';?>" class="img-responsive"></a>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="navbar-collapse collapse">

           <ul class="nav navbar-nav nav-left">
                <li><a href="<?php echo base_url();?>"><?php echo $this->lang->line('main_header_home');?></a></li>
                <li><a href="<?php echo base_url('about_us');?>"><?php echo $this->lang->line('main_header_about_us');?></a></li>
                <li class="drop"><a href="#our-product-content" class="scrollTo"><?php echo $this->lang->line('our_products');?></a>
                  <div class="dropdownContain">
                    <div class="dropOut">
                        <?php
                          $products = get_list_product(lang_convert($this->session->userdata('site_lang')));
                          if($products!=false){
                              echo '<ul>';
                            foreach($products->result() as $item){
                              echo '
                                <li><a href="'.base_url('products/'.$item->id).'">'.$item->name.'</a></li>

                           ';
                            }
                              echo '</ul>';
                          }
                          ?>
                    </div>
                  </div>
                </li>
            </ul>
            <ul class="nav navbar-nav nav-right">
            <li><a href="<?php echo base_url('news');?>"><?php echo $this->lang->line('main_header_news_&_updates');?></a></li>
                <li><a href="<?php echo base_url('hello_electra');?>"><?php echo $this->lang->line('main_header_hello_electra');?></a></li>
                <?php
                $navbar = get_list_pages();
                if($navbar!=false){ ?>
                <li class="drop"><a href="#pages" class="scrollTo"><?php echo $this->lang->line('main_header_pages');?></a>
                    <div class="dropdownContain">
                    <div class="dropOut">
                         <?php
                          foreach($navbar->result() as $nav){
                              echo '
                              <ul>
                                <li><a href="'.base_url('pages/'.$nav->seo).'">'.$nav->title.'</a></li>
                              </ul>
                           ';
                            }
                          ?>
                    </div>
                  </div>
                </li>
                <?php 
                }
                ?>
            </ul>
           <ul class="nav navbar-nav navbar-right">
               
                <li><a style="    padding-top: 2px;border: solid 1px #fff;border-radius: 12px;" href="<?php echo base_url('en');?>">EN</a></li>
                <li style="padding:0"><a style="    padding-top: 2px;margin-left: 5px;;border: solid 1px #fff;border-radius: 12px;" href="<?php echo base_url('id');?>">ID</a></li>
            </ul>
        </div>
    </nav>
    <!--close header nav for tablet, laptop and PC -->
    </section>
<div class="container-fluid ">
  <!--open banner slide content-->
  <section class=" row update-pos-cont" id="update-content">
    <div class="container-fluid carousel-home">
      <!--open banner update-ditel content-->
      <section class=" row banner-pos-cont news-backgroud" id="banner-content" data-speed="4" style="background-image: url(<?php echo base_url($detail_news->image_feature);?>);">
      </section>
        <section class="news-details">
        <span class="news-date"><?php echo date('d M Y', strtotime($detail_news->datecreated));?></span>
        <h1><?php echo $detail_news->title;?></h1>
            </section>
          
      <!--close banner update-ditel slide content-->
      <div class="share-container">
        <a class="btn btn-rounded btn-share btn-default" href="http://twitter.com/share?text=<?php echo $detail_news->title;?>&url=http://<?php echo base_url('detail_news/'.$detail_news->seo);?>&hashtags=#<?php echo COMPANY_NAME;?>" target="_blank"><span class="fa fa-twitter"></span></a>
         <a  class="btn btn-rounded btn-share btn-default" href="https://www.facebook.com/sharer.php?u=<?php echo base_url('detail_news/'.$detail_news->seo);?>&t=<?php echo $detail_news->title;?>"><span class="fa fa-facebook" target="_blank"></span></a>
           <a href="#" class="btn btn-rounded btn-share btn-default copy-btn" data-clipboard-text="<?php echo base_url('detail_news/'.$detail_news->seo);?>"  target="_blank"><span class="fa fa-link"></span></a>
          <a class="btn btn-rounded btn-share btn-default" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo base_url('detail_news/'.$detail_news->seo);?>&title=<?php echo $detail_news->title;?>&source=<?php echo COMPANY_NAME;?>"><span class="fa fa-linkedin"></span></a>
      </div>
      <section class=" row des-pos-cont" id="banner-content">
        <div class="container">
          <div class="pos-des">
            <?php echo $detail_news->content;?>
          </div> 
        </div>
      </section>
      
      
    </div>
  </section>
</div>