<!--
  Jakarta : 17/10/2018
  create By : Maningcorp
-->
<!--DOCTYPE HTML-->
<div class="container-fluid" id="container_body" color="transparent">
  <!--open header content-->
  <section class="row header-pos-cont">
    <div class="col-md-12 header" id="header">
    </div>
  </section>
  <!--close header content-->

  <!--open banner slide content-->
  <section class=" row banner-pos-cont page-section" id="banner-content" data-speed="4">
    <div class="container-fluid content-firs">
      <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <?php 
            $slider = get_list_slider($this->session->userdata('lang_admin'));
            if($slider!=false){
            for($i=0;$i<$slider->num_rows();$i++){
            ?>
          <li data-target="#myCarousel" data-slide-to="<?php echo $i;?>" class="<?php echo ($i==0?'active':'');?>"></li>
            <?php }}?>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner header-carousel">
            <?php 
            $slider = get_list_slider($this->session->userdata('lang_admin'));
            $i=0;
            if($slider!=false){
            foreach($slider->result() as $slide){
                $i++;
            ?>
            <div class="item <?php echo ($i==1?'active':'');?>">
            <div class="slider-banner" data-speed="4"></div>
            <img src="<?php echo base_url($slide->picture);?>" alt="slider 1" style="width:100%;filter:brightness(80%);">
          </div>
            <?php }}?>
        </div>
<!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>           
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
      </div>
    </div>

    <div class="banner-form-text d-sm-none">
      <div class="content-left">
        <div class="pos-text-banner header-caption">
          <h1>Behind Every Great Travel Company</h1>
          <p>We understand every travel industry need to keep up with the pace of change in the digital era in order to appealto customer demand.
          </p>
        </div>
      </div>
    </div>
      <div class="btn-scroll-to-explore">
          <a href="#about-us-content" data-text="scroll to explore" class="scrollTo effect-shine"></a>
      </div>
    
  </section>
</div>
    <section id="navbar"  >
            <!--open header nav for tablet, laptop and PC -->
        <div class="overlay"></div>

        <!-- Page Content -->
        <nav class=" navbar-default background-color nav-tlp" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand navbar-center" href="<?php echo base_url();?>"><img src="<?php echo ASSETS.'img/electra.png';?>" class="img-responsive"></a>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="navbar-collapse collapse">

           <ul class="nav navbar-nav nav-left">
                <li><a href="<?php echo base_url();?>"><?php echo $this->lang->line('main_header_home');?></a></li>
                <li><a href="<?php echo base_url('about_us');?>"><?php echo $this->lang->line('main_header_about_us');?></a></li>
                <li class="drop"><a href="#our-product-content" class="scrollTo"><?php echo $this->lang->line('our_products');?></a>
                  <div class="dropdownContain">
                    <div class="dropOut">
                        <?php
                          $products = get_list_product(lang_convert($this->session->userdata('site_lang')));
                          if($products!=false){
                              echo '<ul>';
                            foreach($products->result() as $item){
                              echo '
                                <li><a href="'.base_url('products/'.$item->id).'">'.$item->name.'</a></li>

                           ';
                            }
                              echo '</ul>';
                          }
                          ?>
                    </div>
                  </div>
                </li>
            </ul>
            <ul class="nav navbar-nav nav-right">
            <li><a href="<?php echo base_url('news');?>"><?php echo $this->lang->line('main_header_news_&_updates');?></a></li>
                <li><a href="<?php echo base_url('hello_electra');?>"><?php echo $this->lang->line('main_header_hello_electra');?></a></li>
                <?php
                $navbar = get_list_pages();
                if($navbar!=false){ ?>
                <li class="drop"><a href="#pages" class="scrollTo"><?php echo $this->lang->line('main_header_pages');?></a>
                    <div class="dropdownContain">
                    <div class="dropOut">
                         <?php
                          foreach($navbar->result() as $nav){
                              echo '
                              <ul>
                                <li><a href="'.base_url('pages/'.$nav->seo).'">'.$nav->title.'</a></li>
                              </ul>
                           ';
                            }
                          ?>
                    </div>
                  </div>
                </li>
                <?php 
                }
                ?>
            </ul>
           <ul class="nav navbar-nav navbar-right">
               
                <li><a style="    padding-top: 2px;border: solid 1px #fff;border-radius: 12px;" href="<?php echo base_url('en');?>">EN</a></li>
                <li style="padding:0"><a style="    padding-top: 2px;margin-left: 5px;;border: solid 1px #fff;border-radius: 12px;" href="<?php echo base_url('id');?>">ID</a></li>
            </ul>
        </div>
    </nav>
    <!--close header nav for tablet, laptop and PC -->
    </section>
  <!--close banner slide content-->
<div class="container-fluid" id="container_body" color="transparent">
  <!--open about us content-->
  <section class=" row about-pos-cont background-white page-section" id="about-us-content">
    <div class="container-fluid content-firs">
      <div class="content-text about-us" data-aos="fade-up">
        <h3 style="font-weight: lighter; font-family: helvetica;"><?php echo $about->who_are_we_content;?>
        </h3>
        <a href="<?php echo base_url('about_us');?>" class="button-all btn-about"><?php echo $this->lang->line('about_us');?></a>
      </div>
    </div>
  </section>
  <!--close about us content-->

  <!--open our product content-->
  <section class=" row pro-pos-cont page-section" id="our-product-content">
      <div class=" col-md-4 col-md-offset-4" data-aos="fade-up" data-aos-duration="200">
          <div class="card-pro-title" style="text-align:center">
            <?php if($this->session->userdata('site_lang')=='english'){ ?>
            <h2 class="title-pro-unborder"><?php echo $this->lang->line('our');?> <b><?php echo $this->lang->line('product');?></b></h2>
            <?php } else { ?> 
                <h2 class="title-pro-unborder"><b><?php echo $this->lang->line('product');?></b><?php echo $this->lang->line('our');?></h2>
                <?php } ?>
          </div>
          <br>
        </div>  
    <div class="container pos pro-con">    
        <?php
          $product = get_list_product(lang_convert($this->session->userdata('site_lang')));
          if($product!=false){
              $duration = 500;
              $i=0;
            foreach($product->result() as $item){   
                $i++;
              echo '
               <div class="size-for-mobile" data-aos="fade-up" data-aos-duration="'.$duration.'" >
                 <a href="'.base_url('products/'.$item->id).'">
                  <div class="card-pro pos-card-pro-tema2" data-desc="body-card-desc'.$i.'" data-title="body-card-title'.$i.'" data-icon="title-icon'.$i.'">
                    <div class=" card-pro-tema2">
                      <div class="head-card head-card-tema2">
                        <img id="title-icon'.$i.'" src="'.base_url($item->icon).'" alt="responsive">
                      </div>
                      <div class="body-card body-card-tema2" >
                        <h3 id="body-card-title'.$i.'">'.$item->name.'</h3>
                        <p id="body-card-desc'.$i.'">'.$item->description.'</p>
                      </div>
                      <div class="footer-card">
                        <a class="more" href="'.base_url('products/'.$item->id).'">'.$this->lang->line('see_more').'</a>
                      </div>
                    </div>
                  </div>
                  </a>
                </div> 
           ';
                $duration+=100;
            }
          }
          ?> 
      

    
     
      <!-- close view our product-->
      
    </div>
    <!--close container-->
  </section>
  <!--close our product content-->

  <!--open why choos us content-->
  <section class=" row why-pos-cont page-section" id="choose-us-content">
    <div class="container pos pro-con">
      <div class="col-md-12 pos-mo-why">
        <div class="title-why">
          <h2 class="title-all-h2"><?php echo $this->lang->line('why');?><b> <?php echo $this->lang->line('chooseus');?></b></h2>
        </div>
        
           <?php
          $why_choose_us = get_list_why_choose_us(lang_convert($this->session->userdata('site_lang')));
          if($why_choose_us!=false){
              $duration = 200;
            foreach($why_choose_us->result() as $item){
              echo '
                <div class="size-for-mobile " data-aos="fade-up" data-aos-duration="'.$duration.'">
                  <div class="card-pro-why ">
                    <div class="head-card-why">
                      <img src="'.base_url($item->logo).'" alt="icon Have Great">
                    </div>
                    <div class="body-card-why">
                      <h3>'.$item->title.'</h3>
                      <p>'.$item->description.'</p>
                    </div>
                  </div>
                </div>

           ';
                $duration+=100;
            }
          } 
          ?>  
          
      </div>
    </div>
  </section>
  <!--close why choos us content-->

  <!--open our clients content-->
  <section class=" row clients-pos-cont-tema2 page-section" id="our-clients-content">
    <div class="container pos pro-con">
      <div class="content-our-clients">
        <div class="title-why">
            <?php if($this->session->userdata('site_lang')=='english') {?>
            <h2 class="title-all-h2-tema2"><?php echo $this->lang->line('our');?> <b><?php echo $this->lang->line('client');?></b> </h2>
            <?php } else { ?>
            <h2 class="title-all-h2-tema2"><b><?php echo $this->lang->line('client');?></b> <?php echo $this->lang->line('our');?></h2>
            <?php } ?>
        </div>
        <?php
          $client = get_list_client();
          if($client!=false){
            foreach($client->result() as $item){
              echo '
              
              <div class="our-client-size-tema2 clients">
              <img src="'.base_url($item->client_logo).'">
              </div>
           ';
            }
          }
          ?>        
    </div>
    </div>
  </section>

  <!--open testimony slide content-->
  <section class="row testimony-pos-cont page-section" id="banner-content">
    <div class="container">
      <div class="title-why">
          <h2 class="title-all-h2-tema2-tema2-tema2-tema2-tema2-tema2-tema2"><?php echo $this->lang->line('whatour');?><b> <?php echo $this->lang->line('clientsay');?></b></h2>
      </div>
      <div class="pos-testimony-tema2">
         <?php
          $testimonial = get_list_testimonial(lang_convert($this->session->userdata('site_lang')));
          if($testimonial!=false){
            foreach($testimonial->result() as $item){
              echo '
                  <div class="card-testimony-tema2">
                  <div class="head">
                    <div class="span-icon-testimony">
                      <img src="'.base_url("assets/tema2/img/icon/Icon-testi-theme-1-08.png").'" alt="icon testimony">
                    </div>
                    <div class="head-user-picture">
                    <img src="'.base_url($item->picture).'" alt="image clients">
                    </div>
                  </div>
                  <div class="body">
                    <p>
                      '.$item->comment.'
                    </p>
                  </div>
                  <div class="footer">
                    <h4>'.$item->name.' - Facebook</h4>
                  </div>
                </div>
           ';
            }
          } 
          ?>   
<!--
        <div class="card-testimony-tema2">
          <div class="head">
            <div class="span-icon-testimony">
              <img src="<?php echo BASE_URL;?>assets/tema2/img/icon/Icon-testi-theme-1-08.png" alt="icon testimony">
            </div>
            <div class="head-user-picture">
            <img src="<?php echo BASE_URL;?>assets/img/testi-02.png" alt="image clients">
            </div>
          </div>
          <div class="body">
            <p>
              Solution for insurance company to manage their product and to market their product via online with the exiting distribution channel
              Solution for insurance company to manage their product and to market their product via online with the exiting distribution channel
              
            </p>
          </div>
          <div class="footer">
            <h4>Name Of Clients</h4>
          </div>
        </div>
        
        <div class="card-testimony-tema2">
          <div class="head">
            <div class="span-icon-testimony">
              <img src="<?php echo BASE_URL;?>assets/tema2/img/icon/Icon-testi-theme-1-08.png" alt="icon testimony">
            </div>
            <div class="head-user-picture">
              <img src="<?php echo BASE_URL;?>assets/img/testi-03.png" alt="image clients">
            </div>
          </div>
          <div class="body">
            <p>
              Solution for insurance company to manage their product and to market their product via online with the exiting distribution channel
              Solution for insurance company to manage their product and to market their product via online with the exiting distribution channel
              
            </p>
          </div>
          <div class="footer">
            <h4>Name Of Clients</h4>
          </div>
        </div>
        
        <div class="card-testimony-tema2">
          <div class="head">
            <div class="span-icon-testimony">
              <img src="<?php echo BASE_URL;?>assets/tema2/img/icon/Icon-testi-theme-1-08.png" alt="icon testimony">
            </div>
            <div class="head-user-picture">
              <img src="<?php echo BASE_URL;?>assets/img/testi-03.png" alt="image clients">
            </div>
          </div>
          <div class="body">
            <p>
              Solution for insurance company to manage their product and to market their product via online with the exiting distribution channel
              Solution for insurance company to manage their product and to market their product via online with the exiting distribution channel
              
            </p>
          </div>
          <div class="footer">
            <h4>Name Of Clients</h4>
          </div>
        </div>
-->
      </div>
    </div>
  </section>
  <!--close testimony slide content-->
</div>
<!--close container-->
