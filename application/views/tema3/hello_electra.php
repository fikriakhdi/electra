    <section id="navbar"  >
            <!--open header nav for tablet, laptop and PC -->
        <div class="overlay"></div>

        <!-- Page Content -->
        <nav class=" navbar-default background-red nav-tlp" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand navbar-center" href="<?php echo base_url();?>"><img src="<?php echo ASSETS.'img/electra.png';?>" class="img-responsive"></a>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="navbar-collapse collapse">

           <ul class="nav navbar-nav nav-left">
                <li><a href="<?php echo base_url();?>"><?php echo $this->lang->line('main_header_home');?></a></li>
                <li><a href="<?php echo base_url('about_us');?>"><?php echo $this->lang->line('main_header_about_us');?></a></li>
                <li class="drop"><a href="#our-product-content" class="scrollTo"><?php echo $this->lang->line('our_products');?></a>
                  <div class="dropdownContain">
                    <div class="dropOut">
                        <?php
                          $products = get_list_product(lang_convert($this->session->userdata('site_lang')));
                          if($products!=false){
                              echo '<ul>';
                            foreach($products->result() as $item){
                              echo '
                                <li><a href="'.base_url('products/'.$item->id).'">'.$item->name.'</a></li>

                           ';
                            }
                              echo '</ul>';
                          }
                          ?>
                    </div>
                  </div>
                </li>
            </ul>
            <ul class="nav navbar-nav nav-right">
            <li><a href="<?php echo base_url('news');?>"><?php echo $this->lang->line('main_header_news_&_updates');?></a></li>
                <li><a href="<?php echo base_url('hello_electra');?>"><?php echo $this->lang->line('main_header_hello_electra');?></a></li>
                <?php
                $navbar = get_list_pages();
                if($navbar!=false){ ?>
                <li class="drop"><a href="#pages" class="scrollTo"><?php echo $this->lang->line('main_header_pages');?></a>
                    <div class="dropdownContain">
                    <div class="dropOut">
                         <?php
                          foreach($navbar->result() as $nav){
                              echo '
                              <ul>
                                <li><a href="'.base_url('pages/'.$nav->seo).'">'.$nav->title.'</a></li>
                              </ul>
                           ';
                            }
                          ?>
                    </div>
                  </div>
                </li>
                <?php 
                }
                ?>
            </ul>
           <ul class="nav navbar-nav navbar-right">
               
                <li><a style="    padding-top: 2px;border: solid 1px #fff;border-radius: 12px;" href="<?php echo base_url('en');?>">EN</a></li>
                <li style="padding:0"><a style="    padding-top: 2px;margin-left: 5px;;border: solid 1px #fff;border-radius: 12px;" href="<?php echo base_url('id');?>">ID</a></li>
            </ul>
        </div>
    </nav>
    <!--close header nav for tablet, laptop and PC -->
    </section>
<div class="container-fluid" id="hello_electra">
        <section class="row hello_electra_map" id="pos-con-map">
        <div id="map"></div>
    </section>
    <section class="row background-red" id="content-hello-electra">
        <div class="col-md-6 contact">
            <div class="content list-add">
                <h3><?php echo $this->lang->line('contact');?></h3>
              <ul>
                <li><span class="fa fa-map-marker"></span> <p><?php echo get_settings('company_address');?><p></li>
                <li><span class="fa fa-phone"></span> <p><?php echo get_settings('company_phone');?><p></li>
                <li><span class="fa fa-envelope message"></span> <p><a href="mailto:<?php echo get_settings('company_email');?>"><?php echo get_settings('company_email');?></a><p></li>
              </ul>
              
            </div>
        </div>
        <div class="col-md-6 drop_message">
            <?php if(!empty($this->session->userdata('message'))) echo $this->session->userdata('message');?>
            <div class="hello_electra_contact">
                <h3><?php echo $this->lang->line('send_message');?></h3>
                <div class="pos-form-contact">
                    <form method="post" action="<?php echo base_url('kirim_email');?>">
                        <div class="form-group">
                          <label class="label1">To: <?php echo get_settings('company_email');?></label>
                        </div>
                        <div class="form-group"> 
                            <input type="text" class="form-controls" id="name" name="name" required placeholder="Your Name">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-controls" id="email" name="email" required placeholder="Your Email">
                        </div>
                        <div class="form-group">
                            <textarea  class="form-controls" id="message" name="message" required placeholder="Your Message"></textarea>
                        </div>
                        <button class="btn btn-transparent" type="submit">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div><!--close container fluid-->
  <!--javascript for map-->
  <script src="http://maps.google.com/maps/api/js?key=AIzaSyD98vdNV_tuiFcWI1C_9df63zTjznqTQtU&sensor=false&libraries=places" type="text/javascript"></script>
<script type="text/javascript">
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 15,
    center: new google.maps.LatLng(<?php echo get_settings('latitude');?>, <?php echo get_settings('longitude');?>),
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });
  var position={lat: <?php echo get_settings('latitude');?>, lng: <?php echo get_settings('longitude');?>};
   var marker=new google.maps.Marker({
        map: map,
        // icon: icon,
        title: "Sabre Indonesia",
        position: position,
        draggable: false
      });
</script>