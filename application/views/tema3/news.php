<?php
$product = get_list_product(lang_convert($this->session->userdata('site_lang')));
?>
    <section id="navbar"  >
            <!--open header nav for tablet, laptop and PC -->
        <div class="overlay"></div>

        <!-- Page Content -->
        <nav class=" navbar-default background-color nav-tlp" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand navbar-center" href="<?php echo base_url();?>"><img src="<?php echo ASSETS.'img/electra.png';?>" class="img-responsive"></a>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="navbar-collapse collapse">
           <ul class="nav navbar-nav nav-left">
                <li><a href="<?php echo base_url();?>"><?php echo $this->lang->line('main_header_home');?></a></li>
                <li><a href="<?php echo base_url('about_us');?>"><?php echo $this->lang->line('main_header_about_us');?></a></li>
                <li class="drop"><a href="#our-product-content" class="scrollTo"><?php echo $this->lang->line('our_products');?></a>
                  <div class="dropdownContain">
                    <div class="dropOut">
                        <?php
                          if($product!=false){
                              echo '<ul>';
                            foreach($product->result() as $item){
                              echo '
                                <li><a href="'.base_url('products/'.$item->id).'">'.$item->name.'</a></li>

                           ';
                            }
                              echo '</ul>';
                          }
                          ?>
                    </div>
                  </div>
                </li>
            </ul>
            <ul class="nav navbar-nav nav-right">
            <li><a href="<?php echo base_url('news');?>"><?php echo $this->lang->line('main_header_news_&_updates');?></a></li>
                <li><a href="<?php echo base_url('hello_electra');?>"><?php echo $this->lang->line('main_header_hello_electra');?></a></li>
                <?php
                $navbar = get_list_pages();
                if($navbar!=false){ ?>
                <li class="drop"><a href="#pages" class="scrollTo"><?php echo $this->lang->line('main_header_pages');?></a>
                    <div class="dropdownContain">
                    <div class="dropOut">
                         <?php
                          foreach($navbar->result() as $nav){
                              echo '
                              <ul>
                                <li><a href="'.base_url('pages/'.$nav->seo).'">'.$nav->title.'</a></li>
                              </ul>
                           ';
                            }
                          ?>
                    </div>
                  </div>
                </li>
                <?php 
                }
                ?>
            </ul>
           <ul class="nav navbar-nav navbar-right">
               
                <li><a style="    padding-top: 2px;border: solid 1px #fff;border-radius: 12px;" href="<?php echo base_url('en');?>">EN</a></li>
                <li style="padding:0"><a style="    padding-top: 2px;margin-left: 5px;;border: solid 1px #fff;border-radius: 12px;" href="<?php echo base_url('id');?>">ID</a></li>
            </ul>
        </div>
    </nav>
    <!--close header nav for tablet, laptop and PC -->
    </section>
<div class="container-fluid under-navbar">
  <!--open banner slide content-->
  <section class=" row update-pos-cont" id="update-content">
      <br>
    <div class="container-fluid">
        
        
    		  <?php
              $news = get_list_news(lang_convert($this->session->userdata('site_lang')));
              if($news!=false){
                  $news_single = $news->row();
                  ?>
                  <div class="col-md-12">
                  <a href="<?php echo base_url('detail_news/'.$news_single->seo);?>">
                    <div class="card-upadte-tema2">
                      <div class="card-news-tema2-headline" style="background-image:url(<?php echo base_url($news_single->image_feature);?>)">
                      </div>
                      <div class="text-body-headline">
                          <span><?php echo date("Y-m-d", strtotime($news_single->datecreated));?></span> 
                        <h4><?php echo view_short($news_single->title);?></h4>
                      </div>
                    </div>
                    </a>
                  </div>
                  
                <?php 
                      $i=0;
                foreach($news->result() as $item){
                    $i++;
                    if($i>1){
                  ?>
                  <div class="col-md-3">
                  <a href="<?php echo base_url('detail_news/'.$item->seo);?>">
                    <div class="card-upadte-tema2">
                      <div class="card-news-tema2" style="background-image:url(<?php echo base_url($item->image_feature);?>)">
                      </div>
                      <div class="text-body">
                      <span><?php echo date("Y-m-d", strtotime($item->datecreated));?></span>
                        <h4><?php echo view_short($item->title);?></h4>
                      </div>
                    </div>
                    </a>
                  </div>
                 
              <?php } ?>
            <?php } ?>
        <?php } ?>
            
        <div class="col-md-12">
             <div class="col-md-12">
                <p style="text-align:center">
                    <?php echo $pagination; ?>
                      </p>
                    </div>
            <br>
        </div>
    </div>
               
  </section>
</div>