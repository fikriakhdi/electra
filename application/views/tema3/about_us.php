    <section id="navbar"   >
            <!--open header nav for tablet, laptop and PC -->
        <div class="overlay"></div>

        <!-- Page Content -->
        <nav class=" navbar-default background-color nav-tlp" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand navbar-center" href="<?php echo base_url();?>"><img src="<?php echo ASSETS.'img/electra.png';?>" class="img-responsive"></a>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="navbar-collapse collapse">

           <ul class="nav navbar-nav nav-left">
                <li><a href="<?php echo base_url();?>"><?php echo $this->lang->line('main_header_home');?></a></li>
                <li><a href="<?php echo base_url('about_us');?>"><?php echo $this->lang->line('main_header_about_us');?></a></li>
                <li class="drop"><a href="#our-product-content" class="scrollTo"><?php echo $this->lang->line('our_products');?></a>
                  <div class="dropdownContain">
                    <div class="dropOut">
                        <?php
                          $products = get_list_product(lang_convert($this->session->userdata('site_lang')));
                          if($products!=false){
                              echo '<ul>';
                            foreach($products->result() as $item){
                              echo '
                                <li><a href="'.base_url('products/'.$item->id).'">'.$item->name.'</a></li>

                           ';
                            }
                              echo '</ul>';
                          }
                          ?>
                    </div>
                  </div>
                </li>
            </ul>
            <ul class="nav navbar-nav nav-right">
            <li><a href="<?php echo base_url('news');?>"><?php echo $this->lang->line('main_header_news_&_updates');?></a></li>
                <li><a href="<?php echo base_url('hello_electra');?>"><?php echo $this->lang->line('main_header_hello_electra');?></a></li>
                <?php
                $navbar = get_list_pages();
                if($navbar!=false){ ?>
                <li class="drop"><a href="#pages" class="scrollTo"><?php echo $this->lang->line('main_header_pages');?></a>
                    <div class="dropdownContain">
                    <div class="dropOut">
                         <?php
                          foreach($navbar->result() as $nav){
                              echo '
                              <ul>
                                <li><a href="'.base_url('pages/'.$nav->seo).'">'.$nav->title.'</a></li>
                              </ul>
                           ';
                            }
                          ?>
                    </div>
                  </div>
                </li>
                <?php 
                }
                ?>
            </ul>
           <ul class="nav navbar-nav navbar-right">
               
                <li><a style="    padding-top: 2px;border: solid 1px #fff;border-radius: 12px;" href="<?php echo base_url('en');?>">EN</a></li>
                <li style="padding:0"><a style="    padding-top: 2px;margin-left: 5px;;border: solid 1px #fff;border-radius: 12px;" href="<?php echo base_url('id');?>">ID</a></li>
            </ul>
        </div>
    </nav>
    <!--close header nav for tablet, laptop and PC -->
    </section>
<div class="position-aboutus-tema2 ">
 <div class="section-image-text-tema3">
   <div class="content-post-tema3-aboutus">
     <div class="head" style="background-image: url(<?php echo base_url($about->who_are_we_pic);?>);filter:brightness(50%)">
     </div>
     <div class="body">
       <h2>Who Are We ?</h2><p><?php echo $about->who_are_we_content;?></p>
   </div>
  </div>
 </div>
  
   <div class="section-image-text-tema3">
   <div class="content-post-tema3-aboutus">
     <div class="head" style="background-image: url(<?php echo base_url($about->what_we_do_pic);?>);filter:brightness(50%)">
     </div>
     <div class="body">
       <h1>What We Do ?</h1>
      <p><?php echo $about->what_we_do_content;?></p>
   </div>
  </div>
 </div>
  
   <div class="section-image-text-tema3">
   <div class="content-post-tema3-aboutus">
     <div class="head" style="background-image: url(<?php echo base_url($about->who_do_we_work_with_pic);?>);filter:brightness(50%)">
     </div>
     <div class="body">
        <h1>Who Do We Work With ?</h1>
       <p><?php echo base_url($about->who_do_we_work_with_content);?></p>
      <a href="#">Our Client</a><hr>
   </div>
  </div>
 </div>
  
     <div class="section-image-text-tema3">
   <div class="content-post-tema3-aboutus">
     <div class="head" style="background-image: url(<?php echo base_url($about->what_we_can_do_for_you_pic);?>);filter:brightness(50%)">
     </div>
     <div class="body">
       <h1>What We Can Do For You ?</h1>
      <p><?php echo base_url($about->what_we_can_do_for_you_content);?></p>
      <a href="#">Our Product</a><hr>
   </div>
  </div>
 </div>
  
<!-----------------close about content view for pc--------------------------->
  
  <!--open testimony slide content-->
  <section class="row testimony-pos-cont" id="banner-content">
      <div class="cont-form">
        <div class="title-why">
          <h2 class="title-all-h2">Interested With <b>Our Product?</b></h2>
        </div>
        <div class="pos-form-product">
          <form method="post" action="">
             <div class="form-group"> 
                <label class="label1">To: <?php echo get_settings('company_email');?></label>
              </div>
               <div class="form-group"> 
                  <input type="text" class="form-controls" id="name" name="name" required placeholder="Your Name">
                </div>
               <div class="form-group">
                   <input type="email" class="form-controls" id="email" name="email" required placeholder="Your Email">
                </div>
                <div class="form-group">
                    <textarea  class="form-controls" id="message" name="message" required placeholder="Your Message"></textarea>
                </div>
              <button class="btn btn-transparent" type="submit">Submit</button>
          </form>
        </div>
      </div>
  </section>
  <!--close testimony slide content-->
</div>
            