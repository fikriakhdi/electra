<div class="container-fluid under-navbar">
  <!-----------------open about content view for pc--------------------------->
		<div class="row about-for-pc">
		    <div class="col-md-6 pic-about-1" style="background-image: url(<?php echo base_url($about->who_are_we_pic);?>);">
		    </div>
		    <div class="col-md-6 copy">
		      <div class="" data-aos="fade-left" data-aos-duration="500">
		      	<h3>Who</h3><h3 style="font-weight: bold;">We Are?</h3><br>
		      	<p><?php echo $about->who_are_we_content;?></p>
		      </div>
		    </div>
		    <div class="col-md-6 copy">
		    	<div class="" data-aos="fade-right" data-aos-duration="500">
		      	<h3>What</h3><h3 style="font-weight: bold;">We Do?</h3><br>
		      	<p><p><?php echo $about->what_we_do_content;?></p>
		      </div>
		    </div>
		    <div class="col-md-6 pic-about-2" style="background-image: url(<?php echo base_url($about->what_we_do_pic);?>);">
		      <div class="pic2">
		      </div>
		    </div>
		    <div class="col-md-6 pic-about-3" style="background-image: url(<?php echo base_url($about->who_do_we_work_with_pic);?>);">
		      <div class="pic3">
		      </div>
		    </div>
		    <div class="col-md-6 copy">
		      <div class="" data-aos="fade-left" data-aos-duration="500">
		      	<h3>Who Do</h3><h3 style="font-weight: bold;">We Work With?</h3><br>
		      	<p><?php echo $about->who_do_we_work_with_content;?></p>
                  <a href="<?php echo base_url('#our-clients-content');?>" class="about_us_link"><h5 style="text-align: center; padding-top: 30px; font-weight:bold">Our Clients</h5></a>
		      </div>
		    </div>
		    <div class="col-md-6 copy">
		    	<div class="" data-aos="fade-right" data-aos-duration="500">
		      	<h3>What We</h3><h3 style="font-weight: bold;">Can Do For You?</h3><br>
		      	<p><?php echo $about->what_we_can_do_for_you_content;?></p>
                    <a href="<?php echo base_url('#our-product-content');?>" class="about_us_link"><h5 style="text-align: center; padding-top: 30px;font-weight:bold">Our Products</h5></a>
		      </div>
		    </div>
		    <div class="col-md-6 pic-about-4" style="background-image: url(<?php echo base_url($about->what_we_can_do_for_you_pic);?>);">
		      <div class="pic4">
		      </div>
		    </div>
		</div>
<!-----------------close about content view for pc--------------------------->
  
  
  <!-----------------open about content view for mobile--------------------------->
  <div class="row about-for-mobile">
        <div class="col-md-6 pic-about-1" style="background-image: url(<?php echo base_url($about->who_are_we_pic);?>);">
		    </div>
		    <div class="col-md-6 copy">
		      <div class="text-cont">
		      	<h3>Who</h3><h3 style="font-weight: bold;">We Are?</h3><br>
		      	<p>Electra (Electronic Ticketing and Reservation Advance) is an operation unit of Sabre Travel Network Indonesia which has been in operation since 2013. The Electra business began with inventory and distribution management system for travel industry companies. With the support of competent and skilled team members, we constantly upgrade our technology. Operating an advanced facilities we utilize modern technology in an effort to provide the best service.</p>
		      </div>
		    </div>
    
		    <div class="col-md-6 pic-about-2" style="background-image: url(<?php echo base_url($about->what_we_do_pic);?>);">
		      <div class="pic2">
		      </div>
		    </div>
		    <div class="col-md-6 copy">
		    	<div class="text-cont">
		      	<h3>What</h3><h3 style="font-weight: bold;">We Do?</h3><br>
		      	<p>Electra (Electronic Ticketing and Reservation Advance) is an operation unit of Sabre Travel Network Indonesia which has been in operation since 2013. The Electra business began with inventory and distribution management system for travel industry companies. With the support of competent and skilled team members, we constantly upgrade our technology. Operating an advanced facilities we utilize modern technology in an effort to provide the best service.</p>
		      </div>
		    </div>
    
		    <div class="col-md-6 pic-about-3" style="background-image: url(<?php echo base_url($about->who_do_we_work_with_pic);?>);">
		      <div class="pic3">
		      </div>
		    </div>
        <div class="col-md-6 copy">
		      <div class="text-cont">
		      	<h3>Who Do</h3><h3 style="font-weight: bold;">We Work With?</h3><br>
		      	<p>Electra (Electronic Ticketing and Reservation Advance) is an operation unit of Sabre Travel Network Indonesia which has been in operation since 2013. The Electra business began with inventory and distribution management system for travel industry companies. With the support of competent and skilled team members, we constantly upgrade our technology. Operating an advanced facilities we utilize modern technology in an effort to provide the best service.</p>
                  <a href="<?php echo base_url('#our-clients-content');?>" class="about_us_link" style="text-align: center; padding-top: 30px; font-weight:bold">Our Clients</a>
		      </div>
		    </div>
		    
		    <div class="col-md-6 pic-about-4" style="background-image: url(<?php echo base_url($about->what_we_can_do_for_you_pic);?>);">
		      <div class="pic4">
		      </div>
		    </div>
		    <div class="col-md-6 copy">
		    	<div class="text-cont">
		      	<h3>What We</h3><h3 style="font-weight: bold;">Can Do For You?</h3><br>
		      	<p>Electra (Electronic Ticketing and Reservation Advance) is an operation unit of Sabre Travel Network Indonesia which has been in operation since 2013. The Electra business began with inventory and distribution management system for travel industry companies. With the support of competent and skilled team members, we constantly upgrade our technology. Operating an advanced facilities we utilize modern technology in an effort to provide the best service.</p>
                    <a href="<?php echo base_url('#our-product-content');?>" class="about_us_link" style="text-align: center; padding-top: 30px; font-weight:bold">Our Products</a>
		      </div>
		    </div>
		</div>
  <!-----------------open about content view for mobile--------------------------->
  
  <!--open testimony slide content-->
  <section class="row testimony-pos-cont" id="banner-content">
    <div class="row">
      <div class="cont-form">
        <div class="title-why">
          <h2 class="title-all-h2">Interested With <b>Our Product? Request For Demo</b></h2>
        </div>
        <div class="pos-form-product">
          <form method="post" action="">
            <div class="form-group">
              <input type="name" class="form-controls" id="name" name="name" required placeholder="Your Name">
            </div>
            <div class="form-group">
              <input type="email" class="form-controls" id="email" name="email" required placeholder="Your Email">
            </div>
              <div class="form-group">
              <input type="text" class="form-controls" id="name" name="name" required placeholder="Your Phone Number">
            </div>
            <div class="form-group">
                <select class="form-control" id="industry">
                              <option value="industry1">----Select Industry----</option>
                              <option value="industry2">industry 2</option>
                              <option value="industry3">industry 3</option>
                              <option value="industry4">industry 4</option>
                            </select>
              </div>
              <div class="form-group">
                <select class="form-control" id="product">
                              <option value="product1">----Select product----</option>
                              <option value="product2">product 2</option>
                              <option value="product3">product 3</option>
                              <option value="product4">product 4</option>
                            </select>
              </div><br>
            <button class="btn btn-transparent" type="submit">Submit</button>
          </form>
        </div>
      </div>
    </div>
  </section>
  <!--close testimony slide content-->
</div>
            