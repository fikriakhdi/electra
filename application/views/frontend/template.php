<!--
  Jakarta : 17/10/2018
  create By : Maningcorp
-->
<!--DOCTYPE HTML-->

<html>

<head>
    <link rel="shortcut icon" type="image/png" href="<?php echo ASSETS;?>img/Favicon-02.png"/>
    <title><?php echo $title;?></title>
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo BASE_URL;?>assets/css/bootstrap.min.css">
  <!-- Electra Custom Style -->
  <link rel="stylesheet" href="<?php echo BASE_URL;?>assets/css/electra_style.css?<?php echo rand();?>">
  <!--Responsive style-->
  <link rel="stylesheet" href="<?php echo BASE_URL;?>assets/css/responsive_electra.css">
  <!--animation scroll-->
  <link rel="stylesheet" href="<?php echo BASE_URL;?>assets/css/aos.css">
  <!--animation button share spread-->
  <link rel="stylesheet" href="<?php echo BASE_URL;?>assets/css/style_btn_share.css">
  <!-- font awesome-->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- font awesome-->
  <link rel="stylesheet" href="<?php echo BASE_URL;?>assets/css/fontawesome-webfont.woff2">
</head>
<body >
  <section>
    <!--open header nav for tablet, laptop and PC -->
    <nav class="navbar navbar-default background-color nav-tlp" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand" href="<?php echo base_url();?>"><img src="<?php echo ASSETS.'img/electra.png';?>" class="img-responsive"></a>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="navbar-collapse collapse">

            <ul class="nav navbar-nav navbar-center">
                <li><a href="<?php echo base_url();?>">Home</a></li>
                <li><a href="<?php echo base_url('about_us');?>">About Us</a></li>
                <li class="drop"><a href="#our-product-content" class="scrollTo">Products</a>
                  <div class="dropdownContain">
                    <div class="dropOut">
                        <ul>
                          <li><a href="<?php echo base_url('products');?>">airline ticketing reservation system</a></li>
                          <li><a href="<?php echo base_url('products');?>">electra booking engine</a></li>
                          <li><a href="<?php echo base_url('products');?>">hotel reservation system</a></li>
                          <li><a href="<?php echo base_url('products');?>">Digital insurance Software</a></li>
                          <li><a href="<?php echo base_url('products');?>">Leisure Reservation System</a></li>
                        </ul>
                    </div>
                  </div>
                </li>
                <li><a href="<?php echo base_url('news');?>">News & Updates</a></li>
                <li><a href="<?php echo base_url('hello_electra');?>">Hello Electra!</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#">EN/ID</a></li>
            </ul>
        </div>
    </nav>
    <!--close header nav for tablet, laptop and PC -->
    
    <!--open header nav for mobile -->
    <nav class="navbar navbar-default transparent_red nav-mobile" role="navigation">
<!--       <span class="icon-menu" onclick="openNav()">&#9776;</span> -->
      <!--open icon animasi navbar mobile-->
      <div class="icon-electra-for-mobile">
        <a href="<?php echo base_url();?>"><img class="img-mobile"src="<?php echo ASSETS.'img/electra.png';?>" class="img-responsive"></a>
      </div>
      
      <div id="toggle_menusv2">
        <div class="one"></div>
        <div class="two"></div>
        <div class="three"></div>
      </div>

      <div id="menu_v2">
        <ul>
          <li><a href="<?php echo base_url();?>">Home</a></li>
          <li><a href="<?php echo base_url('about_us');?>">About Us</a></li>
          <li><a id="show-hidden-menufm">Products</a>
            <div class="hidden-menufm" style="display: none;">
              <ul>
                <li>List item 1</li>
                <li>List item 2</li>
                <li>List item 3</li>
              </ul>
            </div>
          </li>
          <li><a href="<?php echo base_url('news');?>">News & Updates</a></li>
          <li><a href="<?php echo base_url('hello_electra');?>">Hello Electra!</a></li>
        </ul>
      </div>
      
<!--       <div onclick="openNav()" id="menu-toggle">
        <div id="hamburger" onclick="openNav()" >
          <span></span>
          <span></span>
          <span></span>
        </div>
        <div id="cross" onclick="closeNav()">
          <span></span>
          <span></span>
        </div>
      </div> -->
      <!--close icon animasi navar mobile-->
    </nav>
    <!--close header nav for mobile-->
    
  </section>
  <?php $this->load->view($content);?>

  <footer class="footer-content" >
    <!--open footer for pc-->
    <div class="container "id="view-pc-footer">
    <div class="col-md-12">
      <div class="col-md-4 left-footer">
        <h4 class="title-cont-foot">Electronic Ticketing and Reservation Advance</h4>
        <p>Copyright 2018 by Electra</p>
      </div>

      <div class="col-md-4 center-footer">
        <h4 class="title-cont-foot">Get in Touch</h4>
        <form action="" method="post">
          <div class="form-group right-foot-center">
            <input type="text" class="form-control" name="email" id="email" placeholder="You Email">
          </div>
          <div class="btn-submit">
            <button class="btn-submite-custom" type="submite" >SUBMIT</button>
          </div>
        </form>
      </div>

      <div class="col-md-4 right-footer">
        <h4 class="title-cont-foot">Our Channel</h4>
        <ul>
          <li><a href="#"><span class="fa fa-instagram"></span></a></li>
            <li><a href="#"><span class="fa fa-linkedin"></span></a></li>
            <li><a href="#"><span class="fa fa-facebook"></span></a></li>
            <li><a href="#"><span class="fa fa-youtube-play"></span></a></li>
        </ul>
      </div>
    
    </div>
    </div>
    <!--close footer for pc-->
    
    <!--open footer for mobile-->
    <div class="container "id="view-mobile-footer">
    <div class="col-md-12">

      <div class="foot-col-size center-footer">
        <h4 class="title-cont-foot">Get in Touch</h4>
        <form action="" method="post">
          <div class="form-group right-foot-center">
            <input type="text" class="form-control" name="email" id="email" placeholder="You Email">
          </div>
          <div class="btn-submit">
            <button class="btn-submite-custom" type="submite" >SUBMIT</button>
          </div>
        </form>
      </div>

      <div class="foot-col-size right-footer">
        <h4 class="title-cont-foot">Our Channel</h4>
        <ul>
          <li><a href="#"><span class="fa fa-instagram"></span></a></li>
            <li><a href="#"><span class="fa fa-linkedin"></span></a></li>
            <li><a href="#"><span class="fa fa-facebook"></span></a></li>
            <li><a href="#"><span class="fa fa-youtube-play"></span></a></li>
        </ul>
      </div>
    
      <div class="foot-col-size left-footer">
        <h4 class="title-cont-foot">Electronic Ticketing and Reservation Advance</h4>
        <p>Copyright 2018 by Electra</p>
      </div>
      
    </div>
    </div>
    <!--close footer for mobile-->
    <div id="stop" class="scrollTop">
      <a href=""><img src="<?php echo base_url('assets/img/icon/');?>upbutton-01.png"></a>
    </div>
      <div class="floating-button">
        <a class="whatsapp" href="#"><img src="<?php echo base_url('assets/img/icon/');?>iconwa-03.png"></a>
    </div>
  </footer>
    
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="<?php echo BASE_URL;?>assets/js/bootstrap.min.js"></script>
  <!--  custom js-->
  <script src="<?php echo BASE_URL;?>assets/js/custom.js?<?php echo uniqid();?>"></script>
  <!--javascript animation scroll-->
  <script src="<?php echo BASE_URL;?>assets/js/aos.js"></script>
  <!--javascript animation scroll very smooth-->
<!--   <script src="<?php echo BASE_URL;?>assets/js/parachute.js"></script> -->
  <!--javascript animation scroll-->
  <script>
  AOS.init({
    easing: 'ease-in-out-sine'
  });
</script>
  <!--javascript animation scroll to ID element-->
    <script>
      $(document).ready(function() {
      'use strict';

      // Paralax
      $('header').each(function() {
      var e = $(this);
      $(window).scroll(function() {
        var t = ($(window).scrollTop() / e.data("speed"));
        var n = "50% "+ t + "px";
        e.css({backgroundPosition: n})
      })
      });

      // Heading Transition
      (function() {
      var header = $('.header-caption');
      var range = 200;
      $(window).on('scroll', function () {
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height / 2;
        var calc = 1 - (scrollTop - offset + range) / range;
        header.css({opacity: calc});
        if (calc > '1') {
          header.css({opacity: 1});
        } else if (calc < '0') {
          header.css({opacity: 0});
        }
      });
      }());
      })
  </script>
  <!--------------open js for very smooth scroll animation--------------------->
    <script>

// 		;(function($){
// 			$(window).ready(function(){



// 					Parachute.page({
// 						scrollContainer: '#scrollContainer',
// 						heightContainer: '#fakeScrollContainer'
// 					});

// 					Parachute.parallax({
// 						element: '.js-parallax-1',
// 						pxToMove: -400
// 						// topTriggerOffset: 200
// 					});

// 					Parachute.parallax({
// 						element: '.js-parallax-2',
// 						pxToMove: -200
// 					});



// 				Parachute.sequence({
// 					element: '.js-parallax-1',
// 					offset: 0,
// 					callback: function(active) {
// 						if (active) {
// 							$(this.$element).addClass('test');
// 						} else {
// 							$(this.$element).removeClass('test');
// 						}
// 					}
// 				});

// 				Parachute.init();

// 			});

// 		})(jQuery);

	</script>
</body>
</html>