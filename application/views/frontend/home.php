<!--
  Jakarta : 17/10/2018
  create By : Maningcorp
-->
<!--DOCTYPE HTML-->
<div class="container-fluid" id="container_body" color="transparent">
  <!--open header content-->
  <section class="row header-pos-cont">
    <div class="col-md-12 header" id="header">
    </div>
  </section>
  <!--close header content-->

  <!--open banner slide content-->
  <section class=" row banner-pos-cont" id="banner-content" data-speed="4">
    <div class="container-fluid content-firs">
      <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
          <li data-target="#myCarousel" data-slide-to="2"></li>
          <li data-target="#myCarousel" data-slide-to="3"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner header-carousel">
          <div class="item active">
            <div class="slider-banner" data-speed="4"></div>
            <img src="<?php echo BASE_URL;?>/assets/img/banner/slider2-01.jpg" alt="slider 1" style="width:100%;">
          </div>

          <div class="item">
            <div class="slider-banner" data-speed="4"></div>
            <img src="<?php echo BASE_URL;?>/assets/img/banner/slider-02.jpg" alt="slider 2" style="width:100%;">
          </div>

          <div class="item">
            <div class="slider-banner" data-speed="4"></div>
            <img src="<?php echo BASE_URL;?>/assets/img/banner/slider2-02.jpg" alt="slider 3" style="width:100%;">
          </div>
          <div class="item">
            <div class="slider-banner" data-speed="4"></div>
               <img src="<?php echo BASE_URL;?>/assets/img/banner/slider-04.jpg" alt="slider 4" style="width:100%;">
          </div>
        </div>
<!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>           
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
      </div>
    </div>

    <div class="banner-form-text d-sm-none">
      <div class="content-left">
        <div class="pos-text-banner header-caption">
          <h1>Behind Every Great Travel Company</h1>
          <p>We understand every travel industry need to keep up with the pace of change in the digital era in order to appealto customer demand.
          </p>
        </div>
      </div>
<!--       <div class="col-md-6 content-right">
        <div class="pos-form background-color">
          <div class="form-head">
            <h3>Ask us anything or request demo</h3>
          </div>
          <div class="body-form">
            <form action="" method="post">
              <div class="form-group">
                <input class="form-controls" type="text" placeholder="Name" name="name" id="name" required>
              </div>
              <div class="form-group">
                <input class="form-controls" type="text" placeholder="Email" name="email" id="email" required>
              </div>
              <div class="form-group">
                <select class="form-control" id="industry">
                              <option value="industry1">----Select Industry----</option>
                              <option value="industry2">industry 2</option>
                              <option value="industry3">industry 3</option>
                              <option value="industry4">industry 4</option>
                            </select>
              </div>
              <div class="form-group">
                <select class="form-control" id="product">
                              <option value="product1">----Select product----</option>
                              <option value="product2">product 2</option>
                              <option value="product3">product 3</option>
                              <option value="product4">product 4</option>
                            </select>
              </div>
              <button type="submit" class="btn btn-block-custom">Submit</button>
            </form>
          </div>
        </div>
      </div> -->
    </div>
      <div class="btn-scroll-to-explore">
          <a href="#about-us-content" data-text="scroll to explore" class="scrollTo effect-shine"></a>
      </div>
    
  </section>
  <!--close banner slide content-->

  <!--open about us content-->
  <section class=" row about-pos-cont background-color" id="about-us-content">
    <div class="container-fluid content-firs">
      <div class="content-text about-us" data-aos="fade-up">
        <h3 style="font-weight: lighter; font-family: helvetica;">Electra (Electronic Tiketing and Reresvation Advance) is an opration unit of Sabre Travel Network Indonesia Which has been in opration since 2013. The Electra business began with inventory and distribution management system for travel industry
          companies.
        </h3>
        <a href="<?php echo base_url('about_us');?>" class="button-all btn-about">About Us</a>
      </div>
    </div>
  </section>
  <!--close about us content-->

  <!--open our product content-->
  <section class=" row pro-pos-cont" id="our-product-content">
    <div class="container pos pro-con">

        <div class="size-for-mobile" data-aos="fade-up" data-aos-duration="200">
          <div class="card-pro-title">
            <h2 class="title-pro-unborder">Our</h2>
            <h2 class="title-pro-border">Products</h2>
          </div>
        </div>

        <div class="size-for-mobile" data-aos="fade-up" data-aos-duration="500">
            <a href="<?php echo base_url('products');?>">
          <div class="card-pro">
            <div class="head-card">
              <img src="<?php echo BASE_URL;?>assets/img/icon/ICON-ELECTRA-01.png" alt="icon airplane">
            </div>
            <div class="body-card">
              <h3>airline ticketing reservation system</h3>
              <p>An integrited system start from the process of reservation, inventory until check-in that are needed for daily airline opration </p>
            </div>
            <div class="footer-card">
              <a href="<?php echo base_url('products');?>" class="btn-card-pro">See more</a>
            </div>
              
          </div>
                </a>
        </div>

        <div class="size-for-mobile" data-aos="fade-up" data-aos-duration="800">
             <a href="<?php echo base_url('products');?>">
          <div class="card-pro">
            <div class="head-card">
              <img src="<?php echo BASE_URL;?>assets/img/icon/ICON-ELECTRA-02.png" alt="icon engine">
            </div>
            <div class="body-card">
              <h3>electra booking engine</h3>
              <p>electra booking engine (EBE) is an online reservation sytem for travel agent that wants to runs a business in the industry of online travel agent</p>
            </div>
            <div class="footer-card">
              <a href="<?php echo base_url('products');?>" class="btn-card-pro">See more</a>
            </div>
          </div>
            </a>
        </div>

        <div class="size-for-mobile" data-aos="fade-up" data-aos-duration="200">
             <a href="<?php echo base_url('products');?>">
          <div class="card-pro">
            <div class="head-card">
              <img src="<?php echo BASE_URL;?>assets/img/icon/ICON-ELECTRA-03.png" alt="icon hotel">
            </div>
            <div class="body-card">
              <h3>hotel reservation system</h3>
              <p>Provide an online hotel booking and ticketing engine for hotel group/chain, hosting product inventory and distribution</p>
            </div>
            <div class="footer-card">
              <a href="<?php echo base_url('products');?>" class="btn-card-pro">See more</a>
            </div>
          </div>
            </a>
        </div>

        <div class="size-for-mobile" data-aos="fade-up"data-aos-duration="500">
             <a href="<?php echo base_url('products');?>">
          <div class="card-pro">
            <div class="head-card">
              <img src="<?php echo BASE_URL;?>assets/img/icon/ICON-ELECTRA-04.png" alt="icon Digital">
            </div>
            <div class="body-card">
              <h3>Digital insurance Software</h3>
              <p>Solution for insurance company to manage their product and to market their product via online with the exiting distribution channel</p>
            </div>
            <div class="footer-card">
              <a href="<?php echo base_url('products');?>" class="btn-card-pro">See more</a>
            </div>
          </div>
            </a>
        </div>

        <div class="size-for-mobile" data-aos="fade-up" data-aos-duration="800">
             <a href="<?php echo base_url('products');?>">
          <div class="card-pro">
            <div class="head-card">
              <img src="<?php echo BASE_URL;?>assets/img/icon/ICON-ELECTRA-05.png" alt="icon Leisure">
            </div>
            <div class="body-card">
              <h3>Leisure Reservation System</h3>
              <p>An integrited system for the company runs in leisure industry to manage ther product and market their product via online with the exiting channel</p>
            </div>
            <div class="footer-card">
              <a href="<?php echo base_url('products');?>" class="btn-card-pro">See more</a>
            </div>
          </div>
            </a>
        </div>

    </div>
    <!--close container-->
  </section>
  <!--close our product content-->

  <!--open why choos us content-->
  <section class=" row why-pos-cont" id="choose-us-content">
    <div class="container pos pro-con">
      <div class="col-md-12">
        <div class="title-why">
          <h2 class="title-all-h2">Why <b>Choose Us?</b></h2>
        </div>
        <div class="size-for-mobile" data-aos="fade-up" data-aos-duration="200">
          <div class="card-pro-why background-color">
            <div class="head-card-why">
              <img src="<?php echo BASE_URL;?>assets/img/icon/ICON-ELECTRA-06.png" alt="icon Have Great">
            </div>
            <div class="body-card-why">
              <h3>have great experience</h3>
              <p>Solution for insurance company to manage their product and to market their product via online with the exiting distribution channel</p>
            </div>
          </div>
        </div>

        <div class="size-for-mobile" data-aos="fade-up" data-aos-duration="500">
          <div class="card-pro-why background-color">
            <div class="head-card-why">
              <img src="<?php echo BASE_URL;?>assets/img/icon/ICON-ELECTRA-07.png" alt="icon profesional team">
            </div>
            <div class="body-card-why">
              <h3>professional development team</h3>
              <p>Solution for insurance company to manage their product and to market their product via online with the exiting distribution channel</p>
            </div>
          </div>
        </div>

        <div class="size-for-mobile" data-aos="fade-up" data-aos-duration="800">
          <div class="card-pro-why background-color">
            <div class="head-card-why">
              <img src="<?php echo BASE_URL;?>assets/img/icon/ICON-ELECTRA-08.png" alt="icon fully support">
            </div>
            <div class="body-card-why">
              <h3>fully technical support</h3>
              <p>Solution for insurance company to manage their product and to market their product via online with the exiting distribution channel</p>
            </div>
          </div>
        </div>

      </div>
    </div>
  </section>
  <!--close why choos us content-->

  <!--open our clients content-->
  <section class=" row clients-pos-cont" id="our-clients-content">
    <div class="container pos pro-con">
      <div class="content-our-clients">
        <div class="title-why">
            <h2 class="title-all-h2">Our <b>Clients</b></h2>
        </div>
          <div class="our-client-size clients">
          <img src="<?php echo BASE_URL;?>assets/img/client-01.png">
          </div>
          <div class="our-client-size clients">
          <img src="<?php echo BASE_URL;?>assets/img/client-02.png">
          </div>
          <div class="our-client-size clients">
          <img src="<?php echo BASE_URL;?>assets/img/client-03.png">
          </div>
          <div class="our-client-size clients">
          <img src="<?php echo BASE_URL;?>assets/img/client-01.png">
          </div>
          <div class="our-client-size clients">
          <img src="<?php echo BASE_URL;?>assets/img/client-02.png">
          </div>
          <div class="our-client-size clients">
          <img src="<?php echo BASE_URL;?>assets/img/client-03.png">
          </div>
          <div class="our-client-size clients">
          <img src="<?php echo BASE_URL;?>assets/img/client-01.png">
          </div>
          <div class="our-client-size clients">
          <img src="<?php echo BASE_URL;?>assets/img/client-02.png">
          </div>
          <div class="our-client-size clients">
          <img src="<?php echo BASE_URL;?>assets/img/client-03.png">
          </div>
          <div class="our-client-size clients">
          <img src="<?php echo BASE_URL;?>assets/img/client-01.png">
          </div>
          <div class="our-client-size clients">
          <img src="<?php echo BASE_URL;?>assets/img/client-02.png">
          </div>
          <div class="our-client-size clients">
          <img src="<?php echo BASE_URL;?>assets/img/client-03.png">
          </div>
      </div>
    </div>
  </section>

  <!--open testimony slide content-->
  <section class="row testimony-pos-cont" id="banner-content">
    <div class="container-fluid content-firs pro-con">
      <div class="title-why">
        <h2 class="title-all-h2">What Our <b>Clients Say</b></h2>
      </div>
      <div id="myCarousel-testimony" class="carousel slide carousel-indicator" data-ride="carousel">
        <!-- Indicators -->
        <!-- Indicators -->
        <ol class="carousel-indicators carousel-center" style="bottom:0;">
          <li data-target="#myCarousel-testimony" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel-testimony" data-slide-to="1"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner bottom-carousel">
          <div class="item active">
            <div class="container-fluid">
              <div class="pos-cont-testimony">
                <div class="head">
                  <img src="<?php echo BASE_URL;?>assets/img/testi-02.png" alt="image clients">
                </div>
                <div class="body">
                  <p>Solution for insurance company to manage their product and to market their product via online with the exiting distribution channel. Solution for insurance company to manage their product and to market their product via online with the
                    exiting distribution channel</p>
                </div>
                <div class="foot">
                  <h4>Juvenile Jodjana - CEO Transnusa Aviation Mandiri</h4>
                </div>
              </div>
            </div>
          </div>

          <div class="item">
            <div class="container-fluid">
              <div class="pos-cont-testimony">
                <div class="head">
                  <img src="<?php echo BASE_URL;?>assets/img/testi-03.png" alt="image clients">
                </div>
                <div class="body">
                  <p>Solution for insurance company to manage their product and to market their product via online with the exiting distribution channel. Solution for insurance company to manage their product and to market their product via online with the
                    exiting distribution channel</p>
                </div>
                <div class="foot">
                  <h4>Rusmiati - Patih Indo Permai Owner</h4>
                </div>
              </div>
            </div>
          </div>
        </div>

        <!-- Left and right controls -->
        <a style="z-index:0" class="left carousel-control" href="#myCarousel-testimony" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left"></span>
              <span class="sr-only">Previous</span>
            </a>
        <a style="z-index:0" class="right carousel-control" href="#myCarousel-testimony" data-slide="next">
              <span class="glyphicon glyphicon-chevron-right"></span>
              <span class="sr-only">Next</span>
            </a>
      </div>
    </div>
  </section>
  <!--close testimony slide content-->
</div>
<!--close container-->
