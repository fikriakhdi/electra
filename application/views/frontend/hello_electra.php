<div class="container-fluid" id="hello_electra">
    <section class="row" id="content-hello-electra">
        <div class="col-md-6 contact">
            <div class="content list-add">
                <h3>Contact</h3>
                <h4>Head Office:</h4>
              <ul>
                <li><span class="fa fa-map-marker"></span> <p>Jalan Mampang Prapatan Raya NO. 93, <br>Tegal Parang, Mampang Prapatan Jakarta Selatan, DKI Jakarta, Indonesia<p></li>
                <li><span class="fa fa-phone"></span> <p>Phone : +622127535399<p></li>
                <li><span class="fa fa-envelope"></span> <p>Email : info@electra-indonesia.com<p></li>
              </ul>
              
            </div>
        </div>
        <div class="col-md-6 drop_message">
            <div class="hello_electra_contact">
                <h3>Drop Your Message</h3>
                <div class="pos-form-contact">
                    <form method="post" action="">
                        <div class="form-group">
                            <input type="name" class="form-controls" id="name" name="name" required placeholder="Your Name">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-controls" id="email" name="email" required placeholder="Your Email">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-controls" id="phone" name="phone" required placeholder="Your Phone Number">
                        </div>
                        <div class="form-group">
                            <textarea  class="form-controls" id="message" name="message" required placeholder="Your Message"></textarea>
                        </div>
                        <button class="btn btn-transparent" type="submit">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <section class="row hello_electra_map" id="pos-con-map">
        <div id="map"></div>
    </section>
</div><!--close container fluid-->
  <!--javascript for map-->
  <script src="http://maps.google.com/maps/api/js?key=AIzaSyD98vdNV_tuiFcWI1C_9df63zTjznqTQtU&sensor=false&libraries=places" type="text/javascript"></script>
<script type="text/javascript">
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 15,
    center: new google.maps.LatLng(-6.250862, 106.827104),
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });
  var position={lat: -6.250862, lng: 106.827104};
   var marker=new google.maps.Marker({
        map: map,
        // icon: icon,
        title: "Sabre Indonesia",
        position: position,
        draggable: false
      });
</script>