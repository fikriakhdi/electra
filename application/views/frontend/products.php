<!--
  Jakarta : 17/10/2018
  create By : Maningcorp
-->
<!--DOCTYPE HTML-->
<style>
  .body ul {
    padding: 0;
    font-size: 18px;
    list-style-image: url(<?php echo BASE_URL;?>/assets/img/icon/list-style.png);
  }
</style>
<div class="container-fluid">
  <!--open banner product content-->
  <section class=" row banner-pos-cont products-backgroud" id="banner-content" data-speed="4">
    <div class="container-fluid content-firs">
    </div>
    <div class="col-md-12 banner-form-text-product header-caption">
      <h1>Airline Ticketing Reservation System</h1>
    </div>
  </section>
  <!--close banner product slide content-->

  <!--open product content-->
  <header class=" row pos-cont-product " id="product-content">
    <div class="container-fluid content-firs-product">
      <div class="content-text product" data-aos="fade-up">
        <h3>Electra (Electronic Tiketing and Reresvation Advance) is an opration unit of Sabre Travel Network Indonesia Which has been in opration since 2013. The Electra business began with inventory and distribution management system for travel industry
          companies.
        </h3>
      </div>
    </div>
  </header>
  <!--close product content-->

  <!--open product owner content-->
  <section class=" row pro-pos-cont" id="product-owner-content">
    <div class="container">
      <div class="col-md-12">

        <div class="col-md-6 left-cont">
          <div class="post-img">
            <img src="<?php echo BASE_URL;?>/assets/img/icon/electraios-01.png">
          </div>
        </div>
        
        <div class="col-md-6 right-cont" data-aos="fade-left" data-aos-duration="500">
          <div class="post-list">
            <div class="head">
                <h3>Benefit for <b>Product Owner</b></h3>
            </div>
            <div class="body">
              <ul>
                <li>Sales Monitoring to each type of client (B2C,Travel Agent, Corporate, Reser)</li>
                <li>Integrated system for the front office until the back office management</li>
                <li>Dashboard Management for real time business</li>
                <li>Promotion tools to Buyer/Travel Agent</li>
              </ul>
            </div>
          </div>
        </div>

      </div>
    </div>
    <!--close container-->
  </section>
  <!--close product owner content-->
  
  
   <!--open travel agent content-->
  <section class=" row pro-pos-cont" id="product-agent-content">
    <div class="container">
      <div class="col-md-12">

        <div class="col-md-6 left-cont" data-aos="fade-right" data-aos-duration="500">
          <div class="post-list">
            <div class="head">
              <h3>Benefit for <b>Travel Agent</b></h3>
            </div>
            <div class="body">
              <ul>
                <li>Ticket reservation via online</li>
                <li>Show the available fight schedule and the booking status that are already been made</li>
                <li>virtual account payment via ATM, Internet Banking and Teler</li>
                <li>Can be access via mobile</li>
              </ul>
            </div>
          </div>
        </div>
        
        <div class="col-md-6 right-cont">
          <div class="post-img">
            <img src="<?php echo BASE_URL;?>/assets/img/icon/electraios-02.png" class="laptop">
              <img src="<?php echo BASE_URL;?>/assets/img/icon/electraios-03.png" class="iphone">
          </div>
        </div>

      </div>
    </div>
    <!--close container-->
  </section>
  <!--close travel agen content-->

  <!--open Our Key Feature content-->
  <section class=" row why-pos-cont" id="our-key-feature-content">
    <div class="container">
      <div class="col-md-12">
        <div class="title-why">
          <h2 class="title-all-h2">Our <b>Key Feature</b></h2>
        </div>
        <div class="col-sm-3">
          <div class="pos-icon-key">
            <div class="head"> 
              <img src="<?php echo BASE_URL;?>assets/img/icon/ICON-ELECTRA-09v2.png">
            </div>
            <div class="body">
              <h4>Multi Payment</h4>
            </div>
          </div>
        </div>

        <div class="col-sm-3">
          <div class="pos-icon-key">
            <div class="head">
            <img src="<?php echo BASE_URL;?>assets/img/icon/ICON-ELECTRA-10v2.png">
            </div>
            <div class="body">
              <h4>System Integration</h4>
            </div>
          </div>
        </div>

        <div class="col-sm-3">
          <div class="pos-icon-key">
            <div class="head">
                <img src="<?php echo BASE_URL;?>assets/img/icon/ICON-ELECTRA-11v2.png">
            </div>
            <div class="body">
              <h4>Channel Distribution</h4>
            </div>
          </div>
        </div>
        
        <div class="col-sm-3">
          <div class="pos-icon-key">
            <div class="head">
                <img src="<?php echo BASE_URL;?>assets/img/icon/ICON-ELECTRA-12v2.png">
            </div>
            <div class="body">
              <h4>Auto Refund</h4>
            </div>
          </div>
        </div>
      </div>
      
      <div class="col-md-12 post-3-icon">
          <br><br>
       <div class="col-md-3 pos-icon-pro" >
          <div class="pos-icon-key" style="margin-left:50%">
            <div class="head">
                <img src="<?php echo BASE_URL;?>assets/img/icon/ICON-ELECTRA-13.png">
            </div>
            <div class="body">
              <h4>Departure Control System</h4>
            </div>
          </div>
        </div>
        
        <div class="col-md-3 pos-icon-pro" >
          <div class="pos-icon-key" >
            <div class="head">
                <img src="<?php echo BASE_URL;?>assets/img/icon/ICON-ELECTRA-14.png">
            </div>
            <div class="body">
              <h4>B2B</h4>
            </div>
          </div>
        </div>
        
        <div class="col-md-3 pos-icon-pro">
          <div class="pos-icon-key" style="margin-right:50%">
            <div class="head">
                <img src="<?php echo BASE_URL;?>assets/img/icon/ICON-ELECTRA-15.png">
            </div>
            <div class="body">
              <h4>B2C</h4>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--close  Our Key Feature content-->

  <!--open our clients content-->
  <section class=" row clients-pos-cont" id="our-clients-content">
    <div class="container-fluid full-width">
      <div class="post-video">
        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/tWsNi9w0VWk?rel=0&amp;autoplay=1&loop=1&playlist=kC1jy2se9mM&mute=1&amp;controls=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
      </div>
    </div>
  </section>

  <!--open testimony slide content-->
  <section class="row testimony-pos-cont" id="banner-content">
    <div class="container">
      <div class="cont-form">
        <div class="title-why">
          <h2 class="title-all-h2">Interested With <b>Our Product? Request For Demo</b></h2>
        </div>
        <div class="pos-form-product">
          <form method="post" action="">
            <div class="form-group">
              <input type="name" class="form-controls" id="name" name="name" required placeholder="Your Name">
            </div>
            <div class="form-group">
              <input type="email" class="form-controls" id="email" name="email" required placeholder="Your Email">
            </div>
              <div class="form-group">
              <input type="text" class="form-controls" id="name" name="name" required placeholder="Your Phone Number">
            </div>
            <div class="form-group">
                <select class="form-control" id="industry">
                              <option value="industry1">----Select Industry----</option>
                              <option value="industry2">industry 2</option>
                              <option value="industry3">industry 3</option>
                              <option value="industry4">industry 4</option>
                            </select>
              </div>
              <div class="form-group">
                <select class="form-control" id="product">
                              <option value="product1">----Select product----</option>
                              <option value="product2">product 2</option>
                              <option value="product3">product 3</option>
                              <option value="product4">product 4</option>
                            </select>
              </div><br>
            <button class="btn btn-transparent" type="submit">Submit</button>
          </form>
        </div>
      </div>
    </div>
  </section>
  <!--close testimony slide content-->
</div>
<!--close container-->