<div class="container-fluid ">
  <!--open banner slide content-->
  <section class=" row update-pos-cont" id="update-content">
    <div class="container-fluid carousel-home">
      <!--open banner update-ditel content-->
      <section class=" row banner-pos-cont news-backgroud" id="banner-content" data-speed="4" style="background-image: url(<?php echo base_url($detail_news->image_feature);?>);">
        <div class="col-md-12 banner-form-text-product header-caption">
          <h1><?php echo $detail_news->title;?></h1>
          <span class="news-date"><?php echo date('d M Y', strtotime($detail_news->datecreated));?></span>
        </div>
      </section>
      <!--close banner update-ditel slide content-->
      <div class="share-container" >
<!--         <button id="show-hidden-menu" class="btn btn-default btn-share" ><span class="fa fa-share-alt"></span> </button>
              <section class="everything">
                <input type="checkbox" class="everything__checkbox" id="action-toogle">
                <label for="action-toogle" class="everything__button"></label>
                <div class="everything__link-area">
                    <a class="everything__link everything__link--1" href="#">
                        About me
                    </a>
                    <a class="everything__link everything__link--2" href="#">
                        Portfolio
                    </a>
                    <a class="everything__link everything__link--3" href="#">
                        Contact
                    </a>
                </div>
            </section> -->

        <button id="show-hidden-menu" class="btn btn-default btn-share" ><span class="fa fa-share-alt"></span> </button>
        <div class="hidden-menu" style="display: none;">
          <table>
            <tr>
              <td><a href="#"><span class="fa fa-instagram"></span></a></td>
              <td><a href="#"><span class="fa fa-linkedin"></span></a></td>
              <td><a href="#"><span class="fa fa-facebook"></span></a></td>
              <td><a href="#"><span class="fa fa-youtube-play"></span></a></td>
            </tr>
          </table>
        </div>
      </div>
      <section class=" row des-pos-cont" id="banner-content">
        <div class="container">
          <div class="pos-des">
            <?php echo $detail_news->content;?>
          </div> 
        </div>
      </section>
      
      
    </div>
  </section>
</div>