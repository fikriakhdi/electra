<!--
  Jakarta : 17/10/2018
  create By : Maningcorp
-->
<!--DOCTYPE HTML-->

<div class="container-fluid" id="container_body" color="transparent">
  <!--open header content-->
  <section class="row header-pos-cont">
    <div class="col-md-12 header" id="header">
    </div>
  </section>
  <!--close header content-->

  <!--open banner slide content-->
  <section class="row banner-pos-cont page-section" id="banner-content" data-speed="4" style="overflow:hidden">
    <div class="container-fluid content-firs">
      <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <?php 
            $slider = get_list_slider($this->session->userdata('lang_admin'));
            if($slider!=false){
            for($i=0;$i<$slider->num_rows();$i++){
            ?>
          <li data-target="#myCarousel" data-slide-to="<?php echo $i;?>" class="<?php echo ($i==0?'active':'');?>"></li>
            <?php }}?>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner header-carousel">
            <?php 
            $slider = get_list_slider($this->session->userdata('lang_admin'));
            $i=0;
            if($slider!=false){
            foreach($slider->result() as $slide){
                $i++;
            ?>
            <div class="item <?php echo ($i==1?'active':'');?>">
            <div class="slider-banner" data-speed="4"></div>
            <img src="<?php echo base_url($slide->picture);?>" alt="slider 1" style="width:100%;filter:brightness(80%);">
          </div>
            <?php }}?>
        </div>
<!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>           
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
      </div>
    </div>
    <div class="banner-form-text d-sm-none">
      <div class="content-left">
        <div class="pos-text-banner header-caption">
          <h1>Behind Every Great Travel Company</h1>
          <p>We understand every travel industry need to keep up with the pace of change in the digital era in order to appealto customer demand.
          </p>
        </div>
      </div>
    </div>


      <div class="btn-scroll-to-explore">
          <a href="#about-us-content" data-text="scroll to explore" class="scrollTo effect-shine"></a>
      </div>
    
  </section>
  <!--close banner slide content-->

  <!--open about us content-->
  <section class=" row about-pos-cont background-color page-section" id="about-us-content">
    <div class="container-fluid content-firs">
      <div class="content-text about-us" data-aos="fade-up">
        <h3 style="font-weight: lighter; font-family: helvetica;"><?php echo $about->who_are_we_content;?>
        </h3>
        <a href="<?php echo base_url('about_us');?>" class="button-all btn-about"><?php echo $this->lang->line('about_us');?></a>
      </div>
    </div>
  </section>
  <!--close about us content-->

  <!--open our product content-->
  <section class=" row pro-pos-cont page-section" id="our-product-content">
    <div class="container pos pro-con">

        <div class="size-for-mobile" data-aos="fade-up" data-aos-duration="200">
          <div class="card-pro-title">
            <?php if($this->session->userdata('site_lang')=='english'){ ?>
            <h2 class="title-pro-unborder"><?php echo $this->lang->line('our');?></h2>
            <h2 class="title-pro-border"><?php echo $this->lang->line('product');?></h2>
            <?php } else { ?> 
                <h2 class="title-pro-border"><?php echo $this->lang->line('product');?></h2>
            <h2 class="title-pro-unborder"><?php echo $this->lang->line('our');?></h2>
                <?php } ?>
          </div>
        </div>      
        <?php
          $product = get_list_product(lang_convert($this->session->userdata('site_lang')));
          if($product!=false){
              $duration = 500;
            foreach($product->result() as $item){   
              echo '
               <div class=" flip-container size-for-mobile" ontouchstart="this.classList.toggle(\'hover\');" data-aos="fade-up" data-aos-duration="'.$duration.'">
                <a href="'.base_url('products/'.$item->id).'">
                 <div class="flipper">
                  <div class="front card-pro">
                    <div class="head-card">
                      <img src="'.base_url($item->icon).'" alt="responsive">
                    </div>
                    <div class="body-card">
                      <h3>'.$item->name.'</h3>
                      <p>'.$item->description.'</p>
                    </div>
                    <div class="footer-card">
                    </div>
                  </div>
                  </a>
                  <a href="'.base_url('products/'.$item->id).'">
                  <div class="back card-pro">
                    <div class="body-card">
                      <div class="pos-btn-flip-more">
                        <a class="more" href="'.base_url('products/'.$item->id).'">'.$this->lang->line('see_more').'</a>
                      </div>
                      <div class="pos-btn-flip-more">
                        <a class="more" href="http://'.$item->demo_url.'" target="_blank">'.$this->lang->line('go_to_demo_site').'</a>
                      </div>
                    </div>
                  </div>
                  </div>
                  </a>
             </div>
           ';
                $duration+=100;
            }
          }
          ?> 
      

    
     
      <!-- close view our product-->
      
    </div>
    <!--close container-->
  </section>
  <!--close our product content-->

  <!--open why choos us content-->
  <section class=" row why-pos-cont page-section" id="choose-us-content">
    <div class="container pos pro-con">
      <div class="col-md-12 pos-mo-why">
        <div class="title-why">
          <h2 class="title-all-h2"><?php echo $this->lang->line('why');?><b> <?php echo $this->lang->line('chooseus');?></b></h2>
        </div>
        
           <?php
          $why_choose_us = get_list_why_choose_us(lang_convert($this->session->userdata('site_lang')));
          if($why_choose_us!=false){
              $duration = 200;
            foreach($why_choose_us->result() as $item){
              echo '
                <div class="size-for-mobile" data-aos="fade-up" data-aos-duration="'.$duration.'">
                  <div class="card-pro-why background-color-why">
                    <div class="head-card-why">
                      <img src="'.base_url($item->logo).'" alt="icon Have Great">
                    </div>
                    <div class="body-card-why">
                      <h3>'.$item->title.'</h3>
                      <p>'.$item->description.'</p>
                    </div>
                  </div>
                </div>

           ';
                $duration+=100;
            }
          } 
          ?>  
          
      </div>
    </div>
  </section>
  <!--close why choos us content-->

  <!--open our clients content-->
  <section class=" row clients-pos-cont page-section" id="our-clients-content">
    <div class="container pos pro-con">
      <div class="content-our-clients">
        <div class="title-why">
            <?php if($this->session->userdata('site_lang')=='english') {?>
            <h2 class="title-all-h2"><?php echo $this->lang->line('our');?> <b><?php echo $this->lang->line('client');?></b> </h2>
            <?php } else { ?>
            <h2 class="title-all-h2"><b><?php echo $this->lang->line('client');?></b> <?php echo $this->lang->line('our');?></h2>
            <?php } ?>
        </div>
                  <?php
          $client = get_list_client();
          if($client->num_rows()!=0){
            foreach($client->result() as $item){
              echo '
              
              <div class="our-client-size clients">
              <img src="'.base_url($item->client_logo).'">
              </div>
           ';
            }
          } else echo 'Tidak ada pilihan saat ini';
          ?> 
      </div>
    </div>
  </section>

    <?php 
          $testimonial = get_list_testimonial(lang_convert($this->session->userdata('site_lang')));
          if($testimonial!=false){
          ?>
  <!--open testimony slide content-->
  <section class="row testimony-pos-cont page-section" id="banner-content-testimony">
    <div class="container-fluid content-firs pro-con">
      <div class="title-why">
        <h2 class="title-all-h2"><?php echo $this->lang->line('whatour');?><b> <?php echo $this->lang->line('clientsay');?></b></h2>
      </div>
      <div id="myCarousel-testimony" class="carousel slide carousel-indicator" data-ride="carousel">
        <!-- Indicators -->
        <!-- Indicators -->
        <ol class="carousel-indicators carousel-center" style="bottom:0;">
          <?php
              $n = 0;
              foreach($testimonial->result() as $item){ 
                  $n++;
            ?>
          <li data-target="#myCarousel-testimony" data-slide-to="<?php echo $n-1;?>" class="<?php echo ($n==1?'active':'');?>"></li>
          <?php } ?>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner bottom-carousel">
            
             <?php
                  $n = 0;
            foreach($testimonial->result() as $item){ 
                $n++;
            ?>
              
                 <div class="item <?php echo ($n==1?'active':'');?>">
                    <div class="container-fluid">
                      <div class="pos-cont-testimony">
                        <div class="head">
                          <img src="<?php echo base_url($item->picture);?>" alt="image clients">
                        </div>
                        <div class="body">
                          <p><?php echo $item->comment;?>'</p>
                        </div>
                        <div class="foot">
                          <h4><?php echo ($item->name);?> - <?php echo ($item->company);?></h4>
                        </div>
                      </div>
                    </div>
                  </div>
           <?php
            }
          ?>   

        </div>

        <!-- Left and right controls -->
        <a style="z-index:0" class="left carousel-control" href="#myCarousel-testimony" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left"></span>
              <span class="sr-only">Previous</span>
            </a>
        <a style="z-index:0" class="right carousel-control" href="#myCarousel-testimony" data-slide="next">
              <span class="glyphicon glyphicon-chevron-right"></span>
              <span class="sr-only">Next</span>
            </a>
      </div>
    </div>
  </section>
    <?php } ?>
  <!--close testimony slide content-->
</div>
<!--close container-->
