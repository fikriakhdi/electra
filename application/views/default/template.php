<!--
  Jakarta : 17/10/2018
  create By : Maningcorp
-->
<!--DOCTYPE HTML-->

<html>

<head>
    
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-129093008-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-121833832-2');
        </script>
    
  <meta name="viewport" content="width=device-width" />
    <link rel="shortcut icon" type="image/png" href="<?php echo ASSETS;?>img/Favicon-02.png"/>
    <title><?php echo $title;?></title>
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo BASE_URL;?>assets/css/bootstrap.min.css">
  <!-- Electra Custom Style -->
  <link rel="stylesheet" href="<?php echo BASE_URL;?>assets/css/electra_style.css?<?php echo rand();?>">
  <!--Responsive style-->
  <link rel="stylesheet" href="<?php echo BASE_URL;?>assets/css/responsive_electra.css?<?php echo uniqid();?>">
  <!--animation scroll-->
  <link rel="stylesheet" href="<?php echo BASE_URL;?>assets/css/aos.css">
  <!--animation button share spread-->
  <link rel="stylesheet" href="<?php echo BASE_URL;?>assets/css/style_btn_share.css">
   <!--button share animation scc style-->
  <link rel="stylesheet" href="<?php echo BASE_URL;?>assets/css/animation_btn_share.css">
  <!-- font awesome-->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- font awesome-->
  <link rel='stylesheet' type='text/css' href="<?php echo BASE_URL;?>assets/css/fontawesome-webfont.woff2">
<!--    scroll indicator-->
  <link rel="stylesheet" href="<?php echo BASE_URL;?>assets/plugins/indicator_bullets/dist/jquery.scrollindicatorbullets.css">
</head>
<body >
  <section>
    <!--open header nav for tablet, laptop and PC -->
    <nav class="navbar navbar-default background-color nav-tlp" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand" href="<?php echo base_url();?>"><img src="<?php echo ASSETS.'img/electra.png';?>" class="img-responsive"></a>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="navbar-collapse collapse">

            <ul class="nav navbar-nav navbar-center">
                <li><a href="<?php echo base_url();?>"><?php echo $this->lang->line('main_header_home');?></a></li>
                <li><a href="<?php echo base_url('about_us');?>"><?php echo $this->lang->line('main_header_about_us');?></a></li>
                <li class="drop"><a href="#our-product-content" class="scrollTo"><?php echo $this->lang->line('our_products');?></a>
                  <div class="dropdownContain">
                    <div class="dropOut">
                        <?php
                          $products = get_list_product(lang_convert($this->session->userdata('site_lang')));
                          if($products!=false){
                              echo '<ul>';
                            foreach($products->result() as $item){
                              echo '
                                <li><a href="'.base_url('products/'.$item->id).'">'.$item->name.'</a></li>

                           ';
                            }
                              echo '</ul>';
                          }
                          ?>
                    </div>
                  </div>
                </li>
                <li><a href="<?php echo base_url('news');?>"><?php echo $this->lang->line('main_header_news_&_updates');?></a></li>
                <li><a href="<?php echo base_url('hello_electra');?>"><?php echo $this->lang->line('main_header_hello_electra');?></a></li>
                <?php
                $navbar = get_list_pages();
                if($navbar!=false){ ?>
                <li class="drop"><a href="#pages" class="scrollTo"><?php echo $this->lang->line('main_header_pages');?></a>
                    <div class="dropdownContain">
                    <div class="dropOut">
                         <?php
                          foreach($navbar->result() as $nav){
                              echo '
                              <ul>
                                <li><a href="'.base_url('pages/'.$nav->seo).'">'.$nav->title.'</a></li>
                              </ul>
                           ';
                            }
                          ?>
                    </div>
                  </div>
                </li>
                <?php 
                }
                ?>
            </ul>
           <ul class="nav navbar-nav navbar-right">
                <li><a style="    padding-top: 2px;border: solid 1px #fff;border-radius: 12px;" href="<?php echo base_url('en');?>">EN</a></li>
                <li style="padding:0"><a style="    padding-top: 2px;margin-left: 5px;;border: solid 1px #fff;border-radius: 12px;" href="<?php echo base_url('id');?>">ID</a></li>
            </ul>
        </div>
    </nav>
    <!--close header nav for tablet, laptop and PC -->
    
    <!--open header nav for mobile -->
    <nav class="navbar navbar-default transparent_red nav-mobile" role="navigation">
<!--       <span class="icon-menu" onclick="openNav()">&#9776;</span> -->
      <!--open icon animasi navbar mobile-->
      <div class="icon-electra-for-mobile">
        <a href="<?php echo base_url();?>"><img class="img-mobile"src="<?php echo ASSETS.'img/electra.png';?>" class="img-responsive"></a>
      </div>
      <ul class="nav navbar-right">
           <li><a style="    padding-top: 2px;border: solid 1px #fff;border-radius: 12px;" href="<?php echo base_url('en');?>">EN</a></li>
           <li style="padding:0"><a style="    padding-top: 2px;margin-left: 5px;;border: solid 1px #fff;border-radius: 12px;" href="<?php echo base_url('id');?>">ID</a></li>
       </ul>
      <div id="toggle_menusv2">
        <div class="one"></div>
        <div class="two"></div>
        <div class="three"></div>
      </div>

      <div id="menu_v2">
        <ul>
          <li><a href="<?php echo base_url();?>"><?php echo $this->lang->line('main_header_home');?></a></li>
          <li><a href="<?php echo base_url('about_us');?>"><?php echo $this->lang->line('main_header_about_us');?></a></li>
          <li><a id="show-hidden-menufm"><?php echo $this->lang->line('our_products');?></a><div class="fa fa-chevron-down rotate"></div>
            <div class="hidden-menufm" style="display: none;">
               <?php
                  $products = get_list_product(lang_convert($this->session->userdata('site_lang')));
                  if($products!=false){
                      echo '<ul>';
                    foreach($products->result() as $item){
                      echo '
                        <li><a class="new-page-title" href="'.base_url('products/'.$item->id).'">'.$item->name.'</a></li>
                      
                   ';
                    }
                      echo '</ul>';
                  }
                  ?>
            </div>
          </li>
          <li><a href="<?php echo base_url('news');?>"><?php echo $this->lang->line('main_header_news_&_updates');?></a></li>
          <li><a href="<?php echo base_url('hello_electra');?>"><?php echo $this->lang->line('main_header_hello_electra');?></a></li>
            <?php
                $navbar = get_list_pages();
                if($navbar!=false){ ?>
          <li><a id="show-hidden-menufm2"><?php echo $this->lang->line('pages');?></a>
          <div class="fa fa-chevron-down rotate"></div>
            <div class="hidden-menufm2" style="display: none;">
              <ul>
                         <?php
                          foreach($navbar->result() as $nav){
                              echo '
                              
                                <li><a class="new-page-title" href="'.base_url('pages/'.$nav->seo).'">'.$nav->title.'</a></li>
                              
                           ';
                            }
                          ?>
                </ul>
                    </div>
                </li>
                <?php 
                }
                ?>
        </ul>
      </div>
    </nav>
    <!--close header nav for mobile-->
    
  </section>
  <?php $this->load->view($content);?>

  <footer class="footer-content" >
    <!--open footer for pc-->
    <div class="container "id="view-pc-footer">
    <div class="col-md-12">
      <div class="col-md-4 left-footer">
        <h4 class="title-cont-foot left-footer">Electronic Ticketing and Reservation Advance</h4>
        <p>Copyright 2018 by Electra</p>
      </div>

      <div class="col-md-4 center-footer">
        <h4 class="title-cont-foot"><?php echo $this->lang->line('get_in_touch');?></h4>
        <form action="<?php echo base_url('backend/create_subscribe');?>" method="post" id="subscribe-form">
                <span id="warning_message"></span>
          <div class="form-group right-foot-center">
            <input type="text" class="form-control" name="email" id="email" placeholder="Email">
          </div>
          <div class="btn-submit">
            <button class="btn-submite-custom submit-btn" form-id="subscribe-form" >submit</button>
          </div>
        </form>
      </div>

      <div class="col-md-4 right-footer">
        <h4 class="title-cont-foot"><?php if($this->session->userdata('site_lang')=='english'){ ?>
            <?php echo $this->lang->line('our'); echo $this->lang->line('channel');?>
            <?php } else { ?> 
                <?php echo $this->lang->line('channel'); echo $this->lang->line('our');?>
          <?php } ?></h4>
        <ul>
          <?php 
            $settings_list = get_settings_list('social_media');
            if($settings_list!=false){
            foreach($settings_list->result() as $setting){
                if($setting->status==1){
            ?>  
          <li><a href="<?php echo $setting->value;?>"><span class="fa fa-<?php echo $setting->name;?>"></span></a></li>
            <?php }}}?>
        </ul>
      </div>
    
    </div>
    </div>
    <!--close footer for pc-->
    
    <!--open footer for mobile-->
    <div class="container "id="view-mobile-footer">
    <div class="col-md-12">

      <div class="foot-col-size center-footer">
        <h4 class="title-cont-foot"><?php echo $this->lang->line('get_in_touch');?></h4>
        <form action="" method="post">
          <div class="form-group right-foot-center">
            <input type="text" class="form-control" name="email" id="email" placeholder="You Email">
          </div>
          <div class="btn-submit">
            <button class="btn-submite-custom" type="submite" >Submit</button>
          </div>
        </form>
      </div>

      <div class="foot-col-size right-footer">
        <h4 class="title-cont-foot"><?php if($this->session->userdata('site_lang')=='english'){ ?>
            <?php echo $this->lang->line('our'); echo $this->lang->line('channel');?>
            <?php } else { ?> 
                <?php echo $this->lang->line('channel'); echo $this->lang->line('our');?>
          <?php } ?></h4>
        <ul>
            <?php 
            $settings_list = get_settings_list('social_media');
            if($settings_list!=false){
            foreach($settings_list->result() as $setting){
                if($setting->status==1){
            ?>  
          <li><a href="<?php echo $setting->value;?>"><span class="fa fa-<?php echo $setting->name;?>"></span></a></li>
            <?php }}}?>
        </ul>
      </div>
    
      <div class="foot-col-size fot-bot">
        <h4 class="title-cont-foot1">Electronic Ticketing and Reservation Advance</h4>
        <p>Copyright 2018 by <?php echo COMPANY_NAME;?></p>
      </div>
      
    </div>
    </div>
    <!--close footer for mobile-->
    <div id="stop" class="scrollTop">
      <a href=""><img src="<?php echo base_url('assets/img/icon/');?>upbutton-01.png"></a>
    </div>
      <div class="floating-button">
        <!--uji-->
        <div class="share-button">
          <input class="toggle1-input" id="toggle1-input" type="checkbox" />
          <label for="toggle1-input" class="toggle1" style="background-image:url(<?php echo base_url('assets/img/icon/iconchat-01.png');?>);background-size:cover"></label>
          <ul class="network-list">
              <?php
              $whatsapp_list = get_list_whatsapp();
              if($whatsapp_list!=false){
              foreach($whatsapp_list->result() as $whatsapp){
              ?>
            <li class="whatsapp">
              <a href="https://wa.me/<?php echo $whatsapp->number;?>" target="_blank"></a>
            </li>
              <?php } } ?>
          </ul>
        </div>
                <style>
              <?php 
              $i = 0;
              $n = -60;
              $whatsapp_list = get_list_whatsapp();
              if($whatsapp_list!=false){
              foreach($whatsapp_list->result() as $whatsapp){
              $i++;
            ?>
              input:checked ~ .network-list li:nth-child(<?php echo $i;?>) {
                      left: <?php echo $n;?>px;
                    }
                    input:checked ~ .network-list li:nth-child(<?php echo $i;?>) {
                          background-image:url("<?php echo base_url('assets/img/icon/iconwa-03.png');?>");
                          background-size:cover;
                          height: 92%;
                          border-radius:60px;
                        }
                    <?php $n-=60; } } ?>
              </style>
        <!--uji-->
      </div>
  </footer>
    
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
    <script src="<?php echo BASE_URL;?>assets/js/css3-mediaqueries.js"></script>
  <script src="<?php echo BASE_URL;?>assets/js/css3-mediaqueries.min.js"></script>
  <!-- media query -->
  <script src="<?php echo BASE_URL;?>assets/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.js"></script>
      <!-- Scroll Indicator -->
  <script src="<?php echo BASE_URL;?>assets/plugins/indicator_bullets/dist/jquery.scrollindicatorbullets.min.js"></script>
    <script src="https://cdn.rawgit.com/imakewebthings/waypoints/4.0.0/lib/noframework.waypoints.min.js"></script>
  <!--  custom js-->
  <script src="<?php echo BASE_URL;?>assets/js/custom.js?<?php echo uniqid();?>"></script>
  <!--  js for scroll indicator-->
<!--   <script src="<?php echo BASE_URL;?>assets/js/js_scroll_indicator.js"></script> -->
  <!--javascript animation scroll-->
  <script src="<?php echo BASE_URL;?>assets/js/aos.js"></script>
    <!--javascript submit  js-->
  <script src="<?php echo BASE_URL;?>assets/js/submit.js"></script>
  <!--javascript animation scroll very smooth-->
<!--   <script src="<?php echo BASE_URL;?>assets/js/parachute.js"></script> -->
  <!--javascript animation scroll-->
  <script>
  AOS.init({
    easing: 'ease-in-out-sine'
  });
</script>
  <!--javascript animation scroll to ID element-->
    <script>
      $(document).ready(function() {
      'use strict';

      // Paralax
      $('header').each(function() {
      var e = $(this);
      $(window).scroll(function() {
        var t = ($(window).scrollTop() / e.data("speed"));
        var n = "50% "+ t + "px";
        e.css({backgroundPosition: n})
      })
      });

      // Heading Transition
      (function() {
      var header = $('.header-caption');
      var range = 200;
      $(window).on('scroll', function () {
        var scrollTop = $(this).scrollTop();
        var offset = header.offset().top;
        var height = header.outerHeight();
        offset = offset + height / 2;
        var calc = 1 - (scrollTop - offset + range) / range;
        header.css({opacity: calc});
        if (calc > '1') {
          header.css({opacity: 1});
        } else if (calc < '0') {
          header.css({opacity: 0});
        }
      });
      }());
      })
  </script>
  <!--------------open js for very smooth scroll animation--------------------->
    <script>

// 		;(function($){
// 			$(window).ready(function(){



// 					Parachute.page({
// 						scrollContainer: '#scrollContainer',
// 						heightContainer: '#fakeScrollContainer'
// 					});

// 					Parachute.parallax({
// 						element: '.js-parallax-1',
// 						pxToMove: -400
// 						// topTriggerOffset: 200
// 					});

// 					Parachute.parallax({
// 						element: '.js-parallax-2',
// 						pxToMove: -200
// 					});



// 				Parachute.sequence({
// 					element: '.js-parallax-1',
// 					offset: 0,
// 					callback: function(active) {
// 						if (active) {
// 							$(this.$element).addClass('test');
// 						} else {
// 							$(this.$element).removeClass('test');
// 						}
// 					}
// 				});

// 				Parachute.init();

// 			});

// 		})(jQuery);

	</script>
  <script>
    $(".rotate").click(function () {
        $(this).toggleClass("down");
    });
  </script>
  <!-------------js for scroll animat------------->
  <script type="text/javascript">
//		var deleteLog = false;
//
//		$(document).ready(function() {
//	    	$('#pagepiling').pagepiling({
//	    		menu: '#menu',
//	    		anchors: ['page1', 'page2', 'page3'],
//	    		navigation: {
//		            'textColor': '#f2f2f2',
//		            'bulletsColor': '#ccc',
//		            'position': 'right',
//		            'tooltips': ['Page 1', 'Page 2', 'Page 3', 'Page 4']
//		        }
//			});
//	    });
    </script>
</body>
</html>