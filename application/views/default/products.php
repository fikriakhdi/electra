<!--
  Jakarta : 17/10/2018
  create By : Maningcorp
-->
<!--DOCTYPE HTML-->
<style>
  .body ul {
    padding: 0;
    font-size: 18px;
    list-style-image: url(<?php echo BASE_URL;?>/assets/img/icon/list-style.png);
  }
</style>
<div class="container-fluid" style="overflow:hidden">
  <!--open banner product content-->
  <section class=" row banner-pos-cont products-backgroud page-section" id="banner-content" data-speed="4" style="background-image: url(<?php echo base_url($products->header_image);?>);">
    <div class="container-fluid content-firs">
    </div>
    <div class="col-md-12 banner-form-text-product header-caption">
      <h1><?php echo $products->name;?></h1>
    </div>
  </section>
  <!--close banner product slide content-->

  <!--open product content-->
  <section class=" row pos-cont-product page-section " id="product-content">
    <div class="container-fluid content-firs-product">
      <div class="content-text product" data-aos="fade-up">
        <h3><?php echo $products->description;?>
        </h3>
      </div>
    </div>
  </section>
  <!--close product content-->

  <!--open product owner content-->
  <section class=" row pro-pos-cont page-section" id="product-owner-content">
    <div class="container">
      <div class="col-md-12">

        <div class="col-md-6 left-cont">
          <div class="post-img">
            <img src="<?php echo BASE_URL;?>/assets/img/icon/electraios-01.png">
              <div class="screen-content1 screen-content-scroll" style="background-image:url(<?php echo BASE_URL.$products->desktop_image;?>)">
              </div>
          </div>
        </div>
        
        <div class="col-md-6 right-cont" data-aos="fade-left" data-aos-duration="500">
          <div class="post-list">
            <div class="head">
                <h3><?php echo $this->lang->line('benefit_for'); echo '<b> '.$this->lang->line('product_owner').'</b>';?></h3>
            </div>
            <div class="body">
              <ul>
                <?php
                  $benefit_for_product_owner = explode(";", $products->benefits_product_owner);
                  foreach($benefit_for_product_owner as $item){
                  ?>
                  <li><?php echo $item;?></li>
                  <?php } ?>
              </ul>
            </div>
          </div>
        </div>

      </div>
    </div>
    <!--close container-->
  </section>
  <!--close product owner content-->
  
  
   <!--open travel agent content-->
  <section class=" row pro-pos-cont page-section" id="product-agent-content">
    <div class="container">
      <div class="col-md-12">

        <div class="col-md-6 left-cont" data-aos="fade-right" data-aos-duration="500">
          <div class="post-list">
            <div class="head">
                <h3><?php echo $this->lang->line('benefit_for'); echo '<b> '.$this->lang->line('travel_agent').'</b>';?></h3>
            </div>
            <div class="body">
              <ul>
                <?php
                  $benefit_for_travel_agent = explode(";", $products->benefits_travel_agent);
                  foreach($benefit_for_travel_agent as $item){
                  ?>
                  <li><?php echo $item;?></li>
                  <?php } ?>
              </ul>
            </div>
          </div>
        </div>
        
        <div class="col-md-6 right-cont">
          <div class="post-img">
              
            <img src="<?php echo BASE_URL;?>/assets/img/icon/electraios-02.png" class="laptop">
              <div class="screen-content2 screen-content-scroll" style="background-image:url(<?php echo BASE_URL.$products->desktop_image;?>)">
              </div>
              <img src="<?php echo BASE_URL;?>/assets/img/icon/electraios-03.png" class="iphone">
              <div class="screen-content3 screen-content-scroll" style="background-image:url(<?php echo BASE_URL.$products->mobile_image;?>)">
              
          </div>
        </div>

      </div>
    </div>
      </div>
    <!--close container-->
  </section>
  <!--close travel agen content-->

  <!--open Our Key Feature content-->
    <?php 
      $key_feature = get_list_key_feature($products->id);
      if($key_feature!=false){
      ?>
  <section class=" row why-pos-cont page-section" id="our-key-feature-content">
    <div class="container">
      <div class="col-md-12">
        <div class="title-why">
          <h2 class="title-all-h2">Our <b>Key Feature</b></h2>
        </div>
          
          <?php
            foreach($key_feature->result() as $key_feature){
              echo '
                    <div class="col-sm-3">
                      <div class="pos-icon-key">
                        <div class="head"> 
                          <img src="'.base_url($key_feature->icon).'">
                        </div>
                        <div class="body">
                          <h4>'.$key_feature->title.'</h4>
                        </div>
                      </div>
                    </div>
           ';
            } ?>
      </div>
    </div>
  </section>
    <?php 
                    }
          ?> 
  <!--close  Our Key Feature content-->
 <?php if($products->video_url!="") { ?>
  <!--open our clients content-->
  <section class=" row clients-pos-cont page-section" id="our-clients-content">
    <div class="container-fluid full-width">
      <div class="post-video">
        <iframe width="100%" height="100%" src="<?php echo $products->video_url;?>?rel=0&amp;=1&loop=1&playlist=kC1jy2se9mM&mute=1&amp;controls=0&amp;showinfo=0"></iframe>
      </div>
    </div>
  </section>
    <?php } ?>

  <!--open testimony slide content-->
  <section class="row testimony-pos-cont page-section" id="banner-content">
    <div class="container">
      <div class="cont-form">
        <div class="title-why">
            <h2 class="title-all-h2"><?php echo $this->lang->line('interested_with'); echo '<b> '.$this->lang->line('our_product?').'</b>';?></h2>
        </div>
        <div class="pos-form-product">
          <form method="post" action="">
             <div class="form-group"> 
                <label class="label1">To: <?php echo get_settings('company_email');?></label>
              </div>
               <div class="form-group"> 
                  <input type="text" class="form-controls" id="name" name="name" required placeholder="Your Name">
                </div>
               <div class="form-group">
                   <input type="email" class="form-controls" id="email" name="email" required placeholder="Your Email">
                </div>
                <div class="form-group">
                    <textarea  class="form-controls" id="message" name="message" required placeholder="Your Message"></textarea>
                </div>
              <button class="btn btn-transparent" type="submit">Submit</button>
          </form>
        </div>
      </div>
    </div>
  </section>
  <!--close testimony slide content-->
</div>
<!--close container-->