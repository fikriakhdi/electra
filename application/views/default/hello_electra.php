<div class="container-fluid page-section" id="hello_electra">
    <section class="row" id="content-hello-electra">
        <div class="col-md-6 contact">
            <div class="content list-add">
                <h3><?php echo $this->lang->line('contact');?></h3>
              <ul>
                <li><span class="fa fa-map-marker"></span> <p><?php echo get_settings('company_address');?><p></li>
                <li><span class="fa fa-phone"></span> <p><?php echo get_settings('company_phone');?><p></li>
                <li><span class="fa fa-envelope message"></span> <p><a href="mailto:<?php echo get_settings('company_email');?>"><?php echo get_settings('company_email');?></a><p></li>
              </ul>
              
            </div>
        </div>
        <div class="col-md-6 drop_message">
            <?php if(!empty($this->session->userdata('message'))) echo $this->session->userdata('message');?>
            <div class="hello_electra_contact">
                <h3><?php echo $this->lang->line('send_message');?></h3>
                <div class="pos-form-contact">
                    <form method="post" action="<?php echo base_url('kirim_email');?>">
                        <div class="form-group">
                          <label class="label1">To: <?php echo get_settings('company_email');?></label>
                        </div>
                        <div class="form-group"> 
                            <input type="text" class="form-controls" id="name" name="name" required placeholder="Your Name">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-controls" id="email" name="email" required placeholder="Your Email">
                        </div>
                        <div class="form-group">
                            <textarea  class="form-controls" id="message" name="message" required placeholder="Your Message"></textarea>
                        </div>
                        <button class="btn btn-transparent" type="submit">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <section class="row hello_electra_map page-section" id="pos-con-map">
        <div id="map"></div>
    </section>
</div><!--close container fluid-->
  <!--javascript for map-->
  <script src="http://maps.google.com/maps/api/js?key=AIzaSyD98vdNV_tuiFcWI1C_9df63zTjznqTQtU&sensor=false&libraries=places" type="text/javascript"></script>
<script type="text/javascript">
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 15,
    center: new google.maps.LatLng(<?php echo get_settings('latitude');?>, <?php echo get_settings('longitude');?>),
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });
  var position={lat: <?php echo get_settings('latitude');?>, lng: <?php echo get_settings('longitude');?>};
   var marker=new google.maps.Marker({
        map: map,
        // icon: icon,
        title: "Sabre Indonesia",
        position: position,
        draggable: false
      });
</script>