<div class="container-fluid under-navbar">
  <!--open banner slide content-->
  <section class=" row update-pos-cont page-section" id="update-content">
      <br>
    <div class="container-fluid">
    		  <?php
              if($news->num_rows()!=0){
                foreach($news->result() as $item){
                  ?>
                  <div class="col-md-4">
                    <div class="card-upadte">
                      <div class="card-upadte-img" style="background-image:url(<?php echo base_url($item->image_feature);?>)">
                        <div class="pos-img-shr" data-toggle="modal" data-target="#Modal-share">
                            <a><span class="fa fa-share-alt"></span></a>
                        </div>
                        <div class="overlay">
                          <h3><?php echo view_short($item->title);?></h3>
                          <span><?php echo date("Y-m-d", strtotime($item->datecreated));?></span>
                          <p><a href="<?php echo base_url('detail_news/'.$item->seo);?>" class="link_white" ><?php echo $this->lang->line('continue_reading');?></a></p>
                        </div>
                      </div>
                    </div>
                  </div>
                  
                <?php } ?>
              <?php } ?>
            
        <div class="col-md-12">
             <div class="col-md-12">
                <p style="text-align:center">
                    <?php echo $pagination; ?>
                      </p>
                    </div>
            <br>
        </div>
    </div>
      </div>      
  </section>
    <!-- Modal -->
  <div class="modal fade" id="Modal-share" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
        </div>
        <div class="modal-body">
          <table>
            <tr>
              <td><a class="face" href="#"><span class="fa fa-facebook"></span>Facebook</a></td>
              <td><a class="twit" href="#"><span class="fa  fa-twitter"></span>Twitter</a></td>
              <td><a class="yout" href="#"><span class="fa fa-youtube-square"></span>Youtube</a></td>
              <td><a class="what" href="#"><span class="fa fa-whatsapp"></span>Whatsapp</a></td>
            </tr>
            <tr>
              <td><a class="link" href="#"><span class="fa fa-linkedin"></span>Linkedin</a></td>
              <td><a class="goog" href="#"><span class="fa fa-google"></span> Fa-google</a></td>
              <td><a class="slac" href="#"><span class="fa fa-slack"></span>Slack</a></td>
              <td><a class="inst" href="#"><span class="fa fa-instagram"></span>Instagram</a></td>
            </tr>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" style="color:#fff;background:#bb1c32" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>