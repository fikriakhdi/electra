<div class="container-fluid ">
  <!--open banner slide content-->
  <section class=" row update-pos-cont" id="update-content">
    <div class="container-fluid carousel-home">
      <!--open banner update-ditel content-->
      <section class=" row banner-pos-cont news-backgroud page-section" id="banner-content" data-speed="4" style="background-image: url(<?php echo base_url($detail_news->image_feature);?>);">
        <div class="col-md-12 banner-form-text-product header-caption">
          <h1><?php echo $detail_news->title;?></h1>
          <span class="news-date"><?php echo date('d M Y', strtotime($detail_news->datecreated));?></span>
        </div>
      </section>
      <!--close banner update-ditel slide content-->
      <div class="share-container">
        <div class="share-button" style="margin:auto">
          <input class="toggle2-input" id="toggle2-input" type="checkbox" />
          <label for="toggle2-input" class="toggle2"></label>
          <ul class="network-list1">
            <li class="twitter">
              <a href="http://twitter.com/share?text=<?php echo $detail_news->title;?>&url=http://<?php echo base_url('detail_news/'.$detail_news->seo);?>&hashtags=#<?php echo COMPANY_NAME;?>">Share on Twitter</a>
            </li>
            <li class="facebook">
              <a href="https://www.facebook.com/sharer.php?u=<?php echo base_url('detail_news/'.$detail_news->seo);?>&t=<?php echo $detail_news->title;?>">Share on Facebook</a>
            </li> 
            <li class="googleplus">
              <a href="#" class="copy-btn" data-clipboard-text="<?php echo base_url('detail_news/'.$detail_news->seo);?>">Copy Link</a>
            </li>
            <li class="googleplus1">
              <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo base_url('detail_news/'.$detail_news->seo);?>&title=<?php echo $detail_news->title;?>&source=<?php echo COMPANY_NAME;?>">Share on Linkedin</a>
            </li>
          </ul>
        </div>
      </div>
      <section class=" row des-pos-cont page-section" id="banner-content">
        <div class="pos-content-news-default-tema">
          <div class="pos-des">
            <?php echo $detail_news->content;?>
          </div> 
        </div>
      </section>
      
      
    </div>
  </section>
</div>