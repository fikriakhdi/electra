<style>
  .content-abt {
    width:100%;
    float:left;
  }
  .content-abt .left {
    width:50%;
    float:left;
  }
  .content-abt .right {
    width:50%;
    float:left;
  }
  .content-abt .left img {
    width:100%;
  }
  .content-abt .right img {
    width:100%;
  }
</style>
<div class="container-fluid" id="container_body" color="transparent" style="padding:0">
  <section class="pos-abt" id="pos-abt1">
    <div class="content-abt" >
      <div class="left" data-aos="fade-right" data-aos-duration="500">
        <img src="<?php echo ASSETS.'img/aboutus-01.jpg';?>">
      </div>
      <div class="right" data-aos="fade-left" data-aos-duration="500">
        <h3>Who</h3><h3 style="font-weight: bold;">We Are?</h3><br>
		    <p><?php echo $about->who_are_we_content;?></p>
      </div>
    </div>
  </section>
  
  <section class="pos-abt" id="pos-abt1">
    <div class="content-abt">
      <div class="left" data-aos="fade-right" data-aos-duration="500">
        <h3>Who</h3><h3 style="font-weight: bold;">We Are?</h3><br>
		    <p><?php echo $about->who_are_we_content;?></p>
      </div>
      <div class="right" data-aos="fade-left" data-aos-duration="500">
         <img src="<?php echo ASSETS.'img/aboutus-02.jpg';?>">
      </div>
    </div>
  </section>
  
  <section class="pos-abt" id="pos-abt1">
    <div class="content-abt">
      <div class="left" data-aos="fade-right" data-aos-duration="500">
        <img src="<?php echo ASSETS.'img/aboutus-03.jpg';?>">
      </div>
      <div class="right" data-aos="fade-left" data-aos-duration="500">
        <h3>Who</h3><h3 style="font-weight: bold;">We Are?</h3><br>
		    <p><?php echo $about->who_are_we_content;?></p>
      </div>
    </div>
  </section>
</div>