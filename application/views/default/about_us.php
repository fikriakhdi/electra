
<div class="container-fluid under-navbar "  id="container_body">
  <!-----------------open about content view for pc--------------------------->
    <div class="header-caption"></div>
    <div>
		  <section class="row about-for-pc page-section">
        <div data-aos="fade-right" data-aos-duration="500" >
          <div class="col-md-6 pic-about-1" style="background-image: url(<?php echo base_url($about->who_are_we_pic);?>);">
          </div>
        </div>
		    <div class="col-md-6 copy">
		      <div class="" data-aos="fade-up" data-aos-duration="500">
                  <?php echo'<h3>'.$this->lang->line('who').'</h3>'; echo '<h3 style="font-weight: bold;">'.$this->lang->line('are_we').'?</h3><br>';?>
		      	<p><?php echo $about->who_are_we_content;?></p>
		      </div>
		    </div>
      </section>
      <section class="row about-for-pc page-section">
		    <div class="col-md-6 copy">
		    	<div class="" data-aos="fade-right" data-aos-duration="500">
          
            <?php echo'<h3>'.$this->lang->line('what').'</h3>'; echo '<h3 style="font-weight: bold;">'.$this->lang->line('we_do').'?</h3><br>';?>

		      	<p><?php echo $about->what_we_do_content;?></p>
		      </div>
		    </div>
        <div data-aos="fade-left" data-aos-duration="500" >
          <div class="col-md-6 pic-about-2" style="background-image: url(<?php echo base_url($about->what_we_do_pic);?>);">
          </div>
        </div>
      </section>
      <section class="row about-for-pc page-section">
        <div data-aos="fade-right" data-aos-duration="500" >
          <div class="col-md-6 pic-about-3" style="background-image: url(<?php echo base_url($about->who_do_we_work_with_pic);?>);">
          </div>
        </div>
		    <div class="col-md-6 copy">
		      <div class="" data-aos="fade-left" data-aos-duration="500">
                
            <?php echo'<h3>'.$this->lang->line('who_do').'</h3>'; echo '<h3 style="font-weight: bold;">'.$this->lang->line('we_work_with').'?</h3><br>';?>
        
		      	<p><?php echo $about->who_do_we_work_with_content;?></p>
                  <a href="<?php echo base_url('#our-clients-content');?>" class="about_us_link"><h5 style="text-align: center; padding-top: 30px; font-weight:bold">
                    <?php if($this->session->userdata('site_lang')=='english'){ ?>
                    <?php echo $this->lang->line('our'); echo $this->lang->line('client');?>
                    <?php } else { ?> 
                    <?php echo $this->lang->line('channel'); echo $this->lang->line('our');?>
                    <?php } ?>
                      </h5></a><hr class="line">
		      </div>
		    </div>
      </section>
      <section class="row about-for-pc page-section">
		    <div class="col-md-6 copy">
		    	<div class="" data-aos="fade-right" data-aos-duration="500">
            <?php echo'<h3>'.$this->lang->line('what_we').'</h3>'; echo '<h3 style="font-weight: bold;">'.$this->lang->line('can_do_for_you').'?</h3><br>';?>
		      	<p><?php echo $about->what_we_can_do_for_you_content;?></p>
            <a href="<?php echo base_url('#our-product-content');?>" class="about_us_link"><h5 style="text-align: center; padding-top: 30px;font-weight:bold">
                    <?php if($this->session->userdata('site_lang')=='english'){ ?>
                    <?php echo $this->lang->line('our'); echo $this->lang->line('product');?>
                    <?php } else { ?> 
                    <?php echo $this->lang->line('product'); echo $this->lang->line('our');?>
                    <?php } ?>
                </h5></a><hr class="line">
		      </div>
		    </div>
        <div data-aos="fade-left" data-aos-duration="500" >
          <div class="col-md-6 pic-about-4" style="background-image: url(<?php echo base_url($about->what_we_can_do_for_you_pic);?>);">
          </div>
        </div>
		  </section>
  </div>
<!-----------------close about content view for pc--------------------------->
  
  
  <!-----------------open about content view for mobile--------------------------->
  <div class="row about-for-mobile">
        <div data-aos="fade-left" data-aos-duration="500" class="col-md-6 pic-about-1" style="background-image: url(<?php echo base_url($about->who_are_we_pic);?>);">
		    </div>
		    <div class="col-md-6 copy">
		      <div class="text-cont" data-aos="fade-right" data-aos-duration="500">
                  <?php echo'<h3>'.$this->lang->line('who').'</h3>'; echo '<h3 style="font-weight: bold;">'.$this->lang->line('are_we').'?</h3><br>';?>
		      	<p><?php echo $about->who_are_we_content;?></p>
		      </div>
		    </div>
    
		    <div data-aos="fade-left" data-aos-duration="500" class="col-md-6 pic-about-2" style="background-image: url(<?php echo base_url($about->what_we_do_pic);?>);">
		    </div>
		    <div class="col-md-6 copy">
		    	<div class="text-cont" data-aos="fade-right" data-aos-duration="500">
            <?php echo'<h3>'.$this->lang->line('what').'</h3>'; echo '<h3 style="font-weight: bold;">'.$this->lang->line('we_do').'?</h3><br>';?>
		      	<p><?php echo $about->what_we_do_content;?></p>
		      </div>
		    </div>
    
		    <div data-aos="fade-left" data-aos-duration="500" class="col-md-6 pic-about-3" style="background-image: url(<?php echo base_url($about->who_do_we_work_with_pic);?>);">
		    </div>
        <div class="col-md-6 copy">
		      <div class="text-cont" data-aos="fade-right" data-aos-duration="500">
            <?php echo'<h3>'.$this->lang->line('who_do').'</h3>'; echo '<h3 style="font-weight: bold;">'.$this->lang->line('we_work_with').'?</h3><br>';?>
		      	<p><?php echo $about->who_do_we_work_with_content;?></p>
                  <a href="<?php echo base_url('#our-clients-content');?>" class="about_us_link" style="text-align: center; padding-top: 30px; font-weight:bold">
                    <?php if($this->session->userdata('site_lang')=='english'){ ?>
                    <?php echo $this->lang->line('our'); echo $this->lang->line('client');?>
                    <?php } else { ?> 
                    <?php echo $this->lang->line('channel'); echo $this->lang->line('our');?>
                    <?php } ?>
                  </a><hr class="line">
		      </div>
		    </div>
		    
		    <div data-aos="fade-left" data-aos-duration="500" class="col-md-6 pic-about-4" style="background-image: url(<?php echo base_url($about->what_we_can_do_for_you_pic);?>);">
		    </div>
		    <div class="col-md-6 copy">
		    	<div class="text-cont" data-aos="fade-right" data-aos-duration="500">
            <?php echo'<h3>'.$this->lang->line('what_we').'</h3>'; echo '<h3 style="font-weight: bold;">'.$this->lang->line('can_do_for_you').'?</h3><br>';?>
		      	<p><?php echo $about->what_we_can_do_for_you_content;?><</p>
            <a href="<?php echo base_url('#our-product-content');?>" class="about_us_link" style="text-align: center; padding-top: 30px; font-weight:bold">
                    <?php if($this->session->userdata('site_lang')=='english'){ ?>
                    <?php echo $this->lang->line('our'); echo $this->lang->line('product');?>
                    <?php } else { ?> 
                    <?php echo $this->lang->line('product'); echo $this->lang->line('our');?>
                    <?php } ?>
                    </a><hr class="line">
		      </div>
		    </div>
		</div>
  <!-----------------open about content view for mobile--------------------------->
  
  <!--open testimony slide content-->
  <section class="row testimony-pos-cont" id="banner-content">
    <div class="">
      <div class="cont-form">
        <div class="title-why">
          <h2 class="title-all-h2"><?php echo $this->lang->line('interested_with');?><b> <?php echo $this->lang->line('our_product?');?> <?php echo $this->lang->line('our_product');?></b></h2>
        </div>
        <div class="pos-form-product">
          <form method="post" action="">
             <div class="form-group"> 
                <label class="label1">To: <?php echo get_settings('company_email');?></label>
              </div>
               <div class="form-group"> 
                  <input type="text" class="form-controls" id="name" name="name" required placeholder="Your Name">
                </div>
               <div class="form-group">
                   <input type="email" class="form-controls" id="email" name="email" required placeholder="Your Email">
                </div>
                <div class="form-group">
                    <textarea  class="form-controls" id="message" name="message" required placeholder="Your Message"></textarea>
                </div>
              <button class="btn btn-transparent" type="submit">Submit</button>
          </form>
        </div>
      </div>
    </div>
    <div class='scrolltop'>
      <div class='scroll icon'><i class="fa fa-4x fa-angle-up"></i></div>
    </div>
  </section>
  <!--close testimony slide content-->
  <div id="stop" class="scrollTop">
      <a href=""><img src="<?php echo base_url('assets/img/icon/');?>upbutton-01.png"></a>
    </div>
</div>

            