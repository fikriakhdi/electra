$("#toggle_menusv2_tema2").click(function() {
  $(this).toggleClass("on");
  $("#menu_v2_tema2").slideToggle();
});

$(document).ready(function () {
      w3.includeHTML(init);
});

function init() {
     $('#test').BootSideMenu({
         side: "right",
         pushBody: false,
         remember: false
      });
}

/**-----------------------------Js Untuk parallax animation about us---------------------------------**/
$(window).scroll(function(){

  	// Add parallax scrolling to all images in .paralax-image container
		  $('.parallax-image-tema2').each(function(){
		    // only put top value if the window scroll has gone beyond the top of the image
				    if ($(this).offset().top < $(window).scrollTop()) {
      			// Get ammount of pixels the image is above the top of the window
      			var difference = $(window).scrollTop() - $(this).offset().top;
			      // Top value of image is set to half the amount scrolled
			      // (this gives the illusion of the image scrolling slower than the rest of the page)
			      var half = (difference / 2) + 'px',
                transform = 'translate3d( 0, ' + half + ',0)';

			      $(this).find('img').css('transform', transform);
    		} else {
			      // if image is below the top of the window set top to 0
      			$(this).find('img').css('transform', 'translate3d(0,0,0)');
		    }
		  });
});

/**-----------------------------Js for animation hello electra tema2---------------------------------**/
function infolocation (){
  var locid = document.getElementByid("BtnInfoLoc").classList;
  if (locid.contains("InfoHide")) {
    locid.remove("InfoHide");
  }
  else { locid.add("InfoHide") }
  
  if (locid.content("InfShow")) {
    locid.remove("InfoShow");
  }
  else {
    locid.add("InfoShow");
  }
}

function toggleColor() {
  var myButtonClasses = document.getElementById("btn1").classList;

  if (myButtonClasses.contains("blue")) {
    myButtonClasses.remove("blue");
  } else {
    myButtonClasses.add("blue");
  }
  if (myButtonClasses.contains("red")) {
    myButtonClasses.remove("red");
  } else {
    myButtonClasses.add("red");
  }
}













