$(window).scroll(function() {

    //After scrolling 100px from the top...
    if ( $(window).scrollTop() >= 100 ) {

        $('.navbar').removeClass('transparent_red');
        $('.navbar').addClass('background-color');
        
    //Otherwise remove inline styles and thereby revert to original stying
    } else {
        var container = $("#container_body");
        if(container.attr("color")=="transparent"){
        $('.navbar').addClass('transparent_red');
        $('.navbar').removeClass('background-color');
        }
    }
});

$('.page-section').scrollIndicatorBullets({
waypointOffsetDown: 100
});
$(document).ready(function(){
    var container = $("#container_body");
        if(container.attr("color")=="transparent"){
        $('.navbar').addClass('transparent_red');
        $('.navbar').removeClass('background-color');
        }
    
    $(".datatable").DataTable();
});

/**open js for navbar mobile **/
$(document).ready(function() {
  $('#show-hidden-menufm').click(function() {
    $('.hidden-menufm').slideToggle("slow");
    // Alternative animation for example
    // slideToggle("fast");
  });
});

/**open js for navbar mobile **/
$(document).ready(function() {
  $('#show-hidden-menufm2').click(function() {
    $('.hidden-menufm2').slideToggle("slow");
    // Alternative animation for example
    // slideToggle("fast");
  });
});

function openNav() {
    document.getElementById("mySidenav").style.width = "100%";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
function myFunction() {
    var x = document.getElementById("mySidenav");
    if (x.style.width === "0") {
        x.style.width = "100%";
    } else {
        x.style.width = "0";
    }
}
// $('#menu-toggle').click(function(){
//     $(this).toggleClass('open');
//  })
var percent = 0;
var direction = "down";
window.setInterval(function(){
  if(direction=="down" && percent>=0 && percent<100)
  percent+=1;
  if(percent==100) direction="up";
  if(percent==0) direction="down";
  if(direction=="up" && percent>0 && percent<=100)
  percent-=1;
  $(".screen-content-scroll").animate({"background-position-y":percent+"%"},100);
}, 125);

$("#toggle_menusv2").click(function() {
  $(this).toggleClass("on");
  $("#menu_v2").slideToggle();
});

// $(document).ready(function() {
//   $('#show-hidden-menu').click(function() {
//     $('.hidden-menu').slideToggle("slow");
//     // Alternative animation for example
//     // slideToggle("fast");
//   });
// });
/**close js for navbar mobile **/



$(document).ready(function() {
	'use strict';

  // Paralax for news page
	$('.news-backgroud').each(function() {
		var e = $(this);
		$(window).scroll(function() {
			var t = ($(window).scrollTop() / e.data("speed"));
			var n = "10% "+ t + "px";
			e.css({backgroundPosition: n});
		});
	});
  
	// Paralax for product page
	$('.products-backgroud').each(function() {
		var e = $(this);
		$(window).scroll(function() {
			var t = ($(window).scrollTop() / e.data("speed"));
			var n = "10% "+ t + "px";
			e.css({backgroundPosition: n});
		});
	});
	
	// Heading Transition
	(function() {
		var header = $('.header-caption');
		var range = 200;
		$(window).on('scroll', function () {
			var scrollTop = $(this).scrollTop();
			var offset = header.offset().top;
			var height = header.outerHeight();
			offset = offset + height / 2;
			var calc = 1 - (scrollTop - offset + range) / range;
			header.css({opacity: calc});
			if (calc > '1') {
				header.css({opacity: 1});
			} else if (calc < '0') {
				header.css({opacity: 0});
			}
		});
	}());
});

/**button scroll to top**/

$(document).ready(function() {
  /******************************
      BOTTOM SCROLL TOP BUTTON
   ******************************/

  // declare variable
  var scrollTop = $(".scrollTop");

  $(window).scroll(function() {
    // declare variable
    var topPos = $(this).scrollTop();

    // if user scrolls down - show scroll to top button
    if (topPos > 100) {
      $(scrollTop).css("opacity", "1");

    } else {
      $(scrollTop).css("opacity", "0");
    }

  }); // scroll END

  //Click event to scroll to top
  $(scrollTop).click(function() {
    $('html, body').animate({
      scrollTop: 0
    }, 800);
    return false;

  }); // click() scroll top EMD

  /*************************************
    LEFT MENU SMOOTH SCROLL ANIMATION
   *************************************/
  // declare variable
  var h1 = $("#h1").position();
  var h2 = $("#h2").position();
  var h3 = $("#h3").position();

  $('.link1').click(function() {
    $('html, body').animate({
      scrollTop: h1.top
    }, 500);
    return false;

  }); // left menu link2 click() scroll END

  $('.link2').click(function() {
    $('html, body').animate({
      scrollTop: h2.top
    }, 500);
    return false;

  }); // left menu link2 click() scroll END

  $('.link3').click(function() {
    $('html, body').animate({
      scrollTop: h3.top
    }, 500);
    return false;

  }); // left menu link3 click() scroll END

}); // ready() END

/**-------------button share animation------------------**/
$(document).ready(function() {
  $('#show-hidden-menu').click(function() {
    $('.hidden-menu').slideToggle("slow");
    // Alternative animation for example
    // slideToggle("fast");
  });
});

/**------------smooth scroll to id--------------**/
$('.scrollTo').click(function(){
    $('html, body').animate({
        scrollTop: $( $(this).attr('href') ).offset().top
    }, 500);
    return false;
});

/**-------------js for rotated span menu dropdown mobile------------**/
$(".rotate").click(function () {
    $(this).toggleClass("down");
});

/**-------------js for scroll indicator------------------**/

/**-------------js for share button modal popup------------------**/

 $('.copy-btn').on("click", function(){
        value = $(this).data('clipboard-text'); //Upto this I am getting value
 
        var $temp = $("<input>");
          $("body").append($temp);
          $temp.val(value).select();
          document.execCommand("copy");
          $temp.remove();
     alert("Link Copied");
    });

